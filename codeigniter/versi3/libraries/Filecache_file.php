<?php
class CI_Filecache_file
{
	public $userId = NULL;

	function __construct($userId=NULL){
    	$this->CI = &get_instance();
    	$this->userId = $userId;
	}

	function reset_id($userId=NULL){
    	$this->userId = $userId;
	}

	function init(){
		// define path
		$this->user = 'temp';
		if(!empty($this->userId))
			$this->user = $this->userId;
		elseif(array_key_exists('profile_userId',$_SESSION) && !empty($_SESSION['profile_userId']))
			$this->user = $_SESSION['profile_userId'];

		// $this->user = (!empty($this->userId)?$this->userId:'temp');
		$this->PATH = dirname(APPPATH).'/cache/'.base64_encode($this->user).'/';
		$this->expDate = 'exp_date';
	}

	function cache_save($key=NULL,$value=NULL,$filename=NULL,$user=''){
		$filename = base64_encode($filename);

		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);

		$arrKeys = array();
		$arrValues = array();
		$header = "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n";
		if(!file_exists($currPath.$filename)){
			//create path
			if(!$this->CI->additional_function->create_path($currPath)){
				return FALSE;
			}
		}else{
			// get old data
			if($fp=fopen($currPath.$filename, 'r')){
				while (!feof($fp))
				{
				    $line[]=fgets($fp);
				}
				fclose($fp);

				//define existed menu
				$arrKeys = (isset($line[1])?json_decode($line[1]):array());

				//define existed values
				if(count($arrKeys)>0){
					$orderLine = 2;
					foreach($arrKeys as $item){
						$arrValues[$item] = $line[$orderLine++];
					}
				}
			}else{
				die('file tidak bisa dibaca');
			}
		}

		// set expired days
		$arrValues[$this->expDate] = json_encode(strval(time()))."\n";

		// replace or update new value
		if(isset($key) && $key != '')
			$arrValues[$key] = (is_numeric($value)?json_encode(strval($value)):json_encode($value))."\n";

		$msg = $header;
		$msg .= json_encode(array_keys($arrValues))."\n";
		foreach ($arrValues as $values) {
			$msg .= $values;	
		}

		// fill in new arr key and new arr values
		if ( ! $fp = @fopen($currPath.$filename, 'w'))
		{
			return FALSE;
		}

		if($filename == 'session'){
			$_SESSION[$key] = $value;
		}

		fwrite($fp, $msg."\n");
		fclose($fp);
	}
	
	function cache_read($key=NULL,$filename=NULL,$user=''){
		$result = NULL;
		$filename = base64_encode($filename);

		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);

		if(!file_exists($currPath.$filename)){
			$result = FALSE;
		}else{
			$arrKeys = json_decode($this->CI->additional_function->read_line($currPath.$filename,2));
			$search_key = $this->CI->additional_function->array_search_by_value($arrKeys,$key);

			// cari expdate
			$expline = $this->CI->additional_function->array_search_by_value($arrKeys,$this->expDate);
			$expTime = json_decode($this->CI->additional_function->read_line($currPath.$filename,($expline+3)));

			$minElaps = (time() - $expTime) / 60;
			// if($key == 'agent_id') {var_dump($minElaps,$arrKeys);exit;};

			// jika pake cache expired, selalu perhatikan batas waktu expired
			// jika tidak pake cache_expired, berarti bypass selalu proses ini (OR TRUE)

			if(
				is_numeric($search_key)
				&& (
					$key != 'agent_id'
					|| $minElaps < 120
				)
			){ // batas data filecache maksimum 2 jam (120)
				$valueLine = $search_key + 3;
				$values = $this->CI->additional_function->read_line($currPath.$filename,$valueLine);
				$lastValue = str_replace(array("\n"),"",$values);
				if($lastValue != "null"){
					$result = json_decode($lastValue);
				}
				// setiap read data, akan memperpanjang umur cache agent_id
				$this->cache_save($this->expDate,strval(time()),$filename,$user);
			}
		}
		return $result;
	}

	function get_all_key($filename=NULL,$user=''){
		if(empty($filename))
			return NULL;

		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);

		$filename = base64_encode($filename);
		if(file_exists($currPath.$filename)){
			return json_decode($this->CI->additional_function->read_line($currPath.$filename,2));
		}
		return NULL;
	}

	function get_all_file($user=''){
		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);

		$arrFile = array();
		if(file_exists($currPath)){
			$tmpFile = $this->CI->additional_function->get_file_list($currPath,array('.','..'));
			if(is_array($tmpFile)){
				foreach ($tmpFile as $file) {
					array_push($arrFile,base64_decode($file));
				}
			}
		}
		return $arrFile;
	}

	function remove_file($filename=NULL,$user=''){
		$filename = base64_encode($filename);
		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);
		if(is_dir($currPath.$filename))
			$this->CI->additional_function->delete_dir($currPath.$filename);
		elseif(file_exists($currPath.$filename))
			unlink($currPath.$filename);
	}

	function get_path($filename=NULL,$user=''){
		$filename = base64_encode($filename);
		$this->init();
		$currPath = (!empty($user)?dirname(APPPATH).'/cache/'.base64_encode($user).'/':$this->PATH);
		return $currPath.$filename;
	}
}
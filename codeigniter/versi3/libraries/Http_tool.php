<?php
class CI_Http_tool
{
	function __construct() 
	{
    	$this->CI = &get_instance();
    	$this->credential = $this->CI->user_data->set_credential();
	}
	
	// !! jangan pernah url make memakai tanda ?
	function get_body(){
		$return = '';

		$fields_string = '';
		if(!empty($_POST)){
			foreach($_POST as $key=>$value){ 
				if($key!='action' && $key!='method' && $key!='function' && $key!='xml' && $key!='header'){
					$fields_string .= $key.'='.urlencode($value).'&'; 
				}elseif($key=='xml'){
					$fields_string = $value; // xml tidak boleh digabung dengan post dan get
				}
			}
			$fields_string = rtrim($fields_string,'&');		 
		}

		if(@strtolower($_POST['method'])==='post'){  // REQUIRE POST OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);

			if(!empty($_POST['header']))
				curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,TRUE); 
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			curl_setopt($ch, CURLOPT_POST      ,TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS    ,$fields_string);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='get'){  // REQUIRE GET OR DIEz
			$ch = curl_init($_POST['action'].'?credential='.$this->credential."&".$fields_string);

			if(!empty($_POST['header']))
				curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,TRUE); 
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='put'){  // REQUIRE GET OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);

			if(!empty($_POST['header']))
				curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,TRUE); 
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			curl_setopt($ch, CURLOPT_POST      ,TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS    ,$fields_string);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='delete'){  // REQUIRE GET OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);

			if(!empty($_POST['header']))
				curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,TRUE); 
			curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);  // RETURN THE CONTENTS OF THE CALL
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			curl_setopt($ch, CURLOPT_POST      ,TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS    ,$fields_string);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		} 
		else{
			$return = 'Hacking attempt Logged!';
		}

		if(isset($_POST['request']) && $_POST['request'] == 'as vars'){
			return $this->CI->services_json->decode($Rec_Data);
		}elseif(isset($_POST['request']) && $_POST['request'] == 'as plain'){
			return $Rec_Data;
		}else{
			echo $return;
		}
	}
	
	function get_header(){
		$return = '';
		if(@strtolower($_POST['method'])==='post'){  // REQUIRE POST OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);

			curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FILETIME, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			curl_setopt($ch, CURLOPT_POST,TRUE);
			//curl_setopt($curl, CURLOPT_NOBODY, TRUE);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='get'){  // REQUIRE GET OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);

			curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FILETIME, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			//curl_setopt($curl, CURLOPT_NOBODY, TRUE);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='put'){  // REQUIRE GET OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);
			curl_setopt($ch, CURLOPT_POST,TRUE);

			curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FILETIME, TRUE);

			//curl_setopt($curl, CURLOPT_NOBODY, TRUE);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		}elseif(@strtolower($_POST['method'])==='DELETE'){  // REQUIRE GET OR DIE
			$ch = curl_init($_POST['action'].'?credential='.$this->credential);
			curl_setopt($ch, CURLOPT_POST,TRUE);

			curl_setopt($ch, CURLOPT_HTTPHEADER,array($_POST['header']));		
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_FILETIME, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

			//curl_setopt($curl, CURLOPT_NOBODY, TRUE);
			$Rec_Data = curl_exec($ch);
			curl_close($ch);
			$return = "\r\n<pre>\r\n".htmlspecialchars($Rec_Data)."\r\n</pre>\r\n";
		} 
		else{
			$return = 'Hacking attempt Logged!';
		}

		if($_POST['request'] == 'as vars'){
			return $Rec_Data;
		}else{
			echo $return;
		}
	}
	
	public function request_header(){
		foreach($_SERVER as $key => $value) {
			if(strpos($key, 'HTTP_') === 0) {
				echo $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
			}
		}	
		print_r(apache_request_headers());
	}
}
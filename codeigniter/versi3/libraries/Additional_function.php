<?php
class CI_Additional_function
{
	function __construct() 
	{
    	$this->CI = &get_instance();
	}
	
	function is_php_passed()
	{
		if(version_compare(phpversion(), '5.1.4') < 0) 
		{
	        header("Content-Type: text/html; charset='UTF-8'");
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Merge any array
	 * 
	 * @param $param_array => any array that will be merged
	 */
	function array_merge($param_array)
	{
		if(! empty($param_array) && ! is_array($param_array)) die("param must in array format");
		$num_array = count($param_array);
		$temp = $param_array[0];
		for($i=0; $i<$num_array; $i++)
		{
			if($i == ($num_array-1)) return $temp;
			else
			{
				if((! empty($temp) && is_array($temp)) && (! empty($param_array[($i+1)]) && is_array($param_array[($i+1)])))
					$temp = array_merge($temp,$param_array[($i+1)]);
				else if(! empty($param_array[($i+1)]) && is_array($param_array[($i+1)]))
					$temp = $param_array[($i+1)];
				else if(! empty($temp) && is_array($temp))
					$temp = $temp;
				else
					$temp = '';
			}
		}
		return $temp;
		// ex : $this->array_merge(array(array1,array2,array3))
	}

	// hanya bisa 2 level
	// obj1 = {'join':'string join 1','where':[{'key':'string'}],'where_in':{'key':'string 1'}};
	// obj2 = {'join':'string join 2','where':['string in array1','string in array2'],'where_in':{'key':'string 2'}};
	// result = {'join':'string join 2','where':[{'key':'string'},'string in array1','string in array2'],'where_in':{'key':'string 2'}};
	// join akan langsung di override karena value adalah string
	// where akan diambil semua data karena array dengan key nya adalah numeric
	// where akan dioverride karena array dengan key nya adalah string
	function object_merge($obj1,$obj2){
		$result = array();

		if(is_array($obj1) || is_object($obj1)){
			$obj1 = (array) $obj1;
			foreach ($obj1 as $keyObj1 => $valObj1) {
				$result[$keyObj1] = (is_object($valObj1)? (array)$valObj1 : $valObj1);
			}
		}
		if(is_array($obj2) || is_object($obj2)){
			$obj2 = (array) $obj2;
			foreach ($obj2 as $keyObj2 => $valObj2) {
				if(is_numeric($keyObj2) && !empty($result[$keyObj2])){
					$max_key = max(array_keys($result));
					$altkeyObj2 = ++$max_key;
				}else{
					$altkeyObj2 = $keyObj2;
				}

				if(isset($result[$keyObj2])){
					if(is_string($result[$keyObj1]) && is_string($valObj2)){ // jika kedua data adalah string, ambil string terakhir
						$result[$altkeyObj2] = $valObj2;
					}elseif(is_array($result[$keyObj1]) && is_array($valObj2)){ // jika kedua data adalah array maka gabungkan
						foreach ($valObj2 as $key => $value) {
							if(is_numeric($key)){ // jika key adalah angka, lalu di increment
								$max_key2 = max(array_keys($result[$keyObj2]));
								$result[$altkeyObj2][++$max_key2] = $value;
							}else{ // jika key adalah selain angka, lalu di override
								$result[$altkeyObj2][$key] = $value;
							}
						}
					}elseif(is_array($result[$keyObj1]) && is_object($valObj2)){ // jika kedua data adalah array maka gabungkan
						$currObj2  =  (array) $valObj2;
						foreach ($currObj2 as $key => $value) {
							if(is_numeric($key)){ // jika key adalah angka, lalu di increment
								$max_key2 = max(array_keys($result[$keyObj2]));
								$result[$altkeyObj2][++$max_key2] = $value;
							}else{ // jika key adalah selain angka, lalu di override
								$result[$altkeyObj2][$key] = $value;
							}
						}
					}else{
						die('tidak bisa merge dengan data tidak sejenis');
					}
				}else{
					$result[$altkeyObj2] = $valObj2;
				}
			}
		}

		return (object) $result;
	}

	/**
	 * mendapatkan jumlah terbanyak dari data array
	 * $arr = array(1,2,2,3,4,4,5); 
	 * $r = $this->array_remove_duplicate($arr);
	 * $r = array(1,2,3,4,5); 
	 */
	function array_remove_duplicate($array){
		$result = NULL;
		$temp = NULL;
		if(!empty($array)){
			foreach($array as $item){
				if(!empty($temp) && !empty($item) && !in_array($item,$temp)){
					$temp[] = $item;
				}else if(!empty($item) && empty($temp)){
					$temp[] = $item;
				}
			}
			$result = $temp;
		}
		return $result;
	}

	/**
	 * mendapatkan nilai dari $arrSource dikurangi $arrExclude
	 */
	function array_minus($arrSource,$arrExclude)
	{
		if(empty($arrExclude))
			return $arrSource;
		elseif(empty($arrSource))
			return NULL;
		return array_values(array_diff($arrSource, $arrExclude));
	}

	/**
	 * $param_array = array($arrSource1, $arrSource2, dll ..);
	 * mendapatkan nilai intersect dari $arrSource1 dan $arrSource2
	 */
	function array_intersect($param_array)
	{
		if(! empty($param_array) && ! is_array($param_array)) die("param must in array format");
		$num_array = count($param_array);

		// karena intersection, bisa dicek dari array pertama terhadap array lainnya
		$temp = $param_array[0];
		if(is_array($temp)){
			for($i=0; $i<$num_array; $i++)
			{
				if($i != ($num_array-1)){
					if(is_array($param_array[($i+1)]))
						$temp = array_intersect($temp,$param_array[($i+1)]);
					else
						$temp = array();
				}
			}
		}
		
		if(!empty($temp))
			$temp;
		else 
			$temp = array();
		return $temp; //array_values($temp);
	}

	/**
	 * mendapatkan jumlah terbanyak dari data array
	 * $arr = array([0,1,2,3,4],[2,3,4],[0],[4,5]); 
	 * count = 5 for $arr[0],
	 * count = 3 for $arr[1],
	 * count = 1 for $arr[2],
	 * count = 1 for $arr[3]
	 * $r = $this->array_maxcount($arr);
	 * $r = 5 as max;
	 */
	function array_maxcount($allArr)
	{
		$count = 0;
		if(is_array($allArr))
		{
			foreach($allArr as $arr)
			{
				$count = max($count,count($arr));
			}
			return $count;
		}
		return NULL;
	}

	/**
	 *	search value by key or key order
	 *  @input $a = array(array(idx1=>val1,idx2=>val2,idx3=>val3,idx4=>val4));
	 *  @output $res = array_seek(0,$a);
	 *  @output $res = array_seek(idx1,$a);
	 */
	function array_seek($key, $hash = array())
	{
		$hash = (array) $hash; // => pastikan bentuk data adalah array
	    $keys = array_keys($hash); // => dapatkan semua key yang ada

	    // => check if value is not null if key is array key
	    $value = $this->set_value($hash,$key,'string',NULL,NULL,TRUE);
	    if(!isset($value)){
		    // => check if value is not null if key is array order
		    $key = $this->set_value($keys,$key,'string',NULL,NULL,TRUE);
		    $value = $this->set_value($hash,$key,'string',NULL,NULL,TRUE);
	    }
	    
	    return $value;
	}


	/**
	 *	hierarchy array to linear array
	 *  @input $a = array(array(id1,value1),array(id2,value2));
	 *	@output $a = array(strtolower(id1)=>value1,strtolower(id2)=>value2);
	 */
	function array_linear($arrInput,$json=FALSE,$keyItem=0,$valItem=1,$forceSmallKey=FALSE){
		$output = NULL;
		if(is_array($arrInput)){
			foreach ($arrInput as $input) {
				$key = $this->array_seek($keyItem,$input);
				$value = $this->array_seek($valItem,$input);
				if($forceSmallKey)
					$output[strtolower($key)] = $value;
				else
					$output[$key] = $value;
			}
		}
		return ($json)?$this->CI->services_json->encode($output):$output;
	}

	// array_search_by_value(array(id1:val1,id2:val2),val2);
	// return id2
	function array_search_by_value($arrData=NULL,$value=NULL){
		if(empty($arrData) || empty($value))
			return NULL;
		return array_search($value,$arrData);
	}

	// array_search_by_value(array(id1:val1,id2:val2),id2);
	// return val2
	function array_search_by_id($arrData=NULL,$id=NULL){
		if(empty($arrData) || empty($id))
			return NULL;
		elseif(!empty($arrData[$id]))
			return $arrData[$id];
		return NULL;
	}

	/**
	 * 
	 */
	function increment_alphanum($last=NULL,$increment=1)
	{
		if(!empty($last)){
			for($i=0; $i<$increment;$i++){
				$last++;
			}
			return $last;
		}else{
			$last = 'A';
			for($i=1; $i<$increment;$i++){
				$last++;
			}
			return $last;
		}
	}
	
	function decrement_alphanum($last=NULL){
		return (chr(ord($last) - 1));
	}
	
	/**
	 * 
	 */
	function decrement_letter($char=NULL)
	{
		$len = strlen($char);
		// last char is A or a
		if(ord($char[$len-1]) === 65 || ord($char[$len-1]) === 97){
			if($len === 1){ // one char left
				return NULL;
			}else{ // 'ABA'-- => AAZ
				$char = $this->decrement_letter(substr($char,0,-1)).'Z';
			}
		}else{
			$char[$len - 1] = chr(ord($len-1) - 1);
		}
		return $char;
	}

	/**
	 * get randomize color 
	 */
	function generateColor()
	{
		$color = array('clRed','clGreen','clBlue','clYellow','clBlack');
		$var = rand(0,(count($color)-1));
		return $color[$var];
	}	
	
	/**
	 * define for any variable if it is odd type or not
	 * 
	 * @param $a => variable for checked
	 */
	function is_odd($a)
	{
		if (($a /2) != (floor($a /2))){
		      return TRUE;
		}
		if (($a /2) == (floor($a /2))){
		      return FALSE;
		}		
	}
	
	/**
	 * Get randomized string
	 * 
	 * @param string $numchars => length of randomized string
	 * @param boolean $digits define valued with 1 : true, 0 : false
	 * @param boolean $letters define valued with 1 : true, 0 : false
	 */
	function generateCode($numchars=5,$digits=1,$letters=1)
	{
		$str = $randomized = '';
		$dig = "012345678923456789";
		$abc = "abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
		if($letters == 1){$str .= $abc;}
		if($digits == 1){$str .= $dig;}
		for($i=0; $i < $numchars; $i++){$randomized .= $str[rand() % strlen($str)];}
		return $randomized;
	}	

	/**
	 * Adding index to the table
	 * 
	 * @param array $arrParams[tableName,indexName,array(fields)] => parameters that will be used
	 */
	function _addReferenceIndex($arrParams)
	{
		$tableName = $arrParams["tableName"];
		$indexName = $arrParams["indexName"];
		$fields = $arrParams["fields"];
		$fields = implode(",", $fields);
		
		$r = $this->CI->db->query("SHOW INDEX FROM $tableName");
		$arrIndex = $r->result();
		$exist = 0;
		if (is_array($arrIndex)) 
		{
			foreach($arrIndex as $index)
			{
				if($index->Key_name == $indexName) $exist = 1;
			}
		}
		if($exist != 1)
		{
			$this->CI->db->query("CREATE INDEX $indexName ON $tableName($fields)");
			return TRUE;
		}
		return FALSE;
	}
	
	//a = [array('id'=>1),array('id'=>2),array('id'=>3),array('id'=>4),array('id'=>5)]
	//a = get_array(a,'id')
	//a = [1,2,3,4,5]
	function get_array($data,$keyName,$duplicate=FALSE,$emptyIncl=FALSE)
	{
		$result = NULL;
		if(is_array($data))
		{
			foreach($data as $value)
			{
				if(is_array($value))
				{
					if($duplicate){
						if($emptyIncl && empty($value[$keyName]) || isset($value[$keyName]))
				 			$result[] = trim($value[$keyName]); 
					}else if(empty($result) || (!empty($result) && !in_array($value[$keyName], $result))){
						if($emptyIncl && empty($value[$keyName]) || isset($value[$keyName]))
				 			$result[] = trim($value[$keyName]); 
					}elseif(is_array($value[$keyName])){
				 		$result[] = trim($value[$keyName]); 
					}
				}
				else if(is_object($value))
				{
					if($duplicate){
						if($emptyIncl && empty($value->{$keyName}) || isset($value->{$keyName}))
						 	$result[] = trim($value->{$keyName}); 
					}elseif(empty($result) || (!empty($result) && !in_array($value->{$keyName}, $result))){
						if($emptyIncl && empty($value->{$keyName}) || isset($value->{$keyName}))
					 		$result[] = trim($value->{$keyName}); 
					}elseif(is_array($value->{$keyName})){
					 	$result[] = trim($value->{$keyName}); 
					}
				}

			}
		}
		return $result;
	}
	
	// $arrParam = ['user_id',123,'name','joni','age',25,'urutan',0]; => value 0 mengharus kan isset
	// $arrParam = set_param($arrParam);
	// $arrParam = ('user_id'=>123,'name'=>'joni','age'=>25)
	function set_param($arrParam = NULL){
		
		$data = NULL;
		if(is_array($arrParam) && isset($arrParam[1]))
		{
			$count = count($arrParam);
			for($i=0;$i<$count;$i+=2)
			{
				if(isset($arrParam[$i]) && isset($arrParam[($i+1)]))
				if(strpos(urldecode($arrParam[$i]),'[]') !== FALSE){
					$idx = str_replace('[]','',urldecode($arrParam[$i]));
					$data[$idx][] = $arrParam[($i+1)];
				}
				else{
					$data[$arrParam[$i]] = $arrParam[($i+1)];
				}
			}
		}
		return $data;
	}
	
	// $a = ['a','b','c'] or $a = a;b;c
	// $a = string_to_array($a,";")
	// $a = ['a','b','c']
	function string_to_array($string,$block=NULL){ // block for string input
		$block = (!empty($block)?$block:',');
		$arrResult = array();
		if(!is_array($string)) 
			$arrData = explode($block,$string); // explode if it is not array
		else 
			$arrData = $string; // just assign if it is already array

		if(!empty($arrData)){
			foreach($arrData as $data)
			{
				if(isset($data)) $arrResult[] = $data;
			}
		}
		return $arrResult;
	}
	
	// $a = ['a','b','c'] /* $a = a;b;c */
	// $a = array_to_string($a,";")
	// $a = ;a;b;c;
	// trim for ignore head and tail block
	function array_to_string($arrData,$block="'",$trim=FALSE,$oldBlock=','){
		$arrData = $this->string_to_array($arrData,$oldBlock);
		if(!empty($arrData))
		{
			return ($trim?'':$block).(implode($block,$arrData)).($trim?'':$block);
		}
		return '';
	}

	function secondsToTime($seconds){
	    // extract hours
	    $hours = floor($seconds / (60 * 60));
	 
	    // extract minutes
	    $divisor_for_minutes = $seconds % (60 * 60);
	    $minutes = floor($divisor_for_minutes / 60);
	 
	    // extract the remaining seconds
	    $divisor_for_seconds = $divisor_for_minutes % 60;
	    $seconds = ceil($divisor_for_seconds);
	 
	    // return the final array
	    $obj = array(
	        "h" => (int) $hours,
	        "m" => (int) $minutes,
	        "s" => (int) $seconds,
	    );
	    return $obj;
	}
	
	function get_collect_id($arrIds){
		$arrIds = $this->array_remove_duplicate($arrIds);

		$fieldName = $this->CI->user_data->get_field_name('collect_id');
		$tableName = $this->CI->user_data->get_table_name('simas_collect_id');

		$this->CI->user_data->remove_attribute($tableName,NULL,FALSE);
		if(!empty($arrIds)){
			foreach ($arrIds as $id) {
				$data = array($fieldName => $id);
				$this->CI->user_data->set_attribute($data,$tableName,'insert');
			}
		}

		return 'select '.$fieldName.' from '.$tableName;
	}

	function read_tail_file($file, $lines) {
		if ( ! file_exists($file))
			return FALSE;
		
		//global $fsize;
		$handle = fopen($file, "r");
		$linecounter = $lines;
		$pos = -2;
		$beginning = false;
		$text = array();
		while ($linecounter > 0) {

			$t = " ";

			while ($t != "\n") {
				if(fseek($handle, $pos, SEEK_END) == -1) {
					$beginning = true;
					break;
				}

				$t = fgetc($handle);
				$pos --;
			}

			$linecounter --;

			if ($beginning) {
				rewind($handle);
			}

			$text[$lines-$linecounter-1] = fgets($handle);
			if ($beginning) break;
		}

		fclose ($handle);
		return array_reverse($text);
	}	

	function read_last_line($filename){
		$lastLine = $this->read_tail_file($filename,1);
		if(!empty($lastLine)){
			$lastLine = str_replace(array("\r","\n","\r\n"),"",$lastLine[0]);
			$lastLine = trim($lastLine);
			return $lastLine;
		}
		return NULL;
	}

	// lineno is first line of file and start from 1
	function read_line($filePath=NULL, $lineno=1)
	{
		if(!file_exists($filePath)){
			return FALSE;
		}

	    $line=false;
	    if($fp=fopen($filePath, 'r')){
	    	while (!feof($fp) && $lineno--)
	    	{
	    	    $line=fgets($fp);
	    	}
	    	fclose($fp);
	    }
	    return ($lineno==-1)?$line:false;
	}	

	function ext_mail($to,$cc=NULL,$subject,$message,$files=NULL,$appName='CRM SYSTEM',$priority=0){
		$this->CI->load->model('master/access');

		/*
		// test = hanya menambahkan ke server test
		// main = menambahkan ke server main
		if(ENVIRONMENT == 'server_production'){
			// kirim email hanya dari aplikasi yang di production
			$this->extConnection = $this->CI->load->database($this->CI->access->extra_connection_config,TRUE);
		}else{
			// karena kms make db, jadi source utamanya mysql, jadi lagi harus di paksa utk testing make tns beta
			$this->extConnection = $this->CI->load->database($this->CI->access->db_connection_config,TRUE);
		}
		/**/

		/**/
		// test = hanya kirim ke akhyar
		// appname = 'crm test'
		$this->extConnection = $this->CI->load->database($this->CI->access->extra_connection_config,TRUE);
		if(ENVIRONMENT != 'server_production'){
			$to = 'akhyar@sinarmasmsiglife.co.id';
			$cc = NULL;
			$appName = 'CRM TEST';
		}
		/**/

		$maxCountQ = $this->extConnection->query('select msco_value from eka.mst_counter where msco_number = 144 and LCA_ID = \'01\'');
		$rMaxCount = $maxCountQ->row();
		$maxCount = 0;
		if($maxCountQ->num_rows() > 0)
			$maxCount = $rMaxCount->MSCO_VALUE;

		$nextCount = intval($maxCount)+1;
		$updateCountQ = $this->extConnection->query(vsprintf('update eka.mst_counter set msco_value=%d where msco_number = 144 and LCA_ID = \'01\'',array($nextCount)));
		
		$nextEmailId = date('Ymd').sprintf('%06d',$nextCount);
		// $files = str_replace('/','\\',APPPATH).'data/test.xls';

		$alias = 'AJSJAVA';
		$to = $this->array_to_string($to,';',TRUE);
		$cc = $this->array_to_string($cc,';',TRUE);
		$files = $this->string_to_array($files,';');
		$destFolder = config_item('mail_folder').date('Y').'/'.date('m').'/'.$nextEmailId.'/';
		$this->create_path(str_replace('\\','/',$destFolder));

		if(!empty($files)){
			foreach ($files as $file) {
				if(!empty($file)){
					$srcPath = str_replace('\\','/',$file);
					$srcPath = explode('/',$srcPath);
					$filename = $srcPath[(count($srcPath)-1)];

					copy($file,$destFolder.$filename);
				}
			}
		}

		$this->extConnection->trans_strict(TRUE);
		$this->extConnection->trans_start();
		$insertMailQ = $this->extConnection->query(
			vsprintf("
						Insert into eka.mst_email
						(me_id,me_system,me_show_system,me_show_footer,me_from,me_from_alias,me_to,me_cc,me_subject,me_message,mec_id,me_lus_id,me_count,me_html,me_status,me_input_date,priority) 
						values 
						(%s,'$appName',1,1,'ajsjava@sinarmasmsiglife.co.id','%s','%s','%s','%s','%s',0,0,0,1,0,sysdate,$priority)",
						array(
							$nextEmailId,
							$alias,
							$to,
							$cc,
							$subject,
							str_replace('\'','"',$message)
						)
					)			
		);
		$this->extConnection->trans_complete();
		return $nextCount;
	}

	function send_mail1($to='akhyar_az@rocketmail.com',$from='akhyar_az@rocketmail.com',$sender='akhyar',$subject='subject',$html='content'){
		$this->CI->load->library('Mail_fake');
		return $this->CI->mail_fake;
	}

	function send_mail2($to='akhyar@activelab.dev',$from='support@activelab.dev',$sender='support',$subject='subject',$html='content',$dest='akhyar'){
		$this->CI->load->library('mail');
		$this->CI->mail->setFrom($from);
		// $this->CI->mail->setFrom('support@activelab.dev');
		$this->CI->mail->setTo(urldecode($to));
		// $this->CI->mail->setTo(urldecode('akhyar@activelab.dev'));
		$this->CI->mail->setSender($sender);
		$this->CI->mail->setReplyTo($from);
		$this->CI->mail->setSubject($subject);

		// $msg = '<html>';
		// $msg .= '<head>';
		// $msg .= '</head>';
		// $msg .= '<body>';
		// $msg .= sprintf('Hi, %s',strtoupper($dest));
		// $msg .= '<br />';
		// $msg .= '<br />';
		// // $msg .= 'silahkan klik link berikut untuk aktivasi akun';
		// // $msg .= '<br />';
		// // $msg .= sprintf('<a href="%s">%s</a>',config_item('global_UserUrl').'set_activation/act_key/'.$actKey,'reactivation');
		// $msg .= $html;
		// $msg .= '<br />';
		// $msg .= '<br />';
		// $msg .= 'Regards,';
		// $msg .= '<br />';
		// $msg .= 'Team';
		// $msg .= '</body>';
		// $msg .= '</html>';

		$msg = $html;

		$this->CI->mail->setHtml($msg);

		// $this->CI->mail->addAttachment($filename);
		return $this->CI->mail;

		// $this->CI->mail->send();
	}

	function send_mail3($to='akhyar_az@rocketmail.com',$from='akhyar_az@rocketmail.com',$sender='akhyar_az@rocketmail.com',$subject='subject',$html='content'){
        $this->CI->load->library('phpmailer/PHPMailer');
        $this->CI->phpmailer->IsSMTP();
        $this->CI->phpmailer->SMTPDebug  = 1;                  // enable SMTP authentication
        $this->CI->phpmailer->SMTPAuth   = TRUE;                  // enable SMTP authentication
        $this->CI->phpmailer->SMTPSecure = "ssl";                 // sets the prefix to the servier

        // $this->CI->phpmailer->Host       = "mail.testing1-protecsoft.com";      // sets GMAIL as the SMTP server
        // $this->CI->phpmailer->Port       = 25;                   // set the SMTP port for the GMAIL server
        // $this->CI->phpmailer->Username   = "admin@testing1-protecsoft.com";  // GMAIL username
        // $this->CI->phpmailer->Password   = "123456";            // GMAIL password

        $this->CI->phpmailer->Host       = "smtp.mail.yahoo.com";      // sets GMAIL as the SMTP server
        $this->CI->phpmailer->Port       = 465;                   // set the SMTP port for the GMAIL server
        $this->CI->phpmailer->Username   = "progg_begg@yahoo.com";  // GMAIL username
        $this->CI->phpmailer->Password   = "Programm3r";            // GMAIL password

        $this->CI->phpmailer->SetFrom($from,$sender);

        $this->CI->phpmailer->AddReplyTo($from,$sender);

        $this->CI->phpmailer->Subject    = $subject;

        $this->CI->phpmailer->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        $this->CI->phpmailer->MsgHTML($html);

        $this->CI->phpmailer->AddAddress($to,$to);

        // if(!$this->CI->phpmailer->Send()) {
        //     die("Mailer Error: " . $this->CI->phpmailer->ErrorInfo);
        //     // echo "Mailer Error: " . $this->CI->phpmailer->ErrorInfo;
        // } else {
        //     // echo "Message sent!";
        // }

        return $this->CI->phpmailer;
	}

    function is_valid_email($email){
    	return filter_var($email, FILTER_VALIDATE_EMAIL);
    	// $pattern='#^[w.-]+@[w.-]+.[a-zA-Z]{2,5}$#';
    	// return preg_match($pattern,$email);
    }

	// tidak boleh ada param karena param otomatis didapat dari fungsi func_get_args()
	// !! parameter ke-tiga adalah string dengan pemisah spasi "satu dua tiga empat"
	// !! setiap parameter tidak boleh memiliki spasi
	function background_exec(){
		$params = func_get_args();
		$controller = (!empty($params[0])?$params[0]:'main');
		$method = (!empty($params[1])?$params[1]:'no_page');
		$sync = (isset($params[2])?$params[2]:TRUE);
		$cliParam = $this->array_minus($params,array($params[0],$params[1],$params[2]));
		if(count($cliParam) > 0)
			$cliParam = explode(" ",$cliParam[0]);
		$cliParam = $this->array_to_string($cliParam,'" "',TRUE);
		$cliParam = "\"$cliParam\"";

		$strUrl = $controller.' '.$method.' '.$cliParam;

		$phpCompiler = config_item('phpCompiler');
		$appIndex = config_item('appIndex');
		$execPath = $phpCompiler.' '.$appIndex.' '.$strUrl;

		$headSyncProc = ($sync?config_item('bgProc'):''); /* for windows */
		$tailSyncProc = ($sync?config_item('endProc'):''); /* for unix */

		$debug = $this->CI->filecache->cache_read('cli_debug','session');
		if($debug === TRUE){
			die($headSyncProc."$execPath".$tailSyncProc);
		}

		// exec($execPath);

		// $descriptorspec = array(
		//     0 => array('pipe', 'r'),
		//     0 => array('pipe', 'w'),
		//     0 => array('pipe', 'a')
  		//);

		// $res = proc_open("start /B $execPath", $descriptorspec, $pipes, null, null);
		// $info = proc_get_status($res);
		// proc_close($res);
		// return $info['pid'];
		pclose(popen($headSyncProc."$execPath".$tailSyncProc,"r"));
	}

	// check variable apakah jenisnya seperti output dari oracle database DD-MM-YY
	function parse_oracle_time($date=NULL,$dateFormat=NULL){
		$partDate = explode(' ',$date);

		$return = FALSE;
		if(!empty($dateFormat)){
			$date = DateTime::createFromFormat($dateFormat,$date);
		}elseif(strlen(trim($partDate[0])) == 8 && !(FALSE === strpos($partDate[0],'-'))){
			$date = DateTime::createFromFormat('d-m-y H:i:s',$partDate[0].' '.(!empty($partDate[1])?$partDate[1]:'00:00:00'));
		}elseif(strlen(trim($partDate[0])) == 8 && !(FALSE === strpos($partDate[0],'/'))){
			$date = DateTime::createFromFormat('d/m/y H:i:s',$partDate[0].' '.(!empty($partDate[1])?$partDate[1]:'00:00:00'));
		}else{
			$date = new DateTime($date);
		}

		if(is_object($date))  $return = $date->format('Y-m-d H:i:s');

		return $return;
	}

	// $result['nama'] = '00019';
	// if has variable $result = array('nama'=>'akhyar','alamat'=>'slipi')
	// set value to set_value($result,'nama','string',NULL)
	// leave key as it is since some time array data get from external process so don't lower it
	function set_value($val=NULL,$key=NULL,$type='string',$dfltOutput=NULL,$dateFormat=NULL,$preventCond=NULL){
		$output = NULL;
		// check if $val is object or array or string
		if(is_object($val) && isset($val->{$key})){
			$val = $val->{$key};
		}elseif(is_array($val) && isset($val[$key])){
			$val = $val[$key];
		}elseif(is_string($val) || is_numeric($val)){
			$val = $val;
		}else{
			$val = FALSE;
		}

		// selalu cek
		// untuk nilai preventCond bernilai number, yang hanya di prevent adalah number yang lebih kecil dari yang di set
		// selainnya, $pastikan $prevent dan $val bernilai sama, jika sama maka nilai val diganti dengan default output
		// note : terkadang perlu ganti nilai NULL dengan nilai default, yang berarti nilai NULL harus dicek juga sebagai prevent condition
		if(is_numeric($preventCond) && $val < $preventCond){
			$val = $dfltOutput;
		}elseif($preventCond === $val){
			$val = $dfltOutput;
		}

		// sanitasi
		if(isset($val)){
			switch ($type) {
				case 'date':
							$output = (!empty($val)?$this->parse_oracle_time($val):FALSE);
							break;
				case 'number':
							if(is_numeric($val)) $output = intval($val);
							break;
				case 'string':
							if(isset($val) && (is_string($val) || is_numeric($val))){
								$output = urldecode($this->clean_xss($val));
							}
							break;
				default:
							$output = ($val===FALSE?FALSE:$this->clean_xss($val));
							break;
			}
		}

		// return trim($output); 
		// tidak boleh pake trim karena fungsi set value juga digunakan pada pengambilan key dan value dari object
		// kadang2 key butuh spasi di depan agar urutan di checkbox atau lainnya tetap berurut dari header dengan key -1
		return $output;
	}

	/** 
	 * recursively create a long directory path
	 */
	function create_path($path){
	    if (is_dir($path)) return true;
	    $prev_path = substr($path, 0, strrpos(str_replace('\\','/',$path),'/', -2) + 1 );
	    $return = $this->create_path($prev_path);

	    if($return && is_writable($prev_path)){
	    	$oldmask = umask(0); 
	    	$return = mkdir($path, 0777, true); 
	    	umask($oldmask);
	    }else{
	    	$return = FALSE;
	    }

	    return $return;
	}	

	// file inside folder = . .. file1 file2
	// $files = get_file_list(path)
	// $files = array(file1,file2)
	function get_file_list($path,$exclude=NULL,$maxFiles=NULL,$contained=NULL){
		$arrFile = NULL;
		if(file_exists($path)){
			if ($handle = opendir($path)) {
			    /* This is the correct way to loop over the directory. */
			    while (false !== ($entry = readdir($handle))) {

			    	if($entry !== '.' && $entry !== '..'){
				    	if(!empty($exclude) && !in_array($entry,$exclude)){
				    		$arrFile[] = $entry;
				    	}elseif(!empty($contained) && FALSE !== strpos($entry,$contained)){
				    		$arrFile[] = $entry;
				    	}elseif(empty($exclude) && empty($contained)){
				    		$arrFile[] = $entry;
				    	}
			    	}

			    	if($maxFiles>0 && count($arrFile) == $maxFiles) break;
			    }

			    closedir($handle);
			}
		}
		return $arrFile;
	}

	// Function for resizing jpg, gif, or png image files
	// resize_image('source.png','result.jpg',100,100);
	// $result = $this->mrktPlace->tool->upload_image(
	//     $_FILES['avatar'],
	//     array('path'=>$this->mrktPlace->basePath.'assets/img/avatar/merchant/'.$merchantId.'/')
	// );
	// if($result['state']){
	//     $this->mrktPlace->tool->resize_image($result['path'],$result['path'],200,200);
	// }
	function resize_image($target, $newcopy, $w, $h) {

		// get information from file
	    list($w_orig, $h_orig) = getimagesize($target);

	    // get extension
	    $partPath = explode('/',$target);
	    $endPath = end($partPath);
	    $partFile = explode('.',$endPath);
	    $ext = strtolower(end($partFile));

	    $scale_ratio = $w_orig / $h_orig;
	    if (($w / $h) > $scale_ratio) {
	           $w = $h * $scale_ratio;
	    } else {
	           $h = $w / $scale_ratio;
	    }

	    $img = "";
	    $ext = strtolower($ext);
	    if ($ext == "gif"){ 
	      $img = imagecreatefromgif($target);
	    } else if($ext =="png"){ 
	      $img = imagecreatefrompng($target);
	    } else { 
	      $img = imagecreatefromjpeg($target);
	    }
	    $tci = imagecreatetruecolor($w, $h);

	    // Save the image
	    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
	    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
	    imagejpeg($tci, $newcopy, 80);

	    // Free up memory
	    imagedestroy($tci);   

	    return $img; 
	}

	// for real size should be showImage(path,0,0,0)
	function showImage($imgPath,$width=0,$height=0,$type=0){
		$img = NULL;
		$width = intval($width);
		$height = intval($height);
		$sExtension = strtolower(@end(explode('.', $imgPath)));
		$enforce = 'proportional';
		if(isset($type) && $type == 1)
			$enforce = 'force';
		if(isset($type) && $type == 2)
			$enforce = 'crop';
			
		if ($sExtension == 'jpg' || $sExtension == 'jpeg') {
		 
		    $img = @imagecreatefromjpeg($imgPath)
		        or die("Cannot create new JPEG image");
		 
		} else if ($sExtension == 'png') {
		 
		    $img = @imagecreatefrompng($imgPath)
		        or die("Cannot create new PNG image");
		 
		} else if ($sExtension == 'gif') {
		 
		    $img = @imagecreatefromgif($imgPath)
		        or die("Cannot create new GIF image");
		}

		if($img){

			$iOrigWidth = imagesx($img);
			$iOrigHeight = imagesy($img);

			if($enforce=="proportional" && $width==0 && $height==0){
				$width = $iOrigWidth;
				$height = $iOrigHeight;
				$enforce = "force";
			}

			$startX = 0;
			$startY = 0;
		    if($enforce == 'proportional'){
		        $fScale = min($width/$iOrigWidth,$height/$iOrigHeight);
		        $iNewWidth = floor($fScale*$iOrigWidth);
		        $iNewHeight = floor($fScale*$iOrigHeight);

		        $width = $iNewWidth;
		        $height = $iNewHeight;
		    }elseif($enforce == 'force'){
		    	$iNewWidth = $width;
		    	$iNewHeight = $height;
		    }elseif($enforce == 'crop'){
		        $fScale = max($width/$iOrigWidth,$height/$iOrigHeight);
		        $iNewWidth = floor($fScale*$iOrigWidth); // new original width
		        $iNewHeight = floor($fScale*$iOrigHeight); // new original height

		    	if($iNewWidth>$width){
		    		$startX = ceil($iNewWidth - $width)/2;
		    	}
		    	if($iNewHeight>$height){
		    		$startY = ceil($iNewHeight - $height);
		    	}
		    }
		    $tmpimg = imagecreatetruecolor($width,$height);
		    imagecopyresampled($tmpimg, $img, 0, 0, $startX, $startY, $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
		    imagedestroy($img);
		    $img = $tmpimg;

		    // $tmpimg = imagecreatetruecolor($iNewWidth,$iNewHeight);
		    // imagecopyresampled($tmpimg, $img, 0, 0, $startX, $startY, $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
		    // imagedestroy($img);
		    // $img = $tmpimg;

		    header("Content-type: image/jpeg");
		    imagejpeg($img);exit;
		}
	}

	// for real size should be showImage(path,0,0,0)
	// or
	// load_image
	function load_image($pathToFilename=NULL){

	    $partFile = explode('/',$pathToFilename);
	    $file = end($partFile);
	    $partExt = explode('.',$file);
	    $ext = strtolower(end($partExt));

	    switch( $ext ){
	        case "gif": $ctype="image/gif"; break;
	        case "png": $ctype="image/png"; break;
	        case "jpeg":
	        case "jpg": $ctype="image/jpeg"; break;
	        default: $ctype="image/jpeg"; break;
	    }
	    header('Content-type: ' . $ctype);        
	    
	    $descContent = '';
	    if(is_readable($pathToFilename))
	        $descContent = file_get_contents($pathToFilename);
	    echo $descContent;exit;
	}

	function downloadFile($filePath,$download=TRUE){
		$filePath = urldecode($filePath);
		$partFile = explode('/',str_replace(array('/','\\'),'/',$filePath));
		$fileName = end($partFile);

		if($download===TRUE){
			header('Content-Disposition: attachment; filename="'.urldecode($fileName).'"');
		}else{
			header('Content-Disposition: inline; filename="'.urldecode($fileName).'"');
		}

		$partExtension = explode('.',$fileName);
		$extension = end($partExtension);

		if(strtolower($extension) == 'pdf') header("Content-Type: application/pdf");
		else header("Content-Type: application/octet-stream");

		header("Content-Description: File Transfer");            
		header("Content-Length: " . filesize($filePath));
		flush(); // this doesn't really matter.

		$fp = fopen($filePath, "r");
		while (!feof($fp))
		{
		    echo fread($fp, 65536);
		    flush(); // this is essential for large downloads
		} 
		fclose($fp);exit;
	}

	function writetoFile($realPath,$msg=NULL,$mode='a'){
		// $msg = (file_exists($realPath)?"\n":"").$msg;

		if($fp=fopen($realPath, $mode)){
			fwrite($fp, $msg."\n");
			fclose($fp);
		}else{
			return FALSE;
		}
	}

	function get_microtime(){
		return floor(microtime(date('Y-m-d H:i:s')));
		//sysadmin	sysadmin	47f76530d574c519bc44a4fc4cd43f1f	a@b.c	1		05-FEB-15
	}

	function getmicrosecond(){
		list($usec,$sec) = explode(' ',microtime());
		$number = ((float)$usec + (float)$sec)*10000;
		$number = sprintf('%f',$number);
		$number = str_replace('.','',$number);
		return $number;
	}

	function page_load_start(){
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];

		// hampir mirip dengan $this->get_microtime();

		return $this->sLoadTime = $time;
	}	

	function page_load_end($start=NULL){
		if(empty($start)){
			$start = $this->sLoadTime;
		}

		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$total_time = round(($finish - $start), 4);
		return array(
			'result' => $total_time,
			'string' => 'Page generated in '.$total_time.' seconds.'
		);
	}	

	function strTime($s) {
	  $d = intval($s/86400);
	  $s -= $d*86400;

	  $h = intval($s/3600);
	  $s -= $h*3600;

	  $m = intval($s/60);
	  $s -= $m*60;

	  $str = '';
	  if ($d) $str = $d . 'd ';
	  if ($h) $str .= $h . 'h ';
	  if ($m) $str .= $m . 'm ';
	  if ($s) $str .= $s . 's';

	  return $str;
	}	

	function memoryLoaded(){
		$memoryLoad = memory_get_usage();
		return array(
			'result' => $memoryLoad,
			'string' => 'memory loaded in '.$memoryLoad.' bytes.'
		);
	}	

	// $a = [A]NDIKA [B]ONI
	// $b = $this->get_init($a,2);
	// echo $b; // AB;
	function get_init($name,$length=2){
		$split = explode(' ',$name);
		$init = '';
		for($i=0;$i<$length;$i++){
			$init .= (!empty($split[$i][0])?$split[$i][0]:'');
		}
		return $init;
	}

	// $first['value'] = NULL;
	// $second['value'] = 'semangka';
	// $third['value'] = NULL;
	// $fourth['value'] = 'kentang';
	// urutannya disini :
	// $a = $this->additional_function->get_elm_priority(
	// 	array('key'=>'value','val'=>$first),
	// 	array('key'=>'value','val'=>$second),
	// 	array('key'=>'value','val'=>$third),
	// 	array('key'=>'value','val'=>$fourth),
	//  'mangga'
	// );
	// echo $a; // semangka
	function get_elm_priority(){
		$arrParam = func_get_args();
		$check = NULL;
		if(!empty($arrParam) && is_array($arrParam)){
			foreach($arrParam as $param){
				if(is_array($param)){
					$key = $param['key'];
					$val = $param['val'];
					if(isset($val[$key])){
						$check = $val[$key];
						break;
					}
				}elseif(isset($param)){
					$check = $param;
					break;
				}
			}
		}
		return $this->clean_xss($check);
	}

	function clean_xss($data){
		return $this->CI->security->xss_clean($data);
	}

	// png2jpg('/img.png','/img.jpg',100)
	function png2jpg($originalFile, $outputFile, $quality) {
		unlink($outputFile);
	    $source = imagecreatefrompng($originalFile);
	    $image = imagecreatetruecolor(imagesx($source), imagesy($source));

	    $white = imagecolorallocate($image, 255, 255, 255);
	    imagefill($image, 0, 0, $white);

	    imagecopy($image, $source, 0, 0, 0, 0, imagesx($image), imagesy($image));

	    imagejpeg($image, $outputFile, $quality);
	    imagedestroy($image);
	    imagedestroy($source);
	}	

	// $res = getDatesBetween2Dates(10-06-1984,12-06-1984) // 2 hari
	function getDatesBetween2Dates($startTime, $endTime) {
	    $day = 86400;
	    $format = 'd-m-Y';
	    $startTime = strtotime($startTime);
	    $endTime = strtotime($endTime);
	    $numDays = round(($endTime - $startTime) / $day) + 1;
	    $days = array();
	        
	    for ($i = 0; $i < $numDays; $i++) {
	        $days[] = date($format, ($startTime + ($i * $day)));
	    }
	        
	    return $days;
	}

	// $res = get_date_diff(10-06-1984,12-06-1984,Y-m-d H:i:s) // 2
	function get_date_diff($strStart=NULL, $strEnd=NULL) {
		$strStart = (empty($strStart)?date('Y-m-d'):$strStart);
		$strEnd = (empty($strEnd)?date('Y-m-d'):$strEnd);

		$startTime = new DateTime($strStart); 
		$endTime   = new DateTime($strEnd); 
		$dteDiff  = date_diff($startTime,$endTime);
		// return $dteDiff->format("%H:%I:%S"); 
		return $dteDiff->format('%a'); 
	}

	// $id = get_next_id('ACC') // ACC0001
	// $id = get_next_id('ACC',$id) // ACC0002
	function get_next_id($prefix=NULL,$instId=NULL){
		if(empty($instId))
		{
			$next_id = $prefix.'0001';
		}
		else
		{
			$instId = explode($prefix,$instId);
			$instId = sprintf('%04d',($instId[1]+1));
			$next_id = $prefix.$instId;
		}
		return $next_id;
	}

	function createRange($startDate, $endDate) {
	    $tmpDate = new DateTime($startDate);
	    $tmpEndDate = new DateTime($endDate);

	    $outArray = array();
	    do {
	        $outArray[] = $tmpDate->format('d-m-Y');
	    } while ($tmpDate->modify('+1 day') <= $tmpEndDate);

	    return $outArray;
	}

	function set_hash($str,$salt=''){
		$salt = (!empty($salt)?$salt:date('dMY'));
		return md5($str.$salt);
	}	

	function set_sql_date($date){
		// checkdate(month, day, year)
		if(FALSE!==strpos($date,'/')){ // day/month/year
			$part = explode('/',$date);
			if(checkdate(intval($part[1]),intval($part[0]),intval($part[2]))){
				return sprintf("%04d-%02d-%02d",intval($part[2]),intval($part[1]),intval($part[0]));
			}
		}elseif(FALSE!==strpos($date,'-')){ // year-month-day
			$part = explode('-',$date);
			if(checkdate(intval($part[1]),intval($part[2]),intval($part[0]))){
				return sprintf("%04d-%02d-%02d",intval($part[2]),intval($part[1]),intval($part[0]));
			}
		}else{ // any format with month not in number
			$date = (!empty($date)?date_create($date):FALSE);
			if($date != FALSE) 
				return date_format($date,'Y-m-d');
		}
		return NULL;
	}

	// $arrParam = array('lead_id','lead0001');
	// $defaultParam = array('lead_id'=>'lead0001');
	function get_inst_con($arrParam=NULL,$defaultParam=NULL){

		$defParFromArr = $this->set_param($arrParam);

		$defaultParam = $this->array_merge(array($defParFromArr,$defaultParam));
		$con = array();
		$check = $this->get_elm_priority(
			array('key'=>'acc_id','val'=>$_POST),
			array('key'=>'acc_id','val'=>$_GET),
			array('key'=>'acc_id','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['acc_id'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'lead_id','val'=>$_POST),
			array('key'=>'lead_id','val'=>$_GET),
			array('key'=>'lead_id','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['lead_id'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'act_id','val'=>$_POST),
			array('key'=>'act_id','val'=>$_GET),
			array('key'=>'act_id','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['act_id'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'agent','val'=>$_POST),
			array('key'=>'agent','val'=>$_GET),
			array('key'=>'agen','val'=>$_POST),
			array('key'=>'agen','val'=>$_GET),
			array('key'=>'localPic','val'=>$_POST),
			array('key'=>'localPic','val'=>$_GET),
			array('key'=>'user_id','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['user_id'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'pipeline_id','val'=>$_POST),
			array('key'=>'pipeline_id','val'=>$_GET),
			array('key'=>'pipeline_id','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['pipeline_id'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'accName','val'=>$_POST),
			array('key'=>'accName','val'=>$_GET),
			array('key'=>'accName','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['accName'] = $this->clean_xss($check);
		}
		$check = $this->get_elm_priority(
			array('key'=>'spaj','val'=>$_POST),
			array('key'=>'spaj','val'=>$_GET),
			array('key'=>'spaj','val'=>$defaultParam)
		);
		if(!empty($check)){
			$con['spaj'] = $this->clean_xss($check);
		}
		return $con;		
	}

	// recurse_copy ('/wampp','/xampp')
	function recurse_copy($src,$dst) { 
	    $dir = opendir($src); 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) ) { 
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) { 
	                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	            else { 
	                copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	        } 
	    } 
	    closedir($dir); 
	}

	// delete_dir('/wampp')
	function delete_dir($src) { 
	    if(is_dir($src) && opendir($src)){
	    	$dir = opendir($src);
		    while(false !== ( $file = readdir($dir)) ) { 
		        if (( $file != '.' ) && ( $file != '..' )) { 
		            if ( is_dir($src . '/' . $file) ) { 
		                delete_dir($src . '/' . $file); 
		            } 
		            else { 
		                unlink($src . '/' . $file); 
		            } 
		        } 
		    } 
		    rmdir($src);
		    closedir($dir); 
	    }
	}	

	//format mata uang
	function formatMoney($number) {
		return number_format($number,2,',','.');
	} 

	/**
	 * mendapatkan tanggal yang diinput dengan format yang sudah ditentukan
	 * $check = isRealDate('2015-01-01');
	 */
	function isRealDate($date){ 
	    if (false === strtotime($date)){ 
	        return false;
	    }else{ 
	        list($year, $month, $day) = explode('-', $date); 
	        if (false === checkdate($month, $day, $year)) 
	        { 
	            return false;
	        } 
	    } 
	    return true;
	}

	/**
	 *	Get day base on week view
	 *  first date of week start from monday and end to sunday
	 */
	function get_date_range($string_date=NULL,$returnType=NULL){
		
		if(empty($string_date))
			$string_date = date('Y-m-d');

		$day_of_week = FALSE;
		$week_first_day = FALSE;
		$week_last_day = FALSE;

		if($this->isRealDate($string_date)){
			$day_of_week = date('N', strtotime($string_date));
			$week_first_day = date('Y-m-d', strtotime($string_date . " - " . ($day_of_week - 1) . " days"));
			$week_last_day = date('Y-m-d', strtotime($string_date . " + " . (7 - $day_of_week) . " days"));
		}

		if(empty($returnType))
			return $string_date;
		elseif($returnType == 'dayOfWeek')
			return $day_of_week;
		elseif($returnType == 'weekFirstDate')
			return $week_first_day;
		elseif($returnType == 'weekLastDate')
			return $week_last_day;

		// 10 June 2012
		// result become "2012-06-10" if no return type
		// result become "7" if return type = dayOfWeek => senin - minggu / 1 - 7
		// result become "2012-06-4" if returntype = weekFirstDate
		// result become "2012-06-10" if returntype = weekLastDate
	}

	/**
	 *	Get day base on week view
	 *  first date of week start from monday and end to sunday
	 */
	function get_week_range($weekNumber,$month,$year,$returnType=NULL)
	{
		$srcDate = "01-$month-$year";
		$currDate = date_format(date_create($srcDate),'d M y');

		$firstDay = date('N', strtotime($currDate)); // day position in week
		$firstLine = 8-$firstDay; // num of date in the first weeek
		$firstRow = ceil($firstLine/7); // num of date in the first weeek
		// $lastDate = date("Y-m-t",strtotime($currDate)); // last date of month
		$lastDate = date("t",strtotime($currDate)); // last date of month
		$weekCount = ceil(($lastDate-$firstLine)/7) + $firstRow;

		if($weekNumber > $weekCount)
			die('week number is bigger in current Month');

		$n = ($weekNumber-1);
		$tempDate = date('Y-m-d',strtotime("+$n week",strtotime($srcDate)));

		if($weekNumber == 1)
			$sDate = date("Y-m-01",strtotime($currDate));
		else
			$sDate = $this->get_date_range($tempDate,'weekFirstDate');

		if($weekNumber == $weekCount)
			$eDate = date("Y-m-t",strtotime($currDate));
		else
			$eDate = $this->get_date_range($tempDate,'weekLastDate');

		if($returnType == 'weekFirstDate')
			return $sDate;
		elseif($returnType == 'weekLastDate')
			return $eDate;
		else
			return $sDate;
	}

	function get_week_limit($n=0){
		$weekDiff = 7*$n;
		$checkDate = date('Y-m-d',mktime (0, 0, 0, date('m'), (date('d') + $weekDiff), date('y')));
		$firstDate = $this->get_date_range($checkDate,'weekFirstDate');
		$lastDate = $this->get_date_range($checkDate,'weekLastDate');
		return array('start'=>$firstDate,'end'=>$lastDate);
	}

	function get_current_week(){
		return $this->get_week_limit(0);
	}

	function get_last_week(){
		return $this->get_week_limit(-1);
	}

	function get_next_week(){
		return $this->get_week_limit(1);
	}

	// $arrFiles = array('name'=>,'size'=>,'tmp_name'=>); spt $_FILES
	// $config = array('path'=>,'maxsize'=>,'maxwidth'=>,'maxheight'=>,'minwidth'=>,'minheight'=>,'type'=>array('zip','doc','docx'))
	function upload_file($arrFiles,$config=array()){
        $message = FALSE;
		$state = FALSE;
		// update file
		$imgsets = array(
			'filename' => NULL
			,'path' => 'upload_file/'
		 	// ,'maxsize' => 200000          // maximum file size, in KiloBytes
		 	// ,'maxwidth' => 5000          // maximum allowed width, in pixels
		 	// ,'maxheight' => 5000         // maximum allowed height, in pixels
		 	// ,'minwidth' => 10           // minimum allowed width, in pixels
		 	// ,'minheight' => 10          // minimum allowed height, in pixels
		 	// ,'type' => array('xls', 'doc', 'ppt', 'xlsx', 'docx', 'pptx', 'bmp', 'gif', 'jpg', 'jpe', 'png' ,'pdf' ,'exe' ,'zip')        // allowed extensions
		);
		foreach ($imgsets as $key => $value) {
			$config[$key] = (empty($config[$key])?$imgsets[$key]:$config[$key]);
		}

		if(!empty($arrFiles['name'])){
		    $tmpName = $arrFiles['tmp_name'];
		    $fileSize = $arrFiles['size'];

		    $fileName = str_replace(' ','',$arrFiles['name']);
		    $sepext = explode('.', strtolower($fileName));
		    $type = end($sepext);// gets extension

		    $imgName = (empty($config['filename'])?basename($fileName):$config['filename'].'.'.strtolower($type));

		    $this->create_path(str_replace('\\','/',$config['path']));

		    $err = '';// to store the errors

		    // Checks if the file has allowed type, size, width and height (for images)
		    if(!empty($config['type']) && !in_array($type, $config['type'])) 
		        $err .= 'The file: '. $fileName. ' has not the allowed extension type.';

		    // Check size
		    if(!empty($config['maxsize']))
		    	if($fileSize > $config['maxsize']*1000) $err .= 'Size : '.($fileSize/1000).'KB Maximum file size must be: '. $config['maxsize']. ' KB.';

		    $width = 0;
		    $height = 0;
		    if(in_array(strtolower($type),array('jpg','jpeg','gif','gif','bmp'))){

		    	list($width, $height) = getimagesize($tmpName);     // gets image width and height

			    if(isset($width) && isset($height) && !empty($config['maxwidth']) && !empty($config['maxheight']) && !empty($config['minwidth']) && !empty($config['minheight'])) {
			      if($width > $config['maxwidth'] || $height > $config['maxheight']) 
			        $err .= 'Width x Height = '. $width .' x '. $height .' The maximum Width x Height must be: '. $config['maxwidth']. ' x '. $config['maxheight'];
			      if($width < $config['minwidth'] || $height < $config['minheight']) 
			        $err .= 'Width x Height = '. $width .' x '. $height .'The minimum Width x Height must be: '. $config['minwidth']. ' x '. $config['minheight'];
			    }
		    }
		    

		    // If no errors, upload the image, else, output the errors
		    if($err == '') {
		      if(move_uploaded_file($tmpName,$config['path'].$imgName)) {
		      // if(copy($tmpName,$config['path'].$imgName)) {
		        $message = $imgName .' successfully uploaded: - Size: '. number_format($fileSize/1024, 3, '.', '') .' KB - Image Width x Height: '. $width. ' x '. $height;
		        $state = TRUE;
		      }
		      else{
		        $message = "Unable to upload the file";
		      }
		    }else{
		        $message = $err;
		    }
		}
		// var_dump(array('msg'=>$message,'path'=>$config['path'].$imgName,'filename'=>$imgName,'state'=>$state));exit;
		return array('msg'=>$message,'path'=>$config['path'].@$imgName,'filename'=>@$imgName,'extension'=>@strtolower($imgName),'state'=>$state,'width'=>@$width,'height'=>@$height);
	}

	function img2base64($imgPath){
		$type = pathinfo($imgPath, PATHINFO_EXTENSION);
		$data = file_get_contents($imgPath);
		return 'data:image/' . $type . ';base64,' . base64_encode($data);
		// echo '<img src="'.img2base64($imgPath).'" />';
	}

	function file2base64($filePath){
		$partFile = explode('/',$filePath);
		$file = end($partFile);
		$partExt = explode('.',$file);
		$ext = strtolower(end($partExt));

		if(!file_exists($filePath)){
			$ch = curl_init($filePath);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_NOBODY, 1);
			curl_exec($ch);
			$type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		}else{
			$type = pathinfo($filePath, PATHINFO_EXTENSION);
			$type = mime_content_type($filePath);
		}
		$fileData = base64_encode(file_get_contents($filePath));
		return 'data: '.$type.';base64,'.$fileData;
	}

	// $imgPath sudah termasuk nama file
	// pastikan extension yg dipilih adalah yg di file $imgPath atau proses $extension di dalam fungsi
	function base64toImg($imgPath,$data){
		$part64Img = explode(';base64,',$data);
		$base64Img = (!empty($part64Img[1]) ? $part64Img[1] : $part64Img[0]);

		$extension = '';
		if(!empty($part64Img[0]) && !empty($part64Img[1])){
			$extension = str_replace('data:image/','',$part64Img[0]);
			$imgPath.= '.'.$extension;
		}

		$success = FALSE;
		if($base64Img){
			$data = base64_decode(str_replace(' ', '+', $base64Img));
			$success = file_put_contents($imgPath, $data);
		}
		if($success){
			return array('state'=>TRUE,'path'=>$imgPath,'extension'=>$extension,'msg'=>'saved');
		}else{
			return array('state'=>FALSE,'msg'=>'failed');
		}
	}

	// $url = $this->additional_function->get_proposal_url('LEAD10000');
	function get_proposal_url($instId=NULL){
		$main_instance = $this->CI->access->get_access('main_instance');
		$main_instance = $this->CI->additional_function->set_value($main_instance,0);
		if(empty($main_instance) || $main_instance == 'lead'){
			// agent id yang didapat adalah agent id dari schema ajsdb
			$agent = $this->CI->user_data->get_detail(array('filter_sl_id'=>$instId),NULL,$this->CI->user_data->SINGLE);

			$this->CI->load->specific_model('lead');
			$lead = $this->CI->model_lead->get_detail(array('filter_sl_id'=>$instId,'field'=>'sl_bdate_time_incl'),NULL,$this->CI->user_data->SINGLE);
			$bDate = $this->set_value($lead,$this->CI->model_lead->get_field_name('sl_bdate'));
		}elseif($this->set_value($_SESSION,'responsive_main_instance') == 'lead'){
			// agent id yang didapat adalah agent id dari schema ajsdb
			$agent = $this->CI->user_data->get_detail(array('filter_sa3_id'=>$instId),NULL,$this->CI->user_data->SINGLE);

			$this->CI->load->specific_model('account');
			$account = $this->CI->model_account->get_detail(array('filter_sa3_id'=>$instId,'field'=>'sl_bdate_time_incl'),NULL,$this->CI->user_data->SINGLE);
			$bDate = $this->set_value($account,$this->CI->model_lead->get_field_name('sl_bdate'));			
		}

		$credential = $this->CI->access->set_credential();
		$agentId = $this->set_value($agent,'agent_id');

		// dari external data
		$this->CI->load->specific_model('external');
		$extJalurDist = $this->CI->model_external->get_ext_jalur_dist($agentId);
		$extJalurDist = $this->set_value($extJalurDist,strtoupper('id_dist'));
		$extTypeBis = $this->CI->model_external->get_ext_type_bis();
		$crmType = 27;

		$id1 = $this->generateCode(12);
		$id2 = $this->generateCode(12);
		$query = sprintf("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s',%s,%s)",
			'EKA.MST_URL_SECURE',
			$id1,
			$id2,
			$id1,
			$id2,
			$this->generateCode(6),
			'web=27~kode='.$agentId,
			sprintf("to_date('%s','YYYY-MM-DD')",date('Y-m-d')),
			99
		);
		$curr_connection = $this->CI->load->database(config_item('proposal_server'),TRUE);
		$result = $curr_connection->query($query);

		$hostname = base64_encode(config_item('hostname'));
		$url = config_item('proposal_login');

		$return = NULL;

		if(!empty($instId) && !empty($agentId) && !empty($extTypeBis)  && !empty($extJalurDist)  && !empty($bDate)  && !empty($hostname)){
			$return = array('state'=>TRUE,'url'=>$url."?web=$crmType&kode=$agentId&t=$extTypeBis&jal=$extJalurDist&lead=$instId&credential=$credential&hostname=$hostname&id1=$id1&id2=$id2",'desc'=>'');
		}else{
			$return = array('state'=>FALSE,'url'=>$url."?web=$crmType&kode=$agentId&t=$extTypeBis&jal=$extJalurDist&lead=$instId&credential=$credential&hostname=$hostname&id1=$id1&id2=$id2");
			if(empty($instId))
				$return['desc'][] = 'calon nasabah belum ada';
			if(empty($agentId))
				$return['desc'][] = 'nomor agent tidak ada';
			if(empty($extTypeBis))
				$return['desc'][] = 'type bisnis tidak ada';
			if(empty($extJalurDist))
				$return['desc'][] = 'jalur distribusi tidak ada';
			if(empty($bDate))
				$return['desc'][] = 'tanggal lahir belum terisi';
			if(empty($hostname))
				$return['desc'][] = 'hostname belum di set';
		}
		return $return;
		
	}

	function calculate_age($bdate){
		$from = new DateTime($bdate);
		$to   = new DateTime('today');
		return strval($from->diff($to)->y);
	}

	// ketika
	// $cond = new stdClass();
	// $cond->var1 = 'var1';
	// function use_obj($cond){
	// 	$cond->var2 = 'var2';
	// 	unset($cond);
	// }
	// var_dump($cond) => $cond->var1, $cond->var2
	// untuk menghindari pass by reference object maka gunakan clone object secara iteratif
	function clone_obj($oldObj=NULL){
		$newObj = new stdClass();
		if(!empty($oldObj) && is_object($oldObj)){
			foreach ($oldObj as $idxObj => $valObj) {
				$newObj->{$idxObj} = $valObj;
			}
		}
		return $newObj;
	}

	// mendapatkan beberapa kata awal dari html untuk rangkuman berita
	function get_words_from_html($sentence, $count = 10) {
	  	$sentence = preg_replace("/<img[^>]+\>/i", "(image) ",$sentence);
		$sentence = strip_tags($sentence);
		$partSentence = explode(' ',$sentence);
		$newSentence = array_slice($partSentence,0,$count);
		return implode(' ',$newSentence);
	}

	function get_repo(){
		// Author: Ngo Minh Nam
		$dir = "/path/to/your/repo/";
		$output = array();
		chdir($dir);
		exec("git log",$output);
		$history = array();
		foreach($output as $line){
		    if(strpos($line, 'commit')===0){
			if(!empty($commit)){
			    array_push($history, $commit);	
			    unset($commit);
			}
			$commit['hash']   = substr($line, strlen('commit'));
		    }
		    else if(strpos($line, 'Author')===0){
			$commit['author'] = substr($line, strlen('Author:'));
		    }
		    else if(strpos($line, 'Date')===0){
			$commit['date']   = substr($line, strlen('Date:'));
		    }
		    else{		
			$commit['message']  .= $line;
		    }
		}
		print_r($history);
	}

	// 'Test SATU dua tiga' = 'test_satu_dua_tiga'
	function get_slug($title=NULL){
		return strtolower(preg_replace('/\s+/','-',urldecode($title)));
	}

	function currency_convert($src='IDR',$dest='USD',$value=1){
		// rp = 20000
		// currency_convert('IDR','USD',20000) = 

		// $srcHtml = @file_get_contents("http://download.finance.yahoo.com/d/quotes.csv?s=USD".$src."=X&f=nl1d1t1");
		// @list($currency,$srcVal,$date,$time) = explode(',',$srcHtml);
		// $destHtml = @file_get_contents("http://download.finance.yahoo.com/d/quotes.csv?s=USD".$dest."=X&f=nl1d1t1");
		// @list($currency,$destVal,$date,$time) = explode(',',$destHtml);
		// return ($srcHtml && $destHtml?$this->ceiling($value*$destVal/$srcVal,0.000005):FALSE);
		
		return $value;
	}

	function round_up($value, $places=0) {
		//echo round_up (56.77001, 2); // displays 56.78
		//echo round_up (-0.453001, 4); // displays -0.4530
	  	if ($places < 0) { $places = 0; }
	  	$mult = pow(10, $places);
	  	return ceil($value * $mult) / $mult;
	}

	function ceiling($number, $significance = 1)
	{
		// echo ceiling(1001, 1000);  // 2000
		// echo ceiling(1.27, 0.05);  // 1.30
	    return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
	}

	// significance 0.05 = 0.05 step = 0, 0.05, 0.1, 0.15, 0.2, 0.25 ... dst
	// jika number 10.025 akan berada antara 10.0 sampai 10.05
	// jika number 10.075 akan berada antara 10.05 sampai 10.1
	// rounding(10.075,0.05) = 10.1 => lewat setengah jalan, naik keatas
	function rounding($number, $significance = 1){
		$exp = 1;
		$result = NULL;
		$ribuan = 1;
		if(is_numeric($number) && is_numeric($significance)){
			// pastikan $significant dalam keadatan tanpa koma
			if(FALSE !== strpos($significance,'.')){
				list($front,$end) = explode('.',$significance);
				$number *= pow(10,strlen($end));
				$significance *= pow(10,strlen($end));
				$ribuan *= pow(10,strlen($end));
			}
			// pastikan $number dalam keadatan tanpa koma
			if(FALSE !== strpos($number,'.')){
				list($front,$end) = explode('.',$number);
				$number *= pow(10,strlen($end));
				$significance *= pow(10,strlen($end));
				$ribuan *= pow(10,strlen($end));
			}
			// cari hasil pembulatan
			$resExp = round($number/$significance)*$significance;
			// kembali ke asal
			$result = $resExp/$ribuan;
		}
	    return $result;
	}
	
	// get_numeric(SPAJ10.075,0.05) = 10075005
	function get_numeric($sanitize){
		return str_replace(array('+','-'),'',filter_var($sanitize, FILTER_SANITIZE_NUMBER_INT));
	}

	function guidv4()
	{
	    if (function_exists(‘com_create_guid’) === true) return trim(com_create_guid(), '{}');

	    $data = openssl_random_pseudo_bytes(16);
	    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
	    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
	    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	function getRequestHeaders() {
	    $headers = array();
	    foreach($_SERVER as $key => $value) {
	        if (substr($key, 0, 5) <> 'HTTP_') {
	            continue;
	        }
	        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
	        $headers[$header] = $value;
	    }
	    return $headers;
	}

}
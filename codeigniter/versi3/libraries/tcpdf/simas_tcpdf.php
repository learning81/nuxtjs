<?php
require_once BASEPATH.'libraries/tcpdf/tcpdf.php';
require_once(BASEPATH.'libraries/tcpdf/config/lang/eng.php');

chdir(BASEPATH);

class Simas_tcpdf extends TCPDF
{
	var $img = NULL;
	var $imgType = NULL;

	function __construct($defaultParam=NULL)
	{
		if(empty($defaultParam))
		{
			$orientation='P';
			$unit='mm';
			$format='A4';
		}
		else
		{
			$orientation = (!empty($defaultParam['orientation'])?$defaultParam['orientation']:'P');
			$unit = (!empty($defaultParam['unit'])?$defaultParam['unit']:'mm');
			$format=(!empty($defaultParam['format'])?$defaultParam['format']:'A4');
		}

		parent::__construct($orientation,$unit,$format);

        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Akhyar Azni');
        $this->SetTitle('Sinarmas Reporting System');
        $this->SetSubject('Sinarmas Reporting System');
        $this->SetKeywords('report');

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $this->SetAutoPageBreak(TRUE, 0);

        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // $this->AddPage('L',array(420,100));  // manual width and height      
	}

    //Page header
    public function Header() {
        // Logo
        $image_file = '../assets/img/msig_logo_30.png';
        $this->Image($image_file, 15, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->setXY(($this->getX()+20),($this->getY()+5));
        //$this->Cell(0, 15, 'SIMAS CRM', 0, false, 'L', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }    

    /*
     *  Keterangan pemindahan posisi:
     * 
     *  fungsi : 
     *  $ratio = array(x,y,width,height,fontSize,bold,line,hPos,vPos,color);
     * 
     *  x = posisi X, semakin ke kanan nilai semakin besar
     *  y = posisi Y, semakin ke bawah nilai semakin besar
     *  width = lebar dari text cell
     *  height = tinggi dari text cell
     *  fontSize = ukuran dari font
     *  bold (TRUE/FALSE) = kondisi font untuk bold(TRUE) atau tidak bold(FALSE)
     *  line (TRUE/FALSE) = kondisi border untuk cell apakah diborder(TRUE) atau tidak diborder(FALSE)
     *  hPos = posisi text pada cell yaitu 'C' untuk center, 'L' untuk left dan 'R' untuk right
     *  vPos = T=top,M=middle,B=bottom
     *  color = 1 dan 0 utk warna solid atau tidak dari $this->simas_tcpdf->SetFillColor(249, 252, 177);
     * 
     */
    public function insert_data($string,$dateFormat=NULL,$ratio){

        $this->SetFillColor(249,249,249);
        
        $val = NULL;
        if(isset($string)){
            if(!empty($dateFormat)){
                    $val = date_format(date_create($string),$dateFormat);
            }else{
                    $val = $string;
            }
        }

        $x = (!empty($ratio[0])?$ratio[0]:$this->getX());
        $y = (!empty($ratio[1])?$ratio[1]:$this->getY());
        $width = (!empty($ratio[2])?$ratio[2]:100);
        $height = (!empty($ratio[3])?$ratio[3]:10);
        $font = (!empty($ratio[4])?$ratio[4]:12);

        $bold = (!empty($ratio[5]) && $ratio[5]?'B':'');
        $line = (!empty($ratio[6]) && $ratio[6]?1:0);
        $align = (!empty($ratio[7])?$ratio[7]:'L');
        $valign = (!empty($ratio[8])?$ratio[8]:'M');
        $color = (!empty($ratio[9]) && $ratio[9]?1:0);

        $this->SetFont('helvetica',$bold,$font);
        //MultiCell($widht,$height,$txt,$borderFlag=0,$alignFlag='J',$cellFillFlag=FALSE,$newLine=1,$x='',$y='',$resetLastHeightFlat=TRUE,$stretch=0,$htmlFlag=FALSE,$paddingFlag=TRUE,$maxh=0,$valignFlag='T',$fitcell=FALSE);

        $this->MultiCell($width,$height,trim($val),$line,$align,$color,0,$x,$y,TRUE,0,FALSE,TRUE,$height,$valign,FALSE);
        return array('x'=>$this->getX(),'y'=>$this->getY());
    }
}
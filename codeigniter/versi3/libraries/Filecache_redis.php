<?php
class CI_Filecache_redis
{
	public $userId = 'temp';

	function __construct($userId=NULL){
    	$this->CI = &get_instance();
    	$this->isRedis = FALSE;

		$redisLoaded = extension_loaded('redis');
    	if($redisLoaded){
    		require BASEPATH.'libraries/redis/Autoloader.php';
    		Predis\Autoloader::register();
    		$this->redis = new Predis\Client();

    		try
    		{
    		    $this->redis->connect();
    			$this->isRedis = TRUE;
    		}
    		catch (Predis\Connection\ConnectionException $exception)
    		{
    		    // exit("whoops, couldn't connect to the remote redis instance!");
    		}
    	}

    	/* --------------------
    	/* Tutorial redis
    	/* tutorial : https://www.sitepoint.com/an-introduction-to-redis-in-php-using-predis/
    	/* -------------------
    		$a = $this->redis->ping();
    		$this->redis->info();
    		$this->redis->set("hello_world", "Hi from php!");
			$value = $this->redis->get("hello_world");
			echo ($this->redis->exists("Santa Claus")) ? "true" : "false";
			var_dump($value); -> "Hi from php!"
			$this->redis->hset("storage", "var1", "val1"); // set
			$this->redis->hset("storage", "var2", "val2"); // set
			$this->redis->hget("storage", "var1"); // get
			$this->redis->hdel("storage", "var1"); // del
			$this->redis->del("storage", "var1"); // del all
			$this->redis->hgetall("storage"); // getall
			$this->redis->hkeys("storage"); // getall keys

			$redis->expire("expire in 1 hour", 3600);
			$ttl = $redis->ttl("expire in 1 hour"); // will be 3600 seconds			
		*/
	}

	function init($userId=NULL){
		// define path
		if(!empty($userId))
			$this->userId = $userId;
		elseif(array_key_exists('profile_userId',$_SESSION) && !empty($_SESSION['profile_userId']))
			$this->userId = $_SESSION['profile_userId'];

		$this->PATH = dirname(APPPATH).'/cache/'.base64_encode($this->userId).'/';
		$this->expDate = 'exp_date';
	}

	function cache_save($key=NULL,$value=NULL,$filename=NULL,$user=NULL){
		$this->init($user);
		if($this->isRedis){ // redis tidak menggunakan filename
			$this->redis->hset($this->userId,$key,$this->CI->services_json->encode($value));
		}else{
			$filename = base64_encode($filename);

			$arrKeys = array();
			$arrValues = array();
			$header = "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n";
			if(!file_exists($this->PATH.$filename)){
				//create path
				if(!$this->CI->additional_function->create_path($this->PATH)){
					return FALSE;
				}
			}else{
				// get old data
				if($fp=fopen($this->PATH.$filename, 'r')){
					while (!feof($fp))
					{
					    $line[]=fgets($fp);
					}
					fclose($fp);

					//define existed menu
					$arrKeys = ($line[1]?json_decode($line[1]):array());

					//define existed values
					if(count($arrKeys)>0){
						$orderLine = 2;
						foreach($arrKeys as $item){
							$arrValues[$item] = $line[$orderLine++];
						}
					}
				}else{
					die('file tidak bisa dibaca');
				}
			}

			// set expired days
			$arrValues[$this->expDate] = json_encode(strval(time()))."\n";

			// replace or update new value
			if(isset($key) && $key != '')
				$arrValues[$key] = (is_numeric($value)?json_encode(strval($value)):json_encode($value))."\n";

			$msg = $header;
			$msg .= json_encode(array_keys($arrValues))."\n";
			foreach ($arrValues as $values) {
				$msg .= $values;	
			}

			// fill in new arr key and new arr values
			if ( ! $fp = @fopen($this->PATH.$filename, 'w'))
			{
				return FALSE;
			}

			if($filename == 'session'){
				$_SESSION[$key] = $value;
			}

			fwrite($fp, $msg."\n");
			fclose($fp);
		}
	}
	
	function cache_read($key=NULL,$filename=NULL,$user=NULL){
		$result = NULL;
		$this->init($user);
		if($this->isRedis){ // redis tidak menggunakan filename
			$result = $this->CI->services_json->decode($this->redis->hget($this->userId,$key)); // get
		}else{
			$filename = base64_encode($filename);

			if(!file_exists($this->PATH.$filename)){
				$result = FALSE;
			}else{
				$arrKeys = json_decode($this->CI->additional_function->read_line($this->PATH.$filename,2));
				$search_key = $this->CI->additional_function->array_search_by_value($arrKeys,$key);

				// cari expdate
				$expline = $this->CI->additional_function->array_search_by_value($arrKeys,$this->expDate);
				$expTime = json_decode($this->CI->additional_function->read_line($this->PATH.$filename,($expline+3)));

				$minElaps = (time() - $expTime) / 60;
				// if($key == 'agent_id') {var_dump($minElaps,$arrKeys);exit;};

				// jika pake cache expired, selalu perhatikan batas waktu expired
				// jika tidak pake cache_expired, berarti bypass selalu proses ini (OR TRUE)

				if(
					is_numeric($search_key)
					&& (
						$key != 'agent_id'
						|| $minElaps < 120
					)
				){ // batas data filecache maksimum 2 jam (120)
					$valueLine = $search_key + 3;
					$values = $this->CI->additional_function->read_line($this->PATH.$filename,$valueLine);
					$lastValue = str_replace(array("\n"),"",$values);
					if($lastValue != "null"){
						$result = json_decode($lastValue);
					}
					// setiap read data, akan memperpanjang umur cache agent_id
					$this->cache_save($this->expDate,strval(time()),$filename,$user);
				}
			}
		}
		
		return $result;
	}

	function get_all_key($filename=NULL,$user=''){
		die('deprecated since filecache redis has no file inside user');
		$result = NULL;
		$this->init($user);

		if($this->isRedis){
			$result = $this->redis->hGetAll($this->userId); // getall keys
		}elseif(!empty($filename)){
			$filename = base64_encode($filename);
			if(file_exists($currPath.$filename)){
				$result = json_decode($this->CI->additional_function->read_line($currPath.$filename,2));
			}
		}

		return $result;
	}

	//tidak dibutuhkan karena di redis tidak ada file di dalam user
	function get_all_file($user=NULL){
		$arrFile = array();
		$this->init();

		if($this->isRedis){
			$result = $this->redis->hGetAll($this->userId); // getall keys
		}else{
			if(file_exists($this->PATH)){
				$tmpFile = $this->CI->additional_function->get_file_list($this->PATH,array('.','..'));
				if(is_array($tmpFile)){
					foreach ($tmpFile as $file) {
						array_push($arrFile,base64_decode($file));
					}
				}
			}
		}
		return $arrFile;
	}

	function get_all_user(){
		$arrFile = array();
		$this->init();

		if($this->isRedis){
			// pending
		}else{
			if(file_exists(dirname(APPPATH).'/cache/')){
				$tmpFile = $this->CI->additional_function->get_file_list(dirname(APPPATH).'/cache/',array('.','..'));
				if(is_array($tmpFile)){
					foreach ($tmpFile as $file) {
						array_push($arrFile,base64_decode($file));
					}
				}
			}
		}
		return $arrFile;
	}

	function remove_file($filename=NULL,$user=NULL){
		$this->init($user);
		if($this->isRedis){
			// $cmdSet = $this->redis->createCommand('delete');
			// $cmdSet->setArgumentsArray($this->userId);
			// $cmdSetReply = $this->redis->executeCommand($cmdSet);
			if(!empty($filename))
				$result = $this->redis->del($this->userId,$filename);
			else
				$result = $this->redis->del($this->userId);
		}else{
			$filename = base64_encode($filename);
			if(is_dir($this->PATH.$filename))
				$this->CI->additional_function->delete_dir($this->PATH.$filename);
			elseif(file_exists($this->PATH.$filename))
				unlink($this->PATH.$filename);
		}
	}

	function remove_user($agentId=NULL){
		if($this->isRedis){
			//pending
		}else{
			$filename = base64_encode($agentId);
			if(is_dir(dirname(APPPATH).'/cache/'.$filename))
				$this->CI->additional_function->delete_dir(dirname(APPPATH).'/cache/'.$filename);
			elseif(file_exists(dirname(APPPATH).'/cache/'.$filename))
				unlink(dirname(APPPATH).'/cache/'.$filename);
		}
	}

	function get_path($filename=NULL,$user=NULL){
		$this->init($user);
		if($this->isRedis){
			return NULL;
		}else{
			$filename = base64_encode($filename);
			return $this->PATH.'/'.$filename;
		}
	}
}

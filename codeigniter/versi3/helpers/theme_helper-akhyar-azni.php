<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 *
 * @package	CodeIgniter
 * @author	Akhyar Azni
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		akhyar
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('load_template'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function load_template($viewpath,$arrParam=array(),$forceTemplate='',$headerMenu=TRUE){
		
		$CI =& get_instance();

		// 1st priority
		$theme = $forceTemplate;
		// 2nd priority
		if(empty($theme))
			$theme = $CI->additional_function->set_value($_SESSION,'profile_theme','string','default',NULL,FALSE).'/';
		// default theme
		if(empty($theme))
			$theme = 'default';

		if($headerMenu){
			$public = ($CI->user_data->check_user_session() ? FALSE : TRUE); // apakah anonymous (blm login)

			// jika belum login, cari semua halaman yang bisa diakses publik dari databasee
			// jika sudah login, cari semua halaman yang bisa diakses oleh user akses sekarang
			$arrParams = array(
				'bypass_access'=>TRUE,
				'filter_mm_active'=>1,
				'include_public'=>$public,
				'include_child'=>TRUE,
				'disable_header'=>TRUE,
				'ordid'=>'mm_order',
				'ordtype'=>'asc',
				'only_parent'=>TRUE,
				'disable_header'=>TRUE,
				'include_child'=>TRUE,
				'ordid'=>'mm_order',
				'ordtype'=>'asc'
			);
			$mainmenu = $CI->main_menu->get_detail2($arrParams);

			// tambahkan semua page yang belum ditambahkan di database,
			// sebagai aware ada page yang belum dikasih akses
			$otherPage = $CI->main_menu->get_all_pages(FALSE);

			$arrParam['mainmenu'] = $mainmenu;
			$arrParam['otherPage'] = $otherPage;
			$CI->load->view($theme.$viewpath,$arrParam);
		}else{
			$CI->load->view($theme.$viewpath,$arrParam);
		}
	}
}

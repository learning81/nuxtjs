<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 *
 * @package	CodeIgniter
 * @author	Akhyar Azni
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		akhyar
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('load_template'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function load_template($viewpath,$arrParam=array(),$forceTemplate='',$headerMenu=TRUE){
		
		$CI =& get_instance();

		// 1st priority
		$theme = $forceTemplate;

		// 2nd priority
		if(empty($theme))
			$theme = $CI->additional_function->set_value($_SESSION,'default_theme');

		// 3rd priority
		if(empty($theme))
			$theme = $CI->additional_function->set_value($_GET,'default_theme','string');

		// default theme
		if(empty($theme))
			$theme = 'default';

		$currPath1 = $theme.'/'.$viewpath;
		$searchPath1 = str_replace('\\','/',VIEWPATH.$currPath1).'.php';

		// $_SESSION['profile_languange'] = 'english';
		$newPath = explode('/',$currPath1);
		if(array_key_exists('default_language',$_SESSION)) array_splice( $newPath, (count($newPath)-1), 0,$_SESSION['default_language']);
		$currPath2 = implode('/',$newPath);
		$searchPath2 = str_replace('\\','/',VIEWPATH.$currPath2).'.php';

		$currPath = (file_exists($searchPath2)?$currPath2:$currPath1);

		if($headerMenu){
			$public = ($CI->user_data->check_user_session() ? FALSE : TRUE); // apakah anonymous (blm login)

			// jika belum login, cari semua halaman yang bisa diakses publik dari database
			// jika sudah login, cari semua halaman yang bisa diakses oleh user akses sekarang
			$arrParams = array(
				'bypass_access'=>TRUE,
				'filter_dm_active'=>1,
				'include_public'=>$public,
				'include_child'=>TRUE,
				'disable_header'=>TRUE,
				'ordid'=>'dm_order',
				'ordtype'=>'asc',
				'only_parent'=>TRUE,
				'disable_header'=>TRUE,
				'include_child'=>TRUE,
				'ordid'=>'dm_order',
				'ordtype'=>'asc',
				'limit'=>FALSE
			);
			$mainmenu = $CI->main_menu->get_detail($arrParams);

			// tambahkan semua page yang belum ditambahkan di database,
			// sebagai aware ada page yang belum dikasih akses
			$otherPage = $CI->main_menu->get_all_pages(FALSE);

			// tambah lagi page aktif dari main::load_page
			// page dari mplace ingin ditambahkan ke halaman merchant
			$CI->load->specific_model('page');
			$customPage = $CI->model_page->get_detail(array('filter_pp_active'=>1));
			if(!empty($customPage) && is_array($customPage)){
				foreach ($customPage as $idxPage => $valPage) {
					$currSlug = $CI->additional_function->set_value($valPage,'pp_slug');
					$currName = $CI->additional_function->set_value($valPage,'pp_title');
					$currDetail = $CI->additional_function->set_value($valPage,'pp_detail');
					$currUrl = config_item('global_instMainUrl').'load_page_'.$currSlug;
					$mainmenu[] = array(
						'dm_name_tmp'=>$currName,
						'url'=>$currUrl
					);
				}
			}

			$arrParam['mainmenu'] = $mainmenu;
			$arrParam['otherPage'] = $otherPage;
			$CI->load->view($currPath,$arrParam);
		}else{
			$CI->load->view($currPath,$arrParam);
		}
	}
}

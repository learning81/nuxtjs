<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center mb-8">
    <div class="bg-gray-200 w-2/3 mt-10 flex flex-row py-3 px-3">
      Kelas I A - Matematika
    </div>
    <div class="bg-gray-200 w-2/3 mt-10 flex flex-row">
      <div class="box_leftbar bg-pink-500 text-white">
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan 1</div>
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan 2</div>
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan 3</div>
      </div>
      <div class="box_content flex flex-col flex-grow px-10 py-3">
        <div class="grid grid-cols-1 gap-3 mb-4">
          <label>Title</label>
          <input class="w-full p-3" type="" name="">
        </div>
        <div class="grid grid-col mb-4">
          <label>Deskripsi</label>
          <textarea class="p-3"></textarea>
        </div>
        <div class="grid grid-col mb-4">
          <label>Video</label>
          <input class="p-3" type="" name="" placeholder="http://www.youtube.com/channel=abc">
        </div>
        <div class="grid grid-col mb-4 w-full">
          <img class="object-cover w-full" src="<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/youtube.png">
        </div>
        <div>
          <div class="bg-pink-500 py-3 px-5 text-center text-white rounded-md cursor-pointer">Submit</div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('default/common/current_footer');?>

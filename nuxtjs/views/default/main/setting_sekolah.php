<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Setting Sekolah</div>
    <div class="grid md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
      <div class="flex lg:flex-row flex-col border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer" id="box_detail">
        <div class="w-32 mb-8">
          <iframe id="iframe_logo" name="iframe_logo" height="0" width="100%" frameborder="0" scrolling="yes" v-on:load="updatelogo"></iframe>
          <img class="w-full h-32 rounded-full" v-bind:src="pathlogo">
          <form action="<?php echo config_item('global_instMainUrl');?>upload/" id="form_logo" method="post" enctype="multipart/form-data"  target="iframe_logo">
            <input type="file" name="upload_file" class="w-full text-xs text-white mt-4" v-on:change="uploadlogo">
            <input type="hidden" name="upload_path" value="" class="form-control" />
            <input type="hidden" name="upload_id" value="" class="form-control" />
            <input type="hidden" name="upload_name" value="sekolah" class="form-control" />
          </form>
        </div>
        <div class="ml-3 flex flex-1 flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Detail</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info mt-5">
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kode</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_sekolah.item_code">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Nama</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_sekolah.item_name">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">No Telp</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_sekolah.item_telp">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Alamat</label>
              <textarea class="border-2 border-solid border-gray-300" v-model="item_sekolah.item_addr"></textarea>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Provinsi</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_sekolah.item_prop">
                <option value=1>Provinsi 1</option>
                <option value=2>Provinsi 2</option>
                <option value=3>Provinsi 3</option>
                <option value=4>Provinsi 4</option>
                <option value=5>Provinsi 5</option>
              </select>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kota/Kabupaten</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_sekolah.item_kab">
                <option value=1>Kabupaten 1</option>
                <option value=2>Kabupaten 2</option>
                <option value=3>Kabupaten 3</option>
                <option value=4>Kabupaten 4</option>
                <option value=5>Kabupaten 5</option>
              </select>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kecamatan</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_sekolah.item_kec">
                <option value=1>Kecamatan 1</option>
                <option value=2>Kecamatan 2</option>
                <option value=3>Kecamatan 3</option>
                <option value=4>Kecamatan 4</option>
                <option value=5>Kecamatan 5</option>
              </select>
            </div>
            <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
              <div class="" v-on:click="submit">Submit</div>
            </div>
          </div>
        </div>
      </div>
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Informasi Tambahan</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info grid grid-flow-col mt-5">
            <div class="w-full overflow-x-scroll">
              <div id="datalist" title="Jenis Sekolah"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="outer bg-gray-100" v-show="show_modal" id="modal_sekolah" style="display: table; position: fixed; height: 100%; width: 100%; left: 0; top: 0; background-color: rgba(0,0,0,0.5)">
        <div class="middle" style="display: table-cell; vertical-align: middle; ">
               <div class="inner lg:w-1/3 w-full container bg-white p-5 rounded-md" style="margin: 0 auto;">

                 <div class="flex lg:flex-row flex-col p-3 rounded-md">
                   <div class="ml-3 flex flex-1 flex-grow flex-col">
                     <div class="border-b border-gray-500 border-solid">Update Data</div>
                     <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
                     <div class="box_info mt-5">
                       <div class="flex flex-col flex-grow">
                         <label class="mt-3">Jenis</label>
                         <input class="border-2 border-solid border-gray-300" type="" name="" disabled="disabled" v-model="item_derajat.name">
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="mt-3">Sumbangan Minimum</label>
                         <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_derajat.min">
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="mt-3">Sumbangan Maximum</label>
                         <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_derajat.max">
                       </div>
                       <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
                         <div class="cursor-pointer" v-on:click="submit">Submit</div>
                       </div>
                     </div>
                   </div>
                 </div>

              </div>
        </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {
              "idx_derajat":[]
              ,"change_idx_derajat":function(idx_derajat){
                $.vueTable.idx_derajat=idx_derajat;
                $.vueModal.idx_derajat=idx_derajat;
              }
              ,"item_derajat":[]
              ,"change_item_derajat":function(item_derajat){
                $.vueTable.item_derajat=JSON.parse(JSON.stringify(item_derajat));
                $.vueModal.item_derajat=JSON.parse(JSON.stringify(item_derajat));
              }
            }
        });
        <?php 
        $url ="";
        if(file_exists(config_item('data_folder').'sekolah.jpg')){
          $url=config_item('global_instAssetsUrl').'load_thumbnail/sekolah.jpg';
        }else{
          $url="http://localhost/assets/load_image/logo-design-software.png";
        }
        ?>
        $.extend({vueDetail:new Vue({el:'#box_detail'
                  ,data:{
                    "message":"i'm new"
                    ,"item_sekolah":<?php echo json_encode($_SESSION['sekolah']);?>
                    ,"pathlogo":"<?php echo $url;?>?rand="+$.randomString(5,'aA#')
                    ,"urlsave":global['instMainUrl']+'json_sekolah/'
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,updatelogo: function(data){
                      var htmldata = $(this.$el).find("iframe#iframe_logo").contents().find("body").html();
                      if(htmldata){
                        var result = $.parse(htmldata);
                        if(result.state){
                          this.input_logo = result['data'][0]['filename'];
                          this.pathlogo = global['instAssetsUrl']+'load_thumbnail/sekolah.jpg?rand='+$.randomString(5,'aA#')
                        }else{
                          alert(result.msg);
                        }
                      }
                    }
                    ,uploadlogo: function(data){
                      $(this.$el).find('form#form_logo').submit();
                    }                      
                    ,submit: function(data){
                      $.setAjax(this.urlsave,'post',this.item_sekolah,function(result){
                        alert('data sudah diupdate');
                      });
                    }                      
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        $.extend({vueTable:new Vue({el:'#datalist'
                  ,data:{
                    "message":"i'm new"
                    ,"idx_derajat":""
                    ,"item_derajat":[]
                    ,"arr_derajat":[
                      ['1','SMU','1.000.000','10.000.000']
                      ,['2','SMP','1.000.000','10.000.000']
                      ,['3','SD','1.000.000','10.000.000']
                    ]
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      this.reattachtable();
                    }
                    ,changed_arr_derajat: function(arr_derajat){
                      this.arr_derajat=JSON.parse(JSON.stringify(arr_derajat));
                      this.reload_table();
                    }
                    ,reload_table: function(){
                      $.jsObj.table1.option.jsonData=JSON.parse(JSON.stringify(this.arr_derajat));
                      $.jsObj.table1.option.countData=$.jsObj.table1.option.jsonData.length;
                      $.jsObj.table1.rebuildGrid();
                    }
                    ,reattachtable: function(data){
                      $('#datalist').simasgrid({
                            "field":[
                                  {id:"item_id",text:'No',type:'string',width:50,'align':'center'}, //
                                  {id:"item_name",text:'Jenis',type:'string',width:75,'align':'left'},
                                  {id:"item_min",text:'Minimum Sumbangan',type:'string',width:100,'align':'center'},
                                  {id:"item_max",text:'Maksimum Sumbangan',type:'string',width:100,'align':'center'},
                                  {id:"control",text:'',type:'string',width:100,'align':'center','addEvent':false}
                            ]
                            ,"width":'100%'
                            ,"height":'400px'
                            ,"checkBoxAdded":false
                            ,"useSearch":false
                            // ,"jsonUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&field=item_id,tr_code,tr_name,tr_group,"" item_plan,"" item_type,tr_std_rate,tr_overtime,tr_disable,"" control'
                            // ,"countUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&flimit=false&return=count'
                            ,"jsonData":JSON.parse(JSON.stringify($.vueTable.arr_derajat))
                            ,"countData":$.vueTable.arr_derajat.length
                            ,"postData":{'ordid':'item_id','ordtype':'asc'}
                            ,"onDataLoad":function(tr,data,tdStorage,storageTd){
                                  var btnActive = '<div class="grid grid-flow-row gap-2 text-sm">\
                                                    <div class="btn-edit border-solid border-2 border-blue-700 rounded-full py-1 text-center">Edit</div>\
                                                    <div class="btn-active border-solid border-2 border-green-700 rounded-full py-1 text-center">Active</div>\
                                                    <div class="btn-not-active border-solid border-2 border-red-700 rounded-full py-1 text-center">Disable</div>\
                                                  </div>';
                                  $(tr).find('td').eq(storageTd[4]).html(btnActive);
                                  $(tr).find('td').addClass('bg-green-300');
                                  $(tr).find('td').addClass('border-b');
                                  $(tr).find('td').addClass('border-solid');
                                  $(tr).find('td').addClass('border-white');
                            }
                            ,"callback":function(tr,e,gridObj){
                                  var currId = $(tr).attr('id');
                                  var currData = $.jsObj.table1.dataStorage[currId];
                                  var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                                  var rawData = $.jsObj.table1.rawData[currIdx];

                                  $.jsObj.change_idx_derajat(currIdx);
                                  $.jsObj.change_item_derajat({
                                    "id":rawData[0]
                                    ,"name":rawData[1]
                                    ,"min":rawData[2]
                                    ,"max":rawData[3]
                                  });

                                  $.vueModal.show();
                            }

                        }
                      );
                      $('#datalist').simasgrid('addNav',{
                        title:'Reload',
                        className:'navigation',
                        callback:function(simasGrid){
                              $.vueTable.reload_table();
                        }
                      }); 
                      $.jsObj.table1 = $('#datalist').simasgrid('getObj');

                      // dom simasgrid yg bisa diakses hanya gridObj
                      $($.jsObj.table1.gridObj).on('click','.btn-edit',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        var rawData = $.jsObj.table1.rawData[currIdx];

                        $.jsObj.change_idx_derajat(currIdx);
                        $.jsObj.change_item_derajat({
                          "id":rawData[0]
                          ,"name":rawData[1]
                          ,"min":rawData[2]
                          ,"max":rawData[3]
                        });

                        $.vueModal.show();
                      })
                      $($.jsObj.table1.gridObj).on('click','.btn-active',function(){
                        var currTr = $(this).closest('tr');
                        var allTd = $(currTr).find('td');
                        for(var x=0; x<allTd.length; x++){
                          $(allTd[x]).removeClass('bg-red-300');
                          $(allTd[x]).addClass('bg-green-300');
                        }
                      })
                      $($.jsObj.table1.gridObj).on('click','.btn-not-active',function(){
                        var currTr = $(this).closest('tr');
                        var allTd = $(currTr).find('td');
                        for(var x=0; x<allTd.length; x++){
                          $(allTd[x]).removeClass('bg-green-300');
                          $(allTd[x]).addClass('bg-red-300');
                        }
                      })

                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        $.extend({vueModal:new Vue({el:'#modal_sekolah'
                  ,data:{
                    "message":"i'm new"
                    ,"show_modal":false
                    ,"item_derajat":{}
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      $(this.$el).on("click", function (e) {
                            if ($(e.target).closest(".inner").length === 0) {
                                   $.vueModal.hide();
                            }
                      });
                    }
                    ,show: function(data){
                      this.show_modal=true;
                    }
                    ,hide: function(data){
                      this.show_modal=false;
                    }
                    ,submit: function(data){
                      $.jsObj.change_item_derajat(this.item_derajat);
                      var currData = JSON.parse(JSON.stringify($.vueTable.arr_derajat));
                      currData[this.idx_derajat] = [
                        this.item_derajat['id']
                        ,this.item_derajat['name']
                        ,this.item_derajat['min']
                        ,this.item_derajat['max']
                      ];
                      $.vueTable.changed_arr_derajat(currData);
                      this.hide();
                      $.vueTable.reload_table();
                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        
    })( jQuery );
    $(document).ready(function(){
      $.vueTable.init();
      $.vueModal.init();
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
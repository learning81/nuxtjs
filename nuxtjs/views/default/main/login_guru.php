<!DOCTYPE HTML>
<!--
  Projection by TEMPLATED
  templated.co @templatedco
  Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
  <head>
    <title>Generic - Projection by TEMPLATED</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo config_item('asset_url');?>prototype/projection/assets/css/main.css?creq=4b5f590443796801121a196b17222b" />

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo config_item('asset_url');?>master/standard.css" />
    <link rel="stylesheet" href="<?php echo config_item('asset_url');?>uikit-3.5.4/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo config_item('asset_url');?>prototype/uikitsite/css/theme.css" />

    <!-- UIkit JS -->
    <script src="<?php echo config_item('asset_url');?>uikit-3.5.4/js/uikit.min.js"></script>
    <script src="<?php echo config_item('asset_url');?>uikit-3.5.4/js/uikit-icons.min.js"></script>

  </head>
  <body class="subpage">

    <!-- Header -->
      <header id="header">
        <div class="inner">
          <a href="<?php echo config_item('global_instMainUrl');?>" class="logo"><strong>Sokola</strong> by Company Ltd.</a>
          <nav id="nav">
            <a href="<?php echo config_item('global_instMainUrl');?>index">Home</a>
            <a href="<?php echo config_item('global_instMainUrl');?>generic">Generic</a>
            <a href="<?php echo config_item('global_instMainUrl');?>elements">Elements</a>
          </nav>
          <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
        </div>
      </header>

    <!-- Three -->
      <section id="three" class="wrapper">
        <div class="inner">
          <header class="align-center">
            <h2>Login</h2>
            <p>Area Guru</p>
          </header>

          <div class="uk-card uk-card-default col-prfl-md-offset-3 col-prfl-lg-offset-3 side6">
              <div class="uk-card-header">
                  <div class="uk-grid-small uk-flex-middle" uk-grid>
                      <div class="uk-width-auto">
                          <img class="uk-border-circle" width="40" height="40" src="<?php echo config_item('asset_url');?>prototype/projection/images/pic01.jpg?creq=4b5f590443796801121a196b17222b">
                      </div>
                      <div class="uk-width-expand">
                          <h3 class="uk-card-title uk-margin-remove-bottom">Selamat Datang</h3>
                          <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00"><?php echo date('d M Y H:i:s');?></time></p>
                      </div>
                  </div>
              </div>
              <div class="uk-card-body">
                  <div class="row uniform">
                    <div class="12u$">
                      <input type="text" name="demo-name" id="demo-name" value="" placeholder="Username" />
                    </div>
                    <!-- Break -->
                    <div class="12u$">
                      <input type="password" name="demo-password" id="demo-password" value="" placeholder="Password" />
                    </div>
                    <!-- Break -->
                    <div class="12u$">
                      <input type="radio" id="demo-priority-low" name="demo-priority" checked>
                      <label for="demo-priority-low">remember</label>
                    </div>
                    <!-- Break -->
                    <div class="12u$">
                      <ul class="actions">
                        <li><input type="submit" value="Submit" /></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="uk-card-footer">
                  <a href="#" class="uk-button uk-button-text">Read more</a>
              </div>
          </div>

        </div>
      </section>

    <!-- Footer -->
      <footer id="footer">
        <div class="inner">

          <h3>Get in touch</h3>

          <form action="#" method="post">

            <div class="field half first">
              <label for="name">Name</label>
              <input name="name" id="name" type="text" placeholder="Name">
            </div>
            <div class="field half">
              <label for="email">Email</label>
              <input name="email" id="email" type="email" placeholder="Email">
            </div>
            <div class="field">
              <label for="message">Message</label>
              <textarea name="message" id="message" rows="6" placeholder="Message"></textarea>
            </div>
            <ul class="actions">
              <li><input value="Submit" class="button alt" type="submit"></li>
            </ul>
          </form>

          <div class="copyright">
            &copy; Untitled. Design: <a href="https://templated.co">TEMPLATED</a>. Images: <a href="https://unsplash.com">Unsplash</a>.
          </div>

        </div>
      </footer>

    <!-- Scripts -->
      <script src="<?php echo config_item('asset_url');?>prototype/projection/assets/js/jquery.min.js?creq=4b5f590443796801121a196b17222b"></script>
      <script src="<?php echo config_item('asset_url');?>prototype/projection/assets/js/skel.min.js?creq=4b5f590443796801121a196b17222b"></script>
      <script src="<?php echo config_item('asset_url');?>prototype/projection/assets/js/util.js?creq=4b5f590443796801121a196b17222b"></script>
      <script src="<?php echo config_item('asset_url');?>prototype/projection/assets/js/main.js?creq=4b5f590443796801121a196b17222b"></script>

  </body>
</html>
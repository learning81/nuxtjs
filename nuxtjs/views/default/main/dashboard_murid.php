<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Dashboard Murid</div>
    <div class="grid lg:grid-cols-3 md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
      <a href="<?php echo config_item('global_instMainUrl');?>murid_profile">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Profile</div>
            <div class="text-sm italic">Info Detail Murid</div>
          </div>
        </div>
      </a>
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
        <div class="ml-3 flex flex-row flex-grow justify-between">
          <div>
            <a href="<?php echo config_item('global_instMainUrl');?>murid_profile">
              <div>Sekolah</div>
              <div class="text-sm italic">Sekolah Bina Bangsa</div>
            </a>
          </div>
          <div class="flex flex-row mt-2 mb-4">
            <a href="<?php echo config_item('global_instMainUrl');?>murid_sekolah">
              <svg class="w-5 h-5 fill-current text-blue-700" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M80 368H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm0-320H16A16 16 0 0 0 0 64v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16V64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm416 176H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z"/></svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box_sekolah flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Daftar Pelajaran</div>
    <div class="grid lg:grid-cols-3 md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
      <a href="<?php echo config_item('global_instMainUrl');?>murid_pelajaran">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Pelajaran : Matematika</div>
            <div class="text-sm italic">kelas : I A</div>
          </div>
        </div>
      </a>
      <a href="<?php echo config_item('global_instMainUrl');?>murid_pelajaran">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Pelajaran : Biologi</div>
            <div class="text-sm italic">kelas : I A</div>
          </div>
        </div>
      </a>
    </div>
  </div>
<?php $this->load->view('default/common/current_footer');?>

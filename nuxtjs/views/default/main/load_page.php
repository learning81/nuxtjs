        <?php echo (!empty($use_sidebar)?'-->':'');?><div id='main_center' class='col-prfl-12'>
            <?php if(!empty($page_detail)):?>
            <?php $judul = '<div class="alert alert-danger" role="alert">'.strtoupper($page_detail->pp_title).'</div>';?>
            <div class='page'>
                <div class='page_title text-center'><?php echo $judul;?></div>
                <div class='page_date fontsize-75 text-right'><?php echo $page_detail->pp_postdate;?></div>
                <div class='page_content'><?php echo $page_detail->pp_detail;?></div>
            </div>
            <?php endif;?>

            <script type="text/javascript">
                $(document).ready(function(){
                    $.setToggle('page_detail','<?php echo strtoupper($page_detail->pp_title);?>',null,false);
                })
            </script>    
            <style type="text/css">
            </style>
        </div><?php echo (!empty($close_sidebar)?'<!--':'');?>

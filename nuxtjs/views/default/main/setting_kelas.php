<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Setting Kelas</div>
    <div class="grid grid-cols-1 w-full gap-4 p-4">
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Daftar Kelas</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info grid grid-flow-col mt-5">
            <div class="w-full overflow-x-scroll">
              <div id="datalist" title="Daftar Kelas"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="outer bg-gray-100" v-show="show_modal" id="modal_sekolah" style="display: table; position: fixed; height: 100%; width: 100%; left: 0; top: 0; background-color: rgba(0,0,0,0.5)">
        <div class="middle" style="display: table-cell; vertical-align: middle; ">
               <div class="inner lg:w-1/3 w-full container bg-white p-5 rounded-md" style="margin: 0 auto;">

                 <div class="flex lg:flex-row flex-col p-3 rounded-md">
                    <div class="ml-3 flex flex-1 flex-grow flex-col">
                     <div class="box_info">
                       <div class="flex flex-col flex-grow">
                         <label class="">Kelas</label>
                         <input class="mb-3 border-2 border-solid border-gray-300" type="" name="" v-model="item_kelas.kelas">
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="">Mulai</label>
                         <input class="mb-3 border-2 border-solid border-gray-300" type="date" name="" v-model="item_kelas.start">
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="">Selesai</label>
                         <input class="mb-3 border-2 border-solid border-gray-300" type="date" name="" v-model="item_kelas.end">
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="">Level</label>
                         <select class="mb-3 border-2 border-solid border-gray-300" type="" name="" v-model="item_kelas.level">
                           <option value=1>I</option>
                           <option value=2>II</option>
                           <option value=3>III</option>
                           <option value=4>IIII</option>
                           <option value=5>V</option>
                           <option value=6>VI</option>
                           <option value=7>VII</option>
                           <option value=8>VIII</option>
                           <option value=9>VIIII</option>
                           <option value=10>X</option>
                           <option value=11>XI</option>
                           <option value=12>XII</option>
                         </select>
                       </div>
                       <div class="flex flex-col flex-grow">
                         <label class="">Wali Kelas</label>
                         <select class="mb-3 border-2 border-solid border-gray-300" type="" name="" v-model="item_kelas.wali">
                           <option v-for="(item,i) in lst_guru" v-bind:value="i">{{item}}</option>
                         </select>
                       </div>
                       <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
                         <div class="cursor-pointer" v-on:click="submit">Submit</div>
                       </div>
                     </div>
                    </div>
                 </div>

              </div>
        </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {
              "idx_kelas":[]
              ,"change_idx_kelas":function(idx_kelas){
                $.vueTable.idx_kelas=idx_kelas;
                $.vueModal.idx_kelas=idx_kelas;
              }
              ,"item_kelas":[]
              ,"change_item_kelas":function(item_kelas){
                $.vueTable.item_kelas=JSON.parse(JSON.stringify(item_kelas));
                $.vueModal.item_kelas=JSON.parse(JSON.stringify(item_kelas));
              }
              ,"lst_guru":{
                1:'Sri Mulyani'
                ,2:'Boediono'
                ,3:'Yusuf Kalla'
              }
            }
        });
        $.extend({vueTable:new Vue({el:'#datalist'
                  ,data:{
                    "message":"i'm new"
                    ,"idx_kelas":""
                    ,"item_kelas":[]
                    ,"arr_kelas":[
                      ['1','1A','2020-08-30','2020-08-30','1',1]
                      ,['2','1B','2020-08-30','2020-08-30','1',2]
                      ,['3','1C','2020-08-30','2020-08-30','1',3]
                    ]
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      this.reattachtable();
                    }
                    ,change_arr_kelas: function(arr_kelas){
                      this.arr_kelas=JSON.parse(JSON.stringify(arr_kelas));
                      this.reload_table();
                    }
                    ,reload_table: function(){
                      $.jsObj.table1.option.jsonData=JSON.parse(JSON.stringify(this.arr_kelas));
                      $.jsObj.table1.option.countData=$.jsObj.table1.option.jsonData.length;
                      $.jsObj.table1.rebuildGrid();
                    }
                    ,reattachtable: function(data){
                      $('#datalist').simasgrid({
                            "field":[
                                  {id:"item_id",text:'No',type:'string',width:50,'align':'center'}, //
                                  {id:"item_kelas",text:'Kelas',type:'string',width:50,'align':'center'},
                                  {id:"item_start",text:'Mulai',type:'string',width:50,'align':'center'},
                                  {id:"item_end",text:'Selesai',type:'string',width:50,'align':'center'},
                                  {id:"item_level",text:'Level',type:'string',width:50,'align':'center'},
                                  {id:"item_wali",text:'Wali Kelas',type:'string',width:50,'align':'center'},
                                  {id:"control",text:'Kontrol',type:'string',width:50,'align':'center','addEvent':false}
                            ]
                            ,"width":'100%'
                            ,"height":'400px'
                            ,"checkBoxAdded":false
                            ,"useSearch":false
                            // ,"jsonUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&field=item_id,tr_code,tr_name,tr_group,"" item_plan,"" item_type,tr_std_rate,tr_overtime,tr_disable,"" control'
                            // ,"countUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&flimit=false&return=count'
                            ,"jsonData":JSON.parse(JSON.stringify($.vueTable.arr_kelas))
                            ,"countData":$.vueTable.arr_kelas.length
                            ,"postData":{'ordid':'item_id','ordtype':'asc'}
                            ,"onDataLoad":function(tr,data,tdStorage,storageTd){
                                  $(tr).find('td').eq(storageTd[5]).html(data[5]?$.jsObj.lst_guru[data[5]]:'');
                                  var btnActive = '<div class="grid grid-flow-row lg:grid-flow-col gap-2 text-sm">\
                                                    <div class="btn-edit border-solid border-2 border-yellow-700 rounded-full py-1 text-center">Edit</div>\
                                                    <div class="btn-hapus border-solid border-2 border-red-700 rounded-full py-1 text-center">Hapus</div>\
                                                  </div>';
                                  $(tr).find('td').eq(storageTd[6]).html(btnActive);
                            }
                            ,"callback":function(tr,e,gridObj){
                                  var currId = $(tr).attr('id');
                                  var currData = $.jsObj.table1.dataStorage[currId];
                                  var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                                  var rawData = $.jsObj.table1.rawData[currIdx];

                                  var currData = {
                                    'id':rawData[0]
                                    ,'kelas':rawData[1]
                                    ,'start':rawData[2]
                                    ,'end':rawData[3]
                                    ,'level':rawData[4]
                                    ,'wali':rawData[5]
                                  }
                                  $.jsObj.change_idx_kelas(currIdx);
                                  $.jsObj.change_item_kelas(currData);

                                  $.vueModal.show();
                            }

                        }
                      );
                      $('#datalist').simasgrid('addNav',{
                        title:'Reload',
                        className:'navigation',
                        callback:function(simasGrid){
                              $.vueTable.reload_table();
                        }
                      }); 
                      $.jsObj.table1 = $('#datalist').simasgrid('getObj');
                      $($.jsObj.table1.gridObj).simasgrid('addNav',{
                        title:'Tambah',
                        className:'navigation',
                        callback:function(simasGrid){
                          $.jsObj.change_idx_kelas("");
                          $.jsObj.change_item_kelas({});

                          $.vueModal.show();
                        }
                      }); 

                      // dom simasgrid yg bisa diakses hanya gridObj
                      $($.jsObj.table1.gridObj).on('click','.btn-edit',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        var rawData = $.jsObj.table1.rawData[currIdx];

                        var currData = {
                          'id':rawData[0]
                          ,'kelas':rawData[1]
                          ,'start':rawData[2]
                          ,'end':rawData[3]
                          ,'level':rawData[4]
                          ,'wali':rawData[5]
                        }
                        $.jsObj.change_idx_kelas(currIdx);
                        $.jsObj.change_item_kelas(currData);

                        $.vueModal.show();
                      })
                      $($.jsObj.table1.gridObj).on('click','.btn-hapus',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        
                        var currData = JSON.parse(JSON.stringify($.vueTable.arr_kelas));
                        currData.splice(currIdx,1);
                        $.vueTable.change_arr_kelas(currData);
                      })

                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        $.extend({vueModal:new Vue({el:'#modal_sekolah'
                  ,data:{
                    "message":"i'm new"
                    ,"show_modal":false
                    ,"item_kelas":{}
                    ,"lst_guru":{}
                  }
                  ,created: function(){
                    console.log(this.message);
                    this.lst_guru=JSON.parse(JSON.stringify($.jsObj.lst_guru));
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      $(this.$el).on("click", function (e) {
                            if ($(e.target).closest(".inner").length === 0) {
                                   $.vueModal.hide();
                            }
                      });
                    }
                    ,show: function(data){
                      this.show_modal=true;
                    }
                    ,hide: function(data){
                      this.show_modal=false;
                    }
                    ,submit: function(data){
                      $.jsObj.change_item_kelas(this.item_kelas);
                      var currentData = JSON.parse(JSON.stringify($.vueTable.arr_kelas));

                      if(this.idx_kelas != ""){
                        currentData[this.idx_kelas] = [
                          this.item_kelas['id']
                          ,this.item_kelas['kelas']
                          ,this.item_kelas['start']
                          ,this.item_kelas['end']
                          ,this.item_kelas['level']
                          ,this.item_kelas['wali']
                        ];
                      }else{
                        var allTr = $($.jsObj.table1.gridObj).find('table#contentTable').find('tr');
                        var lastId = $(allTr[(allTr.length-1)]).attr('id');

                        if(allTr.length>0){
                          var currData = $.jsObj.table1.dataStorage[lastId];
                          var currId = parseInt(currData[0])+1;
                        }else{
                          var currId = 1;
                        }

                        currentData.push([
                          (currId)
                          ,this.item_kelas['kelas']
                          ,this.item_kelas['start']
                          ,this.item_kelas['end']
                          ,this.item_kelas['level']
                          ,this.item_kelas['wali']
                        ]);

                      }

                      $.vueTable.change_arr_kelas(currentData);
                      this.hide();
                      $.vueTable.reload_table();
                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        
    })( jQuery );
    $(document).ready(function(){
      $.vueTable.init();
      $.vueModal.init();
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
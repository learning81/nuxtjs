<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Setting Kelas</div>
    <div class="grid grid-cols-1 w-full gap-4 p-4">
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Daftar Kelas</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info grid grid-flow-col mt-5">
            <div class="w-full overflow-x-scroll">
              <div id="datalist" title="Daftar Kelas"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {
              "idx_kelas":[]
              ,"change_idx_kelas":function(idx_kelas){
                $.vueTable.idx_kelas=idx_kelas;
                $.vueModal.idx_kelas=idx_kelas;
              }
              ,"item_kelas":[]
              ,"change_item_kelas":function(item_kelas){
                $.vueTable.item_kelas=JSON.parse(JSON.stringify(item_kelas));
                $.vueModal.item_kelas=JSON.parse(JSON.stringify(item_kelas));
              }
              ,"lst_guru":{
                1:'Sri Mulyani'
                ,2:'Boediono'
                ,3:'Yusuf Kalla'
              }
            }
        });
        $.extend({vueTable:new Vue({el:'#datalist'
                  ,data:{
                    "message":"i'm new"
                    ,"idx_kelas":""
                    ,"item_kelas":[]
                    ,"arr_kelas":[
                      ['1','1A','2020-08-30','2020-08-30','1',1]
                      ,['2','1B','2020-08-30','2020-08-30','1',2]
                      ,['3','1C','2020-08-30','2020-08-30','1',3]
                    ]
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      this.reattachtable();
                    }
                    ,change_arr_kelas: function(arr_kelas){
                      this.arr_kelas=JSON.parse(JSON.stringify(arr_kelas));
                      this.reload_table();
                    }
                    ,reload_table: function(){
                      $.jsObj.table1.option.jsonData=JSON.parse(JSON.stringify(this.arr_kelas));
                      $.jsObj.table1.option.countData=$.jsObj.table1.option.jsonData.length;
                      $.jsObj.table1.rebuildGrid();
                    }
                    ,reattachtable: function(data){
                      $('#datalist').simasgrid({
                            "field":[
                                  {id:"item_id",text:'No',type:'string',width:50,'align':'center'}, //
                                  {id:"item_kelas",text:'Kelas',type:'string',width:50,'align':'center'},
                                  {id:"item_start",text:'Mulai',type:'string',width:50,'align':'center'},
                                  {id:"item_end",text:'Selesai',type:'string',width:50,'align':'center'},
                                  {id:"item_level",text:'Level',type:'string',width:50,'align':'center'},
                                  {id:"item_wali",text:'Wali Kelas',type:'string',width:50,'align':'center'},
                                  {id:"control",text:'Kontrol',type:'string',width:50,'align':'center','addEvent':false}
                            ]
                            ,"width":'100%'
                            ,"height":'400px'
                            ,"checkBoxAdded":false
                            ,"useSearch":false
                            // ,"jsonUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&field=item_id,tr_code,tr_name,tr_group,"" item_plan,"" item_type,tr_std_rate,tr_overtime,tr_disable,"" control'
                            // ,"countUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&flimit=false&return=count'
                            ,"jsonData":JSON.parse(JSON.stringify($.vueTable.arr_kelas))
                            ,"countData":$.vueTable.arr_kelas.length
                            ,"postData":{'ordid':'item_id','ordtype':'asc'}
                            ,"onDataLoad":function(tr,data,tdStorage,storageTd){
                                  $(tr).find('td').eq(storageTd[5]).html(data[5]?$.jsObj.lst_guru[data[5]]:'');
                                  var btnActive = '<div class="grid grid-flow-row lg:grid-flow-col gap-2 text-sm">\
                                                    <div class="btn-daftar border-solid border-2 border-yellow-700 rounded-full py-1 text-center">Daftar</div>\
                                                  </div>';
                                  $(tr).find('td').eq(storageTd[6]).html(btnActive);
                            }
                            ,"callback":function(tr,e,gridObj){
                                  var currId = $(tr).attr('id');
                                  var currData = $.jsObj.table1.dataStorage[currId];
                                  var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                                  var rawData = $.jsObj.table1.rawData[currIdx];

                                  alert('anda telah terdaftar, silahkan menunggu approve-an guru');
                            }

                        }
                      );
                      $('#datalist').simasgrid('addNav',{
                        title:'Reload',
                        className:'navigation',
                        callback:function(simasGrid){
                              $.vueTable.reload_table();
                        }
                      }); 
                      $.jsObj.table1 = $('#datalist').simasgrid('getObj');

                      // dom simasgrid yg bisa diakses hanya gridObj
                      $($.jsObj.table1.gridObj).on('click','.btn-daftar',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        var rawData = $.jsObj.table1.rawData[currIdx];

                        alert('anda telah terdaftar, silahkan menunggu approve-an guru');
                      })
                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
    })( jQuery );
    $(document).ready(function(){
      $.vueTable.init();
      $.vueModal.init();
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
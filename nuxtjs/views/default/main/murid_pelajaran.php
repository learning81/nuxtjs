<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center mb-8">
    <div class="bg-gray-200 w-2/3 mt-10 flex flex-row py-3 px-3">
      Kelas I A - Matematika
    </div>
    <div class="bg-gray-200 w-2/3 mt-10 flex flex-row">
      <div class="box_leftbar bg-pink-500 text-white flex-grow">
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan</div>
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan</div>
        <div class="pt-3 px-10 border-solid border-gray-200 border-b">Pertemuan</div>
      </div>
      <div class="box_content flex flex-col flex-grow px-10 py-3">
        <div class="grid grid-cols-1 gap-3 mb-4">
          <label>Title</label>
          <div class="w-full p-3 bg-gray-100">Pendahuluan</div>
        </div>
        <div class="grid grid-col mb-4">
          <label>Deskripsi</label>
          <div class="w-full p-3 bg-gray-100">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
        </div>
        <div class="grid grid-col mb-4">
          <label>Video</label>
          <div class="w-full p-3 bg-gray-100">http://www.youtube.com/channel=abc</div>
        </div>
        <div class="grid grid-col mb-4 w-full">
          <img class="object-cover w-full" src="<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/youtube.png">
        </div>
        <div>
          <div class="bg-pink-500 py-3 px-5 text-center text-white rounded-md cursor-pointer">Next</div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('default/common/current_footer');?>

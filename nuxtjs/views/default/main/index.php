<?php $this->load->view('default/common/current_header');?>
    <div class="box_banner flex flex-col items-start justify-end w-screen h-screen bg-gray-600 bg-cover bg-center text-white p-10" style="background-image: url('<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/image.jpg?width=600&height=600');">
      <h1 class="py-2 m-0 text-white font-sans">INTRANET dan EXTRANET</h1>
      <h2 class="py-2 m-0 text-white font-sans">TITLE 2</h2>
      <div class="py-2">
        Mencari Sekolah yang terdaftar
        <br />di SOKOLA.id
      </div>
      <div class="relative">
        <input class="px-2 rounded-sm text-gray-700" type="" name="">
        <svg class="h-4 w-4 fill-current text-black float-right absolute right-0 top-0 mt-1 mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/></svg>
      </div>
    </div>
    <div class="box_banner flex flex-col items-center justify-start w-screen h-screen bg-gray-200 bg-cover bg-center p-10">
      <h1 class="text-md font-bold font-sans">Daftar Sekolah SOKOLA.ID</h1>
      <div class="grid grid-cols-2 lg:grid-cols-6 gap-10">
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <h2 class=" font-bold text-base">SMUN 1 Tangerang</h2>
          <div>Jl. Tangerang Raya</div>
        </div>
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <h2 class=" font-bold text-base">SMUN 2 Tangerang</h2>
          <div>Jl. Tangerang Raya</div>
        </div>
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <h2 class=" font-bold text-base">SMUN 3 Tangerang</h2>
          <div>Jl. Tangerang Raya</div>
        </div>
      </div>
    </div>
    <div class="box_banner flex flex-col items-center justify-start w-screen h-screen bg-white bg-cover bg-center p-10">
      <h1 class="text-md font-bold font-sans">Daftar Guru SOKOLA.ID</h1>
      <div class="grid grid-cols-2 lg:grid-cols-6 gap-10">
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <img class="w-32 h-32 rounded-full" src="<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/image.jpg?width=600&height=600" />
          <h2 class=" font-bold text-base">Pak Arman</h2>
          <div>Matematika</div>
        </div>
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <img class="w-32 h-32 rounded-full" src="<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/image.jpg?width=600&height=600" />
          <h2 class=" font-bold text-base">Pak Marzuki</h2>
          <div>Fisika</div>
        </div>
        <div class="border-solid border-2 border-white rounded-md bg-white p-4 shadow">
          <img class="w-32 h-32 rounded-full" src="<?php echo config_item('global_instAssetsUrl');?>load_thumbnail/image.jpg?width=600&height=600" />
          <h2 class=" font-bold text-base">Bu Wati</h2>
          <div>Biologi</div>
        </div>
      </div>
    </div>
<?php $this->load->view('default/common/current_footer');?>

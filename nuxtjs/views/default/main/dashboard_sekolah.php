<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Dashboard Sekolah</div>
    <div class="grid lg:grid-cols-3 md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
      <a href="<?php echo config_item('global_instMainUrl');?>setting_sekolah">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Setting</div>
            <div class="text-sm italic">Info Detail Sekolah</div>
          </div>
        </div>
      </a>
      <a href="<?php echo config_item('global_instMainUrl');?>setting_guru">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Guru</div>
            <div class="text-sm italic">Daftar Guru</div>
          </div>
        </div>
      </a>
      <a href="<?php echo config_item('global_instMainUrl');?>setting_kelas">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Kelas</div>
            <div class="text-sm italic">Daftar Kelas</div>
          </div>
        </div>
      </a>
      <a href="<?php echo config_item('global_instMainUrl');?>setting_pelajaran">
        <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
          <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
          <div class="ml-3">
            <div>Pelajaran</div>
            <div class="text-sm italic">Daftar Pelajaran</div>
          </div>
        </div>
      </a>
    </div>
  </div>
<?php $this->load->view('default/common/current_footer');?>

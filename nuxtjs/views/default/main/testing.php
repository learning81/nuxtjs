<!DOCTYPE html>
<html>
<head>
  <title>Belajar Sesuatu</title>
  <script type="text/javascript" src="<?php echo config_item('asset_url');?>vue.js"></script>    
  <script></script>
</head>
<body>
  <div id="app">
    <transition
     name="custom-classes-transition"
     enter-active-class=""
     leave-active-class=""
     >
     <div class="image" style="width: 100px; height: 150px;">
       &nbsp;
     </div> 
    </transition>
  </div>
</body>
</html> 
<script type="text/javascript">
  (function($){
      $.extend({
          newfunc: function (){}
      });
      $.extend({vueElemen:new Vue({el:'#app'
                ,data:{
                  "message":"i'm new"
                }
                ,created: function(){
                  console.log(this.message);
                }
                ,methods:{ // method tidak bisa passing variable di fungsi
                  func: function(data){
                    //tidak bisa sebagai variable karena akan return function(){}
                    //benar: v-for="(item,i) in func()"
                    //salah: v-model="func()""
                    this.message = data;
                    console.log(this.message)
                  }
                  ,fired (event, content, index) {
                    console.log('Click event on DOM element', e.target)
                    console.log('Click event on content index', index)
                    console.log('Click event on content', content)
                  }
                }
                ,computed:{
                  // method tidak bisa passing variable di fungsi
                  // caching sampai elemen didalamnya ada perubahan
                  // fullName computed lebih baik dari pada watcher firstname dan lastname
                  getMessage: function(){
                    return this.message;
                  }
                }
                ,watch:{
                  message: function(newVal,oldVal){
                    alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                  }        
                }
                ,updated: function () {
                  this.$nextTick(function () {
                    // Code that will run only after the
                    // entire view has been re-rendered
                  })
                }            
          })
      });
      
  })( jQuery );
</script>
<style type="text/css">
</style>
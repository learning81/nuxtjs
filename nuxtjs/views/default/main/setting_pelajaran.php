<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Setting Kelas</div>
    <div class="grid grid-cols-1 w-full gap-4 p-4">
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Daftar Kelas</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info grid grid-flow-col mt-5">
            <div class="w-full overflow-x-scroll">
              <div id="datalist" title="Daftar Kelas"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="outer bg-gray-100" v-show="show_modal" id="modal_sekolah" style="display: table; position: fixed; height: 100%; width: 100%; left: 0; top: 0; background-color: rgba(0,0,0,0.5)">
        <div class="middle" style="display: table-cell; vertical-align: middle; ">
               <div class="inner lg:w-1/3 w-full container bg-white p-5 rounded-md" style="margin: 0 auto;">

                 <div class="flex lg:flex-row flex-col p-3 rounded-md">
                    <div class="ml-3 flex flex-1 flex-grow flex-col">
                     <div class="box_info">
                       <div class="flex flex-col flex-grow">
                         <label class="">Pelajaran</label>
                         <input class="mb-3 border-2 border-solid border-gray-300" type="" name="" v-model="item_pelajaran.nama">
                       </div>
                       <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
                         <div class="cursor-pointer" v-on:click="submit">Submit</div>
                       </div>
                     </div>
                    </div>
                 </div>

              </div>
        </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {
              "idx_pelajaran":[]
              ,"change_idx_pelajaran":function(idx_pelajaran){
                $.vueTable.idx_pelajaran=idx_pelajaran;
                $.vueModal.idx_pelajaran=idx_pelajaran;
              }
              ,"item_pelajaran":[]
              ,"change_item_pelajaran":function(item_pelajaran){
                $.vueTable.item_pelajaran=JSON.parse(JSON.stringify(item_pelajaran));
                $.vueModal.item_pelajaran=JSON.parse(JSON.stringify(item_pelajaran));
              }
            }
        });
        $.extend({vueTable:new Vue({el:'#datalist'
                  ,data:{
                    "message":"i'm new"
                    ,"idx_pelajaran":""
                    ,"item_pelajaran":[]
                    ,"arr_pelajaran":[
                      ['1','matematika']
                      ,['2','biologi']
                      ,['3','sejarah']
                    ]
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      this.reattachtable();
                    }
                    ,change_arr_pelajaran: function(arr_pelajaran){
                      this.arr_pelajaran=JSON.parse(JSON.stringify(arr_pelajaran));
                      this.reload_table();
                    }
                    ,reload_table: function(){
                      $.jsObj.table1.option.jsonData=JSON.parse(JSON.stringify(this.arr_pelajaran));
                      $.jsObj.table1.option.countData=$.jsObj.table1.option.jsonData.length;
                      $.jsObj.table1.rebuildGrid();
                    }
                    ,reattachtable: function(data){
                      $('#datalist').simasgrid({
                            "field":[
                                  {id:"item_id",text:'No',type:'string',width:50,'align':'center'}, //
                                  {id:"item_nama",text:'Pelajaran',type:'string',width:250,'align':'center'},
                                  {id:"control",text:'Kontrol',type:'string',width:50,'align':'center','addEvent':false}
                            ]
                            ,"width":'100%'
                            ,"height":'400px'
                            ,"checkBoxAdded":false
                            ,"useSearch":false
                            // ,"jsonUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&field=item_id,tr_code,tr_name,tr_group,"" item_plan,"" item_type,tr_std_rate,tr_overtime,tr_disable,"" control'
                            // ,"countUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&flimit=false&return=count'
                            ,"jsonData":JSON.parse(JSON.stringify($.vueTable.arr_pelajaran))
                            ,"countData":$.vueTable.arr_pelajaran.length
                            ,"postData":{'ordid':'item_id','ordtype':'asc'}
                            ,"onDataLoad":function(tr,data,tdStorage,storageTd){
                                  var btnActive = '<div class="grid grid-flow-row lg:grid-flow-col gap-2 text-sm">\
                                                    <div class="btn-edit border-solid border-2 border-yellow-700 rounded-full py-1 text-center">Edit</div>\
                                                    <div class="btn-hapus border-solid border-2 border-red-700 rounded-full py-1 text-center">Hapus</div>\
                                                  </div>';
                                  $(tr).find('td').eq(storageTd[2]).html(btnActive);
                            }
                            ,"callback":function(tr,e,gridObj){
                                  var currId = $(tr).attr('id');
                                  var currData = $.jsObj.table1.dataStorage[currId];
                                  var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                                  var rawData = $.jsObj.table1.rawData[currIdx];

                                  var currData = {
                                    'id':rawData[0]
                                    ,'nama':rawData[1]
                                  }
                                  $.jsObj.change_idx_pelajaran(currIdx);
                                  $.jsObj.change_item_pelajaran(currData);

                                  $.vueModal.show();
                            }

                        }
                      );
                      $('#datalist').simasgrid('addNav',{
                        title:'Reload',
                        className:'navigation',
                        callback:function(simasGrid){
                              $.vueTable.reload_table();
                        }
                      }); 
                      $.jsObj.table1 = $('#datalist').simasgrid('getObj');
                      $($.jsObj.table1.gridObj).simasgrid('addNav',{
                        title:'Tambah',
                        className:'navigation',
                        callback:function(simasGrid){
                          $.jsObj.change_idx_pelajaran("");
                          $.jsObj.change_item_pelajaran({});

                          $.vueModal.show();
                        }
                      }); 

                      // dom simasgrid yg bisa diakses hanya gridObj
                      $($.jsObj.table1.gridObj).on('click','.btn-edit',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        var rawData = $.jsObj.table1.rawData[currIdx];

                        var currData = {
                          'id':rawData[0]
                          ,'nama':rawData[1]
                        }
                        $.jsObj.change_idx_pelajaran(currIdx);
                        $.jsObj.change_item_pelajaran(currData);

                        $.vueModal.show();
                      })
                      $($.jsObj.table1.gridObj).on('click','.btn-hapus',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        
                        var currData = JSON.parse(JSON.stringify($.vueTable.arr_pelajaran));
                        currData.splice(currIdx,1);
                        $.vueTable.change_arr_pelajaran(currData);
                      })

                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        $.extend({vueModal:new Vue({el:'#modal_sekolah'
                  ,data:{
                    "message":"i'm new"
                    ,"show_modal":false
                    ,"item_pelajaran":{}
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      $(this.$el).on("click", function (e) {
                            if ($(e.target).closest(".inner").length === 0) {
                                   $.vueModal.hide();
                            }
                      });
                    }
                    ,show: function(data){
                      this.show_modal=true;
                    }
                    ,hide: function(data){
                      this.show_modal=false;
                    }
                    ,submit: function(data){
                      $.jsObj.change_item_pelajaran(this.item_pelajaran);
                      var currentData = JSON.parse(JSON.stringify($.vueTable.arr_pelajaran));

                      if(this.idx_pelajaran != ""){
                        currentData[this.idx_pelajaran] = [
                          this.item_pelajaran['id']
                          ,this.item_pelajaran['nama']
                        ];
                      }else{
                        var allTr = $($.jsObj.table1.gridObj).find('table#contentTable').find('tr');
                        var lastId = $(allTr[(allTr.length-1)]).attr('id');

                        if(allTr.length>0){
                          var currData = $.jsObj.table1.dataStorage[lastId];
                          var currId = parseInt(currData[0])+1;
                        }else{
                          var currId = 1;
                        }

                        currentData.push([
                          (currId)
                          ,this.item_pelajaran['nama']
                        ]);

                      }

                      $.vueTable.change_arr_pelajaran(currentData);
                      this.hide();
                      $.vueTable.reload_table();
                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        
    })( jQuery );
    $(document).ready(function(){
      $.vueTable.init();
      $.vueModal.init();
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
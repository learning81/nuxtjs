<?php $this->load->view('default/common/current_header');?>
    <div id="page2_register" class="box_banner flex flex-row w-screen min-h-screen bg-gray-200 bg-cover bg-center p-10">
      <div class="w-1/3">
        <h1 class="text-2xl font-bold font-sans">Registrasi</h1>
        <div class="grid grid-flow-row gap-4 opacity-75 rounded-md bg-gray-700 pt-5 px-10 pb-8" id="box_login">
          <div class="">
            <label class="w-full text-white">Email</label>
            <input class="w-full px-2" type="" name="" v-model="username">
          </div>
          <div class="">
            <label class="w-full text-white">Password</label>
            <input class="w-full px-2" type="" name="" v-model="password">
          </div>
          <div class="">
            <label class="w-full text-white">Alamat</label>
            <textarea class="w-full px-2" name=""></textarea>
          </div>
          <div class="py-2 cursor-pointer px-4 rounded-md text-center bg-pink-600">
            <div class="text-white" v-on:click="register">Register</div>
          </div>
        </div>
        <div class="text-red-100 p-10 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi</div>
      </div>
      <div class="w-2/3">
        <h1 class="text-2xl font-bold font-sans">Keterangan</h1>
        <div class="grid grid-flow-row gap-4 opacity-75 rounded-md bg-gray-300 pt-5 px-10 pb-8 min-h-screen">
          Tes tes tes
        </div>
      </div>
    </div>
    <script type="text/javascript">
      (function($){
          $.extend({
              newfunc: function (){}
              ,jsObj: function (){}
          });
          $.extend({vueElemen:new Vue({el:'#page2_register'
                    ,data:{
                      "message":"i'm new"
                      ,"username":""
                      ,"password":""
                    }
                    ,created: function(){
                      console.log(this.message);
                    }
                    ,methods:{ // method tidak bisa passing variable di fungsi
                      func: function(data){
                        //tidak bisa sebagai variable karena akan return function(){}
                        //benar: v-for="(item,i) in func()"
                        //salah: v-model="func()""
                        this.message = data;
                        console.log(this.message)
                      }
                      ,register: function(data){
                        $.setAjax(global['instMainUrl']+'register','post',{'email':this.username},function(result){
                          alert(result.msg);
                        });
                      }
                      ,fired (event, content, index) {
                        console.log('Click event on DOM element', e.target)
                        console.log('Click event on content index', index)
                        console.log('Click event on content', content)
                      }
                    }
                    ,computed:{
                      // method tidak bisa passing variable di fungsi
                      // caching sampai elemen didalamnya ada perubahan
                      // fullName computed lebih baik dari pada watcher firstname dan lastname
                      getMessage: function(){
                        return this.message;
                      }
                    }
                    ,watch:{
                      message: function(newVal,oldVal){
                        alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                      }        
                    }
                    ,updated: function () {
                      this.$nextTick(function () {
                        // Code that will run only after the
                        // entire view has been re-rendered
                      })
                    }            
              })
          });
      })( jQuery );
    </script>
<?php $this->load->view('default/common/current_footer');?>

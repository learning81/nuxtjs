<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Profile</div>
    <div class="grid md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
      <div class="flex lg:flex-row flex-col border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer" id="box_detail">
        <div class="w-32 mb-8">
          <iframe id="iframe_logo" name="iframe_logo" height="0" width="100%" frameborder="0" scrolling="yes" v-on:load="updatelogo"></iframe>
          <img class="w-full h-32 rounded-full" v-bind:src="pathlogo">
          <form action="<?php echo config_item('global_instMainUrl');?>upload/" id="form_logo" method="post" enctype="multipart/form-data"  target="iframe_logo">
            <input type="file" name="upload_file" class="w-full text-xs text-white mt-4" v-on:change="uploadlogo">
            <input type="hidden" name="upload_path" value="" class="form-control" />
            <input type="hidden" name="upload_id" value="" class="form-control" />
            <input type="hidden" name="upload_name" value="guru" class="form-control" />
          </form>
        </div>
        <div class="ml-3 flex flex-1 flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Detail</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info mt-5">
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kode</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_guru.item_code">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Nama</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_guru.item_name">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">No Telp</label>
              <input class="border-2 border-solid border-gray-300" type="" name="" v-model="item_guru.item_telp">
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Alamat</label>
              <textarea class="border-2 border-solid border-gray-300" v-model="item_guru.item_addr"></textarea>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Provinsi</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_guru.item_prop">
                <option value=1>Provinsi 1</option>
                <option value=2>Provinsi 2</option>
                <option value=3>Provinsi 3</option>
                <option value=4>Provinsi 4</option>
                <option value=5>Provinsi 5</option>
              </select>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kota/Kabupaten</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_guru.item_kab">
                <option value=1>Kabupaten 1</option>
                <option value=2>Kabupaten 2</option>
                <option value=3>Kabupaten 3</option>
                <option value=4>Kabupaten 4</option>
                <option value=5>Kabupaten 5</option>
              </select>
            </div>
            <div class="flex flex-col flex-grow">
              <label class="mt-3">Kecamatan</label>
              <select class="border-2 border-solid border-gray-300" name="" v-model="item_guru.item_kec">
                <option value=1>Kecamatan 1</option>
                <option value=2>Kecamatan 2</option>
                <option value=3>Kecamatan 3</option>
                <option value=4>Kecamatan 4</option>
                <option value=5>Kecamatan 5</option>
              </select>
            </div>
            <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
              <div class="" v-on:click="submit">Submit</div>
            </div>
          </div>
        </div>
      </div>
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Informasi Tambahan</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info flex flex-col items-center p-4 bg-white text-gray-600">
            <div class="p-3 uppercase">Daftar Pelajaran</div>
            <div class="grid md:grid-cols-2 xs:grid-cols-1 w-full gap-4 p-4">
              <a href="<?php echo config_item('global_instMainUrl');?>murid_kelas">
                <div class="flex md:flex-col xs:flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
                  <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
                  <div class="ml-3">
                    <div>Pelajaran : Matematika</div>
                    <div class="text-sm italic">kelas : I A</div>
                  </div>
                </div>
              </a>
              <a href="<?php echo config_item('global_instMainUrl');?>murid_kelas">
                <div class="flex md:flex-col xs:flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
                  <img class="w-20 h-20 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
                  <div class="ml-3">
                    <div>Pelajaran : Biologi</div>
                    <div class="text-sm italic">kelas : I A</div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {
              "idx_derajat":[]
              ,"change_idx_derajat":function(idx_derajat){
                $.vueTable.idx_derajat=idx_derajat;
                $.vueModal.idx_derajat=idx_derajat;
              }
              ,"item_derajat":[]
              ,"change_item_derajat":function(item_derajat){
                $.vueTable.item_derajat=JSON.parse(JSON.stringify(item_derajat));
                $.vueModal.item_derajat=JSON.parse(JSON.stringify(item_derajat));
              }
            }
        });
        <?php 
        $url ="";
        if(file_exists(config_item('data_folder').'murid.jpg')){
          $url=config_item('global_instAssetsUrl').'load_thumbnail/murid.jpg';
        }else{
          $url="http://localhost/assets/load_image/logo-design-software.png";
        }
        ?>
        $.extend({vueDetail:new Vue({el:'#box_detail'
                  ,data:{
                    "message":"i'm new"
                    ,"item_guru":<?php echo json_encode($_SESSION['murid']);?>
                    ,"pathlogo":"<?php echo $url;?>?rand="+$.randomString(5,'aA#')
                    ,"urlsave":global['instMainUrl']+'json_murid/'
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,updatelogo: function(data){
                      var htmldata = $(this.$el).find("iframe#iframe_logo").contents().find("body").html();
                      if(htmldata){
                        var result = $.parse(htmldata);
                        if(result.state){
                          this.input_logo = result['data'][0]['filename'];
                          this.pathlogo = global['instAssetsUrl']+'load_thumbnail/murid.jpg?rand='+$.randomString(5,'aA#')
                        }else{
                          alert(result.msg);
                        }
                      }
                    }
                    ,uploadlogo: function(data){
                      $(this.$el).find('form#form_logo').submit();
                    }                      
                    ,submit: function(data){
                      $.setAjax(this.urlsave,'post',this.item_guru,function(result){
                        alert('data sudah diupdate');
                      });
                    }                      
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
    })( jQuery );
    $(document).ready(function(){
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
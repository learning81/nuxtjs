<?php $this->load->view('default/common/current_header');?>
  <div class="box_sekolah min-h-screen flex flex-col items-center p-10 bg-white text-gray-600">
    <div class="p-3 uppercase">Setting Guru</div>
    <div class="grid grid-cols-1 w-full gap-4 p-4">
      <div class="flex flex-row border-2 p-3 border-solid border-grey-900 rounded-md shadow-lg cursor-pointer">
        <div class="ml-3 flex flex-grow flex-col">
          <div class="border-b border-gray-500 border-solid">Daftar Guru</div>
          <div class="text-sm italic">Lorem ipsum dolor sit amet</div>
          <div class="box_info grid grid-flow-col mt-5">
            <div class="w-full overflow-x-scroll">
              <div id="datalist" title="Daftar Guru"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="outer bg-gray-100" v-show="show_modal" id="modal_sekolah" style="display: table; position: fixed; height: 100%; width: 100%; left: 0; top: 0; background-color: rgba(0,0,0,0.5)">
        <div class="middle" style="display: table-cell; vertical-align: middle; ">
               <div class="inner lg:w-2/3 w-full container bg-white p-5 rounded-md" style="margin: 0 auto;">

                 <div class="flex lg:flex-row flex-col p-3 rounded-md">
                    <div class="w-32 mb-8">
                      <img class="w-full h-32 rounded-full" src="http://localhost/assets/load_image/logo-design-software.png">
                    </div>
                    <div class="ml-3 flex flex-1 flex-grow flex-col">
                     <div class="border-b border-gray-500 border-solid">Abdul Rahman</div>
                     <div class="text-sm italic">001</div>
                     <div class="box_info mt-5">
                       <div class="flex flex-col flex-grow">
                         <label class="mt-3">Alamat</label>
                         <div class="p-3">
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                           tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                           quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                           consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                           cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                           proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                         </div>
                       </div>
                       <div class="text-center mt-4 rounded-md flex-shrink py-2 px-4 text-white bg-pink-500">
                         <div class="cursor-pointer" v-on:click="submit">Tambahkan</div>
                       </div>
                     </div>
                    </div>
                 </div>

              </div>
        </div>
  </div>
  <script type="text/javascript">
    (function($){
        $.extend({
            newfunc: function (){}
            ,jsObj: {}
        });
        $.extend({vueTable:new Vue({el:'#datalist'
                  ,data:{
                    "message":"i'm new"
                    ,"idx_derajat":""
                    ,"item_derajat":[]
                    ,"arr_derajat":[
                      ['1','Rizal Ramli','001','']
                      ,['2','Sri Mulyani','002','']
                      ,['3','Boediono','003','']
                    ]
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      this.reattachtable();
                    }
                    ,change_arr_derajat: function(arr_derajat){
                      this.arr_derajat=JSON.parse(JSON.stringify(arr_derajat));
                      this.reload_table();
                    }
                    ,reload_table: function(){
                      $.jsObj.table1.option.jsonData=JSON.parse(JSON.stringify(this.arr_derajat));
                      $.jsObj.table1.option.countData=$.jsObj.table1.option.jsonData.length;
                      $.jsObj.table1.rebuildGrid();
                    }
                    ,reattachtable: function(data){
                      $('#datalist').simasgrid({
                            "field":[
                                  {id:"item_id",text:'No',type:'string',width:50,'align':'center'}, //
                                  {id:"item_name",text:'Guru',type:'string',width:150,'align':'left'},
                                  {id:"item_min",text:'No Id',type:'string',width:50,'align':'center'},
                                  {id:"item_max",text:'Kelas',type:'string',width:100,'align':'center'},
                                  {id:"control",text:'Kontrol',type:'string',width:50,'align':'center','addEvent':false}
                            ]
                            ,"width":'100%'
                            ,"height":'400px'
                            ,"checkBoxAdded":false
                            ,"useSearch":false
                            // ,"jsonUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&field=item_id,tr_code,tr_name,tr_group,"" item_plan,"" item_type,tr_std_rate,tr_overtime,tr_disable,"" control'
                            // ,"countUrl":global['ResourceUrl']+'jsonrequest/?include_group=true&filter_tmms_resource_group%>trg_position=1&flimit=false&return=count'
                            ,"jsonData":JSON.parse(JSON.stringify($.vueTable.arr_derajat))
                            ,"countData":$.vueTable.arr_derajat.length
                            ,"postData":{'ordid':'item_id','ordtype':'asc'}
                            ,"onDataLoad":function(tr,data,tdStorage,storageTd){
                                  var btnActive = '<div class="grid grid-flow-row gap-2 text-sm">\
                                                    <div class="btn-unlink border-solid border-2 border-red-700 rounded-full py-1 text-center">Unlink</div>\
                                                  </div>';
                                  $(tr).find('td').eq(storageTd[4]).html(btnActive);
                            }
                            ,"callback":function(tr,e,gridObj){
                                  var currId = $(tr).attr('id');
                                  var currData = $.jsObj.table1.dataStorage[currId];
                                  var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                                  var rawData = $.jsObj.table1.rawData[currIdx];

                                  $.vueModal.show();
                            }

                        }
                      );
                      $('#datalist').simasgrid('addNav',{
                        title:'Reload',
                        className:'navigation',
                        callback:function(simasGrid){
                              $.vueTable.reload_table();
                        }
                      }); 
                      $.jsObj.table1 = $('#datalist').simasgrid('getObj');
                      $($.jsObj.table1.gridObj).simasgrid('addNav',{
                        title:'Tambah',
                        className:'navigation',
                        callback:function(simasGrid){
                            $.vueModal.show();
                        }
                      }); 

                      // dom simasgrid yg bisa diakses hanya gridObj
                      $($.jsObj.table1.gridObj).on('click','.btn-unlink',function(){
                        var currTr = $(this).closest('tr');
                        var currId = $(currTr).attr('id');
                        var currData = $.jsObj.table1.dataStorage[currId];
                        var currIdx = $.jsObj.table1.dataStorage[currId][$.jsObj.table1.option.field.length].replace('numrows|','');
                        
                        var currData = JSON.parse(JSON.stringify($.vueTable.arr_derajat));
                        currData.splice(currIdx,1);
                        $.vueTable.change_arr_derajat(currData);
                      })

                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        $.extend({vueModal:new Vue({el:'#modal_sekolah'
                  ,data:{
                    "message":"i'm new"
                    ,"show_modal":false
                    ,"item_derajat":{}
                  }
                  ,created: function(){
                    console.log(this.message);
                  }
                  ,methods:{ // method tidak bisa passing variable di fungsi
                    func: function(data){
                      //tidak bisa sebagai variable karena akan return function(){}
                      //benar: v-for="(item,i) in func()"
                      //salah: v-model="func()""
                      this.message = data;
                      console.log(this.message)
                    }
                    ,init: function(data){
                      $(this.$el).on("click", function (e) {
                            if ($(e.target).closest(".inner").length === 0) {
                                   $.vueModal.hide();
                            }
                      });
                    }
                    ,show: function(data){
                      this.show_modal=true;
                    }
                    ,hide: function(data){
                      this.show_modal=false;
                    }
                    ,submit: function(data){
                      $.jsObj.change_item_derajat(this.item_derajat);
                      var currData = JSON.parse(JSON.stringify($.vueTable.arr_derajat));
                      currData[this.idx_derajat] = [
                        this.item_derajat['id']
                        ,this.item_derajat['name']
                        ,this.item_derajat['min']
                        ,this.item_derajat['max']
                      ];
                      $.vueTable.change_arr_derajat(currData);
                      this.hide();
                      $.vueTable.reload_table();
                    }
                    ,fired (event, content, index) {
                      console.log('Click event on DOM element', e.target)
                      console.log('Click event on content index', index)
                      console.log('Click event on content', content)
                    }
                  }
                  ,computed:{
                    // method tidak bisa passing variable di fungsi
                    // caching sampai elemen didalamnya ada perubahan
                    // fullName computed lebih baik dari pada watcher firstname dan lastname
                    getMessage: function(){
                      return this.message;
                    }
                  }
                  ,watch:{
                    message: function(newVal,oldVal){
                      alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                    }        
                  }
                  ,updated: function () {
                    this.$nextTick(function () {
                      // Code that will run only after the
                      // entire view has been re-rendered
                    })
                  }            
            })
        });
        
    })( jQuery );
    $(document).ready(function(){
      $.vueTable.init();
      $.vueModal.init();
    });

  </script>
<?php $this->load->view('default/common/current_footer');?>
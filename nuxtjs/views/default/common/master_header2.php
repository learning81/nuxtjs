<?php
/* halaman seperti blog di aazni.com */
?>
<?php $template = get_template(NULL,'common/master_header');?>
<?php load_template($template);?>

  <div class="col-prfl-lg-3 col-prfl-md-12 elm-prfl-sm-hidden elm-prfl-xs-hidden box-right-sidebar" id=''>
    <div class="box-card box-card-hadist z-depth-1 col-prfl-12">
      <div class="box-card-title text-center">Hadits Hari Ini</div>
      <div class="box-card-content text-center">Hadits Hari Ini</div>
    </div>
    <script type="text/javascript">
      $.setAjax(global['HadistUrl']+'current','get',{},function(result){
        $('.box-card-hadist>.box-card-content').html(result['ph_content']);
      });
    </script>
    <style type="text/css">
      .box-card-hadist>.box-card-content{
          max-height: 200px;
          overflow: auto;
      }
    </style>
    <div class="box-card box-card-category z-depth-1 elm-prfl-sm-hidden elm-prfl-xs-hidden">
      <div class="box-card-title text-center">Berita</div>
    </div>
    <script type="text/javascript">
      $.setAjax(global['BeritaUrl']+'all_cat','get',{'pm_id':global['merchantId']},function(result){
        if(result){
          for(i=0;i<result.length;i++){
            $('.box-card-category').append('<h4><a href="'+global['BeritaUrl']+'category/'+result[i]+'">'+result[i]+'</a></h4>');
          }
        }
      });
    </script>
  </div><!--
  --><div class="col-prfl-lg-9 col-prfl-md-12 col-prfl-sm-12 box-content" id=''>

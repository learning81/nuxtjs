<?php
/* halaman admin untuk adminlte */
?>
<?php $allPanel = $this->module_panel->index(); ?>
<?php $allManagement = $this->module_panel->index2(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Dashboard - 20191214:151011</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="shortcut icon" href="<?php echo config_item('global_instAssetsUrl').'load_assets/';?>img/favicon.ico?creq=4b5f590443796801121a196b17222b" />
      <!-- Base Grid -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/base_grid.2?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_assets/bootstrap.css?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/font-awesome?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- Ionicons -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/ionicons.min?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/adminlte/AdminLTE?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/adminlte/all-skins.min?creq=4b5f590443796801121a196b17222b';?>" />

      <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/standard.css?creq=4b5f590443796801121a196b17222b';?>" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Google Font -->
      <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
      <!-- jQuery 3 -->
      <script src="<?php echo config_item('global_instAssetsUrl').'load_script/adminlte/jquery.min?creq=4b5f590443796801121a196b17222b';?>"></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="<?php echo config_item('global_instAssetsUrl').'load_script/adminlte/jquery-ui.min?creq=4b5f590443796801121a196b17222b';?>"></script>
      <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery-dateFormat.min?creq=4b5f590443796801121a196b17222b';?>"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="<?php echo config_item('global_instAssetsUrl').'load_assets/bootstrap.js?creq=4b5f590443796801121a196b17222b';?>"></script>

      <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.status>jquery/jquery.utils>jquery/jquery.stringify?creq=4b5f590443796801121a196b17222b';?>"></script>
      <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.simasgrid.2?creq=4b5f590443796801121a196b17222b';?>"></script>

      <script type="text/javascript">
      var global = {};
      global['base_url'] = '<?php echo config_item('base_url');?>';
      <?php $globalConfig = $this->config->config;?>
      <?php if(!empty($globalConfig) && is_array($globalConfig)):?>
      <?php foreach ($globalConfig as $idxCfg => $valCfg):?>
        <?php if(FALSE!==strpos($idxCfg,'global_')):?>
          <?php echo "\nglobal['".str_replace('global_','',$idxCfg)."']='$valCfg';";?>
        <?php endif;?>
      <?php endforeach;?>
      <?php endif;?>
      <?php echo "\n";?>
      global['credential'] = '<?php echo $this->user_data->set_credential();?>';
      global['ociconnect'] = <?php echo ($this->access->check_oracle_connection()?'true':'false');?>;
      </script>
      <style type="text/css">
         .master_navigation,.master_custom,.master_sidemenu,.master_module{
            cursor: pointer;
         }
         .sidebar-menu.tree{
            text-align: left;
         }
      </style>      
   </head>
   <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">
         <header class="main-header">
            <!-- Logo -->
            <a href="" class="logo">
               <!-- mini logo for sidebar mini 50x50 pixels -->
               <span class="logo-mini">MENU</span>
               <!-- logo for regular state and mobile devices -->
               <span class="logo-lg">DASHBOARD</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
               <!-- Sidebar toggle button-->
               <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
               <span class="sr-only">Toggle navigation</span>
               </a>
               <div class="navbar-custom-menu">
                  <ul class="nav navbar-nav">
                     <!-- Messages: style can be found in dropdown.less-->
                     <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                           <li class="header">You have 4 messages</li>
                           <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                                 <li>
                                    <!-- start message -->
                                    <a href="#">
                                       <div class="pull-left">
                                          <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user2-160x160.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                                       </div>
                                       <h4>
                                          Support Team
                                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                       </h4>
                                       <p>Why not buy a new awesome theme?</p>
                                    </a>
                                 </li>
                                 <!-- end message -->
                                 <li>
                                    <a href="#">
                                       <div class="pull-left">
                                          <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user3-128x128.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                                       </div>
                                       <h4>
                                          AdminLTE Design Team
                                          <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                       </h4>
                                       <p>Why not buy a new awesome theme?</p>
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                       <div class="pull-left">
                                          <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user4-128x128.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                                       </div>
                                       <h4>
                                          Developers
                                          <small><i class="fa fa-clock-o"></i> Today</small>
                                       </h4>
                                       <p>Why not buy a new awesome theme?</p>
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                       <div class="pull-left">
                                          <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user3-128x128.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                                       </div>
                                       <h4>
                                          Sales Department
                                          <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                       </h4>
                                       <p>Why not buy a new awesome theme?</p>
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                       <div class="pull-left">
                                          <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user4-128x128.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                                       </div>
                                       <h4>
                                          Reviewers
                                          <small><i class="fa fa-clock-o"></i> 2 days</small>
                                       </h4>
                                       <p>Why not buy a new awesome theme?</p>
                                    </a>
                                 </li>
                              </ul>
                           </li>
                           <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                     </li>
                     <!-- Notifications: style can be found in dropdown.less -->
                     <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                           <li class="header">You have 10 notifications</li>
                           <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                    page and may cause design problems
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-users text-red"></i> 5 new members joined
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                 </li>
                              </ul>
                           </li>
                           <li class="footer"><a href="#">View all</a></li>
                        </ul>
                     </li>
                     <!-- Tasks: style can be found in dropdown.less -->
                     <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                           <li class="header">You have 9 tasks</li>
                           <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                                 <li>
                                    <!-- Task item -->
                                    <a href="#">
                                       <h3>
                                          Design some buttons
                                          <small class="pull-right">20%</small>
                                       </h3>
                                       <div class="progress xs">
                                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                             <span class="sr-only">20% Complete</span>
                                          </div>
                                       </div>
                                    </a>
                                 </li>
                                 <!-- end task item -->
                                 <li>
                                    <!-- Task item -->
                                    <a href="#">
                                       <h3>
                                          Create a nice theme
                                          <small class="pull-right">40%</small>
                                       </h3>
                                       <div class="progress xs">
                                          <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                             <span class="sr-only">40% Complete</span>
                                          </div>
                                       </div>
                                    </a>
                                 </li>
                                 <!-- end task item -->
                                 <li>
                                    <!-- Task item -->
                                    <a href="#">
                                       <h3>
                                          Some task I need to do
                                          <small class="pull-right">60%</small>
                                       </h3>
                                       <div class="progress xs">
                                          <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                             <span class="sr-only">60% Complete</span>
                                          </div>
                                       </div>
                                    </a>
                                 </li>
                                 <!-- end task item -->
                                 <li>
                                    <!-- Task item -->
                                    <a href="#">
                                       <h3>
                                          Make beautiful transitions
                                          <small class="pull-right">80%</small>
                                       </h3>
                                       <div class="progress xs">
                                          <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                             <span class="sr-only">80% Complete</span>
                                          </div>
                                       </div>
                                    </a>
                                 </li>
                                 <!-- end task item -->
                              </ul>
                           </li>
                           <li class="footer">
                              <a href="#">View all tasks</a>
                           </li>
                        </ul>
                     </li>
                     <!-- User Account: style can be found in dropdown.less -->
                     <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user2-160x160.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $this->additional_function->set_value($_SESSION,'profile_name');?></span>
                        </a>
                        <ul class="dropdown-menu">
                           <!-- User image -->
                           <li class="user-header">
                              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                              <p>
                                 <?php echo $this->additional_function->set_value($_SESSION,'profile_name');?> - Web Developer
                                 <small>Member since Nov. 2012</small>
                              </p>
                           </li>
                           <!-- Menu Body -->
                           <li class="user-body">
                              <div class="row">
                                 <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                 </div>
                              </div>
                              <!-- /.row -->
                           </li>
                           <!-- Menu Footer-->
                           <li class="user-footer">
                              <div class="pull-left">
                                 <a href="#" class="btn btn-default btn-flat">Profile</a>
                              </div>
                              <div class="pull-right">
                                 <a href="#" class="btn btn-default btn-flat">Sign out</a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <!-- Control Sidebar Toggle Button -->
                     <li>
                        <a href="#" data-toggle="  "><i class="fa fa-gears"></i></a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- Left side column. contains the logo and sidebar -->
         <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
               <!-- Sidebar user panel -->
               <div class="user-panel">
                  <div class="pull-left image">
                     <img src="<?php echo config_item('global_instAssetsUrl').'load_image/adminlte/user2-160x160.jpg?width=1000&height=1000&creq=4b5f590443796801121a196b17222b';?>" class="img-circle" alt="User Image">
                  </div>
                  <div class="pull-left info">
                     <p><?php echo $this->additional_function->set_value($_SESSION,'profile_name');?></p>
                     <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                  </div>
               </div>
               <!-- search form -->
               <form action="#" method="get" class="sidebar-form">
                  <div class="input-group">
                     <input type="text" name="q" class="form-control" placeholder="Search...">
                     <span class="input-group-btn">
                     <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                     </button>
                     </span>
                  </div>
               </form>
               <!-- /.search form -->
               <!-- sidebar menu: : style can be found in sidebar.less -->
               <ul class="sidebar-menu" data-widget="tree">
                  <li class="header master_navigation">MAIN NAVIGATION</li>

                  <?php if(!empty($mainmenu) || !empty($otherPage)):?>
                     <?php $lang = $this->filecache->cache_read('language','session');?>

                     <?php if(!empty($mainmenu)):?>
                        <?php foreach ($mainmenu as $idxMainmenu => $valMainmenu):?>
                           <?php $menuId = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_id'));?>
                           <?php $menuName = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name'));?>

                           <?php $nameTmp = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name_tmp'));?>
                           <?php $nameLang = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name_'.$lang));?>
                           <?php $name1 = (!empty($nameLang)?$nameLang:$nameTmp);?>
                           <?php $url = $this->additional_function->set_value($valMainmenu,'url');?>

                           <?php $mainUrl = str_replace('[baseUrl]',config_item('base_url'),$url);?>

                           <li class="<?php echo (@$curr_master == $menuName?'active':'');?> <?php echo (!empty($valMainmenu->child)?'treeview':'');?> child_navigation">
                              <a href="<?php echo (!empty($valMainmenu->child)?'#':$mainUrl);?>">
                              <i class="fa fa-dashboard"></i> <span><?php echo strtoupper($name1);?></span>
                              <?php if(!empty($valMainmenu->child)):?>
                              <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                              </span>
                              <?php endif;?>
                              </a>
                              <?php if(!empty($valMainmenu->child)):?>
                              <ul class="treeview-menu">
                                 <?php foreach ($valMainmenu->child as $idxChild => $valChild):?>
                                    <?php $nameTmp = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name_tmp'));?>
                                    <?php $nameLang = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name_'.$lang));?>
                                    <?php $name1 = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name'));?>
                                    <?php $name2 = (!empty($nameLang)?$nameLang:$nameTmp);?>
                                    <?php $url = $this->additional_function->set_value($valChild,'url');?>
                                    <li <?php echo (@$curr_child == $name1?'class="active"':'');?>><a href="<?php echo str_replace('[baseUrl]',config_item('base_url'),$url);?>"><i class="fa fa-circle-o"></i> <?php echo strtoupper($name2);?></a></li>
                                 <?php endforeach;?>
                              </ul>
                              <?php endif;?>
                           </li>

                        <?php endforeach;?>
                     <?php endif;?>

                     <?php if(!empty($otherPage)):?>
                        <?php foreach ($otherPage as $idxOther => $valOther):?>

                           <li class="<?php echo (@$curr_master == $valOther?'active':'');?> child_navigation">
                              <a href="<?php echo config_item('base_url').$valOther;?>">
                              <i class="fa fa-dashboard"></i> <span><?php echo 'NONAME '.$idxOther;?></span>
                              </a>
                           </li>

                        <?php endforeach;?>
                     <?php endif;?>

                  <?php endif;?>

                  <li class="header treeview master_custom">Custom Page</li>
                  <li class="<?php echo (@$master_gitlab1?'active':'');?> child_custom">
                     <a href="<?php echo config_item('global_instMainUrl');?>load_page_testing">
                     <i class="fa fa-list"></i> <span>Halaman Testing</span>
                     </a>
                  </li>

                  <li class="header master_sidemenu">SIDE MENU - Module Panel</li>

                  <?php if(!empty($allPanel['all_panel'])):?>
                     <?php foreach ($allPanel['all_panel'] as $valPanel):?>

                     <li class="<?php echo ($valPanel!='kamus'?'child_sidemenu':'widget');?>">
                        <a href="<?php echo config_item('global_PanelUrl').$valPanel;?>" <?php echo ($valPanel=='kamus'?'target="_blank"':'');?>>
                        <i class="fa fa-table"></i> <span><?php echo str_replace('_',' ',$valPanel);?></span>
                        <?php if($valPanel=='kamus'):?>
                           <span class="pull-right-container">
                           <small class="label pull-right bg-green">prioritas</small>
                           </span>
                        <?php endif;?>
                        </a>
                     </li>

                     <?php endforeach;?>
                  <?php endif;?>

                  <li class="header master_module">SIDE MENU - Module Admin</li>

                  <?php if(!empty($allManagement['all_panel'])):?>
                     <?php foreach ($allManagement['all_panel'] as $name => $management):?>

                     <li class="<?php echo ($name!='kamus'?'child_module':'widget');?>">
                        <a href="<?php echo config_item('global_instMainUrl').$management;?>" <?php echo ($name!='kamus'?'target="_blank"':'');?>>
                        <i class="fa fa-table"></i> <span>Management <?php echo $name;?></span>
                        <?php if($name=='kamus'):?>
                           <span class="pull-right-container">
                           <small class="label pull-right bg-green">prioritas</small>
                           </span>
                        <?php endif;?>
                        </a>
                     </li>

                     <?php endforeach;?>
                  <?php endif;?>

               </ul>
            </section>
            <!-- /.sidebar -->
         </aside>
<script type="text/javascript">
   $('.child_navigation, .child_custom, .child_sidemenu, .child_module').hide();
   $('.master_navigation').on('click',function(){
      $('.child_navigation').toggle();
   });
   $('.master_custom').on('click',function(){
      $('.child_custom').toggle();
   });
   $('.master_sidemenu').on('click',function(){
      $('.child_sidemenu').toggle();
   });
   $('.master_module').on('click',function(){
      $('.child_module').toggle();
   });
</script>

<?php
/* halaman seperti blog di aazni.com */
?>
<?php $template = get_template(NULL,'common/master_header_js');?>
<?php load_template($template);?>
<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/master_header?creq=4b5f590443796801121a196b17222b';?>" />

  <div class="col-prfl-12 box-header" id=''>
    <div class="col-prfl-lg-4 col-prfl-md-6 col-prfl-sm-12 col-prfl-xs-12 box-logo">
      <img src="<?php echo config_item('global_instAssetsUrl').'load_image_thumbnail/';?>app/Azni.F.jpg?width=200&height=200&creq=4b5f590443796801121a196b17222b">
    </div><!--
    --><div class="col-prfl-lg-4 col-prfl-md-6 col-prfl-sm-12 col-prfl-xs-12 elm-prfl-sm-hidden elm-prfl-xs-hidden box-header-profile float-right text-right">
      <div class="col-prfl-12">ONLINE : 10.00 WIB <i class="fa fa-male">&nbsp;</i> </div><!--
      --><div class="col-prfl-12"><?php echo strip_tags(@$system_phone);?> <i class="fa fa-mobile-phone">&nbsp;</i> </div><!--
      --><div class="col-prfl-12"><?php echo strip_tags(@$system_email);?> <i class="fa fa-envelope">&nbsp;</i></div><!--
      --><div class="col-prfl-12 box-common-navigasi">Navigasi <i class="fa fa-list">&nbsp;</i></div>
    </div><!--
    --><div class="col-prfl-lg-4 col-prfl-md-12 col-prfl-sm-12 box-search-prod text-center">
      <input class='col-prfl-10' type="" name="" placeholder="search is disabled" disabled="disabled" />
    </div>
  </div>
      

  <div class="box-common-rightbar elm-prfl-hidden">
    <ul class="box-common-rightmenu vspace-10">
      <li class="text-center col-prfl-12">NAVIGASI</li>
      <!-- trigger hadist hanya tampil di halaman small dan extra small -->
      <!-- hadist hanya tampil di halaman large dan extra small -->
      <?php if(config_item('use_hadist') == TRUE): ?>
      <li class="elm-prfl-lg-hidden elm-prfl-md-hidden col-prfl-sm-12 col-prfl-xs-12 box-common-trigger-hadits">Hadist Hari Ini</li>
      <?php endif;?>
      <li class="col-prfl-12"><a href="<?php echo config_item('global_instProductUrl');?>pulsa">Pesan Pulsa</a></li>
      <li class="col-prfl-12"><a href="<?php echo config_item('global_instProductUrl');?>ppob">Bayar Listrik dan PDAM</a></li>
      <li class="col-prfl-12"><a href="<?php echo config_item('global_instProductUrl');?>pesawat">Tiket Pesawat</a></li>
      <li class="col-prfl-12"><a href="<?php echo config_item('global_instProductUrl');?>pesancepat">Pesan Cepat</a></li>
      <li class="col-prfl-12"><a href="<?php echo config_item('global_instProductUrl');?>custom">Status Pesanan</a></li>
    </ul>
    <div class="col-prfl-12 text-center box-common-sidebar-toggle vspace-10 set-poiter">
      (x)      
    </div>
  </div>
  <style type="text/css">
    .box-common-rightbar{
      position: fixed;
      top: 0;
      right: 0;
      background-color: #6d4c41;
      padding: 10px 25px;
      min-height: 300px;
      color: white;
    }
    .box-common-rightmenu{
      width: 200px;
    }
    .box-common-rightbar li{
      border-bottom: 1px solid #ccc;
      cursor: pointer;
      padding: 10px 0;
    }
    .box-common-rightbar li>a{
      display: block;
      text-decoration: none;
      color: white;
    }
    .box-common-rightbar li:hover{
      background-color: #8d6e63;
    }
  </style>
  <div id='mainmenu' class="col-prfl-12 box-mainmenu">
      <div id='titlemenu' class='elm-prfl-lg-hidden elm-prfl-md-hidden col-prfl-sm-12 elm-prfl-xs-hidden text-center'>MAINMENU</div>
      <?php /*$mainmenu = array(array('dm_id'=>1,'dm_name'=>'menu1','dm_name_tmp'=>'test'),array('dm_id'=>1,'dm_name'=>'menu1','dm_name_tmp'=>'test'));*/?>
      <?php if(!empty($mainmenu) || !empty($otherPage)):?>
        <ul class='elm-prfl-lg-show elm-prfl-md-show elm-prfl-sm-hidden elm-prfl-xs-show'>
        <!-- 
        <?php if(!empty($mainmenu)):?>
        <?php foreach ($mainmenu as $idxMainmenu => $valMainmenu):?>
            <?php $menuId = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_id'));?>
            <?php $menuName = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name'));?>
        --><li id="<?php echo $menuName; ?>" class='<?php echo ($menuId == 99?'logout':'');?> col-prfl-lg-2 col-prfl-md-6 col-prfl-sm-12'>

            <?php $lang = $this->filecache->cache_read('language','session');?>
            <?php $nameTmp = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name_tmp'));?>
            <?php $nameLang = $this->additional_function->set_value($valMainmenu,$this->access->get_field_name('dm_name_'.$lang));?>
            <?php $name1 = (!empty($nameLang)?$nameLang:$nameTmp);?>
            <?php $url = $this->additional_function->set_value($valMainmenu,'url');?>

            <?php $mainUrl = str_replace('[baseUrl]',config_item('base_url'),$url);?>
            <a href='<?php echo (!empty($valMainmenu->child)?'#':$mainUrl);?>' class="col-prfl-12">
              <?php echo strtoupper($name1);?>
            </a>
            <?php if(!empty($valMainmenu->child)):?>
              <ul class="elm-prfl-hidden">
              <?php foreach ($valMainmenu->child as $idxChild => $valChild):?>
                <li>
                  <?php $nameTmp = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name_tmp'));?>
                  <?php $nameLang = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name_'.$lang));?>
                  <?php $name1 = $this->additional_function->set_value($valChild,$this->access->get_field_name('dm_name'));?>
                  <?php $name2 = (!empty($nameLang)?$nameLang:$nameTmp);?>
                  <?php $url = $this->additional_function->set_value($valChild,'url');?>

                  <a id="<?php echo $name1; ?>" href='<?php echo str_replace('[baseUrl]',config_item('base_url'),$url);?>' class="col-prfl-12">
                    <?php echo strtoupper($name2);?>
                  </a>

                </li>
              <?php endforeach;?>
              </ul>
            <?php endif;?>

          </li><!--
        <?php endforeach;?>

        <?php if(count($mainmenu)%2!=0):?>
          --><li class='col-prfl-lg-2 col-prfl-md-6 col-prfl-sm-12'>
            <a href='#' class="col-prfl-12">&nbsp;</a>
          </li><!--
        <?php endif;?>

        <?php endif;?>

        <?php if(!empty($otherPage)):?>
        <?php foreach ($otherPage as $idxOther => $valOther):?>
        --><li class='col-prfl-lg-2 col-prfl-md-6 col-prfl-sm-12'>        
            <a href='<?php echo config_item('base_url').$valOther;?>' class="col-prfl-12">
              <?php echo 'NONAME '.$idxOther;?>
            </a>
          </li><!--
        <?php endforeach;?>
        <?php endif;?>
        -->
        </ul>
      <?php endif;?>
  </div>

    <div class="box_footer p-10 bg-pink-600 text-white">
      <div class=" grid lg:grid-cols-4 sm:grid-cols-2 xs:grid-cols-1 gap-10 text-sm">
        <div class="lg:pr-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris </div>
        <div>
          <div>Menu 1</div>
          <div>Menu 2</div>
          <div>Menu 3</div>
          <div>Menu 4</div>
        </div>
        <div>
          <div>Portfolio1</div>
          <div>Portfolio2</div>
          <div>Portfolio3</div>
          <div>Portfolio4</div>
        </div>
        <div>
          <div>Contact 1</div>
          <div>Contact 2</div>
          <div>Contact 3</div>
          <div>Contact 4</div>
        </div>
      </div>
      <div class="mt-4 text-center text-sm font-sans text-gray-400">@copyfight: aazni</div>
    </div>
  </div>
</body>
</html>

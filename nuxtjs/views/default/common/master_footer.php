  <div class="col-prfl-12 box-footer z-depth-2 text-center" id=''>
      <?php echo @$system_phone;?>
      <?php echo @$system_footer;?>
  </div>
  <?php $template = get_template(NULL,'common/master_footer_js');?>
  <?php load_template($template);?>
  <script type="text/javascript">
      /**/// toogle for header
          var show_header = function(){
              $('.box-header-profile').velocity("fadeIn", { duration: 1500 });
              $('.box-header-profile').css('position','fixed');
              $('.box-header-profile').css('top',0);
              $('.box-header-profile').css('left',0);
          };
          var hide_header = function(){
              $('.box-header-profile').velocity("fadeOut", { duration: 1500 });
              // $('.box-header-profile').hide();
          };

          var intervalID = null;
          $(window).on('scroll',function(){
            var currWindow = $.viewport();
            if(currWindow.width<450){
              var isAnimating = $('.box-header-profile').hasClass('velocity-animating');
              var display = $('.box-header-profile').css('display');
              var isShow = $('.box-header-profile').css('opacity');
              if(!isAnimating && (display=='none' || isShow == 0)) show_header();

                if(intervalID) clearInterval(intervalID); // Will clear the timer.
                intervalID = setInterval(hide_header, 3000); // Will alert every second.
            }
          });
          $('.box-header-profile').on('mouseenter',function(){
            var currWindow = $.viewport();
            if(currWindow.width<450){
                if(intervalID) clearInterval(intervalID); // Will clear the timer.
                $('.box-header-profile').show();
            }         
          });
          $('.box-header-profile').on('mouseleave',function(){
            var currWindow = $.viewport();
            if(currWindow.width<450){
                if(intervalID) clearInterval(intervalID); // Will clear the timer.
                intervalID = setInterval(hide_header, 1500); // Will alert every second.
            }         
          });

      /**/// toggle untuk hadist dan rightbar
          $('.box-common-sidebar-toggle').on('click',function(){
            $(".box-common-rightbar").hide();
          });
          $('.box-common-trigger-hadits').on('click',function(){
            $('.box-right-sidebar').toggle();
          });

      /**/// toggle untuk navigasi
          $('.box-common-navigasi').on('click',function(){ // manual toggle
            $(".box-common-rightbar").show();
          });
          $.closeOnOutside('box-common-rightbar');
  </script>

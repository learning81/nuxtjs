<?php
/* halaman seperti blog di aazni.com */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo config_item('title');?>v1.1.0</title>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="<?php echo config_item('description');?>" />
<meta name="keywords" content="<?php echo config_item('keywords');?>" />
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<meta HTTP-EQUIV="Expires" CONTENT="-1" />
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />

<meta property="og:title" content="<?php echo config_item('og_title');?>"/>
<meta property="og:image" content="<?php echo config_item('og_image');?>" />
<meta property="fb:app_id" content="<?php echo config_item('fb_app_id');?>" />
<meta property="og:description" content="<?php echo config_item('og_description');?>"/>
<meta property="og:url" content="<?php echo config_item('og_url');?>" />
<meta property="og:site_name" content="<?php echo config_item('og_site_name');?>" />
<meta property="og:type" content="article" />

<link rel="shortcut icon" href="<?php echo config_item('global_instAssetsUrl').'load_assets/';?>img/favicon.ico?creq=4b5f590443796801121a196b17222b" />
<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/base_grid.2?creq=4b5f590443796801121a196b17222b';?>" />
<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/base_color2?creq=4b5f590443796801121a196b17222b';?>" />
<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/font-awesome?creq=4b5f590443796801121a196b17222b';?>" />

<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/style2?creq=4b5f590443796801121a196b17222b';?>" />
<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/jquery/jquery-ui?creq=4b5f590443796801121a196b17222b';?>" />

<script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery-1.11.3.min?creq=4b5f590443796801121a196b17222b';?>"></script>
<script src="<?php echo config_item('global_instAssetsUrl').'load_script/velocity.min?creq=4b5f590443796801121a196b17222b';?>"></script>
<script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.status>jquery/jquery.utils>jquery/jquery.stringify?creq=4b5f590443796801121a196b17222b';?>"></script>

<script type="text/javascript">
var global = {};
global['base_url'] = '<?php echo config_item('base_url');?>';
<?php $globalConfig = $this->config->config;?>
<?php if(!empty($globalConfig) && is_array($globalConfig)):?>
<?php foreach ($globalConfig as $idxCfg => $valCfg):?>
<?php if(FALSE!==strpos($idxCfg,'global_')):?>
<?php echo "\nglobal['".str_replace('global_','',$idxCfg)."']='$valCfg';";?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php echo "\n";?>
global['credential'] = '<?php echo $this->user_data->set_credential();?>';
global['ociconnect'] = <?php echo ($this->access->check_oracle_connection()?'true':'false');?>;
global['merchantId'] = '<?php echo config_item('merchant_id');?>';
</script>
</head>
<body>
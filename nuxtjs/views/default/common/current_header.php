<!DOCTYPE html>
<html>
<head>
  <title>SOKOLA.ID</title>

  <!-- <link rel="stylesheet" href="<?php echo config_item('asset_url');?>tailwind/dist/min.css?creq=4b5f590443796801121a196b17222b" /> -->
  <link rel="stylesheet" href="<?php echo config_item('asset_url');?>tailwind/dist/min.css" />

  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.min?creq=4b5f590443796801121a196b17222b';?>"></script>
  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery-ui?creq=4b5f590443796801121a196b17222b';?>"></script>
  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery-dateFormat.min?creq=4b5f590443796801121a196b17222b';?>"></script>
  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.status>jquery/jquery.utils>jquery/jquery.stringify?creq=4b5f590443796801121a196b17222b';?>"></script>
  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/vue?creq=4b5f590443796801121a196b17222b';?>"></script>

  <script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.simasgrid.2?creq=4b5f590443796801121a196b17222b';?>"></script>
  <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/jquery/jquery.simasgrid.2a?creq=4b5f590443796801121a196b17222b';?>" />

  <link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl');?>load_assets/uikit-3.5.4/css/uikit.min.css?creq=4b5f590443796801121a196b17222b" />
  <script src="<?php echo config_item('global_instAssetsUrl');?>load_assets/uikit-3.5.4/js/uikit.min.js?creq=4b5f590443796801121a196b17222b"></script>
  <script src="<?php echo config_item('global_instAssetsUrl');?>load_assets/uikit-3.5.4/js/uikit-icons.min.js?creq=4b5f590443796801121a196b17222b"></script>
  <link href="<?php echo config_item('global_instAssetsUrl');?>load_assets/prototype/uikitsite/css/theme.css?creq=4b5f590443796801121a196b17222b" rel="stylesheet">

  <script type="text/javascript">
  var global = {};
  global['base_url'] = '<?php echo config_item('base_url');?>';
  <?php $globalConfig = $this->config->config;?>
  <?php if(!empty($globalConfig) && is_array($globalConfig)):?>
  <?php foreach ($globalConfig as $idxCfg => $valCfg):?>
    <?php if(FALSE!==strpos($idxCfg,'global_')):?>
      <?php echo "\nglobal['".str_replace('global_','',$idxCfg)."']='$valCfg';";?>
    <?php endif;?>
  <?php endforeach;?>
  <?php endif;?>
  <?php echo "\n";?>
  global['credential'] = '<?php echo $this->user_data->set_credential();?>';
  </script>

</head>
<body>
  <div class="overflow-scroll h-screen">
    <div id="box_sticky" class="sticky top-0 bg-black flex flex-col items-stretch md:flex-row justify-between items-center py-6 px-4 text-white z-10">
      <div class="flex justify-between">
        <div class="md:hidden">
          <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M80 368H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm0-320H16A16 16 0 0 0 0 64v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16V64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm416 176H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z"/></svg>
        </div>
        <div>SOKOLA</div>
        <div class="md:hidden">
          <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M400 54.1c63 45 104 118.6 104 201.9 0 136.8-110.8 247.7-247.5 248C120 504.3 8.2 393 8 256.4 7.9 173.1 48.9 99.3 111.8 54.2c11.7-8.3 28-4.8 35 7.7L162.6 90c5.9 10.5 3.1 23.8-6.6 31-41.5 30.8-68 79.6-68 134.9-.1 92.3 74.5 168.1 168 168.1 91.6 0 168.6-74.2 168-169.1-.3-51.8-24.7-101.8-68.1-134-9.7-7.2-12.4-20.5-6.5-30.9l15.8-28.1c7-12.4 23.2-16.1 34.8-7.8zM296 264V24c0-13.3-10.7-24-24-24h-32c-13.3 0-24 10.7-24 24v240c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24z"/></svg>
        </div>
      </div>
      <div class="col-gap-12 row-gap-4 grid md:grid-flow-col mt-4 md:mt-0 grid-flow-row">
        <div class="relative border border-solid border-white rounded-md text-center bg-white text-black md:hidden">LOGIN</div>
        <div class="relative">
          <?php
            $url = (!empty($_SESSION['active'])?$_SESSION['active']:config_item('global_instMainUrl').'index');
          ?>
          <div class="text-white hover:bg-white page1_mainmenu px-4 rounded-md">
            <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"/></svg>
            <a href="<?php echo $url;?>" class="w-full no-underline text-white hover:no-underline">Home</a>
          </div>
          <div class="hidden absolute mt-2 md:mt-6 bg-black rounded-md p-2 pb-10 z-10 border border-solid border-white">
            <div class="py-2 px-10 bg-black border-b border-solid border-white">Submenu1</div>
            <div class="py-2 px-10 bg-black border-b border-solid border-white">Submenu2</div>
            <div class="py-2 px-10 bg-black border-b border-solid border-white">Submenu3</div>
          </div>
        </div>
        <div class="relative">
          <div class="text-white hover:bg-white page1_mainmenu px-4 rounded-md">
            <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M0 464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V192H0v272zm64-192c0-8.8 7.2-16 16-16h96c8.8 0 16 7.2 16 16v96c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16v-96zM400 64h-48V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H160V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H48C21.5 64 0 85.5 0 112v48h448v-48c0-26.5-21.5-48-48-48z"/></svg>
            <a href="#" class="w-full no-underline text-white hover:no-underline">Header 1</a>
          </div>
        </div>
        <div class="relative">
          <div class="text-white hover:bg-white page1_mainmenu px-4 rounded-md">
            <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 224c-79.41 0-192 122.76-192 200.25 0 34.9 26.81 55.75 71.74 55.75 48.84 0 81.09-25.08 120.26-25.08 39.51 0 71.85 25.08 120.26 25.08 44.93 0 71.74-20.85 71.74-55.75C448 346.76 335.41 224 256 224zm-147.28-12.61c-10.4-34.65-42.44-57.09-71.56-50.13-29.12 6.96-44.29 40.69-33.89 75.34 10.4 34.65 42.44 57.09 71.56 50.13 29.12-6.96 44.29-40.69 33.89-75.34zm84.72-20.78c30.94-8.14 46.42-49.94 34.58-93.36s-46.52-72.01-77.46-63.87-46.42 49.94-34.58 93.36c11.84 43.42 46.53 72.02 77.46 63.87zm281.39-29.34c-29.12-6.96-61.15 15.48-71.56 50.13-10.4 34.65 4.77 68.38 33.89 75.34 29.12 6.96 61.15-15.48 71.56-50.13 10.4-34.65-4.77-68.38-33.89-75.34zm-156.27 29.34c30.94 8.14 65.62-20.45 77.46-63.87 11.84-43.42-3.64-85.21-34.58-93.36s-65.62 20.45-77.46 63.87c-11.84 43.42 3.64 85.22 34.58 93.36z"/></svg>
            <a href="#" class="w-full no-underline text-white hover:no-underline">Header 1</a>
          </div>
        </div>
      </div>
      <div class="md:block hidden">
          <?php if(empty($_SESSION['active'])):?>
          <svg v-on:click="show_login" class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 256H152V152.9c0-39.6 31.7-72.5 71.3-72.9 40-.4 72.7 32.1 72.7 72v16c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24v-16C376 68 307.5-.3 223.5 0 139.5.3 72 69.5 72 153.5V256H48c-26.5 0-48 21.5-48 48v160c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zM264 408c0 22.1-17.9 40-40 40s-40-17.9-40-40v-48c0-22.1 17.9-40 40-40s40 17.9 40 40v48z"/></svg>
          <?php else:?>
          <a href="<?php echo config_item('global_instMainUrl');?>logout">
            <svg class="fill-current text-white inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M400 54.1c63 45 104 118.6 104 201.9 0 136.8-110.8 247.7-247.5 248C120 504.3 8.2 393 8 256.4 7.9 173.1 48.9 99.3 111.8 54.2c11.7-8.3 28-4.8 35 7.7L162.6 90c5.9 10.5 3.1 23.8-6.6 31-41.5 30.8-68 79.6-68 134.9-.1 92.3 74.5 168.1 168 168.1 91.6 0 168.6-74.2 168-169.1-.3-51.8-24.7-101.8-68.1-134-9.7-7.2-12.4-20.5-6.5-30.9l15.8-28.1c7-12.4 23.2-16.1 34.8-7.8zM296 264V24c0-13.3-10.7-24-24-24h-32c-13.3 0-24 10.7-24 24v240c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24z"/></svg>
          </a>
          <?php endif;?>
      </div>
    </div>
    <div class="outer z-10" id="box_login" style="display: none; position: fixed; height: 100%; width: 100%; left: 0; top: 0; background: rgba(10,10,10,0.9)">
        <div class="middle" style="display: table-cell; vertical-align: middle;">
           <div class="inner w-1/3" style="margin: 0 auto;">
             <div class="grid grid-flow-row gap-4 opacity-75 rounded-md bg-gray-700 pt-5 px-10 pb-8" id="box_login">
               <div class="">
                 <label class="w-full text-white">Email</label>
                 <input class="w-full px-2" type="" name="" v-model="username">
               </div>
               <div class="">
                 <label class="w-full text-white">Password</label>
                 <input class="w-full px-2" type="" name="" v-model="password">
               </div>
               <div class="py-2 cursor-pointer px-4 rounded-md text-center bg-pink-600">
                 <div class="text-white" v-on:click="login">Login</div>
               </div>
               <div class="cursor-pointer px-4 rounded-md text-center">
                 <a class="text-white" href="<?php echo config_item('global_instMainUrl');?>registrasi">Register</a>
               </div>
             </div>
             <div class="text-red-100 p-10 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
             quis nostrud exercitation ullamco laboris nisi</div>
          </div>
        </div>
    </div>
    <script type="text/javascript">
      (function($){
          $.extend({
              newfunc: function (){}
              ,jsObj: function (){}
          });
          $.extend({vueSticky:new Vue({el:'#box_sticky'
                    ,data:{
                      "message":"i'm new"
                    }
                    ,created: function(){
                      console.log(this.message);
                    }
                    ,methods:{ // method tidak bisa passing variable di fungsi
                      func: function(data){
                        //tidak bisa sebagai variable karena akan return function(){}
                        //benar: v-for="(item,i) in func()"
                        //salah: v-model="func()""
                        this.message = data;
                        console.log(this.message)
                      }
                      ,show_login (event, content, index) {
                        $.vueLogin.show();
                      }
                      ,fired (event, content, index) {
                        console.log('Click event on DOM element', e.target)
                        console.log('Click event on content index', index)
                        console.log('Click event on content', content)
                      }
                    }
                    ,computed:{
                      // method tidak bisa passing variable di fungsi
                      // caching sampai elemen didalamnya ada perubahan
                      // fullName computed lebih baik dari pada watcher firstname dan lastname
                      getMessage: function(){
                        return this.message;
                      }
                    }
                    ,watch:{
                      message: function(newVal,oldVal){
                        alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                      }        
                    }
                    ,updated: function () {
                      this.$nextTick(function () {
                        // Code that will run only after the
                        // entire view has been re-rendered
                      })
                    }            
              })
          });
          $.extend({vueLogin:new Vue({el:'#box_login'
                    ,data:{
                      "message":"i'm new"
                      ,"username":""
                      ,"password":""
                    }
                    ,created: function(){
                      console.log(this.message);
                    }
                    ,methods:{ // method tidak bisa passing variable di fungsi
                      func: function(data){
                        //tidak bisa sebagai variable karena akan return function(){}
                        //benar: v-for="(item,i) in func()"
                        //salah: v-model="func()""
                        this.message = data;
                        console.log(this.message)
                      }
                      ,init: function(data){
                        // auto hide jika di klik diluar box
                        $(this.$el).on("click", function (e) {
                              if ($(e.target).closest(".inner").length === 0) {
                                  $.vueLogin.hide();
                              }
                        });
                      }
                      ,show: function(data){
                        $(this.$el).css('display','table');
                      }
                      ,hide: function(data){
                        $(this.$el).css('display','none');
                      }
                      ,submit: function(data){
                        alert('data sudah diregistrasi');
                      }
                      ,login: function(data){
                        if(this.username=="sekolah"){
                          window.open('<?php echo config_item('global_instMainUrl');?>dashboard_sekolah','_top');
                        }else if(this.username=="guru"){
                          window.open('<?php echo config_item('global_instMainUrl');?>dashboard_guru','_top');
                        }else if(this.username=="murid"){
                          window.open('<?php echo config_item('global_instMainUrl');?>dashboard_murid','_top');
                        }else{
                          UIkit.notification("Login Gagal", {pos: 'top-right',status:'warning'})
                        }
                      }
                      ,fired (event, content, index) {
                        console.log('Click event on DOM element', e.target)
                        console.log('Click event on content index', index)
                        console.log('Click event on content', content)
                      }
                    }
                    ,computed:{
                      // method tidak bisa passing variable di fungsi
                      // caching sampai elemen didalamnya ada perubahan
                      // fullName computed lebih baik dari pada watcher firstname dan lastname
                      getMessage: function(){
                        return this.message;
                      }
                    }
                    ,watch:{
                      message: function(newVal,oldVal){
                        alert('my product is changed to '+this.message+' from '+oldVal+' to new '+newVal);
                      }        
                    }
                    ,updated: function () {
                      this.$nextTick(function () {
                        // Code that will run only after the
                        // entire view has been re-rendered
                      })
                    }            
              })
          });
          $.vueLogin.init();
      })( jQuery );
    </script>


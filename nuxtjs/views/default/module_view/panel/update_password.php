<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

              <div id="menu1" class='col-12'>
                <form id="myform" name="myform" method='post' action=''>
                  <input type="password" placeholder="New Password" name='new_password' class='form-control' value='<?php echo $this->additional_function->set_value($_POST,'new_password');?>'/><br />
                  <input type="password" placeholder="Confirm Password" name='conf_password' class='form-control' value='<?php echo $this->additional_function->set_value($_POST,'conf_password');?>'/><br />
                  <?php
                  $colour = ($state?'blue':'red');
                  if(!empty($comment)){
                    foreach ($comment as $key => $item) {
                      echo "<div id='error' style='color:".$colour.";'>".$item."<br /></div>";
                    }
                  }
                  ?>
                  <br />
                  <button type='submit' id='submit' class="btn btn-default form-control" />UPDATE</button>     
                </form>
              </div>
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script src="<?php echo config_item('global_instAssetsUrl').'load_script/jquery/jquery.validate';?>"></script>
<script>
  $(document).ready(function(){
    $.setToggle('menu1','Update Password',null,false);

    $("#myform").validate({
      submitHandler: function(form) {
        var new_pass = $('input[name=new_password]').val();
        var conf_pass = $('input[name=conf_password]').val();
        if(!new_pass || !conf_pass){
          alert('Input New Password dan Confirm Password !!!');
        }
        else if(new_pass == conf_pass){
          form.submit();
        }else{
          alert('Confirm Password tidak sama dengan New Password !!!');
        }
      }
    });
  });
</script>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
    
                <div id="menu1" class='col-prfl-12'>
                  <h3><font color='red'>menu khusus support/programmer untuk keperluan debugging</font></h3>
                  <br />
<pre class="text-left">
* sepertinya boleh diakses oleh semua user
* semua user yg login di satu perangkat akan semuanya ditampilkan
* berpindah dari masing-masing session
</pre>
                  <form method='post' action=''>
                    <select class='form-control' name='session'>
                      <?php if(!empty($all_session) && is_array($all_session)):?>
                        <?php foreach ($all_session as $idxSess => $valSess):?>
                          <option value='<?php echo $idxSess;?>'><?php echo $valSess;?></option>
                        <?php endforeach;?>
                      <?php endif;?>
                    </select>
                    <a href='<?php echo config_item('global_instMainUrl');?>'>login</a>
                    <br />
                    <button type='submit' id='submit' class="btn btn-default form-control" />UPDATE</button>     
                  </form>
                </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<style type="text/css">
  body{
    text-align: center;
  }
  a{
    text-decoration: none;
    color: #fff;
  }
  </style>
<script>
  $(document).ready(function(){
        $.setToggle('menu1','Pindah Session / User login',null,false);
  });
</script>
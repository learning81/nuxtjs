    <?php if(!empty($page)):?>
        <div class='news min-height-250 col-prfl-lg-9 col-prfl-md-9 col-prfl-sm-12'>
            <div id='title'><h1><?php echo strtoupper($this->additional_function->set_value($page,'pp_title'));?></h1></div>
            <div id='date'><?php echo date('d M Y H:i:s',strtotime($this->additional_function->set_value($page,'pp_postdate')));?></div>
            <div id='description'><?php echo $this->additional_function->set_value($page,'pp_detail');?></div>
        </div>
    <?php endif;?>

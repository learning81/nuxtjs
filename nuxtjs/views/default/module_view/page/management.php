<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

                <div id="page" class='col-prfl-12'>
                    <div id='list_page'></div>
                    <input type="hidden" name="filter_pp_id"> 
                    <label>Slug :</label>
                    <input type="text" class="form-control" placeholder="Text input" name="pp_slug" disabled="disabled" /> 
                    <label>Title :</label>
                    <input type="text" class="form-control" placeholder="Text input" name="pp_title" /> 
                    <label>Status :</label>
                    <input type="radio" name="pp_active" value=1 /> aktif
                    <input type="radio" name="pp_active" value=0 /> non aktif
                    <br />
                    <label>Isi :</label>
                    <textarea class="form-control" placeholder="Text input" name="pp_detail" id="pp_detail"></textarea> 
                    <br />
                    <input type="button" id='submit' class="btn btn-primary btn-xs col-prfl-12" value="Submit"/> 
                    <br /><br />
                    <input type="button" id='reset' class="btn btn-primary btn-xs col-prfl-12" value="Reset" /> 
                    <br /><br />
                    <input type="button" id='delete' class="btn btn-primary btn-xs col-prfl-12" value="Delete" /> 
                </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<link rel="stylesheet" href="<?php echo config_item('global_instAssetsUrl').'load_style/ckeditor/contents?t=E7KD';?>" />
<script src="<?php echo config_item('global_instAssetsUrl').'load_script/ckeditor/ckeditor';?>"></script>

<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replace('pp_detail', {
           "filebrowserImageUploadUrl": "<?php echo config_item('global_instMainUrl');?>commonpage"
        });

        var option = {
            field:[
                    {id:'pp_id',text:'id',type:'string',width:100,'align':'center','visibility':'hidden'},
                    {id:'pp_merchant',text:'merchant',type:'string',width:100,'visibility':'hidden'},
                    {id:'pp_title',text:'Title',type:'string',width:100},
                    {id:'pp_slug',text:'Slug',type:'string',width:100},
                    {id:'pp_active',text:'Status',type:'string'},
                    {id:'tmp_pp_detail',text:'Detail',type:'string',width:400,'addEvent':false},
                    {id:'pp_postdate',text:'Post Date',type:'string',width:150}
                ],
            jsonUrl:global['PageUrl'],
            countUrl:global['PageUrl']+'?return=count',
            width:'100%',
            height:200,
            postData:{'ordid':'pp_id','ordtype':'asc'},
            checkBoxAdded:true,
            callback:function(tr){
                var trId = $(tr).attr('id');
                $.setAjax(global['PageUrl']+'?return=single','get',{'filter_pp_id':trId},function(result){
                    if(result) $('#cke_pp_detail').find('iframe').contents().find("html").find('body').html(result['pp_detail']);
                    $.setAllInput('page',result);
                });
            },
            onDataLoad:function(tr,data,tdStorage,storageTd){
                var tdActive = storageTd[4];
                $(tr).find('td').eq(tdActive).text(data[4]==1?'active':'nonaktif');
            }
        };

        $('#list_page').simasgrid(option);

        $('#page input[id="delete"]').on('click',function(){
            var trData = $('#list_page').simasgrid('selectedData');
            if(trData.length > 0){
                var finished = false;
                for(var i in trData){
                    var trId = $(trData[i]).attr('id');
                    $.setAjax(global['PageUrl'],'delete',{'filter_pp_id':trId},function(result){
                        if(result) finished = true;
                    },false);
                }
                if(finished)
                    $('#list_page').simasgrid('rebuildGrid');
            }else{
                alert('tidak ada data yang diseleksi');
            }
        });
        $('#page input[id="reset"]').on('click',function(){
            var data = $.getAllInput('#page');
            for(var i in data){
                data[i.replace('filter_','')] = '';
            }
            $.setAllInput('page',data);
            $('#cke_pp_detail').find('iframe').contents().find("html").find('body').html('');
        });
        $('#page input[id="submit"]').on('click',function(){
            var data = $.getAllInput('#page');
            data['pp_detail'] = $('#cke_pp_detail').find('iframe').contents().find("html").find('body').html();
            $.setAjax(global['PageUrl'],'put',data,function(result){
                if(!result.state){
                    $.setAjax(global['PageUrl'],'post',data,function(result){
                        if(result.state){
                            $('#page input[name="filter_pp_id"]').val(result['data']['pp_id']);
                            $('#list_page').simasgrid('rebuildGrid');
                        }else{
                            alert('ada kesalahan input');
                        }
                    });
                }else{
                    $('#list_page').simasgrid('rebuildGrid');
                }
            });
        });
    });
</script>
<style type="text/css">
#banner{
    text-align: center;
}
#pp_detail{
    height: 300px;
}
</style>
            
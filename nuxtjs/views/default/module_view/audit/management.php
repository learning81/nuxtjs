<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div id='list_page'></div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function(){
        var option = {
            field:[
                    {id:'da2_id',text:'id',type:'string',width:100,'align':'center','visibility':'hidden'},
                    {id:'da2_userid',text:'user',type:'string',width:100,'visibility':'hidden'},
                    {id:'da2_inst_id',text:'inst id',type:'string',width:100},
                    {id:'da2_inst_type',text:'inst type',type:'string',width:100},
                    {id:'da2_detail',text:'detail',type:'string'},
                    {id:'da2_action_time_incl',text:'action',type:'string',width:400,'addEvent':false},
                    {id:'da2_data',text:'data',type:'string',width:150},
                    {id:'da2_app',text:'apps',type:'string',width:150}
                ],
            jsonUrl:global['AuditUrl'],
            countUrl:global['AuditUrl']+'?return=count',
            width:'100%',
            height:200,
            postData:{},
            checkBoxAdded:true,
            callback:null,
            onDataLoad:null
        };

        $('#list_page').simasgrid(option);
    });
</script>
<style type="text/css">
#banner{
    text-align: center;
}
</style>
            
<!DOCTYPE html>
<html>
<head>
    <title>Release Version</title>
    <link rel="stylesheet" type="text/css" href="<?php echo config_item('global_instAssetsUrl');?>load_style/standard.css">
  <script src="<?php echo config_item('global_instAssetsUrl');?>load_script/jquery/jquery-1.11.3.min.js?creq=4b5f590443796801121a196b17222b"></script>
  <script src="<?php echo config_item('global_instAssetsUrl');?>load_script/jquery/jquery.utils.js?creq=4b5f590443796801121a196b17222b"></script>
  <script src="<?php echo config_item('global_instAssetsUrl');?>load_script/jquery/jquery.stringify.js?creq=4b5f590443796801121a196b17222b"></script>
</head>
<script type="text/javascript">
  var all_release = {
    'v0.0.1-SIT':['v0.0.1-SIT'
            ,'<div>\
              Versi 1\
              <ul>\
                <li>-</li>\
                <li>branch : 20200128_case_login</li>\
              </ul>\
            </div>'
            ,'open']
    ,'v0.0.2-SIT':['v0.0.2-SIT'
            ,'<div>\
              Versi 2\
              <ul>\
                <li>-</li>\
                <li>branch : 20200128_case_login</li>\
              </ul>\
            </div>'
            ,'close']
    ,'v0.0.3-SIT':['v0.0.3-SIT'
            ,'<div>\
              Versi 3\
              <ul>\
                <li>-</li>\
                <li>branch : 20200128_case_login</li>\
              </ul>\
            </div>'
            ,'merged']
  }
</script>
<body>
    <section class="block_release page0_box1">
    <h1 class="page1_box5">Release Version <?php echo (!empty($_GET['tag'])?'- '.$_GET['tag']:''); ?></h1>
      <div class="list_release page0_box2">
        <label><a href="#" target="_blank">version test</a></label>
        <div class="detail_release page0_box3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
        <div class="status_release page0_box4">Open</div>
      </div>
      <div class="list_release page0_box2">
        <label><a href="#">version test</a></label>
        <div class="detail_release page0_box3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
        <div class="status_release page0_box4">Open</div>
      </div>
    </section>
</body>

<script type="text/javascript">
  $(window).ready(function(){

      $.fn.customprocess = function (methodOrOptions) {

          if(!$.prflutils) {alert('profile utils is not loaded'); console.log('profile utils is not loaded'); return false;}
          $.extend({prflcustom:true});

          var currArgs = arguments;
          var result = null;

          // this context yg di kirim kemari hanya dari object generate
          var currAPI = {
              showMe:function(){
                // console.log(this);
              }

          }
          // this context yg di kirim kemari hanya dari object generate
          function initiate(domObj){
                // assign to curr_state
                this.domObj = domObj;
                this.data = {
                  curr_release:{}
                };
                this.config = {};
                this.domCollection = {
                  element:$(this.domObj).find('element')
                  ,list_release:$(this.domObj).find('.list_release')
                };
                this.getObj=function(){
                    return this;
                };

                this.init=function(){
                      // testing using tool
                      currAPI.showMe.apply(this);

                      if(this.data.tag && typeof this.data.all_release[this.data.tag] != 'undefined'){
                        this.data.all_release[this.data.tag][1] += '<br />';
                        this.data.all_release[this.data.tag][1] += '<a href="<?php echo config_item('global_instMainUrl');?>redirect/?code='+$.randomString(6,'aA#!')+'">Lanjut ke Aplikasi</a>';
                        this.data.curr_release[this.data.tag] = this.data.all_release[this.data.tag];
                      }else{
                        this.data.curr_release = this.data.all_release;
                      }

                      $(this.domCollection.list_release).remove();
                      for(i in this.data.curr_release){
                        var list_release_div = $('<div>').appendTo(this.domObj);
                        $(list_release_div).addClass('list_release');
                        $(list_release_div).addClass('page0_box2');
                        var label_release = $('<label>').appendTo(list_release_div);
                        $(label_release).html('<a href="<?php echo config_item('global_instMainUrl');?>release/?tag='+i+'">'+i+'</a>');
                        var detail_release = $('<div>').appendTo(list_release_div);
                        $(detail_release).html(this.data.curr_release[i][1]);
                        $(detail_release).addClass('detail_release');
                        $(detail_release).addClass('page0_box3');
                        var status_release = $('<div>').appendTo(list_release_div);
                        $(status_release).text(this.data.curr_release[i][2]);
                        $(status_release).addClass('status_release');
                        $(status_release).addClass('page0_box4');
                        $(status_release).addClass(this.data.curr_release[i][2]);
                      }
                };

                this.func1=function(){};

                $(this.domCollection.list_release).remove();
          }

          // jquery proxy
          // $(chkbox).on('change',$.proxy(function(){
          //     $(this.contentTable).empty();
          // },this));
          // wrapping function
          // $(currTr).on('mouseout',(function(param){
          //         return function(e){
          //             param.func(param.arg1,param.arg2);
          //         }
          // })({func:this.callback,arg1:obj1,arg2:obj2}));

          this.each(function(){ // jika return bentuk dom untuk chaining, pake "return this.each(function(){})"
                // case jika inisiasi
                // case jika sudah inisiasi

                var tmpId = $(this).attr('id');
                if(!tmpId){
                  for(var i=0;i<10;i++){
                    if(!prflObj['customprocess'+i]){
                      $(this).attr('id','customprocess'+i);
                      tmpId = $(this).attr('id');
                      break;
                    }
                  }
                }
                
                if(!prflObj[tmpId]){
                  prflObj[tmpId] = new initiate(this);
                }

                /* rule */
                /*
                - $(dom).simascalendar(option); set config
                - $(dom).simascalendar('getobj'); do event with return result
                - $(dom).simascalendar('setevent'); do event
                - $(dom).simascalendar('setting','abc'); setter, set param
                - $(dom).simascalendar('setting'); getter, get param
                - kesimpulan : 
                - jika option : set param dulu baru init
                - jika fungsi : init dulu baru set
                - jika getter : init dulu baru get param
                - jika setter : set param dulu baru init
                */

                      if(typeof prflObj[tmpId][methodOrOptions] == 'function'){ // jika arg1 = fungsi
                        result = prflObj[tmpId][methodOrOptions](Array.prototype.slice.call( currArgs, 1 ));
                      }else if(typeof methodOrOptions == 'object'){ // jika arg1 = object
                        if(methodOrOptions.config) $.extend(prflObj[tmpId].config, methodOrOptions.config);
                        if(methodOrOptions.data) $.extend(prflObj[tmpId].data, methodOrOptions.data);
                        prflObj[tmpId].init();
                      }else if(typeof methodOrOptions == 'string'){ // jika arg1 = string, config getter dan setter hanya pada parameter config
                        if(currArgs[1] !== undefined){
                          prflObj[tmpId].config[methodOrOptions] = currArgs[1];
                          prflObj[tmpId].init();
                        }else{
                          prflObj[tmpId].init();
                          result = prflObj[tmpId].config[methodOrOptions];
                        }
                      }else{
                        prflObj[tmpId].init();
                      }
          });
          return result;
      }
      
    $('.block_release').customprocess({'data':{'tag':"<?php echo (isset($_GET['tag'])?$_GET['tag']:""); ?>","all_release":all_release}});
  });

</script>
</html>

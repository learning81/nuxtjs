<!-- Full Width Column -->
<div class="content-wrapper">

	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        Top Navigation
	        <small>BreadCrumb</small>
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="#">Layout</a></li>
	        <li class="active">Top Navigation</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">
	      
	      <!-- Default box -->
	      <div class="box">
	        <div class="box-header with-border">
	          <h3 class="box-title">Title</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
	              <i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
	              <i class="fa fa-times"></i></button>
	          </div>
	        </div>
	        <div class="box-body">

				<div id='page_content'>
					
					<div class='panel' id='side-bar' style="">
					    <div class="judul-panel">Halaman Tersedia</div>
					    <div class="content-panel">

					    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					    </div>
					</div>

					<table class="table compact table-bordered table-striped">
			            <tr class="block_template">
			              <th width="200">Template</th>
			              <td class="detail_title"></td>
			              <td class="block_select2 page9_part1"><select class="side12 form-control" name="block_template"><option value="">&nbsp;</option></select></td>
			            </tr>
			            <tr class="block_kategori">
			              <th>Kategori</th>
			              <td class="detail_title"></td>
			              <td class="block_select2 page9_part1"><select class="side12 form-control" name="block_kategori"><option value="">&nbsp;</option></select></td>
			            </tr>
			            <tr class="block_subkategori">
			              <th>Sub Kategori</th>
			              <td class="detail_title"></td>
			              <td class="block_select2 page9_part1"><select class="side12 form-control" name="block_subkategori"><option value="">&nbsp;</option></select></td>
			            </tr>
			            <tr class="block_materi">
			              <th>Bidang</th>
			              <td class="detail_title"></td>
			              <td class="block_select2 page9_part1"><select class="side12 form-control" name="block_materi"><option value="">&nbsp;</option></select></td>
			            </tr>
			            <tr class="block_submateri">
			              <th>Sub Bidang</th>
			              <td class="detail_title"></td>
			              <td class="block_select2 page9_part1"><select class="side12 form-control" name="block_submateri"><option value="">&nbsp;</option></select></td>
			            </tr>
			        </table>				

					<div class='panel' id='side-bar' style="">
					    <div class="judul-panel">Halaman Tersedia</div>
					    <div class="content-panel">

					    	<div id='managed'></div>

					    </div>
					</div>


			        <button type="button" class="btn btn-info col-6 btn-outline-primary block float-right" data-toggle="modal" data-target="#modal" id="">
			            <i class="ft-plus-circle"></i> Tambah Data
			        </button>

				</div>

	        </div>
	        <!-- /.box-body -->
	        <div class="box-footer">
	          Footer
	        </div>
	        <!-- /.box-footer-->
	      </div>
	      <!-- /.box -->

	    </section>
	    <!-- /.content -->

	    <div class="modal animated slideInDown text-left" id="modal" tabindex="-1" role="dialog" aria-labelledby="modallabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h4 class="modal-title" id="myModalLabel1">Tambah Perusahaan</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body">
	                    <form action="#" method="post">
	                      <input type="hidden" name="filter_id">
	                      <div class="row">
	                        <div class="col-md-12">
	                          <div class="form-group">
	                            <label>Domain Perusahaan<span class="text-danger">*</span></label>
	                            <input type="text" class="form-control" name="channel" required>
	                          </div>
	                        </div>
	                      </div>
	                      <div class="row">
	                        <div class="col-md-12">
	                          <div class="form-group">
	                            <label>Nama Perusahaan<span class="text-danger">*</span></label>
	                            <input type="text" class="form-control" name="nama_perusahaan" required>
	                          </div>
	                        </div>
	                      </div>
	                      <div class="row">
	                        <div class="col-md-12">
	                          <div class="form-group">
	                            <label>Alamat<span class="text-danger">*</span></label>
	                            <input type="text" class="form-control" name="alamat" required>
	                          </div>
	                        </div>
	                      </div>
	                    </form>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
	                    <button type="button" class="btn btn-outline-primary btn-info btn-submit">Save changes</button>
	                </div>
	            </div>
	        </div>
	    </div>

</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
	$('#managed').simasgrid({
		limit:20,
		field:[
			{id:'dm_id',text:'ID',type:'string',width:50,'visibility':'hidden'}, //
			{id:'alias_dm_name',text:'SLUG',type:'string','sort':true},
			{id:'dm_order',text:'Urutan',type:'string','width':150,'sort':true},
			{id:'alias_dm_detail',text:'Detail',type:'string'},
			{id:'alias_dm_description',text:'Keterangan',type:'string',width:500,'addEvent':false},
			{id:'alias_dm_parent',text:'Parent',type:'string',width:200,'align':'center','addEvent':false},
		],
		jsonData:[{'id':'','nama':'akhyar'},{'id':'','nama':'faisal'},{'id':'','nama':'haikal'}],
		countData:1,
        callback:null,
        onDataLoad:null,
        postData:{}
	});
	$('#managed').simasgrid("resize",'100%');
	$('#managed').simasgrid("addNav",{
	  title:'Reload',
	  className:'navigation',
	  callback:function(simasGrid){
	        $(simasGrid).simasgrid('rebuildGrid');
	  }
	});
});
</script>
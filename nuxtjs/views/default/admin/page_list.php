<style type="text/css">
	#page_content{
		position: relative;
		width: 100%;
	}
	#page_content>#allpage{
		display: inline-block;
		box-sizing: border-box;
		width: 50%;
		vertical-align: top;
	}
		#page_content>#allpage select{
			width: 100%;
			min-height: 300px;
		}
	#page_content #detail{
		display: inline-block;
		box-sizing: border-box;
		width: 50%;
		vertical-align: top;
		padding: 0 10px 0 10px;
	}
	#detail label{
		vertical-align: top;
		width: 120px;
		float: left;
	}
	#managed{
		margin: 25px 0;
	}
</style>

<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

			<div id='page_content'>
				<div id='allpage' class='col-prfl-lg-6 col-prfl-md-12 col-prfl-sm-12 col-prfl-xs-12'>
					<div class='panel' id='side-bar' style="">
					    <div class="judul-panel">Halaman Tersedia</div>
					    <div class="content-panel">
					    	Semua pages yang tersedia<br />
					    	<?php if(!empty($allPages)):?>
					    		<select size=2 id="semuapages" style="width: 95%;">
					    		<?php foreach($allPages as $idxPage => $valPage):?>
					    			<option><?php echo $valPage;?></option>
					    		<?php endforeach;?>
					    		</select>
					    	<?php endif;?>
					    </div>
					</div>
				</div><!--
				--><div id='detail' class='col-prfl-lg-6 col-prfl-md-12 col-prfl-sm-12 col-prfl-xs-12'>
					<div class='panel' id='side-bar' style="">
					    <div class="judul-panel">Update Halaman</div>
					    <div class="content-panel">
					    	Detail<br />
					    	<input class="form-control" name='filter_dm_id' type='hidden' readonly />
					    	<label>ID</label><input class="form-control" name='dm_id' type='text' readonly /><br />
					    	<label>Slug</label><input class="form-control" name='dm_name' type='text' /><br />
					    	<label>Name Tampil</label><input class="form-control" name='dm_name_tmp' type='text' /><br />
					    	<label>Detail</label><input class="form-control" name='dm_detail' disabled type='text' /></span><br />
					    	<label>Active</label>
					    		<input type='radio' name='dm_active' value='1' /> Ya
					    		<input type='radio' name='dm_active' value='0' /> Tidak
					    	<br />
					    	<label>Order</label><input class="form-control" name='dm_order' type='text' /><br />
					    	<label>Parent Id</label><select class="form-control" name='dm_parent'></select><br />
					    	<label>Public</label>
					    		<input type='radio' name='dm_public' value='1' />  Ya
					    		<input type='radio' name='dm_public' value='0' /> Tidak
					    	<br />
					    	<label>Description</label><textarea class="form-control" name='dm_description'></textarea><br />
					    	<label>Fa-Icon</label><input class="form-control" name='dm_fawesome'></input><br />
					    	<br />
					    	<input class="btn btn-default" type='button' name='update' value='update' />
					    	<input class="btn btn-default" type='button' name='delete' value='delete' />
					    	<input class="btn btn-default" type='button' name='reset' value='reset' />
					    	
					    </div>
					</div>
				</div>
				<div class='panel c0l-12' id='side-bar' style="">
				    <div class="judul-panel">Management Halaman</div>
				    <div class="content-panel">
				    	<div id='managed'></div>
				    </div>
				</div>
			</div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#managed').simasgrid({
		limit:20,
		field:[
			{id:'dm_id',text:'ID',type:'string',width:150,'visibility':'hidden'}, //
			{id:'alias_dm_name',text:'SLUG',type:'string','sort':true},
			{id:'dm_order',text:'Urutan',type:'string','width':50,'sort':true},
			{id:'alias_dm_detail',text:'Detail',type:'string'},
			{id:'dm_domain',text:'Domain',type:'string'},
			{id:'alias_dm_description',text:'Keterangan',type:'string',width:500,'addEvent':false},
			{id:'alias_dm_parent',text:'Parent',type:'string',width:200,'align':'center','addEvent':false},
		],
        jsonUrl:global['MenuUrl']+'?bypass_access=true&filter_dm_domain=<?php echo config_item('base_url');?>&field=dm_id,dm_name,dm_order,dm_detail,dm_domain,dm_description,dm_parent',
        countUrl:global['MenuUrl']+'?bypass_access=true&filter_dm_domain=<?php echo config_item('base_url');?>&return=count',
        callback:function(tr,e){ // jika tr diklik
        	var id = $(tr).attr('id');
        	$.setAjax(global['MenuUrl'],'get',{'filter_dm_id':id,'return':'single','bypass_access':true},function(result){
				$.setAllInput('detail',result);
        	});
        },
        onDataLoad:function(tr,data){ // saat tr di load
        	var parent = $(tr).find('td').eq(4).text();
        	if(parent !== ''){
	        	$.setAjax(global['MenuUrl'],'get',{'filter_dm_id':parent,'return':'single','bypass_access':true,'field':'dm_name'},function(result){
	        		if(result){
						var fieldName = (global['ociconnect']?'dm_name'.toUpperCase():'dm_name'.toLowerCase());
		        		var parent = $(tr).find('td').eq(4).text(result[fieldName]);
	        		}
	        	});
        	}
        },
        postData:{'ordid':'dm_order'}
	});
	$('#managed').simasgrid("resize",'100%');
	$('#managed').simasgrid("addNav",{
	  title:'Reload',
	  className:'navigation',
	  callback:function(simasGrid){
	        $(simasGrid).simasgrid('rebuildGrid');
	  }
	});

	update_parent();

	$('#allpage select').on('change',function(){
		// reset semua input
		$('#detail').find("input[type=hidden], input[type=text], select, textarea").val("");

		// isikan data yg diklik
		var currVal = $(this).val();
		$('#detail').find("input[name=dm_detail]").val(currVal);

		$.setAjax(global['MenuUrl'],'get',{'path':currVal,'bypass_access':'true','return':'single'},function(result){
			$.setAllInput('detail',result);
		});
	});
	$('#detail input[name=update]').on('click',function(){
		var data = $.getAllInput('detail');
		data['bypass_access'] = true;
		data['dm_name_tmp'] = (data['dm_name_tmp']?data['dm_name_tmp']:data['dm_name']);
		data['dm_name_indonesia'] = (data['dm_name_indonesia']?data['dm_name_indonesia']:data['dm_name_tmp']);
		$.setAjax(global['MenuUrl'],'put',data,function(result){
			if(!result.state){
				$.setAjax(global['MenuUrl'],'post',data,function(result){
					update_parent();
					$('#managed').simasgrid("rebuildGrid");
				});
			}else{
				update_parent();
				$('#managed').simasgrid("rebuildGrid");
			} 
		});
	});
	$('#detail input[name=reset]').on('click',function(){
		var data = $.getAllInput('detail');
		var newData = {};
		for(var i in data){
			newData[(global['ociconnect']?i.toUpperCase().replace('FILTER_',''):i.toLowerCase().replace('filter_',''))] = '';
		}
		$.setAllInput('detail',newData);
	});
	$('#detail input[name=delete]').on('click',function(){
		var confirm = window.confirm("Akan menghapus halaman ini");
		if (confirm){
			var data = $.getAllInput('detail');
			data['bypass_access'] = true;
			$.setAjax(global['MenuUrl'],'delete',data,function(result){
				update_parent();
				$('#managed').simasgrid("rebuildGrid");
			});
		}
	});
});
function update_parent(){
	$.setAjax(global['MenuUrl'],'get',{'only_parent':true,'bypass_access':'true'},function(result){
		$('#detail select[name=dm_parent]').empty();
		$('#detail select[name=dm_parent]').append('<option value="">== Parent List ==</option');
		if(result && result.length>0){
			for(var i=0; i<result.length; i++){
				var parentId = (global['ociconnect']?'dm_id'.toUpperCase():'dm_id'.toLowerCase());
				var name = (global['ociconnect']?'dm_name'.toUpperCase():'dm_name'.toLowerCase());
				var detail = (global['ociconnect']?'dm_detail'.toUpperCase():'dm_detail'.toLowerCase());
				$('#detail select[name=dm_parent]').append('<option value="'+result[i][parentId]+'">'+result[i][name]+' ('+result[i][detail]+')'+'</option');
			}
		}
	});
}
$('#semuapages').select2();
</script>
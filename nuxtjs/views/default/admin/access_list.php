<style type="text/css">
	#user_content,#access_content{
		box-sizing: border-box;
		display: inline-block;
		vertical-align: top;
	}
	#user_content{
		width: 50%;
		padding-right: 10px;
	}
	#access_content{
		width: 50%;
	}
	#access_table{
		margin-bottom: 10px;
	}
	label{
		float: left;
		width: 150px;
		clear: left;
	}

	select[multiple],
	select[size] {
	  height: 200px;
	}
</style>

<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Top Navigation
        <small>Example 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

			<div class='panel col-prfl-12' id='side-bar' style="">
			    <div class="judul-panel">Daftar Akses</div>
			    <div class="content-panel">
			    	<div id='access_table'></div>
			    </div>
			</div>
			<div id='user_content' class='col-prfl-lg-6 col-prfl-md-12 col-prfl-sm-12'>
				<div class='panel' id='side-bar' style="">
				    <div class="judul-panel">Administrasi User</div>
				    <div class="content-panel">
				    	<input name='filter_du_id' type='hidden'/>
				    	<label>ID</label><input name='du_id' type='text' class="form-control" /><br />
				    	<label>Name</label><input name='du_name' type='text' class="form-control"/><br />
				    	<label>Password</label><input name='du_pswd' type='text' class="form-control"/><br />
				    	<label>Email</label><input name='du_email' type='text' class="form-control"/><br />
				    	<label>Role</label><select name='du_role' type='text' class="form-control" style="height:150px;" multiple></select><br />
				    	<input type='button' name='update' value='update' class="btn btn-default"/>
				    	<input type='button' name='delete' value='delete' class="btn btn-default"/>
				    	<input type='button' name='reset' value='reset' class="btn btn-default" />
				    	<div id='user_table'></div>
				    </div>
				</div>
			</div><!--
			--><div id='access_content' class='col-prfl-lg-6 col-prfl-md-12 col-prfl-sm-12'>
				<div class='panel' id='side-bar' style="">
				    <div class="judul-panel">Administrasi Akses</div>
				    <div class="content-panel">
				    	<label>Nama Akses</label><input name='da_access_name' type='text' class="form-control"/><br />
				    	<label>Alarm</label><input name='alarm_access' type='radio' value='1' />Ya <input name='alarm_access' type='radio' value='0' />Tidak<br />
				    	<label>Disable Header</label><select name='disable_header' class="form-control" style="height:200px;" multiple></select><br />
				    	<label>Folder App</label><input name='main_folder' type="text" class="form-control" /><br />
				    	<label>First Menu</label><select name='first_load_menu' class="form-control"/></select><br />
				    	<label>Page Access</label><select name='page_access' class="form-control" style="height:200px;" multiple /></select><br />
				    	<label>Child Unique</label><select name='child_unique' class="form-control" style="height:200px;" multiple>
				    		<?php if(!empty($arrUnique)):?>
					    		<?php foreach ($arrUnique as $idxUnique => $valUnique):?>
				    				<option value='<?php echo $valUnique;?>'><?php echo $valUnique;?></option>
					    		<?php endforeach;?>
				    		<?php endif;?>
				    	</select><br />
				    	<label>Panel User</label><select name='panel_user' class="form-control" style="height:200px;" multiple /></select><br />
				    	<label>Group App</label><input name='group_app' type='text' class="form-control"/><br />
				    	<label>User Role</label><input name='user_role_member' type='text' class="form-control" disabled /><br />
				    	<label>Jenis App</label><select name='app_type' class="form-control">
				    		<option value=''>== DAFTAR APPLIKASI ==</option>
				    		<?php if(!empty($allApp)):?>
					    		<?php foreach ($allApp as $idxApp => $valApp):?>
				    				<option value='<?php echo $idxApp;?>'><?php echo $valApp;?></option>
					    		<?php endforeach;?>
				    		<?php endif;?>
				    	</select><br />
				    	<label>Maintenance</label><input name='site_maintenance' type='radio' value='1' />Ya <input name='site_maintenance' type='radio' value='0' />Tidak <input name='site_maintenance' type='radio' value='2' />Semua APP<br />
				    	<label>Bypass User Access</label><input name='bypass_user' type='radio' value='1' />Ya <input name='bypass_user' type='radio' value='0' />Tidak<br />
				    	*<i>dibutuhkan pada saat codingan</i><br /><label>Unique Name</label><input name='unique_name' class="form-control" type='text' disabled /><br />
				    	<label>Desc App</label><input name='da_description1' type='text' class="form-control"/><br />
				    	<label>Desc Unique</label><textarea name='da_description2' class="form-control"></textarea><br />
				    	<input type='button' name='update' value='update' class="btn btn-default"/>
				    	<input type='button' name='delete' value='delete' class="btn btn-default"/>
				    	<input type='button' name='reset' value='reset' class="btn btn-default"/>
				    	<input type='button' name='rebuild' value='rebuild app param' class="btn btn-default"/>
				    	<br /><a href='<?php echo config_item('global_instAdminUrl').'page_list';?>'>Administrasi Halaman >></a>
				    </div>
				</div>
			</div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function(){
	/*
	 * Process User
	 */
		$('#user_table').simasgrid({
			jsonUrl: global['UserUrl']+'?bypass_access=true&bypass_instance=true&limit=false&tq=<?php echo TRACECODE;?>',
			countUrl: global['UserUrl']+'?return=count&bypass_access=true&bypass_instance=true&limit=false&tq=<?php echo TRACECODE;?>',
			width: '100%',
			callback:function(tr,e){ // jika tr diklik
	        	var id = $(tr).attr('id');
	        	$.setAjax(global['UserUrl'],'get',{'filter_du_id':id,'return':'single','bypass_access':true,'bypass_instance':true},function(result){
                    $.setAllInput('user_content',result);
	        		$('#user_content input[name=du_pswd]').val('');
	        	});
			},
		 	field:[
		 	        {id:'du_id',text:'Agent Id',type:'string',width:100,'align':'center','sort':true}, //,'visibility':'hidden'
		 	        {id:'alias_du_name',text:'Nama',type:'string',width:150,'align':'center','sort':true},
		 	        {id:'email',text:'Email',type:'string',width:300,'align':'center'},
		 	        {id:'facebook',text:'Facebook ID',type:'string',width:300,'align':'center'},
		 	        {id:'role_id',text:'Role Id',type:'string',width:200,'align':'center'}
		 	    ],
		 	postData: null
		});
		$('#user_table').simasgrid("addNav",{
		  title:'Reload',
		  className:'navigation',
		  callback:function(simasGrid){
		        $(simasGrid).simasgrid('rebuildGrid');
		  }
		});
		$('#user_content input[name=update]').on('click',function(){
			var data = $.getAllInput('user_content');
			data['bypass_access'] = true;
			data['bypass_instance'] = true;
			data['tq'] = '<?php echo TRACECODE;?>';
			$.setAjax(global['UserUrl'],'put',data,function(result){
				if(!result.state){
					$.setAjax(global['UserUrl'],'post',data,function(result){
						if(result.state){
							alert('data user berhasil diperbaharui');
							$('#user_table').simasgrid('rebuildGrid');
						}
					});
				}else{
					$('#user_table').simasgrid('rebuildGrid');
				}
			});
		});
		$('#user_content input[name=delete]').on('click',function(){
			var confirm = window.confirm("Akan menghapus user");
			if (confirm){
				var data = $.getAllInput('user_content');
				// hapus akses
				data['bypass_access'] = true;
				data['bypass_instance'] = true;
				data['tq'] = '<?php echo TRACECODE;?>';
				$.setAjax(global['UserUrl'],'delete',data,function(result){
					if(result.state){
						alert('data user berhasil dihapus');
					}
					$('#access_table').simasgrid('rebuildGrid');
				});
				$('#user_table').simasgrid('rebuildGrid');
			}
		});
		$('#user_content input[name=reset]').on('click',function(){
			var data = $.getAllInput('user_content');
			var newData = {};
			for(var i in data){
				newData[(global['ociconnect']?i.toUpperCase().replace('FILTER_',''):i.toLowerCase().replace('filter_',''))] = '';
			}
			$.setAllInput('user_content',newData);
		});
		$.setAjax(global['AccessUrl'],'get',{'bypass_access':'true','filter_da_rule_access':'user_role_member','tq':'<?php echo TRACECODE;?>'},function(result){
			$('#user_content select[name=du_role]').empty();
			if(result && result.length>0){
				$('#user_content>select[name=du_role]').append('<option value="0">Untuk Super Admin</option');
				for(var i=0; i<result.length; i++){
					var id = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
					var value = (global['ociconnect']?'da_access_name'.toUpperCase():'da_access_name'.toLowerCase());
					$('#user_content select[name=du_role]').append('<option value="'+result[i][id]+'">'+result[i][id]+' ('+result[i][value]+')'+'</option');
				}
			}
		});

	/*
	 * Process Akses
	 */
		$('#access_table').simasgrid({
		 	jsonUrl: global['AccessUrl']+'?filter_da_rule_access=unique_name&bypass_access=true&filter_da_domain=<?php echo config_item('base_url');?>&tq=<?php echo TRACECODE;?>',
		 	countUrl: global['AccessUrl']+'?return=count&bypass_access=true&filter_da_rule_access=unique_name&filter_da_domain=<?php echo config_item('base_url');?>&tq=<?php echo TRACECODE;?>',
		 	width: '100%',
		 	postData: null,
		 	callback: function(tr,e){
		 		var accessName = $(tr).attr('id');
		 		var field = (global['ociconnect']?'da_access_name'.toUpperCase():'da_access_name'.toLowerCase());
		 		var data = {};
		 		data[field] = accessName;
		 		data['tq'] = '<?php echo TRACECODE;?>';
				$.setAllInput('access_content',data);

		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'alarm_access','bypass_access':'true','return':'single'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'alarm_access'.toUpperCase():'alarm_access'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'disable_header','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'disable_header'.toUpperCase():'disable_header'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = (result[value]?result[value]:null);
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'main_folder','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'main_folder'.toUpperCase():'main_folder'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'first_load_menu','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'first_load_menu'.toUpperCase():'first_load_menu'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'page_access','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'page_access'.toUpperCase():'page_access'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = (result[value]?result[value]:null);
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'child_unique','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'child_unique'.toUpperCase():'child_unique'.toLowerCase());
 			 		var data = {};
	 			 	data[field] = (result && result[value]?result[value]:null);
	 				$.setAllInput('access_content',data);
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'panel_user','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'panel_user'.toUpperCase():'panel_user'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = (result[value]?result[value]:null);
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'group_app','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'group_app'.toUpperCase():'group_app'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'site_maintenance','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'site_maintenance'.toUpperCase():'site_maintenance'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'bypass_user','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'bypass_user'.toUpperCase():'bypass_user'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'user_role_member','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var field = (global['ociconnect']?'user_role_member'.toUpperCase():'user_role_member'.toLowerCase());
 			 		var data = {};
 			 		data[field] = (result == null ?'':result[value]);
 					$.setAllInput('access_content',data);
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'app_type','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var value2 = (global['ociconnect']?'da_description'.toUpperCase():'da_description'.toLowerCase());
		 			var field = (global['ociconnect']?'app_type'.toUpperCase():'app_type'.toLowerCase());
		 			var field2 = (global['ociconnect']?'da_description1'.toUpperCase():'da_description1'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 			 		data[field2] = result[value2];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 		$.setAjax(global['AccessUrl'],'get',{'filter_da_access_name':accessName,'filter_da_rule_access':'unique_name','bypass_access':'true','return':'single','tq':'<?php echo TRACECODE;?>'},function(result){
		 			var value = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
		 			var value2 = (global['ociconnect']?'da_description'.toUpperCase():'da_description'.toLowerCase());
		 			var field = (global['ociconnect']?'unique_name'.toUpperCase():'unique_name'.toLowerCase());
		 			var field2 = (global['ociconnect']?'da_description2'.toUpperCase():'da_description2'.toLowerCase());
 			 		var data = {};
 			 		if(result !== null){
	 			 		data[field] = result[value];
	 			 		data[field2] = result[value2];
	 					$.setAllInput('access_content',data);
 			 		}
		 		});
		 	},
		 	field:[
		 	        {id:'access_name',text:'Nama Akses',type:'string',width:300,'align':'left'}, //,'visibility':'hidden'
		 	        {id:'rule_access',text:'Type',type:'string',visibility:'hidden'},
		 	        {id:'rule_value',text:'Nama Unik',type:'string',width:500,'align':'center'}
		 	    ]
		});
		$('#access_table').simasgrid("addNav",{
		  title:'Reload',
		  className:'navigation',
		  callback:function(simasGrid){
		        $(simasGrid).simasgrid('rebuildGrid');
		  }
		});

		$('#access_content input[name=update]').on('click',function(){

				var data = $.getAllInput('access_content');
				console.log(data);
				var access = data['da_access_name'];
				if(access == ''){
					alert('no akses defined');
					return;
				}

				// update alarm akses
				if(data['alarm_access'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'alarm_access',
			    		'da_rule_value':data['alarm_access'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'alarm_access',
			    			'da_rule_value':data['alarm_access'],
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data alaram berhasil ditambahkan');
			    			}
			    		});
			    	});
				}
				// update disable header
				if(data['disable_header'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'disable_header',
			    		'da_rule_value':data['disable_header'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'disable_header',
			    			'da_rule_value':data['disable_header'],
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data disable header berhasil ditambahkan');
			    			}
			    		});
			    	});
				}
				// update main folder path
				console.log({
			    			'da_access_name':access,
			    			'da_rule_access':'main_folder',
			    			'da_rule_value':data['main_folder'],
			    			'bypass_access':true,
			    			'tq':'<?php echo TRACECODE;?>'
			    		});
				if(data['main_folder'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'main_folder',
			    		'da_rule_value':data['main_folder'],
			    		'bypass_access':true,
			    		'tq':'<?php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'main_folder',
			    			'da_rule_value':data['main_folder'],
			    			'bypass_access':true,
			    			'tq':'<?php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data main folder berhasil ditambahkan');
			    			}
			    		});
			    	});
				}
				// update first load menu
				if(data['first_load_menu'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'first_load_menu',
			    		'da_rule_value':data['first_load_menu'],
			    		'bypass_access':true,
			    		'tq':'<?php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'first_load_menu',
			    			'da_rule_value':data['first_load_menu'],
			    			'bypass_access':true,
			    			'tq':'<?php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data first load menu berhasil ditambahkan');
			    			}
			    		});
			    	});
				}
				// update page access
				if(data['page_access'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'page_access',
			    		'da_rule_value':data['page_access'],
			    		'bypass_access':true,
			    		'tq':'<?php echo TRACECODE;?>'
			    	},function(result){
			    			$.setAjax(global['AccessUrl'],'post',{
			    				'da_access_name':access,
			    				'da_rule_access':'page_access',
			    				'da_rule_value':data['page_access'],
			    				'bypass_access':true,
			    				'tq':'<?php echo TRACECODE;?>'
			    			},function(result){
			    				if(result.state){
			    					alert('data page access berhasil ditambahkan');
			    				}
			    			});
			    	});
				}
				// update page access
				if(data['child_unique'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'child_unique',
			    		'da_rule_value':data['child_unique'],
			    		'bypass_access':true,
			    		'tq':'<?php echo TRACECODE;?>'
			    	},function(result){
			    			$.setAjax(global['AccessUrl'],'post',{
			    				'da_access_name':access,
			    				'da_rule_access':'child_unique',
			    				'da_rule_value':data['child_unique'],
			    				'bypass_access':true,
			    				'tq':'<?php echo TRACECODE;?>'
			    			},function(result){
			    				if(result.state){
			    					alert('data page access berhasil ditambahkan');
			    				}
			    			});
			    	});
				}
				// update panel user
				if(data['panel_user'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'panel_user',
			    		'da_rule_value':data['panel_user'],
			    		'bypass_access':true,
			    		'tq':'<?php echo TRACECODE;?>'
			    	},function(result){
			    			$.setAjax(global['AccessUrl'],'post',{
			    				'da_access_name':access,
			    				'da_rule_access':'panel_user',
			    				'da_rule_value':data['panel_user'],
			    				'bypass_access':true,
			    				'tq':'<php echo TRACECODE;?>'
			    			},function(result){
			    				if(result.state){
			    					alert('data panel user berhasil ditambahkan');
			    				}
			    			});
			    	});
				}
				// update group akses
				if(data['group_app'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'group_app',
			    		'da_rule_value':data['group_app'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
		    			$.setAjax(global['AccessUrl'],'post',{
		    				'da_access_name':access,
		    				'da_rule_access':'group_app',
		    				'da_rule_value':data['group_app'],
		    				'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
		    			},function(result){
		    				if(result.state){
		    					alert('data group akses berhasil ditambahkan');
		    				}
		    			});
			    	});
				}
				// update status maintenance
				if(data['bypass_user'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'bypass_user',
			    		'da_rule_value':data['bypass_user'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'bypass_user',
			    			'da_rule_value':(data['bypass_user']>0?1:0),
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data bypass user berhasil ditambahkan');
			    			}
			    		});
			    	});
				}
				// update status maintenance
				if(data['site_maintenance'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'site_maintenance',
			    		'da_rule_value':data['site_maintenance'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'site_maintenance',
			    			'da_rule_value':(data['site_maintenance']>0?1:0),
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){

			    				alert('data status maintenance berhasil ditambahkan');

			    				if(data['site_maintenance'] > 1){
			    					$.setAjax(global['AccessUrl'],'get',{
			    						'filter_da_access_name':access,
			    						'filter_da_rule_access':'app_type',
			    						'bypass_access':true,
			    						'field':'da_rule_value',
			    						'return':'single',
			    						'tq':'<php echo TRACECODE;?>'
			    					},function(result){
			    						var field = (global['ociconnect']?'da_rule_value'.toUpperCase():'da_rule_value'.toLowerCase());
			    						if(result && result[field]){
			    							$.setAjax(global['AccessUrl']+'maintain_app/'+result[field],'get',{'tq':'<?php echo TRACECODE;?>'},function(result){
			    							});
			    						}
			    					});
			    				}

			    			}
			    		});
			    	});
				}
				// update user role id dari simbak_user
				// ada ga' ada data harus dikirim untuk di generate id baru
				// if(data['user_role_member'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'user_role_member',
			    		'da_rule_value':data['user_role_member'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'user_role_member',
			    			'da_rule_value':data['user_role_member'],
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data user role id berhasil ditambahkan');
			    			}
			    		});
			    	});
				// }
				// update app type id
				// if(data['app_type'] != ''){
			    	$.setAjax(global['AccessUrl'],'delete',{
			    		'filter_da_access_name':access,
			    		'filter_da_rule_access':'app_type',
			    		'da_rule_value':data['app_type'],
			    		'da_description':data['da_description1'],
			    		'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
			    	},function(result){
			    		$.setAjax(global['AccessUrl'],'post',{
			    			'da_access_name':access,
			    			'da_rule_access':'app_type',
			    			'da_rule_value':data['app_type'],
			    			'da_description':data['da_description1'],
			    			'bypass_access':true,
			    			'tq':'<php echo TRACECODE;?>'
			    		},function(result){
			    			if(result.state){
			    				alert('data app type id berhasil ditambahkan');
			    			}
			    		});
			    	});
				// }
				// update unique akses
		    	$.setAjax(global['AccessUrl'],'delete',{
		    		'filter_da_access_name':access,
		    		'filter_da_rule_access':'unique_name',
		    		'da_rule_value':access,
		    		'da_description':data['da_description2'],
		    		'bypass_access':true,
			    	'tq':'<php echo TRACECODE;?>'
		    	},function(result){
	    			$.setAjax(global['AccessUrl'],'post',{
	    				'da_access_name':access,
	    				'da_rule_access':'unique_name',
	    				'da_rule_value':access,
	    				'da_description':data['da_description2'],
	    				'bypass_access':true,
			    		'tq':'<php echo TRACECODE;?>'
	    			},function(result){
	    				if(result.state){
	    					alert('data unique akses berhasil ditambahkan');
	    				}
	    			});
		    	});
		});
		$('#access_content input[name=delete]').on('click',function(){
			var confirm = window.confirm("Akan menghapus akses");
			if (confirm){
				var data = $.getAllInput('access_content');
				var access = data['da_access_name'];
				if(access == ''){
					alert('no akses defined');
					return;
				}

				// hapus akses
				$.setAjax(global['AccessUrl'],'delete',{
					'filter_da_access_name':access,
					'bypass_access':true,
			    	'tq':'<php echo TRACECODE;?>'
				},function(result){
					if(result.state){
						alert('data akses berhasil dihapus');
					}
					$('#access_table').simasgrid('rebuildGrid');
				});
			}
		});
		$('#access_content input[name=reset]').on('click',function(){
			var data = $.getAllInput('access_content');
			var newData = {};
			for(var i in data){
				newData[(global['ociconnect']?i.toUpperCase().replace('FILTER_',''):i.toLowerCase().replace('filter_',''))] = '';
			}
			$.setAllInput('access_content',newData);
		});
		$('#access_content input[name=rebuild]').on('click',function(){
			$.setAjax(global['AccessUrl']+'rebuild_global_param','get',{'tq':'<?php echo TRACECODE;?>'},function(result){
				$.windowOpen(global['instAdminUrl'],'_top');
			});
		});

		// data untuk select box di input akses
		$.setAjax(global['MenuUrl'],'get',{'bypass_access':'true','limit':'false','tq':'<?php echo TRACECODE;?>'},function(result){
			$('#access_content select[name=first_load_menu]').empty();
			$('#access_content select[name=page_access]').empty();
			$('#access_content select[name=disable_header]').empty();
			if(result && result.length>0){
				for(var i=0; i<result.length; i++){
					var name = (global['ociconnect']?'dm_name'.toUpperCase():'dm_name'.toLowerCase());
					var detail = (global['ociconnect']?'dm_detail'.toUpperCase():'dm_detail'.toLowerCase());
					$('#access_content select[name=first_load_menu]').append('<option value="'+result[i][name]+'">'+result[i][name]+' ('+result[i][detail]+')'+'</option');
					$('#access_content select[name=page_access]').append('<option value="'+result[i][name]+'">'+result[i][name]+' ('+result[i][detail]+')'+'</option');
					$('#access_content select[name=disable_header]').append('<option value="'+result[i][name]+'">'+result[i][name]+' ('+result[i][detail]+')'+'</option');
				}
			}
		});
		// data untuk select box di input akses
		$.setAjax(global['PanelUrl'],'get',{'tq':'<?php echo TRACECODE;?>'},function(result){
			$('#access_content select[name=panel_user]').empty();
			if(result && result.length>0){
				for(var i=0; i<result.length; i++){
					$('#access_content select[name=panel_user]').append('<option value="'+result[i]+'">'+result[i]+'</option');
				}
			}
		});
});
</script>
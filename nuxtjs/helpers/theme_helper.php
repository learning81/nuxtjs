<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 *
 * @package	CodeIgniter
 * @author	Akhyar Azni
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		akhyar
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('get_template'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function get_template($testTheme=NULL,$testPage,$defPage=NULL){

		$CI =& get_instance();
		$exists = FALSE;
		$currPath = NULL;

		// 1st priority
		if(!empty($testTheme)){
			if(file_exists(str_replace('\\','/',VIEWPATH).$testTheme.'/'.$testPage.'.php')){
				$exists = TRUE;
				$currPath = $testTheme.'/'.$testPage;
			}elseif(file_exists(str_replace('\\','/',VIEWPATH).$testTheme.'/'.$defPage.'.php')){
				$exists = TRUE;
				$currPath = $testTheme.'/'.$defPage;
			}
		}

		// 5th priority
		if(!empty($_SESSION['access_domain'])){
			if(file_exists(str_replace('\\','/',VIEWPATH).$_SESSION['access_domain'].'/'.$testPage.'.php')){
				$exists = TRUE;
				$currPath = $_SESSION['access_domain'].'/'.$testPage;
			}elseif(file_exists(str_replace('\\','/',VIEWPATH).$_SESSION['access_domain'].'/'.$defPage.'.php')){
				$exists = TRUE;
				$currPath = $_SESSION['access_domain'].'/'.$defPage;
			}
		}

		// default theme
		if(!$exists){
			$testTheme = 'default';
			if(file_exists(str_replace('\\','/',VIEWPATH).$testTheme.'/'.$testPage.'.php')){
				$exists = TRUE;
				$currPath = $testTheme.'/'.$testPage;
			}elseif(file_exists(str_replace('\\','/',VIEWPATH).$testTheme.'/'.$defPage.'.php')){
				$exists = TRUE;
				$currPath = $testTheme.'/'.$defPage;
			}
		}

		return $currPath;
	}
}

if ( ! function_exists('load_template'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function load_template($viewPath,$arrParam=array(),$headerMenu=FALSE){

		$CI =& get_instance();

		$realPath = str_replace('\\','/',VIEWPATH.$viewPath).'.php';

		$_SESSION['default_language'] = 'english';
		$newPath = explode('/',$viewPath);
		if(array_key_exists('default_language',$_SESSION)) array_splice( $newPath, (count($newPath)-1), 0,$_SESSION['default_language']);
		$viewPath2 = implode('/',$newPath);
		$realPath2 = str_replace('\\','/',VIEWPATH.$viewPath2).'.php';

		$currPath = (file_exists($viewPath2)?$viewPath2:$viewPath);

		if($headerMenu){
			$public = ($CI->user_data->check_user_session() ? FALSE : TRUE); // apakah anonymous (blm login)

			// jika belum login, cari semua halaman yang bisa diakses publik dari database
			// jika sudah login, cari semua halaman yang bisa diakses oleh user akses sekarang
			$arrParams = array(
				'bypass_access'=>TRUE,
				'filter_dm_active'=>1,
				'filter_dm_domain'=>config_item('base_url'),
				'include_public'=>$public,
				'include_child'=>TRUE,
				'disable_header'=>TRUE,
				'ordid'=>'dm_order',
				'ordtype'=>'asc',
				'only_parent'=>TRUE,
				'disable_header'=>TRUE,
				'include_child'=>TRUE,
				'ordid'=>'dm_order',
				'ordtype'=>'asc',
				'limit'=>FALSE
			);
			$mainmenu = $CI->main_menu->get_detail($arrParams);
			$currMerchant = config_item('route_merchant');
			if(!empty($mainmenu)){
				foreach ($mainmenu as $key => $value) {
					if(strpos($value->dm_detail,'merchant') > -1 && !empty($currMerchant)){
						$mainmenu[$key]->dm_detail = str_replace('merchant','merchant/'.$currMerchant.'/',$value->dm_detail);
						$mainmenu[$key]->url = str_replace('merchant','merchant/'.$currMerchant.'/',$value->url);
					}
				}
			}

			// tambahkan semua page yang belum ditambahkan di database,
			// sebagai aware ada page yang belum dikasih akses
			$otherPage = $CI->main_menu->get_all_pages(FALSE);
			if(!empty($otherPage)){
				$newOtherPage = array();
				foreach ($otherPage as $key => $value) {
					$items = explode('/',$value);
					if(strpos($value,'merchant') > -1 && !empty($currMerchant)){
						$part = explode('/',$value);
						$part[0] = 'merchant/'.$currMerchant;
						array_push($newOtherPage,implode('/',$part));
					}else{
						array_push($newOtherPage,$value);
					}
				}
				$otherPage = $newOtherPage;
			}

			if(empty($arrParam)) $arrParam = [];
			$arrParam['mainmenu'] = $mainmenu;
			$arrParam['otherPage'] = $otherPage;
			$CI->load->view($currPath,$arrParam);
		}else{
			$CI->load->view($currPath,$arrParam);
		}
	}
}

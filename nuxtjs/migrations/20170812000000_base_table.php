<?php
// http://localhost/dropbox/codeigniter/%5bguide%5d/database/forge.html?highlight=dbforge#id7
// http://localhost/dropbox/codeigniter/%5bguide%5d/libraries/migration.html?highlight=migrate
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Base_table extends CI_Migration {

        public function up(){
                $this->db->query("TRUNCATE `default_access`");
                $this->db->query("TRUNCATE `default_mainmenu`");
        }

        public function down(){
                $this->db->query("TRUNCATE `default_access`");
                $this->db->query("TRUNCATE `default_mainmenu`");
        }

}

/*
-- Adminer 4.2.3-dev MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP FUNCTION IF EXISTS `next_val`;;
CREATE FUNCTION `next_val`(keyname varchar(25)) RETURNS int(11)
begin

       declare curr_val int default 0 ;
       declare next_val int default 1 ;
       declare key_exists tinyint default 0 ;
       declare source_max int default 0 ;

       -- LOCK TABLES default_increment WRITE;

       -- check apakah data sudan ada di table increment
       select count(*) into key_exists from default_increment where di_key=keyname limit 1 ;
       IF key_exists > 0 THEN
         select di_max into curr_val from default_increment where di_key=keyname limit 1 ;
         select di_next into next_val from default_increment where di_key=keyname limit 1 ;
         update default_increment set di_max = next_val, di_next = (next_val + 1) where di_key=keyname ;
       ELSE
         select auto_increment into source_max from INFORMATION_SCHEMA.TABLES WHERE table_name = keyname order by update_time desc limit 1;
         insert into default_increment (di_key,di_max,di_next) values (keyname,source_max,(source_max+1)) ;
         select di_max into next_val from default_increment where di_key=keyname limit 1 ;
       END IF ;

       -- UNLOCK TABLES;

       return next_val ;
      end;;

      DELIMITER ;

DROP TABLE IF EXISTS `app_config`;
CREATE TABLE `app_config` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_param` varchar(250) DEFAULT NULL,
  `app_value` text,
  PRIMARY KEY (`app_id`),
  UNIQUE KEY `app_param` (`app_param`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `default_access`;
CREATE TABLE `default_access` (
  `da_access_name` varchar(25) DEFAULT NULL,
  `da_rule_access` varchar(25) DEFAULT NULL,
  `da_rule_value` varchar(1000) DEFAULT NULL,
  `da_description` varchar(200) DEFAULT NULL,
  UNIQUE KEY `ma_idx2` (`da_access_name`,`da_rule_access`),
  KEY `ma_idx1` (`da_access_name`),
  KEY `ma_idx3` (`da_description`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `default_mainmenu`;
CREATE TABLE `default_mainmenu` (
  `dm_id` int(11) NOT NULL AUTO_INCREMENT,
  `dm_name` varchar(50) DEFAULT NULL,
  `dm_name_indonesia` varchar(100) DEFAULT NULL,
  `dm_name_english` varchar(100) DEFAULT NULL,
  `dm_name_tmp` varchar(100) DEFAULT NULL,
  `dm_detail` varchar(250) DEFAULT NULL,
  `dm_active` tinyint(4) DEFAULT NULL,
  `dm_order` smallint(6) DEFAULT NULL,
  `dm_parent` int(11) DEFAULT NULL,
  `dm_description` varchar(50) DEFAULT NULL,
  `dm_public` tinyint(4) DEFAULT NULL,
  `dm_fawesome` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dm_id`),
  KEY `mm_idx1` (`dm_active`),
  KEY `mm_idx2` (`dm_parent`),
  KEY `mm_idx3` (`dm_public`),
  KEY `mm_idx5` (`dm_name`),
  KEY `mm_idx4` (`dm_detail`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `default_increment`;
CREATE TABLE `default_increment` (
  `di_id` int(11) NOT NULL AUTO_INCREMENT,
  `di_key` varchar(50) DEFAULT NULL,
  `di_max` int(11) DEFAULT NULL,
  `di_next` int(11) DEFAULT NULL,
  PRIMARY KEY (`di_id`),
  KEY `mm_idx1` (`di_key`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_merchant`;
CREATE TABLE `profile_merchant` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_code` varchar(150) DEFAULT NULL,
  `pm_name` varchar(150) DEFAULT NULL,
  `pm_slug` varchar(150) DEFAULT NULL,
  `pm_alamat` varchar(500) DEFAULT NULL,
  `pm_open` datetime DEFAULT NULL,
  PRIMARY KEY (`pm_id`),
  UNIQUE KEY `pm_code` (`pm_code`),
  UNIQUE KEY `pm_slug` (`pm_slug`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `sys_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `sys_param` varchar(250) NOT NULL,
  `sys_value` text DEFAULT NULL,
  PRIMARY KEY (`sys_id`),
  KEY `sys_param` (`sys_param`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `profile_page`;
CREATE TABLE `profile_page` (
  `pp_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `pp_merchant` int(11) DEFAULT NULL,
  `pp_title` varchar(150) DEFAULT NULL,
  `pp_slug` varchar(150) DEFAULT NULL,
  `pp_active` tinyint(4) DEFAULT NULL,
  `pp_detail` text DEFAULT NULL,
  `pp_postdate` datetime DEFAULT NULL,
  PRIMARY KEY (`pp_id`),
  KEY `pp_merchant` (`pp_merchant`),
  KEY `pp_slug` (`pp_slug`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2017-08-14 10:50:06
\ No newline at end of file
*/

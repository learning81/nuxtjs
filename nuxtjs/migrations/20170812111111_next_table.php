<?php
// http://localhost/dropbox/codeigniter/%5bguide%5d/database/forge.html?highlight=dbforge#id7
// http://localhost/dropbox/codeigniter/%5bguide%5d/libraries/migration.html?highlight=migrate
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Next_table extends CI_Migration {

        public function up(){
                $attributes = array('ENGINE' => 'InnoDB','COLLATE' => 'utf8_unicode_ci');

                $this->dbforge->add_field(array(
                        'sys_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'sys_param' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 250
                        )
                        ,'sys_value' => array(
                                'type' => 'TEXT'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('sys_id', TRUE);
                $this->dbforge->add_key('sys_param');
                $this->dbforge->create_table('system_config',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'du_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'du_name' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '150'
                                ,'null' => TRUE
                                ,'default' => 'default'
                        )
                        ,'du_pswd' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '50'
                                ,'null' => TRUE
                        )
                        ,'du_email' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '200'
                                ,'null' => TRUE
                        )
                        ,'du_facebook' => array(
                                'type' => 'BIGINT'
                                ,'null' => TRUE
                        )
                        ,'du_bdate' => array(
                                'type' => 'DATE'
                                ,'null' => TRUE
                        )
                        ,'du_hp1' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 15
                                ,'null' => TRUE
                        )
                        ,'du_bbmid' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 15
                                ,'null' => TRUE
                        )
                        ,'du_reactivate' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 15
                                ,'null' => TRUE
                        )
                        ,'du_reset' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 25
                                ,'null' => TRUE
                        )
                        ,'du_role' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 50
                                ,'null' => TRUE
                        )
                        ,'du_active' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'du_leader' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                        ,'du_last_login' => array(
                                'type' => 'DATETIME'
                                ,'null' => TRUE
                        )
                        ,'du_hobby' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'du_ktp' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'du_education' => array(
                                'type' => 'mediumint'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('du_id', TRUE);
                $this->dbforge->create_table('default_user',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'da_access_name' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 25
                        )
                        ,'da_rule_access' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 25
                        )
                        ,'da_rule_value' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 1000
                                ,'null' => TRUE
                        )
                        ,'da_description' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 200
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('da_access_name', TRUE);
                $this->dbforge->add_key('da_rule_access', TRUE);
                $this->dbforge->add_key('da_rule_access');
                $this->dbforge->add_key('da_description');
                $this->dbforge->create_table('default_access',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'dm_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'dm_name' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '100'
                                ,'null' => TRUE
                        )
                        ,'dm_name_indonesia' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '100'
                                ,'null' => TRUE
                        )
                        ,'dm_name_english' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '100'
                                ,'null' => TRUE
                        )
                        ,'dm_name_tmp' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '100'
                                ,'null' => TRUE
                        )
                        ,'dm_detail' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => '258'
                                ,'null' => TRUE
                        )
                        ,'dm_active' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'dm_order' => array(
                                'type' => 'SMALLINT'
                                ,'null' => TRUE
                        )
                        ,'dm_parent' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                        ,'dm_description' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 50
                                ,'null' => TRUE
                        )
                        ,'dm_public' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'dm_fawesome' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 50
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('dm_id', TRUE);
                $this->dbforge->add_key('dm_name');
                $this->dbforge->add_key('dm_active');
                $this->dbforge->add_key('dm_order');
                $this->dbforge->add_key('dm_parent');
                $this->dbforge->add_key('dm_public');
                $this->dbforge->create_table('default_mainmenu',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'da2_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'da2_userid' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                        ,'da2_inst_id' => array(
                                'type' => 'int'
                                ,'null' => TRUE
                        )
                        ,'da2_inst_type' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'da2_detail' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 260
                                ,'null' => TRUE
                        )
                        ,'da2_data' => array(
                                'type' => 'text'
                                ,'null' => TRUE
                        )
                        ,'da2_action' => array(
                                'type' => 'DATETIME'
                                ,'null' => TRUE
                        )
                        ,'da2_app' => array(
                                'type' => 'tinyint'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('da2_id', TRUE);
                $this->dbforge->add_key('da2_userid');
                $this->dbforge->add_key('da2_inst_id');
                $this->dbforge->create_table('default_audit',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'dc_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'dc_table' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'dc_slug' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'dc_title' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'dc_type' => array(
                                'type' => 'VARCHAR'
                                //,'constraint' => array('text','textarea','select','checkbox','radio')
                                ,'constraint' => 10
                                ,'null' => TRUE
                        )
                        ,'dc_multiple' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'dc_default' => array(
                                'type' => 'TEXT'
                                ,'null' => TRUE
                        )
                        ,'dc_show' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'dc_width' => array(
                                'type' => 'MEDIUMINT'
                                ,'null' => TRUE
                        )
                        ,'dc_t_type' => array(
                                'type' => 'VARCHAR'
                                // ,'constraint' => array('char','varchar','date','text','mediumint')
                                ,'constraint' => 10
                                ,'null' => TRUE
                        )
                        ,'dc_t_width' => array(
                                'type' => 'MEDIUMINT'
                                ,'null' => TRUE
                        )
                        ,'dc_order' => array(
                                'type' => 'MEDIUMINT'
                                ,'null' => TRUE
                        )
                        ,'dc_filter' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'dc_protected' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'dc_comment' => array(
                                'type' => 'TEXT'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('dc_id', TRUE);
                $this->dbforge->add_key('dc_table');
                $this->dbforge->add_key('dc_slug');
                $this->dbforge->add_key('dc_order');
                $this->dbforge->add_key('dc_show');
                $this->dbforge->add_key('dc_filter');
                $this->dbforge->create_table('default_column',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'di_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'di_key' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 25
                                ,'null' => TRUE
                        )
                        ,'di_max' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                        ,'di_next' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('di_id', TRUE);
                $this->dbforge->add_key('di_key');
                $this->dbforge->create_table('default_increment',TRUE,$attributes);

                $this->dbforge->add_field(array(
                        'pp_id' => array(
                                'type' => 'INT'
                                ,'constraint' => 5
                                ,'unsigned' => TRUE
                                ,'auto_increment' => TRUE
                        )
                        ,'pp_merchant' => array(
                                'type' => 'INT'
                                ,'null' => TRUE
                        )
                        ,'pp_title' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'pp_slug' => array(
                                'type' => 'VARCHAR'
                                ,'constraint' => 150
                                ,'null' => TRUE
                        )
                        ,'pp_active' => array(
                                'type' => 'TINYINT'
                                ,'null' => TRUE
                        )
                        ,'pp_detail' => array(
                                'type' => 'TEXT'
                                ,'null' => TRUE
                        )
                        ,'pp_postdate' => array(
                                'type' => 'DATETIME'
                                ,'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('pp_id', TRUE);
                $this->dbforge->add_key('pp_merchant');
                $this->dbforge->add_key('pp_slug');
                $this->dbforge->create_table('profile_page',TRUE,$attributes);

        }

        public function down(){
                $this->dbforge->drop_table('system_config',TRUE);
                $this->dbforge->drop_table('default_user',TRUE);
                $this->db->query("TRUNCATE `default_access`");
                $this->db->query("TRUNCATE `default_mainmenu`");
                $this->dbforge->drop_table('default_audit',TRUE);
                $this->dbforge->drop_table('default_column',TRUE);
                $this->dbforge->drop_table('default_increment',TRUE);
                $this->dbforge->drop_table('profile_page',TRUE);
        }

}
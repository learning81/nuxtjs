<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
$active_group = config_item('dbConnection');
$query_builder = TRUE;

$db['default']['dsn'] = '';
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'temporary';
$db['default']['password'] = 'temporary';
$db['default']['database'] = 'temporary';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = FALSE; //(ENVIRONMENT !== 'server_production');
$db['default']['cache_on'] = FALSE; // pake $this->db->cache_on() saja pada masing2 controller/module
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['encrypt'] = FALSE;
$db['default']['compress'] = FALSE;
$db['default']['stricton'] = FALSE;
$db['default']['failover'] = array();
$db['default']['save_queries'] = TRUE; //(ENVIRONMENT !== 'server_production');

$db['profile']['dsn'] = '';
$db['profile']['hostname'] = 'localhost';
$db['profile']['username'] = 'profile';
$db['profile']['password'] = 'profile';
$db['profile']['database'] = 'profile';
$db['profile']['dbdriver'] = 'mysqli';
$db['profile']['dbprefix'] = '';
$db['profile']['pconnect'] = FALSE;
$db['profile']['db_debug'] = FALSE; //(ENVIRONMENT !== 'server_production');
$db['profile']['cache_on'] = FALSE; // pake $this->db->cache_on() saja pada masing2 controller/module
$db['profile']['cachedir'] = '';
$db['profile']['char_set'] = 'utf8';
$db['profile']['dbcollat'] = 'utf8_general_ci';
$db['profile']['swap_pre'] = '';
$db['profile']['encrypt'] = FALSE;
$db['profile']['compress'] = FALSE;
$db['profile']['stricton'] = FALSE;
$db['profile']['failover'] = array();
$db['profile']['save_queries'] = TRUE; //(ENVIRONMENT !== 'server_production');

$db['tnsmain']['dsn'] = '';
$db['tnsmain']['hostname'] = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 128.21.22.31)(PORT = 1530))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = AJSDB)))';
$db['tnsmain']['username'] = 'dev';
$db['tnsmain']['password'] = 'linkdev';
$db['tnsmain']['database'] = '';
$db['tnsmain']['dbdriver'] = 'oci8';
$db['tnsmain']['dbprefix'] = '';
$db['tnsmain']['pconnect'] = TRUE;
$db['tnsmain']['db_debug'] = TRUE; //(ENVIRONMENT !== 'server_production');
$db['tnsmain']['cache_on'] = FALSE; // pake $this->db->cache_on() saja pada masing2 controller/module
$db['tnsmain']['cachedir'] = '';
$db['tnsmain']['char_set'] = 'utf8';
$db['tnsmain']['dbcollat'] = 'utf8_general_ci';
$db['tnsmain']['swap_pre'] = '';
$db['tnsmain']['encrypt'] = FALSE;
$db['tnsmain']['compress'] = FALSE;
$db['tnsmain']['stricton'] = FALSE;
$db['tnsmain']['failover'] = array();
$db['tnsmain']['save_queries'] = (ENVIRONMENT !== 'server_production');
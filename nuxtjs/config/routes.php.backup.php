<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// mplace = aazni.net
// toko = laboratorium-online / localhost
// situs toko : laboratorium-online.aazni.net
// situs toko : laboratorium-online.net
// situs toko : aazni.net/laboratorium-online
// situs toko : aazni.net/merchant/laboratorium-online/method1/method2
// konfig sistem : route_subdomain, route_instance, route_merhant

// load database for important thing
require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();

// $query = $db->get(sprintf("select da_rule_value from default_access where da_rule_access = '%s'",'domain'));
// $get_domain = $query->row(); // $result = $query->result();
// if(!empty($get_domain)){
// 		die('not empty');
// }else{
// 		die('empty');
// }
// var_dump(count($get_domain));exit;
// $allDomain = $this->access->get_all_domain();


$subfolder = $this->uri->segment(1,FALSE);
$route['default_controller'] = 'main'; // hanya menambahkan variable, tapi controller adalah $subfolder
if(file_exists(config_item('app_folder').'/controllers/'.ucfirst($subfolder).EXT) && strtolower($subfolder)!='merchant' && strtolower($subfolder)!='product'){ // rupanya subfolder ini adalah controller !!	tangkap parametetr merchant
	$route['default_controller'] = 'main'; // hanya menambahkan variable, tapi controller adalah $subfolder
}elseif(FALSE){ // !! tangkap parametetr merchant
	$route['default_controller'] = 'main'; // hanya menambahkan variable, tapi controller adalah $subfolder
}else{ // !! tangkap parameter merchant

	$domainStandard = 'aazni.net'; // induk domain apakah profile, marketplace, penganan atau ojekonline
	$this->config->set_item('home_site','http://'.$domainStandard); // config ini di set agar individu bisa akses mplace
	$lowerDomain = trim(strtolower($_SERVER['SERVER_NAME']));

	// mencari subdomain
	$subdomain = NULL;
	if(strpos($lowerDomain,$domainStandard) > -1){ // jika yg diload domain mplace, check subdomain dari subdomain sebenarnya
		$partItem = explode('.',$lowerDomain);
		$maxItem = count($partItem);
		$subDomainItem = $maxItem - 3;
		if(!empty($partItem[$subDomainItem]) && $partItem[$subDomainItem] != 'www'){
			$subdomain = $partItem[$subDomainItem];
		}
	}else{ // jika yg diload domain bukan utama, check subdomain dari bagian url terpanjang
		$partItem = explode('.',$lowerDomain);
		if(!empty($partItem)){
			foreach ($partItem as $key => $value) {
				$checkPath[strlen($value)] = $value;
			}
			$subdomain = $checkPath[max(array_keys($checkPath))];
		}
	}
	if(!empty($subdomain)) // produk yg tampil difilter oleh status akses subdomain
		$this->config->set_item('route_subdomain',$subdomain);

	// simpulkan merchant dari subdomain dan subfolder
	$merchant = (!empty($subdomain)?$subdomain:$subfolder);

	$query = $db->get(sprintf("select * from profile_merchant where lower(pm_slug) = '%s' or lower(pm_code) = '%s'",$merchant,$merchant));
	$merchant_detail = $query->row(); // $result = $query->result();

	// if merchant is exists then route = individu
	if(!empty($merchant_detail)){
		$this->config->set_item('route_instance','individu');
		$this->config->set_item('route_merchant',$merchant_detail->pm_slug);
		$this->config->set_item('merchant_id',$merchant_detail->pm_id);
		$this->config->set_item('merchant_code',$merchant_detail->pm_code);
		// jgn lupa merchant buat header sendiri dan di logo ngarahin ke merchant home
		$route['default_controller'] = 'merchant'; // di controller merchant method index, coba tangkap folder pertama
	}else{
		$this->config->set_item('route_instance','mplace');
		$route['default_controller'] = 'main';
	}

	// check apakah ada product yg di load
	if(!empty($subfolder) && strtolower($subfolder)!='merchant' && strtolower($subfolder)!='product'){ //  !! tangkap parametetr merchant
		require_once( BASEPATH .'database/DB'. EXT );
		$db =& DB();
		$query = $db->get(sprintf("select * from profile_product where lower(pp2_slug) = '%s' or lower(pp2_code) = '%s'",$subfolder,$subfolder));
		$product_detail = $query->row(); // $result = $query->result();
		if(!empty($product_detail)) $route['default_controller'] = 'product'; // di controller product method index, coba tangkap folder pertama

		// benerin path dengan redirect ulang
		$baseUrl = config_item('base_url');  // akses index.php

		// situs product : aazni.net/prd0001 => redirect ke aazni.net/product/prd0001
		// situs toko : laboratorium-online.aazni.net => tidak ada redirect
		// situs toko : laboratorium-online.net => tidak ada redirect
		// situs toko : aazni.net/laboratorium-online => akan redirect ke aazni.net/merchant/laboratorium-online/
		// tidak boleh akses aazni.net/laboratorium-online/method1/method2 karena redirect nggak bawa param
		// boleh akses aazni.net/merchant/laboratorium-online/method1/method2
		if(!empty($product_detail)){ // jika yg di load product, load ulang dengan path product yg sebenarnya dan hanya implikasi pada product::index
			die('a');
			header('location:'.$baseUrl.'product/'.$product_detail->pp2_slug);exit;
		}elseif(empty($subdomain)){ // dari mplace dan memiliki subfolder, load subfolder dari controller merchant
			die('b');
			header('location:'.$baseUrl.'merchant/'.$subfolder);exit;
		}elseif(!empty($subdomain)){ // jika di load dari domain individu dan kembali di set sub folder, load ke domain
			die('c');
			header('location:'.$baseUrl);exit;
		}
	}
}

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

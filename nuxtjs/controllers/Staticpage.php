<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staticpage extends MY_Controller {

	// has no $parent_id because this page will not be displayed in main menu
	var $parent_id = NULL;
	
    function __construct()
    {
    	parent::__construct();
		$this->config->set_item('currCtrlrName',strtolower(get_class()));
		$this->config->set_item('currCtrlrUrl',config_item('base_url')."index.php/".config_item('currCtrlrName')."/");
    }

    public function release(){
    	$release = array('version'=>'v0.0.1-Stable','desc'=>'contoh versi untuk api','date'=>'06012020-033800');
    	echo json_encode($release);exit;
    }

    public function version(){
		return array(
			'header'=>TRUE
			,'footer'=>TRUE
			,'tag'=>(isset($_GET['tag'])?$_GET['tag']:'')
		);
    }
    
}

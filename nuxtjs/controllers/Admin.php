<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	// has no $parent_id because this page will not be displayed in main menu
	var $parent_id = NULL;
	
    function __construct(){
    	parent::__construct();
		$this->config->set_item('currCtrlrName',strtolower(get_class()));
		$this->config->set_item('currCtrlrUrl',config_item('base_url')."index.php/".config_item('currCtrlrName')."/");
    }

	/*
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function page_list(){
		$allPages = $this->main_menu->get_all_pages(NULL,array());
		$allModules = $this->main_menu->get_all_modules();
		$allPages = $this->additional_function->array_merge(array($allPages,$allModules));

		$this->config->set_item('title',''.config_item('title').': Administrasi Page');
		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
			'allPages' => $allPages
		);
	}

	public function access_list(){
		$allPages = $this->main_menu->get_all_pages();
		$allModules = $this->main_menu->get_all_modules();
		$allPages = $this->additional_function->array_merge(array($allPages,$allModules));

		$allApp = $this->filecache->cache_read('apptype','application','global');

		$allUnique = $this->access->get_detail(array('filter_da_rule_access'=>'unique_name','bypass_access'=>TRUE));
		$arrUnique = $this->additional_function->get_array($allUnique,$this->access->get_field_name("da_rule_value"));

		$this->config->set_item('title',''.config_item('title').': Administrasi Page');
		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
			'allPages' => $allPages,
			'arrUnique' => $arrUnique,
			'allApp' => $allApp
		);
	}

}

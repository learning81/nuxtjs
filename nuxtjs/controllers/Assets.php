<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	// has no $parent_id because this page will not be displayed in main menu
	var $parent_id = NULL;
	
    function __construct()
    {
    	parent::__construct();

    	$maxCache = NULL;$expires = NULL;
    	$creq = $this->additional_function->get_elm_priority( // in second
    		array('key'=>'creq','val'=>$this->uri->uri_to_assoc()),
    		array('key'=>'creq','val'=>$_POST),
    		array('key'=>'creq','val'=>$_GET)/*,
    		'405f5d0449793e0c001d325c1c3620'/**/
    	); 
    	// ?creq=4b5f590443796801121a196b17222b = 3600::last_hour
    	// ?creq=4b5f590443796801121a196b17222b = 86400::last_day
    	if(!empty($creq) && FALSE !== strpos($this->crypt->decrypt($creq),'::')) list($maxCache,$expires) = explode('::',$this->crypt->decrypt($creq));

    	$maxCache = $this->additional_function->get_elm_priority( // in second
    		array('key'=>'max_cache','val'=>$this->uri->uri_to_assoc()),
    		array('key'=>'max_cache','val'=>$_POST),
    		array('key'=>'max_cache','val'=>$_GET),
    		$maxCache
    	);
    	$expires = $this->additional_function->get_elm_priority( // harus last_day, last_month, last_year ga' bisa input lain karena expires jadi beda2
    		array('key'=>'expires','val'=>$this->uri->uri_to_assoc()),
    		array('key'=>'expires','val'=>$_POST),
    		array('key'=>'expires','val'=>$_GET),
    		$expires
    	);

    	// $maxCache = /*24 * */60 * 60;
    	$maxCache = $this->additional_function->get_numeric($maxCache);
    	if(empty($maxCache)){
    		@header("cache-control: max-age=no-store");
    	}else{
    		@header("cache-control: max-age=$maxCache, must-revalidate");
    	}

    	if($expires == 'last_hour'){
    		@header("expires: ".gmdate("D, d M Y H:59:59", strtotime(date('Y-m-d H:m:s')))." GMT");
    	}elseif($expires == 'last_day'){
    		@header("expires: ".date("D, d M Y 23:59:59",strtotime(date('Y-m-d')))." GMT");
    	}elseif($expires == 'last_week'){
    		$currWeek = $this->additional_function->get_date_range(date('Y-m-d'),'weekLastDate');
    		@header("expires: ".date("D, d M Y 23:59:59",strtotime($currWeek))." GMT");
    	}elseif($expires == 'last_month'){
    		@header("expires: ".date("D, t M Y 23:59:59",strtotime(date('Y-m-d')))." GMT");
    	}elseif($expires == 'last_year'){
    		@header("expires: ".date("D, t M Y 23:59:59",strtotime(date('Y-12-d')))." GMT");
    	}elseif($expires == 'last_year2'){
    		$expires = /*365 * */24 * 60 * 60;
    		@header("expires: ".gmdate("D, d M Y H:i:s", time() + $expires)." GMT");
    	}else{
    		@header("expires: ".date("D, d M Y 00:00:00",strtotime(date('1970-01-01')))." GMT");
    	}

		$this->config->set_item('currCtrlrName',strtolower(get_class()));
		$this->config->set_item('currCtrlrUrl',$this->config->item('base_url')."index.php/".$this->config->item('currCtrlrName')."/");
    }

	public function index(){
	}

	// http://csscompressor.com/
	/*
	 * !! load style pertama check di folder /final/assets/
	 * !! 
	*/
	function load_style($args){
	    header("content-type: text/css; charset: UTF-8");
	    if(!ob_start("ob_gzhandler")) ob_start();

	    /*
		$filename = implode('/',func_get_args());
		$pathToFilename = config_item('assetpath').$filename;

	    $tmpFile = $pathToFilename = config_item('assetpath').$filename;
	    if(ENVIRONMENT === 'production' && file_exists($tmpFile.'.min.js'))
	        $pathToFilename = $tmpFile.'.min.js';
	    else
	        $pathToFilename = $tmpFile.'.js';

	    $descContent = '';
	    if(is_readable($pathToFilename))
	        $descContent = file_get_contents($pathToFilename);
	    */

        // ^ for path /
        // ] for separated comma ,
        // $newPath = implode('^',func_get_args());
        // $sepPath = explode(']',urldecode($newPath));
        $newPath = implode('^',func_get_args());
        $sepPath = explode('>',urldecode($newPath));
        $descContent = '';
        if(!empty($sepPath) && is_array($sepPath)){
        	foreach ($sepPath as $idxPath => $valPath) {

    		    $appAsset = APPPATH.'assets/'.str_replace('^','/',$valPath); // master asset di final

    		    $defAsset = NULL;
    		    if(!empty($_GET['theme']))
    		    	$defAsset = APPPATH.'views/'.$_GET['theme'].'/assets/'.str_replace('^','/',$valPath); // master asset di final

    		    $pathToFilename = NULL;
    		    if(ENVIRONMENT === 'production' && file_exists($defAsset.'.min.css'))
        		    $pathToFilename = $defAsset.'.min.css';
    		    elseif(file_exists($defAsset.'.css'))
        		    $pathToFilename = $defAsset.'.css';
		    	elseif(file_exists($defAsset))
    		    	$pathToFilename = $defAsset;
					elseif(ENVIRONMENT === 'production' && file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.min.js'))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.min.css';
        		elseif(file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.css'))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.css';
        		elseif(file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath)))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath);
    		    elseif(ENVIRONMENT === 'production' && file_exists($appAsset.'.min.css'))
        		    $pathToFilename = $appAsset.'.min.css';
        		elseif(file_exists($appAsset.'.css'))
        		    $pathToFilename = $appAsset.'.css';
        		elseif(file_exists($appAsset))
        		    $pathToFilename = $appAsset;

        		if(is_readable($pathToFilename))
        		    $descContent .= file_get_contents($pathToFilename)."\n";
        		else
        			die('code is not loaded');

        	}
        }

	    echo $descContent;
	    ob_flush();exit;
	}

	// http://javascript-compressor.com/
	// http://jscompress.com/
	function load_script(){
	    header("content-type: text/javascript; charset: UTF-8");
		if(!ob_start("ob_gzhandler")) ob_start();

	    /*
		$filename = implode('/',func_get_args());
		$pathToFilename = config_item('assetpath').$filename;

	    $tmpFile = $pathToFilename = config_item('assetpath').$filename;
	    if(ENVIRONMENT === 'production' && file_exists($tmpFile.'.min.js'))
	        $pathToFilename = $tmpFile.'.min.js';
	    else
	        $pathToFilename = $tmpFile.'.js';

	    $descContent = '';
	    if(is_readable($pathToFilename))
	        $descContent = file_get_contents($pathToFilename);
	    */

        // ^ for path /
        // ] for separated comma ,
        // $newPath = implode('^',func_get_args());
        // $sepPath = explode(']',urldecode($newPath));
        $newPath = implode('^',func_get_args());
        $sepPath = explode('>',urldecode($newPath));
        $descContent = '';
        if(!empty($sepPath) && is_array($sepPath)){
        	foreach ($sepPath as $idxPath => $valPath) {

    		    $appAsset = APPPATH.'assets/'.str_replace('^','/',$valPath); // master asset di profile

			    $defAsset = NULL;
			    if(!empty($_GET['theme']))
					$defAsset = APPPATH.'views/'.$_GET['theme'].'/assets/'.str_replace('^','/',$valPath); // master asset di final

    		    $pathToFilename = NULL;
    		    if(ENVIRONMENT === 'production' && file_exists($defAsset.'.min.js'))
        		    $pathToFilename = $defAsset.'.min.js';
    		    elseif(file_exists($defAsset.'.js'))
        		    $pathToFilename = $defAsset.'.js';
		    	elseif(file_exists($defAsset))
    		    	$pathToFilename = $defAsset;
				elseif(ENVIRONMENT === 'production' && file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.min.js'))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.min.js';
        		elseif(file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.js'))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath).'.js';
        		elseif(file_exists(MASTERPATH.'assets/'.str_replace('^','/',$valPath)))
        		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$valPath);
				elseif(ENVIRONMENT === 'production' && file_exists($appAsset.'.min.js'))
        		    $pathToFilename = $appAsset.'.min.js';
        		elseif(file_exists($appAsset.'.js'))
        		    $pathToFilename = $appAsset.'.js';
        		elseif(file_exists($appAsset))
        		    $pathToFilename = $appAsset;

        		if(is_readable($pathToFilename))
        		    $descContent .= file_get_contents($pathToFilename)."\n";
        		else
        			die('code is not loaded');

        	}
        }

	    echo $descContent;
	    ob_flush();exit;
	}

	function load_assets(){
		$filename = implode('/',func_get_args());

		$filename = $this->additional_function->get_elm_priority( // in second
			array('key'=>'file','val'=>$_POST),
			array('key'=>'file','val'=>$_GET),
			$filename
		);

		$partFile = explode('/',$filename);
		$file = end($partFile);
		$partExt = explode('.',$file);
		$ext = strtolower(end($partExt));

		switch( $ext ){
		    case "gif": $ctype="image/gif"; break;
		    case "png": $ctype="image/png"; break;
		    case "jpeg":
		    case "jpg": $ctype="image/jpeg"; break;
		    case "css": $ctype="text/css; charset: UTF-8"; break;
		    case "js": $ctype="text/javascript; charset: UTF-8"; break;
		    default: $ctype="text/plain"; break;
		}
		header('Content-type: ' . $ctype);        

		if(!ob_start("ob_gzhandler")) ob_start();

	    $defAsset = NULL;
	    if(!empty($_GET['theme']))
	    	$defAsset = APPPATH.'views/'.$_GET['theme'].'/assets/'.str_replace('^','/',$valPath); // master asset di final

	    $descContent = '';
	    if(!empty($defAsset) && file_exists($defAsset))
	    	$pathToFilename = $defAsset;
		elseif(file_exists(MASTERPATH.'assets/'.str_replace('^','/',$filename)))
		    $pathToFilename = MASTERPATH.'assets/'.str_replace('^','/',$filename);
		elseif(file_exists(APPPATH.'assets/'.str_replace('^','/',$filename)))
		    $pathToFilename = APPPATH.'assets/'.str_replace('^','/',$filename);
		elseif(strpos($filename,'/')){
			$this->load->library('http_tool');
		    $_POST = array(
		    	'action'=>$filename,
		    	'method'=>'get',
		    	'request'=>'as plain',
		    );
		    $descContent = $this->http_tool->get_body();
		    $pathToFilename = '';
		}else
			die('code is not loaded');

		$descContent = ($descContent?$descContent:'');
		if(is_readable($pathToFilename))
		    $descContent = file_get_contents($pathToFilename);
		echo $descContent;
		ob_flush();exit;
	}

	function download_asset(){
		$filename = implode('/',func_get_args());

		if(file_exists(VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(config_item('assetpath').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('assetpath').str_replace('^','/',$filename);
		elseif(file_exists(config_item('data_folder').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('data_folder').str_replace('^','/',$filename);
		elseif(file_exists(APPPATH.'document/'.str_replace('^','/',$filename)))
		    $pathToFilename = APPPATH.'document/'.str_replace('^','/',$filename);
		else
			die('code is not loaded');

	    $this->additional_function->downloadFile($pathToFilename,FALSE);

	}

	function load_image(){
		$filename = implode('/',func_get_args());

		if(file_exists(VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(config_item('assetpath').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('assetpath').str_replace('^','/',$filename);
		elseif(file_exists(config_item('data_folder').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('data_folder').str_replace('^','/',$filename);
		elseif(file_exists(APPPATH.'document/'.str_replace('^','/',$filename)))
		    $pathToFilename = APPPATH.'document/'.str_replace('^','/',$filename);
		else
			die('code is not loaded');

	    $this->additional_function->load_image(urldecode($pathToFilename));
	}

	function load_thumbnail(){
		$filename = implode('/',func_get_args());

		if(file_exists(VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_merchant'])?$_SESSION['access_merchant']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_user'])?$_SESSION['access_user']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename)))
		    $pathToFilename = VIEWPATH.(!empty($_SESSION['access_domain'])?$_SESSION['access_domain']:'undefined').'/assets/img/'.str_replace('^','/',$filename);
		elseif(file_exists(config_item('assetpath').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('assetpath').str_replace('^','/',$filename);
		elseif(file_exists(config_item('data_folder').str_replace('^','/',$filename)))
		    $pathToFilename = config_item('data_folder').str_replace('^','/',$filename);
		elseif(file_exists(APPPATH.'document/'.str_replace('^','/',$filename)))
		    $pathToFilename = APPPATH.'document/'.str_replace('^','/',$filename);
		else
			die('code is not loaded');

		$width = ( (!empty($_GET['width']) && is_numeric($_GET['width'])) ? $_GET['width'] : 250);
		$height = ( (!empty($_GET['height']) && is_numeric($_GET['height'])) ? $_GET['height'] : 250);
	    $this->additional_function->showImage(urldecode($pathToFilename),$width,$height);
	}

	function curl(){
		$function = (! empty($_POST['function'])?$_POST['function']:'get_body');
		$this->load->library('http_tool');
		$this->http_tool->{$function}();exit;
	}

}

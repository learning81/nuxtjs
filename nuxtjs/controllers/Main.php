<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends CI_Controller {
	// has no $parent_id because this page will not be displayed in main menu
	var $parent_id = NULL;
	
    function __construct(){
    	parent::__construct();

		$this->config->set_item('currCtrlrName',strtolower(get_class()));
		$this->config->set_item('currCtrlrUrl',$this->config->item('base_url')."index.php/".$this->config->item('currCtrlrName')."/");
    }

	public function index(){
		$this->load->view('default/main/index');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function register(){
		if(!isset($_POST['email'])){
			$this->load->view('default/main/register');
		}else{
			$email = $this->additional_function->set_value($_POST,'email');
       		$descContent = file_get_contents(APPPATH.'views/default/main/register.php')."\n";

       		if(!filter_var($email, FILTER_VALIDATE_EMAIL) && FALSE) {
       			echo json_encode(array('state'=>FALSE,'msg'=>'format email salah'));exit;
       		}else{
       			try {
       				require BASEPATH.'libraries/phpmailer/class.phpmailer.php';

       				$mail = new PHPMailer(true); //New instance, with exceptions enabled

       				$mail->IsSMTP();                           // tell the class to use SMTP
       				$mail->SMTPAuth   = true;                  // enable SMTP authentication
       				$mail->SMTPSecure   = "tls";                  // enable SMTP authentication
       				$mail->SMTPDebug   = 0;                  // enable SMTP authentication
       				$mail->Port       = 587;                    // set the SMTP server port
       				$mail->Host       = "smtp.gmail.com"; // SMTP server
       				$mail->Username   = "cs@sokola.id";     // SMTP server username
       				$mail->Password   = "04Sept1983";            // SMTP server password

       				$mail->AddReplyTo("cs@sokola.id","Customer Service Sokola.id");

       				$mail->From       = "cs@sokola.id";
       				$mail->FromName   = "Customer Service Sokola.id";

       				$mail->AddAddress($email);

       				$mail->Subject  = "Status Registrasi User";

       				$mail->AltBody    = "Registrasi User"; // optional, comment out and test

       				// $body             = file_get_contents('contents.html');
       				// $body             = preg_replace('/\\\\/','', $body); //Strip backslashes
       				$body             = 'Selamat datang, akun anda sedang dalam status approval';
       				$body             = $descContent;

       				$mail->WordWrap   = 80; // set word wrap

       				$mail->MsgHTML($body);

       				$mail->IsHTML(true); // send as HTML

       				$mail->Send();
       				// echo 'Message has been sent.';
       				echo json_encode(array('state'=>TRUE,'msg'=>'data sudah diproses'));exit;
       			} catch (phpmailerException $e) {
       				echo json_encode(array('state'=>FALSE,'msg'=>$e->errorMessage()));exit;
       				// echo $e->errorMessage();
       			}
       		}
		}
	}

	public function template(){
		$this->load->view('default/main/template');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function registrasi(){
		$this->load->view('default/main/registrasi');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function generic(){
		$this->load->view('default/main/generic');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function elements(){
		$this->load->view('default/main/elements');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function login_sekolah(){
		$this->load->view('default/main/login_sekolah');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function dashboard_sekolah(){
		$_SESSION['active']=config_item('global_instMainUrl').'dashboard_sekolah';

		$this->load->view('default/main/dashboard_sekolah');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function setting_sekolah(){
		$_SESSION['sekolah']=(!empty($_SESSION['sekolah'])?$_SESSION['sekolah']:array());
		$this->load->view('default/main/setting_sekolah');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function json_sekolah(){
		$data = array(
			'item_code'=>@$_POST['item_code']
			,'item_name'=>@$_POST['item_name']
			,'item_telp'=>@$_POST['item_telp']
			,'item_addr'=>@$_POST['item_addr']
			,'item_prop'=>@$_POST['item_prop']
			,'item_kab'=>@$_POST['item_kab']
			,'item_kec'=>@$_POST['item_kec']
		);
		$_SESSION['sekolah'] = $data;
		echo json_encode($data);exit;
	}

	public function setting_guru(){
		$this->load->view('default/main/setting_guru');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function setting_kelas(){
		$this->load->view('default/main/setting_kelas');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function setting_pelajaran(){
		$this->load->view('default/main/setting_pelajaran');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function login_guru(){
		$this->load->view('default/main/login_guru');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function dashboard_guru(){
		$_SESSION['active']=config_item('global_instMainUrl').'dashboard_guru';

		$this->load->view('default/main/dashboard_guru');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function guru_profile(){
		$_SESSION['guru']=(!empty($_SESSION['guru'])?$_SESSION['guru']:array());
		$this->load->view('default/main/guru_profile');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function json_guru(){
		$data = array(
			'item_code'=>@$_POST['item_code']
			,'item_name'=>@$_POST['item_name']
			,'item_telp'=>@$_POST['item_telp']
			,'item_addr'=>@$_POST['item_addr']
			,'item_prop'=>@$_POST['item_prop']
			,'item_kab'=>@$_POST['item_kab']
			,'item_kec'=>@$_POST['item_kec']
		);
		$_SESSION['guru'] = $data;
		echo json_encode($data);exit;
	}

	public function guru_notification(){
		$this->load->view('default/main/guru_notification');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function guru_kelas(){
		$this->load->view('default/main/guru_kelas');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function dashboard_murid(){
		$_SESSION['active']=config_item('global_instMainUrl').'dashboard_murid';
		$this->load->view('default/main/dashboard_murid');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function murid_profile(){
		$_SESSION['murid']=(!empty($_SESSION['murid'])?$_SESSION['murid']:array());
		$this->load->view('default/main/murid_profile');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function murid_sekolah(){
		$this->load->view('default/main/murid_sekolah');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function murid_kelas(){
		$this->load->view('default/main/murid_kelas');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	public function json_murid(){
		$data = array(
			'item_code'=>@$_POST['item_code']
			,'item_name'=>@$_POST['item_name']
			,'item_telp'=>@$_POST['item_telp']
			,'item_addr'=>@$_POST['item_addr']
			,'item_prop'=>@$_POST['item_prop']
			,'item_kab'=>@$_POST['item_kab']
			,'item_kec'=>@$_POST['item_kec']
		);
		$_SESSION['murid'] = $data;
		echo json_encode($data);exit;
	}

	public function murid_pelajaran(){
		$this->load->view('default/main/murid_pelajaran');
		return array(
			'use_header'=>FALSE
			,'use_footer'=>FALSE
		);
	}

	// TODO : my todo
	public function login(){
		if(!empty($_SESSION['prevent_url']) && strtolower($_SESSION['prevent_url']) == strtolower(config_item('global_instMainUrl').'logout'))
			unset($_SESSION['prevent_url']);

		// info variable on page
		$data['error'] = NULL;
		// if current method is processing login
		// on success => go to user first page
		// on failed => just pass to login static page
		$username = $this->input->post('username',TRUE);
		$password = $this->input->post('password',TRUE);
		if(!empty($username) && !empty($password)){
			
			$this->audit_process->insert_data(
				NULL,
				$this->audit_process->USER,
				'user mencoba login',
				array('username'=>$username,'password'=>$password)
			);

			$success = $this->user_data->set_user_session($username,$password);

			if(!$success['status']){
				$data['error'] = $success['msg'];
			}elseif(!empty($_SESSION['prevent_url'])){
				$check_cache = $this->user_data->check_user_cache(2,0,25);
				if($check_cache || TRUE)
					header("location:".$_SESSION['prevent_url']);
				else
					$this->load_default_page();
				exit;
			}
		}

		if($this->user_data->check_user_session()) 
			$this->load_default_page();

		return array(
			'use_header' => TRUE,
			'use_footer' => TRUE,
			'username' => $username,
			'password' => $password,
			'error' => $this->additional_function->set_value($data,'error')
		);
	}

	/**
	 * Logout page
	 * - clear session
	 * - redirect to login page
	 */
	function logout($clicked=TRUE){
		// session_destroy();
		// if($this->additional_function->set_value($_SESSION,'profile_userId')){
		// 	$this->audit_process->insert_data(
		// 		$this->additional_function->set_value($_SESSION,'profile_userId'),
		// 		$this->audit_process->USER,
		// 		'user logout'
		// 	);
		// }

		// $this->user_data->remove_session();

		// if($clicked){// jika logout di klik, maka session prevent_url dihapus
		// 	unset($_SESSION['prevent_url']);
		// }else{// jika logout tidak di klik, maka session prevent_url tidak dihapus

		// }

		unset($_SESSION['active']);
		header("Location: ".$this->config->item('global_instMainUrl'));
		exit;
	}

	function force_logout(){

		$arrParam = func_get_args();
		$defaultParam = $this->additional_function->set_param($arrParam);

		$click = $this->additional_function->get_elm_priority(
			array('key'=>'click','val'=>$defaultParam),
			array('key'=>'click','val'=>$_POST),
			array('key'=>'click','val'=>$_GET)
		);

		$clicked = ((strtolower($click) == 'true' || strtolower($click) == 1) ? TRUE : FALSE);
		$this->logout($clicked);
	}

	function req_password(){
		$key = $this->input->post('key',TRUE);
		$accessName = array();
		$msg = NULL;

		// looping utk check login di semua akses name
		$allSession = $this->filecache->cache_read('session_name','application','global');
		$name = NULL;
		if(!empty($allSession)){
			foreach ($allSession as $idxSession => $valSession){
				$_SESSION['access_matrix'] = $valSession;
				$userDetail = $this->user_data->get_detail(array('filter_su_id'=>$key,'filter_su_active'=>TRUE,'bypass_instance'=>TRUE),NULL,$this->user_data->SINGLE);
				if(!empty($userDetail) && $userDetail !== FALSE){
					array_push($accessName,$valSession);
				}
			}
		}else{
			// seharusnya sudah ada session (dari cache.php) untuk user yang berhasil login untuk di compare
			die('belum ada session di system ini');
		}

		$userExternal = NULL;
		if(!empty($accessName)){
			// check user login dari user_external dulu
			$cond = new stdClass();
			$cond->field = array(
					$this->user_data->get_field_name('id'),
					sprintf("eka.decrypt(%s) as \"password\"",$this->user_data->get_field_name('password')),
					$this->user_data->get_field_name('msag_id'),
					$this->user_data->get_field_name('jenis'),
					$this->user_data->get_field_name('flag_admin'),
					$this->user_data->get_field_name('change_date_time_incl'),
					$this->user_data->get_field_name('online_date_time_incl')
				);
			$cond->where[] = sprintf("lower(%s) like ('%s')",$this->user_data->get_field_name('msag_id'),strtolower($key));
			$cond->where[] = sprintf("%s = '%s'",$this->user_data->get_field_name('jenis'),9); // utk crm
			$userExternal = $this->user_data->get_attribute($cond,$this->user_data->get_table_name('eka.lst_user_external'),$this->user_data->SINGLE,TRUE);

			// create jenis baru di crm untuk user ini

			// ambil data dari akses pertama untuk detail
			$userDetail = $this->user_data->get_detail(array('filter_su_id'=>$key,'filter_su_active'=>TRUE,'bypass_instance'=>TRUE,'unique_name'=>$accessName[0]),NULL,$this->user_data->SINGLE);
		}

		if(!empty($userExternal)){
			$name = $this->additional_function->set_value($userDetail,'name');
			$id = $this->additional_function->set_value($userDetail,'agent_id');
			$email = $this->additional_function->set_value($userDetail,'email');
			$pswd = $this->additional_function->set_value($userExternal,'password');

			$msg = sprintf('Hi %s, <br />login CRM sudah dikirimkan ke %s',$name,$email);
			$subject = 'BAC REQUEST LOGIN';
			$body = sprintf('username:%s <br />password:%s',$id,$pswd);
			// $this->additional_function->ext_mail($email,'akhyar@sinarmasmsiglife.co.id;sultan@sinarmasmsiglife.co.id',$subject,$body);
		}else{
			$msg = sprintf('Maaf, keyword <u><strong>%s</strong></u> tidak bisa login di CRM',$key);
		}

		return array(
			'use_header' => TRUE,
			'use_footer' => TRUE,
			'msg'=>$msg
		);
	}
	
	function close_page(){
		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE
		);
	}

	function no_page(){
		die('no page can be loaded or allowed');
	}

	function external_login(){
		@header("cache-control: max-age=no-store");
		$arrParam = func_get_args();
		$defaultParam = $this->additional_function->set_param($arrParam);

		$username = $this->additional_function->get_elm_priority(
			array('key'=>'username','val'=>$defaultParam),
			array('key'=>'username','val'=>$_POST),
			array('key'=>'username','val'=>$_GET)
		);
		$password = $this->additional_function->get_elm_priority(
			array('key'=>'password','val'=>$defaultParam),
			array('key'=>'password','val'=>$_POST),
			array('key'=>'password','val'=>$_GET)
		);

		$request = $this->additional_function->get_elm_priority(
			array('key'=>'request','val'=>$defaultParam),
			array('key'=>'request','val'=>$_POST),
			array('key'=>'request','val'=>$_GET)
		);

		$this->user_data->set_user_session(urldecode($username),$password,TRUE);
		if($request == 'ajax'){
			if($this->user_data->check_user_session()){
				$credential = $this->access->set_credential();
				// unset($_SESSION['profile_userId']);
				$result = array(
					'state'=>TRUE,
					'credential'=>$credential,
					'msg'=>'login berhasil'
				);
			}else{
				$result = array(
					'state'=>FALSE,
					'msg'=>'login tidak berhasil'
				);
			}
		}else{
			// unset($_SESSION['profile_userId']);
			header("Location: ".config_item('base_url'));
		}
		echo $this->services_json->encode($result);exit;
	}

	public function credential(){
		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'credential' => $this->user_data->set_credential()
		);
	}

	function mangov(){
		return $this->additional_function->array_merge(array(
			array('use_header'=>FALSE,'use_footer'=>FALSE)
		));
	}

	function load_page($pageName=NULL){
		$this->load->specific_model('page');
		$pageDetail = $this->model_page->get_detail(array('filter_pp_slug'=>$pageName),NULL,$this->model_page->SINGLE);

		return array(
			'use_header'=>'master_header2',
			'use_footer'=>'master_footer2',
			'page_detail'=>$pageDetail
		);
	}

	function cke_imgupload($folder="ckeimage"){
		// PHP Upload Script for CKEditor:  http://coursesweb.net/

		// HERE SET THE PATH TO THE FOLDER WITH IMAGES ON YOUR SERVER (RELATIVE TO THE ROOT OF YOUR WEBSITE ON SERVER)

		// HERE PERMISSIONS FOR IMAGE
		$imgsets = array(
		 'maxsize' => 200000,          // maximum file size, in KiloBytes (2 MB)
		 'maxwidth' => 5000,          // maximum allowed width, in pixels
		 'maxheight' => 5000,         // maximum allowed height, in pixels
		 'minwidth' => 10,           // minimum allowed width, in pixels
		 'minheight' => 10,          // minimum allowed height, in pixels
		 'type' => array('bmp', 'gif', 'jpg', 'jpe', 'png')        // allowed extensions
		);

		$re = '';

		if(isset($_FILES['upload']) && strlen($_FILES['upload']['name']) > 1) {
		  $allTime = explode(' ',microtime());

		  $img_name = basename($_FILES['upload']['name']);

		  // get protocol and host name to send the absolute image path to CKEditor
		  $protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';

		  // $urlDestination = config_item('app_url').'data/ckeditor/';
		  // $pathDestination = config_item('data_folder').'ckeditor/';
		  $urlDestination = config_item('data_url')."$folder/".$allTime[1].'/';
		  $pathDestination = config_item('data_folder')."$folder/".$allTime[1].'/';

		  $this->additional_function->create_path(str_replace('\\','/',$pathDestination));

		  $sepext = explode('.', strtolower($_FILES['upload']['name']));
		  $type = end($sepext);       // gets extension
		  list($width, $height) = getimagesize($_FILES['upload']['tmp_name']);     // gets image width and height
		  $err = '';         // to store the errors

		  // Checks if the file has allowed type, size, width and height (for images)
		  if(!in_array($type, $imgsets['type'])) $err .= 'The file: '. $_FILES['upload']['name']. ' has not the allowed extension type.';
		  if($_FILES['upload']['size'] > $imgsets['maxsize']*1000) $err .= '\\n Maximum file size must be: '. $imgsets['maxsize']. ' KB.';
		  if(isset($width) && isset($height)) {
		    if($width > $imgsets['maxwidth'] || $height > $imgsets['maxheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .' \\n The maximum Width x Height must be: '. $imgsets['maxwidth']. ' x '. $imgsets['maxheight'];
		    if($width < $imgsets['minwidth'] || $height < $imgsets['minheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .'\\n The minimum Width x Height must be: '. $imgsets['minwidth']. ' x '. $imgsets['minheight'];
		  }

		  // If no errors, upload the image, else, output the errors
		  if($err == '') {
		    if(move_uploaded_file($_FILES['upload']['tmp_name'],$pathDestination.$img_name)) {

		      $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
		      $message = $img_name .' successfully uploaded: \\n- Size: '. number_format($_FILES['upload']['size']/1024, 3, '.', '') .' KB \\n- Image Width x Height: '. $width. ' x '. $height;
		      $re = "window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '{$urlDestination}{$img_name}', '$message')";
		    }
		    else $re = 'alert("Unable to upload the file")';
		  }
		  else $re = 'alert("'. $err .'")';
		}
		echo "<script>$re;</script>";exit;
	}

	function textboxio_imgupload(){
		$args = func_get_args();
		$path = implode('/',$args);

		reset ($_FILES);
		$currUpload = current($_FILES);

		$imgsets = array(
		 'path' => config_item('data_folder').'image/'.(!empty($path)?$path:'textboxio').'/',
		 'url' => config_item('global_instAssetsUrl').'load_image_thumbnail/'.(!empty($path)?$path:'textboxio').'/'
		);
		$result = $this->additional_function->upload_file($currUpload,$imgsets);
		if($result['state']) {echo json_encode(array('location' => $imgsets['url'].$result['filename']));exit;}
		else {header("HTTP/1.0 500 ".$result['msg']);exit;}
	}

	function testing(){
	}

	/*
	public function module($moduleName=NULL,$modulePart=NULL){
		$modulePart = (!empty($modulePart)?$modulePart:'index');
		$params = func_get_args();
		$uriVars = array_slice($params, 2);
		$this->load->library('module_'.$moduleName,NULL,$moduleName);
		$this->$moduleName->$modulePart($uriVars);
	}
	*/

	public function upload(){
	    $path = $this->additional_function->get_elm_priority( // base folder adalah public agar bisa diakses
	         array('key'=>'upload_path','val'=>$_POST),
	         array('key'=>'upload_path','val'=>$_GET)
	    );
	    $folder_id = $this->additional_function->get_elm_priority( // base folder adalah public agar bisa diakses
	         array('key'=>'upload_id','val'=>$_POST),
	         array('key'=>'upload_id','val'=>$_GET)
	    );
	    $filename = $this->additional_function->get_elm_priority( // base folder adalah public agar bisa diakses
	         array('key'=>'upload_name','val'=>$_POST),
	         array('key'=>'upload_name','val'=>$_GET)
	    );
	    $pathFile = config_item('data_folder').($path?$path.'/':'').($folder_id?$folder_id.'/':'');

	    $result = array('state'=>FALSE,'msg'=>'belum diproses','data'=>array());
	    $state = FALSE;
	    $msgError = '';
	    if(!empty($_FILES)){
	    	$state = TRUE;
	        foreach ($_FILES as $idxFile => $valFile) {
	            $data = $this->additional_function->upload_file($valFile,array(
	                'filename' => ($filename?$filename:date('dmYHis'))
	                ,'path' => $pathFile
	                ,'type' => array('jpg')

	            ));
	            if($data['state']===FALSE) {$state=FALSE;$msgError=$data['msg'];}
	            array_push($result['data'],$data);
	        }
	        $result['state'] = $state;
	        $result['msg'] = ($msgError?$msgError:'data sudah di proses');
	    }

	    echo json_encode($result);exit;
	}

	public function json(){
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT"); 
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
		$result = array('id'=>1,'name'=>'data1');
	    echo json_encode($result);exit;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template extends MY_Controller {
	// has no $parent_id because this page will not be displayed in main menu
	var $parent_id = NULL;
	
    function __construct(){
    	parent::__construct();

		$this->config->set_item('currCtrlrName',strtolower(get_class()));
		$this->config->set_item('currCtrlrUrl',$this->config->item('base_url')."index.php/".$this->config->item('currCtrlrName')."/");
    }

	public function index(){
		return array(
			'use_header'=>'admin_header'
			,'use_footer'=>'admin_footer'
		);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller {

        /*public function index(){
        	die('maintain mode');
        }*/

        public function run(){
                $this->load->library('migration');
                
                if ($this->migration->latest() === FALSE){
                        show_error($this->migration->error_string());
                }
                die('selesai');
        }
        
        public function rollback($id=NULL){
                $this->load->library('migration');

                $id = (empty($id)?20170812000000:$id);
                if ($this->migration->version($id) === FALSE){
                        show_error($this->migration->error_string());
                }
                die('selesai');
        }

        public function truncate(){
                $this->db->query("TRUNCATE TABLE default_kamus_master");
                $this->db->query("TRUNCATE TABLE default_kamus_key");
                die('selesai');
        }

        public function delete(){
                $this->db->query("delete from TABLE default_kamus_master wherer dkm_active !=1 ");
                $this->db->query("delete from TABLE default_kamus_key wherer dkk_active !=1 ");
                die('selesai');
        }

}

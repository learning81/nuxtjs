<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2012, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 *
 *
 Keterangan tambahan tentang login user
 1. login id = id yang melakukan login (email/msag_id/namalain)
 2. agent id = id agent yang tercatat utk membedakan agent id dan agent lainnya (spt : lrb_id)
 3. ajs id = id agent yang tercatant di master agent ajs (msag_id di ajsdb atau di ebdb)
 */
class MY_Model extends CI_Model{

	private $db_tool = NULL;

	function __construct(){
    	// parent::__construct();

		$this->ord226 = '​';

		$this->db_tool = new stdClass();
    	$this->crypt->Key  = 'sim4sCRm';//'!@#$%&*()_+?:';
		$this->schema = config_item('dbName');

		//instance type
		$this->USER = 1;
		$this->ACCESS = 2;
		$this->MAINMENU = 3;
		$this->APP_CONFIG = 4;
		$this->SYS_CONFIG = 5;
		$this->BERITA = 6;
		$this->PAGE = 7;
		$this->PRODUCT = 8;
		$this->MERCHANT = 9;
		$this->TRANSACTION = 10;
		$this->COMMENT = 11;
		$this->KAMUS = 12;
		$this->HADIST = 13;

		//return type
		$this->COUNT = 0;
		$this->SINGLE = 1;
		$this->MULTIPLE = 2;
		$this->QUERY = 3;

		$maxCache = NULL;$expires = NULL;
		$creq = $this->additional_function->get_elm_priority( // in second
			array('key'=>'creq','val'=>$this->uri->uri_to_assoc()),
			array('key'=>'creq','val'=>$_POST),
			array('key'=>'creq','val'=>$_GET)/*,
			'405f5d0449793e0c001d325c1c3620'/**/
		); 
		// ?creq=4b5f590443796801121a196b17222b = 3600::last_hour
		// ?creq=4b5f590443796801121a196b17222b = 86400::last_day
		if(!empty($creq) && FALSE !== strpos($this->crypt->decrypt($creq),'::')) list($maxCache,$expires) = explode('::',$this->crypt->decrypt($creq));

		$maxCache = $this->additional_function->get_elm_priority( // in second
			array('key'=>'max_cache','val'=>$this->uri->uri_to_assoc()),
			array('key'=>'max_cache','val'=>$_POST),
			array('key'=>'max_cache','val'=>$_GET),
			$maxCache
		);
		$expires = $this->additional_function->get_elm_priority( // harus last_day, last_month, last_year ga' bisa input lain karena expires jadi beda2
			array('key'=>'expires','val'=>$this->uri->uri_to_assoc()),
			array('key'=>'expires','val'=>$_POST),
			array('key'=>'expires','val'=>$_GET),
			$expires
		);

		// $maxCache = /*24 * */60 * 60;
		$maxCache = $this->additional_function->get_numeric($maxCache);
		if(empty($maxCache)){
			@header("cache-control: max-age=no-store");
		}else{
			@header("cache-control: max-age=$maxCache, must-revalidate");
		}

		if($expires == 'last_hour'){
			@header("expires: ".gmdate("D, d M Y H:59:59", strtotime(date('Y-m-d H:m:s')))." GMT");
		}elseif($expires == 'last_day'){
			@header("expires: ".date("D, d M Y 23:59:59",strtotime(date('Y-m-d')))." GMT");
		}elseif($expires == 'last_week'){
			$currWeek = $this->additional_function->get_date_range(date('Y-m-d'),'weekLastDate');
			@header("expires: ".date("D, d M Y 23:59:59",strtotime($currWeek))." GMT");
		}elseif($expires == 'last_month'){
			@header("expires: ".date("D, t M Y 23:59:59",strtotime(date('Y-m-d')))." GMT");
		}elseif($expires == 'last_year'){
			@header("expires: ".date("D, t M Y 23:59:59",strtotime(date('Y-12-d')))." GMT");
		}elseif($expires == 'last_year2'){
			$expires = /*365 * */24 * 60 * 60;
			@header("expires: ".gmdate("D, d M Y H:i:s", time() + $expires)." GMT");
		}else{
			@header("expires: ".date("D, d M Y 00:00:00",strtotime(date('1970-01-01')))." GMT");
		}


		@header_remove("pragma");

		$redisLoaded = extension_loaded('redis');
		if($redisLoaded || TRUE){
			$this->load->library('filecache_redis',NULL,'filecache'); // tidak memiliki fungsi ::reset_id(), sudah ada di ::init()
		}else{
			$this->load->library('filecache_file',NULL,'filecache');
		}
		
	}

	// $data = array(
					// array(
					// 	'id' => 1,
					// 	'value' => '1'
					// ),
					// array(
					// 	'id' => 2,
					// 	'value' => '2'
					// )
	// );
	// $file = $data item yang disediakan
	// $arrSelect = array('value'=>'1');
	// $arrLimit = array('id'=>1);
	// single[0/1] = satu data atau multiple yang dikembalikan
	// json = format data json/array
	// allData = include all data that is not selected
	final function readSource($file,$arrSelect=NULL,$arrLimit=NULL,$single=0,$json=FALSE){
		$unique = $this->access->get_access('unique_name');
		$currAccess = $this->access->get_access_by_unique($this->additional_function->set_value($unique,0));
		$appName = $this->additional_function->set_value($currAccess,'da_description');
		$prefixName = strtolower($appName);

		$data = array();
		$file = str_replace('\\','/',$file);
		$files = explode('/',$file);
		if(file_exists($file)){
			include($file); // get all $data value
		}elseif(file_exists($this->dictPath.$prefixName.'/'.end($files))){
			include($this->dictPath.$prefixName.'/'.end($files));
		}elseif(file_exists($this->dictPath.end($files))){
			include($this->dictPath.end($files));
		}
		$selected = array();
		$result = array();
		$currData = array();

		if(!empty($data) && !empty($arrLimit)){
			// if filter is flexible for any field together
			$limitedKey = array();
			if(is_array($arrLimit)){
				foreach ($arrLimit as $idxPrm => $prm) {
					$arrKeys = $this->additional_function->get_array($data,$idxPrm,TRUE,TRUE);
					if(isset($prm)){
						$limitedKey[] = array_keys($arrKeys,urldecode(trim($prm)));
					}
				}
			}elseif(!empty($arrLimit)){
				die('limited param should be in array format');
			}

			$limitedKey = $this->additional_function->array_merge($limitedKey);
			if(!empty($limitedKey)){
				foreach ($data as $idxData => $prtData) {
					if(in_array($idxData,$limitedKey)){
						$currData[] = $data[$idxData];
					}
				}
			}
		}else{
			$currData = $data;
		}

		if(!empty($arrSelect) && !empty($currData)){
			// if filter is flexible for any field together
			$selectedKey = array();
			if(is_array($arrSelect)){
				foreach ($arrSelect as $idxPrm => $prm) {
					$arrKeys = $this->additional_function->get_array($currData,$idxPrm,TRUE);
					$selectedKey[] = array_keys($arrKeys,urldecode(trim($prm)));
				}
			}elseif(!empty($arrSelect)){
				die('selected param should be in array format');
			}

			if(!empty($selectedKey)){
				$selectedKey = $this->additional_function->array_intersect($selectedKey);
			}

			// get all position 
			if(!empty($selectedKey)){
				if($single){
					$result = $currData[$selectedKey[0]];
				}else{
					foreach ($currData as $idxData => $prtData) {
						if(in_array($idxData,$selectedKey)){
							$currData[$idxData]['selected'] = 'selected';
						}
					}
					$result = $currData;
				}
			}else{
				$result = $currData;
			}
		}elseif(!empty($currData)){
			// jika tidak ada yg diseleksi, tidak boleh data dioutput jadi single
			if($single)
				die('no selected value as single data');
			$result = $currData;
		}
		return ($json?json_encode($result):$result);
	}

	function check_oracle_connection(){
		// only check for internal data instead of external data user
		return ($this->db->dbdriver == 'oci8') ? TRUE : FALSE;
	}

	// utk lead_data, account_data, user_data harus buat relasi lagi ke lead atau account atau user
	protected function instance_query($objCond=NULL,$arrParam=NULL,$include=NULL,$activeOnly=FALSE){
		$instance_process = FALSE;
		$newCond = $this->additional_function->clone_obj($objCond);

		// get user first
		if($activeOnly)
			$accessedUser = $this->filecache->cache_read('activeUser2','user');
		else
			$accessedUser = $this->filecache->cache_read('allUser2','user');

		$searchedUser = $this->additional_function->get_elm_priority(
			array('key'=>'filter_su_id','val'=>$arrParam)
		);

		if(isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
			&& isset($arrParam['bypass_access']) 
			&& ($arrParam['bypass_access'] === TRUE || $arrParam['bypass_access'] === 'true')
		){
			if(isset($arrParam['filter_su_id'])){
				$arrUser = $this->additional_function->string_to_array($arrParam['filter_su_id']);
			}
		}elseif(empty($searchedUser)){
			$arrUser = $accessedUser;
		}else{
			$searchedUser = $this->additional_function->string_to_array($searchedUser);
			$arrUser = $this->additional_function->array_intersect(array($searchedUser,$accessedUser));
		}

		$searchedLead = $this->additional_function->get_elm_priority(
			array('key'=>'filter_sl_id','val'=>$arrParam)
		);
		$searchedAcc = $this->additional_function->get_elm_priority(
			array('key'=>'filter_sa3_id','val'=>$arrParam)
		);
		$searchedCamp = $this->additional_function->get_elm_priority(
			array('key'=>'filter_slc_campaign','val'=>$arrParam)
		);
		$searchedProd = $this->additional_function->get_elm_priority(
			array('key'=>'filter_slp_id','val'=>$arrParam)
		);
		$searchedAct = $this->additional_function->get_elm_priority(
			array('key'=>'filter_sla_id','val'=>$arrParam)
		);
		$searchedSPAJ = $this->additional_function->get_elm_priority(
			array('key'=>'filter_sls_id','val'=>$arrParam)
		);

		$main_instance = $this->access->get_access('main_instance','session');
		$main_instance = $this->additional_function->set_value($main_instance,0);
		$usrField = $this->get_field_name('su_id');
		if(empty($main_instance) || $main_instance == 'lead'){
			if(
				isset($include['lead'])
				|| !empty($searchedLead)
				|| isset($include['account'])
				|| !empty($searchedAcc)
				|| isset($include['campaign'])
				|| !empty($searchedCamp)
				|| isset($include['product'])
				|| !empty($searchedProd)
				|| isset($include['activity']) 
				|| !empty($searchedAct)
				|| isset($include['spaj']) 
				|| !empty($searchedSPAJ)
			){
				$instance_process = TRUE;
				$andWhere = '';
				// jika include lead, berarti data yang diambil adalah lead
				$leadTable = $this->get_table_name('simbak_lead');
				$leadField = $this->get_field_name('sl_id');

				if(isset($include['account']) || !empty($searchedAcc)){
					$leadTable = $this->get_table_name('simbak_link_acc_to_lead');
					$leadField = $this->get_field_name('atl_lead_id');
					$accField = $this->get_field_name('atl_acc_id');
					$newCond->join[] = array(
						"refTable"=>$leadTable,
						"cond"=>sprintf("%s.%s = %s.%s",
										$leadTable,
										$accField,
										$this->get_table_name('simbak_account'),
										$this->get_field_name('sa3_id')
									),
						"direction"=>"LEFT"
					);

					if(!empty($arrParam['filter_sa3_active'])){
						$newCond->where[] = sprintf("%s = %s",'sa3_active',$arrParam['filter_sa3_active']);
					}
				}

				if(isset($include['campaign']) || !empty($searchedCamp)){
					$leadTable = $this->get_table_name('simbak_link_campaign');
					$leadField = $this->get_field_name('slc_inst_id');
					$typeField = $this->get_field_name('slc_inst_type');
					$campField = $this->get_field_name('slc_campaign');
					$newCond->join[] = array(
						"refTable"=>$leadTable,
						"cond"=>sprintf("%s.%s = %s.%s",
											$leadTable,
											$campField,
											$this->get_table_name('simbak_campaign'),
											$this->get_field_name('sc_id')
										),
						"direction"=>"LEFT"
					);
					$andWhere = sprintf('and %s.%s = %s',$leadTable,$typeField,$this->LEAD);
				}

				if(
					isset($include['product']) 
					|| !empty($searchedProd)
					|| isset($include['activity']) 
					|| !empty($searchedAct)
					|| isset($include['spaj']) 
					|| !empty($searchedSPAJ)
				){
					$leadTable = $this->get_table_name('simbak_list_product');
					$leadField = $this->get_field_name('slp_inst_id');
					$typeField = $this->get_field_name('slp_inst_type');
					$prodField = $this->get_field_name('slp_id');
					$andWhere = sprintf('and %s.%s = %s',$leadTable,$typeField,$this->LEAD);

					if(
						isset($include['activity']) 
						|| !empty($searchedAct)
						|| isset($include['spaj']) 
						|| !empty($searchedSPAJ)
					){
						$actTable = $this->get_table_name('simbak_list_activity');
						$prodField2 = $this->get_field_name('sla_inst_id');
						$typeField = $this->get_field_name('sla_inst_type');
						$actField = $this->get_field_name('sla_id');
						$andWhere2 = sprintf('and %s.%s = %s',$actTable,$typeField,$this->PRODUCT);

						if(isset($include['spaj']) || !empty($searchedSPAJ)){
							$spajTable = $this->get_table_name('simbak_list_spaj');
							$actField2 = $this->get_field_name('sls_inst_id');
							$typeField = $this->get_field_name('sls_inst_type');
							$spajField = $this->get_field_name('sls_id');
							$andWhere3 = sprintf('and %s.%s = %s',$spajTable,$typeField,$this->ACTIVITY);
							$newCond->join[] = array(
								"refTable"=>$actTable,
								"cond"=>sprintf("%s.%s = %s.%s %s",
													$actTable,
													$actField,
													$spajTable,
													$actField2,
													$andWhere3
												),
								"direction"=>"LEFT"
							);
						}

						$newCond->join[] = array(
							"refTable"=>$leadTable,
							"cond"=>sprintf("%s.%s = %s.%s %s",
												$leadTable,
												$prodField,
												$actTable,
												$prodField2,
												$andWhere2
											),
							"direction"=>"LEFT"
						);

						if(!empty($arrParam['filter_sla_active'])){
							$newCond->where[] = sprintf("%s = %s",'sla_active',$arrParam['filter_sla_active']);
						}
					}
				}

				// utk yg tidak direset dan tidak termasuk lead, pastikan table lead di include agar bisa pastikan sl_app = apptype
				$usrTable = $this->get_table_name('simbak_link_usr_to_lead');
				$leadField2 = $this->get_field_name('utl_lead_id');
				$usrField = $this->get_field_name('utl_user_id');
				
				if(isset($include['lead'])){
					$newCond->join[] = array(
						"refTable"=>$this->get_table_name('simbak_lead'),
						"cond"=>sprintf("%s.%s = %s.%s",
										$this->get_table_name('simbak_lead'),
										$this->get_field_name('sl_id'),
										$usrTable,
										$leadField2
									),
						"direction"=>"LEFT"
					);

					if(!empty($arrParam['filter_sl_active'])){
						$newCond->where[] = sprintf("%s = %s",'sl_active',$arrParam['filter_sl_active']);
					}
				}

				$newCond->join[] = array(
					"refTable"=>$usrTable,
					"cond"=>sprintf("%s.%s = %s.%s %s",
									$usrTable,
									$leadField2,
									$leadTable,
									$leadField,
									$andWhere
								),
					"direction"=>"LEFT"
				);
			}
		}elseif($main_instance == 'account'){
		}else{
			return FALSE;
		}
		// include user condition
		// jika admin bisa bypass akses
		if(isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
			&& isset($arrParam['bypass_access']) 
			&& ($arrParam['bypass_access'] === TRUE || $arrParam['bypass_access'] === 'true')
		){
			// nothing
		}else{
			// pastikan yang dicari hanya sesuai akses
			if(count($arrUser) > 100){
				$newCond->where_in[] = array(
					"fieldName"=>$usrField,
					"fieldValue"=>$this->additional_function->get_collect_id($arrUser)
				);
			}else{
				$newCond->where_in[] = array(
					"fieldName"=>$usrField,
					"fieldValue"=>(empty($arrUser)?array(''):$arrUser)
				);
			}
			if($instance_process){
				$appType = $this->access->get_access('app_type');
				if(empty($main_instance) || $main_instance == 'lead')
					$newCond->where[] = sprintf("sl_app = '%s'",$this->additional_function->set_value($appType,0));
				elseif($main_instance == 'account')
					$newCond->where[] = sprintf("sa3_app = '%s'",$this->additional_function->set_value($appType,0));
			}
		}
		if(!empty($searchedLead)){
			$newCond->where_in[] = array(
				"fieldName"=>$leadField,
				"fieldValue"=>$searchedLead
			);
		}
		if(!empty($searchedAcc)){
				$newCond->where_in[] = array(
					"fieldName"=>$accField,
					"fieldValue"=>$searchedAcc
				);
		}
		if(!empty($searchedCamp)){
				$newCond->where_in[] = array(
					"fieldName"=>$campField,
					"fieldValue"=>$searchedCamp
				);
		}
		if(!empty($searchedProd)){
				$newCond->where_in[] = array(
					"fieldName"=>$prodField,
					"fieldValue"=>$searchedProd
				);
		}
		if(!empty($searchedAct)){
				$newCond->where_in[] = array(
					"fieldName"=>$actField,
					"fieldValue"=>$searchedAct
				);
		}
		if(!empty($searchedSPAJ)){
				$newCond->where_in[] = array(
					"fieldName"=>$spajField,
					"fieldValue"=>$searchedSPAJ
				);
		}

		return ($instance_process?$newCond:FALSE);
	}

	protected function base_query()
	{
		if(!empty($this->db_tool->min)){
			$this->curr_connection->select_min($this->db_tool->min);
		}elseif(!empty($this->db_tool->max)){
			$this->curr_connection->select_max($this->db_tool->max);
		}

		if(!empty($this->db_tool->field))
		{
			$fields = NULL;
			if(is_array($this->db_tool->field))
			{
				foreach($this->db_tool->field as $field)
				{
					$fields[] = $this->get_field_name($field);
				}
				$fields = implode(",",$fields);
			}
			else
			{
				$fields = $this->get_field_name($this->db_tool->field);
			}

			$this->curr_connection->select($fields,FALSE);
		}
		if(!empty($this->db_tool->limit) && !empty($this->db_tool->limit['row']))
		{
			if(empty($this->db_tool->limit['offset'])){
				$this->db_tool->limit['offset'] = 0;
			}
			$this->curr_connection->limit($this->db_tool->limit['row'],($this->db_tool->limit['offset']));
		}
		if(!empty($this->db_tool->order) && !empty($this->db_tool->order['id'])){
			if(empty($this->db_tool->order['type']) && 
				!(	FALSE!==strpos($this->db_tool->order['id'],'asc') ||
					FALSE!==strpos($this->db_tool->order['id'],'ASC') ||
					FALSE!==strpos($this->db_tool->order['id'],'desc') ||
					FALSE!==strpos($this->db_tool->order['id'],'DESC')
				)
			)
				$this->db_tool->order['type'] = 'asc';
			$this->curr_connection->order_by($this->get_field_name(
				$this->db_tool->order['id']),
				$this->additional_function->set_value($this->db_tool->order,'type'),
				FALSE
			);
		}
		if(!empty($this->db_tool->group_by)){ // array or string in only one variable
			if(is_array($this->db_tool->group_by)){
				foreach ($this->db_tool->group_by as $groupby) {
					$splittedGroup = explode(' ',$groupby);
					$this->curr_connection->group_by($this->get_field_name($splittedGroup[0]));
				}
			}else if(is_string($this->db_tool->group_by)){
				$splittedGroup = explode(' ',$this->db_tool->group_by);
				$this->curr_connection->group_by($this->get_field_name($splittedGroup[0]));
			}
		}
		if(!empty($this->db_tool->like)){ // array or string in only one variable
			if(is_array($this->db_tool->like)){
				foreach($this->db_tool->like as $key => $cond){
					if(is_int($key)){ // means that has multiple like
						if(is_array($cond)){
							$this->curr_connection->like(array($this->get_field_name(key($cond))=>current($cond)));
						}
						if(is_string($cond))
							$this->curr_connection->like($cond,NULL,FALSE);
					}
					else{
						$this->curr_connection->like(array($this->get_field_name($key)=>$cond));
						break;
					}
				}
			}
		}
		if(!empty($this->db_tool->where)){ // array or string in only one variable
			if(is_array($this->db_tool->where)){
				foreach($this->db_tool->where as $key => $cond){
					if(is_int($key)){ // means that has multiple where
						if(is_array($cond)){
							$this->curr_connection->where(array($this->get_field_name(key($cond))=>current($cond)));
						}
						if(is_string($cond))
							$this->curr_connection->where($cond,NULL,FALSE);
					}
					else{
						$this->curr_connection->where(array($this->get_field_name($key)=>$cond));
						break;
					}
				}
			}
			else if(is_string($this->db_tool->where))
				$this->curr_connection->where($this->db_tool->where,NULL,FALSE); // if where var is string
		}
		if(!empty($this->db_tool->having)){ // array or string in only one variable
			if(is_array($this->db_tool->having)){
				foreach($this->db_tool->having as $key => $cond){
					if(is_int($key)){ // means that has multiple having
						if(is_array($cond)){
							$this->curr_connection->having(array($this->get_field_name(key($cond))=>current($cond)));
						}
						if(is_string($cond))
							$this->curr_connection->having($cond,NULL,FALSE);
					}
					else{
						$this->curr_connection->having(array($this->get_field_name($key)=>$cond));
						break;
					}
				}
			}
			else if(is_string($this->db_tool->having))
				$this->curr_connection->having($this->db_tool->having,NULL,FALSE); // if where var is string
		}
		if(!empty($this->db_tool->or_where)){ // array or string in only one variable
			if(is_array($this->db_tool->or_where)){
				foreach($this->db_tool->or_where as $key => $cond){
					if(is_int($key)){ // means that has multiple or_where
						if(is_array($cond)){
							$this->curr_connection->or_where(array($this->get_field_name(key($cond))=>current($cond)));
						}
						if(is_string($cond))
							$this->curr_connection->or_where($cond,NULL,FALSE);
					}
					else{
						$this->curr_connection->or_where(array($this->get_field_name($key)=>$cond));
						break;
					}
				}
			}
			else if(is_string($this->db_tool->or_where))
				$this->curr_connection->or_where($this->db_tool->or_where,NULL,FALSE); // if or_where var is string
		}
		if(!empty($this->db_tool->where_in)){
			if(!empty($this->db_tool->where_in[0]['fieldName'])){
				foreach($this->db_tool->where_in as $cond){
					if(! isset($cond['fieldValue']))
						$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),array(''));
					else if(isset($cond['fieldValue']) && empty($cond['fieldValue'])) 
						$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),array(''));
					else if(isset($cond['fieldValue']) && is_array($cond['fieldValue'])) 
						$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);
					else if(isset($cond['fieldValue']) && preg_match('/,/', $cond['fieldValue'])) 
						{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
					else if(isset($cond['fieldValue']))
						{$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$this->additional_function->string_to_array($cond['fieldValue']));}
				}
			}
			else if(!empty($this->db_tool->where_in['fieldName'])){
				$cond = $this->db_tool->where_in;
				if(! isset($cond['fieldValue']))
					$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),array(''));
				else if(isset($cond['fieldValue']) && empty($cond['fieldValue'])) 
					$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),array(''));
				else if(isset($cond['fieldValue']) && is_array($cond['fieldValue'])) 
					$this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);
				else if(isset($cond['fieldValue']) && preg_match('/,/', $cond['fieldValue'])) 
					{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
				else if(isset($cond['fieldValue']))
					{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
			}
		}
		if(!empty($this->db_tool->where_not_in)){ // for where_not_in make sure var[0] is not empty
			if(!empty($this->db_tool->where_not_in[0]['fieldValue']))
			{
				foreach($this->db_tool->where_not_in as $cond)
				{
					if(isset($cond['fieldValue']) && is_array($cond['fieldValue'])) 
						$this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);
					else if(isset($cond['fieldValue']) && preg_match('/,/', $cond['fieldValue'])) 
						{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
					else if(isset($cond['fieldValue'])) 
						{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
				}
			}
			else if(!empty($this->db_tool->where_not_in['fieldValue'])){
				$cond = $this->db_tool->where_not_in;
				if(isset($cond['fieldValue']) && is_array($cond['fieldValue'])) 
					$this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);
				else if(isset($cond['fieldValue']) && preg_match('/,/', $cond['fieldValue'])) 
					{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
				else if(isset($cond['fieldValue'])) 
					{$cond['fieldValue'] = explode(",",$cond['fieldValue']); $this->curr_connection->where_not_in($this->get_field_name($cond['fieldName']),$cond['fieldValue']);}
			}
		}
		// $this->db_tool->join 
		// 1. "refTable"
		// 2. "cond"
		// 3. "direction"
		if(!empty($this->db_tool->join) && is_array($this->db_tool->join)){
			$currTable = array();
			// reset first position of array
			reset($this->db_tool->join);
			if(is_numeric(key($this->db_tool->join))){
				foreach($this->db_tool->join as $key => $join){
					$refTable = (!empty($join['refTable'])?$this->get_table_name($join['refTable']):NULL);
					if(!in_array($refTable,$currTable)){
						$cond = (!empty($join['cond'])?$join['cond']:NULL);
						$direction = (!empty($join['direction'])?$join['direction']:"LEFT");
						$this->curr_connection->join($refTable,$cond,$direction,FALSE);
						$currTable[] = $refTable;
					}
				}
			}else{
				$join = $this->db_tool->join;
				$refTable = (!empty($join['refTable'])?$this->get_table_name($join['refTable']):NULL);
				$cond = (!empty($join['cond'])?$join['cond']:NULL);
				$direction = (!empty($join['direction'])?$join['direction']:"LEFT");
				$this->curr_connection->join($refTable,$cond,$direction,FALSE);
			}
		}
	}	
	
	// get any field from db with any condition
	// $returnType : 0=count,1=single,2=multi
	function get_attribute($cond=NULL,$tableName=NULL,$returnType=2,$external=FALSE,$start=NULL,$limit=NULL){
		$this->curr_connection->reset_query();

		$userConnection = config_item('userConnection');
		
		if(is_string($external))
			$this->curr_connection = $this->load->database($external,TRUE);
		elseif($external && !empty($userConnection))
			$this->curr_connection = $this->load->database($this->extra_connection_config,TRUE);
		else
			$this->curr_connection = $this->load->database($this->db_connection_config,TRUE);
		$this->curr_connection->reset_query();

		if(!empty($start) && is_int($start)){
			$cond->limit['offset'] = $start;
		}

		if(!empty($limit) && is_int($limit)){
			$cond->limit['row'] = $limit;
		}

		$this->db_tool = $cond;
		$query = NULL;

		$result = NULL;
		if($returnType == $this->COUNT){
			if(!empty($this->db_tool->query)){
				$query = $this->db_tool->query;
			}else{
				if(empty($tableName)) 
					$tableName = $this->tableName;

				$this->base_query();
				$this->curr_connection->from($this->get_table_name($tableName));
				$query = $this->curr_connection->get_compiled_select();
			}
			$result = $this->curr_connection->count_all_results($query);
		}else if($returnType == $this->SINGLE){
			if(!empty($this->db_tool->query)){
				$query = $this->db_tool->query;
				$r = $this->curr_connection->query($query);
			}else{
				if(empty($tableName)) 
					$tableName = $this->tableName;

				$this->base_query();
				$this->curr_connection->from($this->get_table_name($tableName));
				$r = $this->curr_connection->get();
			}
			$result = $r->row();
		}else if($returnType == $this->MULTIPLE){
			if(!empty($this->db_tool->query)){
				$query = $this->db_tool->query;
				$r = $this->curr_connection->query($query);
			}else{
				if(empty($tableName)) 
					$tableName = $this->tableName;

				$this->base_query();
				$this->curr_connection->from($this->get_table_name($tableName));
				$r = $this->curr_connection->get();
			}

			$result = $r->result();
			if(empty($result))
				$result = NULL;
		}else if($returnType == $this->QUERY){
			if(!empty($this->db_tool->query)){
				$result = $this->db_tool->query;
			}else{
				if(empty($tableName)) 
					$tableName = $this->tableName;

				$this->base_query();
				$this->curr_connection->from($this->get_table_name($tableName));
				$result = $this->curr_connection->get_compiled_select();
			}
		}
		$this->db_tool = new stdClass();
		$this->cache_db = $this->curr_connection;
		$this->curr_connection = $this->db;
		$this->db->reset_query();
		$this->curr_connection->reset_query();

		return $result;
	}

	
	//Melihat query sql
	function get_last_query()
	{
		return $this->cache_db->last_query();
	}


	function set_attribute($data=NULL,$tableName=NULL,$state='update',$cond=NULL,$mandatory=FALSE){
		$this->curr_connection = $this->load->database($this->db_connection_config,TRUE);		
		$this->curr_connection->reset_query();
		$this->db_tool = $cond;

		if($state=='update' && $mandatory && empty($this->db_tool->where) && empty($this->db_tool->where_in) && empty($this->db_tool->where_not_in))
			die('no condition described');
		
		$this->base_query();

		if(empty($tableName)) 
			$tableName = $this->tableName;

		if($state == 'update'){
			if(is_array($data)){
				foreach ($data as $idxData => $valData){
					if(!isset($valData) || $valData == '')
						$data[$idxData] = 'NULL';
					else
						$data[$idxData] = sprintf("'%s'",$valData);
				}
			}
			$this->curr_connection->update($this->get_table_name($tableName),$data,NULL,NULL,FALSE);
		}elseif($state == 'insert'){
			if(is_array($data)){
				foreach ($data as $idxData => $valData) {
					if(FALSE !== strpos(strtolower($valData),'pac_crm') || strpos(strtolower($valData),'select max') === 0 || FALSE !== strpos(strtolower($idxData),'time_incl') || 0 === strpos(strtolower($valData),'replace'))
						$data[$idxData] = $valData;
					elseif(!isset($valData) || $valData == '')
						$data[$idxData] = 'NULL';
					else
						$data[$idxData] = sprintf("'%s'",$valData);
				}
			}
			$this->curr_connection->insert($this->get_table_name($tableName),$data,FALSE);
		}else{
			$this->curr_connection->count_all_results();
		}
		$state = $this->curr_connection->affected_rows();

		$this->db_tool = new stdClass();
		$this->cache_db = $this->curr_connection;
		$this->curr_connection = $this->db;
		$this->db->reset_query();
		$this->curr_connection->reset_query();
		return $state;
	}

	function remove_attribute($tableName=NULL,$cond=NULL,$mandatory=TRUE){
		$this->curr_connection->reset_query();
		$this->db_tool = $cond;

		if(empty($tableName)) 
			$tableName = $this->tableName;

		if(!$mandatory){
			$res = $this->curr_connection->query('delete from '.$tableName);
		}else{
			if($mandatory && empty($this->db_tool->where) && empty($this->db_tool->where_in) && empty($this->db_tool->where_not_in))
				die('no condition described');
			$this->base_query();
			$res = $this->curr_connection->delete($this->get_table_name($tableName));
		}
		$state = $this->curr_connection->affected_rows();

		$this->db_tool = new stdClass();
		$this->cache_db = $this->curr_connection;
		$this->curr_connection = $this->db;
		$this->db->reset_query();
		$this->curr_connection->reset_query();

		return $state;
	}

	public function get_last_id(){
		// input dipindahkan ke pengambilan parameter dinamis
		list($fieldName,$tableName) = func_get_args();

		$this->curr_connection->select_max($fieldName);
		$r = $this->curr_connection->get($this->get_table_name($tableName));
		$result = $r->row();
		if($result){
			return $result->{$fieldName};
		}else{
			return NULL;
		}
	}

	function get_special_last_id(){
		// input dipindahkan ke pengambilan parameter dinamis
		list($fieldName,$tableName) = func_get_args();

		$cond = new stdClass();
		$query = vsprintf("select %s from %s where %s is not null order by LENGTH(%s) desc , %s desc",
			array(
				$fieldName,
				$tableName,
				$fieldName,
				$fieldName,
				$fieldName
			)
		);
		$cond->query = $query;
		// $res = $this->get_detail(NULL,$cond,$this->SINGLE);
		$res = $this->get_attribute($cond,NULL,$this->SINGLE);
		if(!empty($res))
			return $res->{$fieldName};
		return NULL;
	}
	
	function get_next_id(){
		// input dipindahkan ke pengambilan parameter dinamis
		list($fieldName,$tableName) = func_get_args();

		$last_id = $this->get_special_last_id($fieldName,$tableName);
		return ($last_id+1);
	}

	function get_next_id2($keyName=''){

		/*
			-- delete from default_increment;
			-- jika auto increment sudah terlalu banyak, reduplikate kembali table;
			SHOW FUNCTION STATUS where Db = 'project';
			drop function if exists next_val;

			delimiter //
			create function next_val(keyname varchar(25)) returns int
			begin

			 declare curr_val int default 0 ;
			 declare next_val int default 1 ;
			 declare key_exists tinyint default 0 ;
			 declare source_max int default 1 ;

			 -- LOCK TABLES default_increment WRITE;

			 -- check apakah data sudan ada di table increment
			 select count(*) into key_exists from default_increment where di_key=keyname limit 1 ;
			 IF key_exists > 0 THEN
			   select di_max into curr_val from default_increment where di_key=keyname limit 1 ;
			   select di_next into next_val from default_increment where di_key=keyname limit 1 ;
			   update default_increment set di_max = next_val, di_next = (next_val + 1) where di_key=keyname ;
			 ELSE
			   select auto_increment into source_max from INFORMATION_SCHEMA.TABLES WHERE table_name = keyname order by update_time desc limit 1;
			   insert into default_increment (di_key,di_max,di_next) values (keyname,source_max,(source_max+1)) ;
			   select di_max into next_val from default_increment where di_key=keyname limit 1 ;
			 END IF ;

			 -- UNLOCK TABLES;

			 return next_val ;
			end
			//
			delimiter ;

			select next_val('berita');
			select next_val('berita');
			select next_val('activity');
			select * from default_increment;
		*/

		/*
		$cond = new stdClass();
		$query = sprintf("select next_val('%s') as next_value",$keyName);
		$cond->query = $query;
		// $res = $this->get_detail(NULL,$cond,$this->SINGLE);
		$result = $this->get_attribute($cond,NULL,$this->SINGLE);
		$nextId = $this->additional_function->set_value($result,'next_value');
		*/

		$cond = new stdClass();
		$cond->order = array('id'=>'di_id desc');
		$cond->where[] = array('di_key'=>$keyName);
		$lastRow = $this->get_attribute($cond,'default_increment',$this->SINGLE);

		if(!empty($lastRow)){
			$nextId = $this->additional_function->set_value($lastRow,'di_next');
			$newCurrId = $nextId;
			$newNextId = $nextId + 1;
		}else{
			$cond = new stdClass();
			$cond->field = array('auto_increment');
			$cond->where[] = array('table_name'=>$keyName);
			$cond->order = array('id'=>'update_time desc');
			$dataIncrement = $this->get_attribute($cond,'INFORMATION_SCHEMA.TABLES',$this->SINGLE);
			$lastIncrement = $this->additional_function->set_value($dataIncrement,'auto_increment');

			$nextId = (!empty($lastIncrement)?$lastIncrement:1);
			$newCurrId = (!empty($lastIncrement)?$lastIncrement:1); 
			$newNextId = (!empty($lastIncrement)?$lastIncrement+1:2);
		}

		$cond = new stdClass();
		if(!empty($keyName)){
			$data = array('di_max'=>$newCurrId,'di_next'=>$newNextId);
			if(!empty($lastRow)){
				$cond->where[] = array('di_key'=>$keyName);
				$this->set_attribute($data,'default_increment','update',$cond,TRUE);
			}else{
				$data['di_key'] = $keyName;
				$data['di_id'] = $this->get_next_id('di_id','default_increment');
				$this->set_attribute($data,'default_increment','insert',$cond,TRUE);
			}
		}

		return $nextId;
	}

	function get_next_id3(){
		$cond = new stdClass();
		$cond->query = 'select uuid() as uuid';
		$getUUID = $this->access->get_attribute($cond,NULL,$this->access->SINGLE);
		list($part1,$part2,$part3,$part4,$part5) = explode('-',$this->additional_function->set_value($getUUID,'uuid'));
		return $part3.$part2.$part1.$part4.$part5;
	}

	function get_table_name($tableName)
	{
		// check if schema is defined
		$tableName = (strpos(strtolower($tableName),'.') !== FALSE) ? $tableName : $this->schema.'.'.$tableName;

		// return table name
		return ($this->check_oracle_connection()) ? strtoupper($tableName) : strtolower($tableName);
	}
	
	function get_field_name($fieldName){
		return ($this->check_oracle_connection() ? strtoupper($fieldName) : strtolower($fieldName));
	}
	
	function get_ora_blob_value($value)
	{
		if($this->check_oracle_connection() && $value)
		{
			$size = $value->size();
			$result = $value->read($size);
			return ($result)?$result:NULL;
		}
		else
		{
			return $value;
		}
	}
	
	function cblob_casting_in_ora()
	{
		return ($this->check_oracle_connection()) ? 'to_char' : '';
	}
	
	// convert date before insertin
	function convert_date_format($date,$timeIncluded=FALSE)
	{
		$timeFormat = ($timeIncluded?' H:i:s':'');
		// intranet use : d-M-y
		// my local use : d-m-Y
		// mysql use : Y-m-d
		if($date == NULL || $date == '') return NULL;

		$result = NULL;	
		if($this->check_oracle_connection()){
			$result = date('d-m-Y'.$timeFormat,strtotime($date));
		}else{
			$result = date('Y-m-d'.$timeFormat,strtotime($date));
		}

		return $result;
	}

	// convert date before showing
	function get_date_value($date,$timeIncluded=FALSE,$format='d M Y')
	{
		$timeFormat = ($timeIncluded?' H:i:s':'');

		$result = NULL;
		if(!empty($date))
		{
			$date = date_create($date);
			if($date != FALSE) 
				$result = date_format($date,$format.$timeFormat);
			else
				$result = NULL;
		}
		return $result;
	}

	// get encrypt value from eka.encorypt
	function get_encrypt_value($string=''){
		$cond = new stdClass();
		$cond->query = "select eka.encrypt('$string') as \"encrypt\" from dual";
		$result = $this->get_attribute($cond,NULL,$this->SINGLE,TRUE);
		return $this->additional_function->set_value($result,'encrypt');
	}

	/*
		instA::get_by_instB(Bids,Aids=NULL)
		[1]A : {[1]B,[2]B,[3]B}
		[2]A : {[2]B,[3]B}
		[3]A : {[4]B,[5]B}
		[allid] : {1A,2A,3A}
		[count] : count1+count2+count3
		mendapatkan inst a dari inst b dari table link,
		jika ada [allid] berarti ada link
	 */
	// instA::get_by_instB(Bids,Aids=NULL)
	// dibutuhkan karena didapat dari table link dengan bentuk sudah di format
	function get_by_inst($bIds,$aIds=NULL,$active){
		$return = NULL;
		$cond = new stdClass();
		$cond->join = array(
			'refTable' => 'table link',
			'cond' => 'table inst B.col b id = table link.col b id',
			'direction' => 'right' // patokan adalah table link
		);
		$cond->where_in[] = array(
			'fieldName' => 'table link.col b id',
			'fieldValue' => $bIds
		);
		if(!empty($aIds)){
			$cond->where_in[] = array(
				'fieldName' => 'table link.col a id',
				'fieldValue' => $aIds
			);
		}
		if(isset($active)){
			if($active === TRUE)
				$cond->where[] = sprintf('%s is not null','table link.extra id');
			elseif($active === FALSE)
				$cond->where[] = sprintf('%s is null','table link.extra id');
		}
		$this->load->specific_model('inst_b');
		$result = $this->model_inst_b->get_detail(NULL,$cond);
		if(!empty($result)){
			foreach ($result as $valData) {
				$currId = $this->additional_function->set_value($valData,'col a id');
				if(!empty($return[$currId])){
					array_push($return[$currId],$valData);
				}else{
					$return[$currId] = array($valData);
				}
				// id hasil bisa duplikat
				// array_push($return['allid'],$currId);
			}
			// id hasil tidak duplikat
			$return['allid'] = array_keys($return);
			$return['count'] = count(array_keys($result));
		}
		return $return;
	}

	// instA::set_by_instB(bIds,Aids=NULL)
	// dibutuhkan tautan multiple id
	function set_by_inst($bIds,$aIds,$active){
		$result = array('state'=>NULL,'msg'=>'belum dijalankan');
		$bIds = $this->additional_function->string_to_array($bIds);
		$aIds = $this->additional_function->string_to_array($aIds);

		if($active===TRUE) $active = 1;
		elseif($active===FALSE) $active = 0;

		if(!empty($bIds) && !empty($aIds)){
			foreach ($bIds as $valB){
				foreach ($aIds as $valA) {
					$existsData = $this->get_by_inst($valB,$valA);

					if($existsData && count($existsData[$valA])>1){
						$this->remove_by_inst($valB,$valA);
						$existsData = NULL;
					}

					if(empty($existsData)){
						$main['link id'] = $this->get_next_id2('table link');
						$main['col a id'] = $valA;
						$main['col b id'] = $valB;
						if($active) $main['active'] = TRUE;
						$affected = $this->set_attribute($main,'table link','insert');
						if($affected > 0 && $result['state'] !== FALSE)
							$result = array('state'=>TRUE,'msg'=>'data sudah berhasil di input');
						else
							$result = array('state'=>FALSE,'msg'=>'ada data yang gagal diinput');
					}else{
						$linkId = $this->additional_function->set_value($existsData[$valA][0],'link id');
						$cond = new stdClass();
						$cond->where[] = sprintf('link id = %s',$linkId);
						$main['col a id'] = $valA;
						$main['col b id'] = $valB;
						if($active) $main['active'] = TRUE;
						$affected = $this->set_attribute($main,'table link','update',$cond,TRUE);
						if($affected > 0 && $result['state'] !== FALSE)
							$result = array('state'=>TRUE,'msg'=>'data sudah berhasil di update');
						else
							$result = array('state'=>FALSE,'msg'=>'ada data yang gagal diupdate');
					}
				}
			}
			if($result['state'] === NULL) $result['state'] = FALSE;
			$result['data'] = array('col b id'=>$bIds,'col a id'=>$aIds);
		}
		return $result;
	}
	
	// instA::remove_by_instB(bIds,Aids=NULL)
	// dibutuhkan untuk mencegah penghapusan link instA secara keseluruhan, harus ditentukan minimal id instB
	function remove_by_inst($bIds,$aIds=NULL){
		$result = array('state'=>FALSE,'msg'=>'belum dijalankan');

		if(!empty($bIds)){
			$cond = new stdClass();
			$cond->where_in[] = array(
				'fieldName' => 'col b id',
				'fieldValue' => $bIds
			);
			if(!empty($aIds)){
				$cond->where_in[] = array(
					'fieldName' => 'col a id',
					'fieldValue' => $aIds
				);
			}
			$affected = $this->remove_attribute('table link',$cond,TRUE);
			$result['state'] = TRUE;
			$result['data'] = array('col b id'=>$bIds,'col a id'=>$aIds);
			if($affected>0)
				$result['msg'] = 'data sudah di hapus';
			else
				$result['msg'] = 'tidak ada data yang dihapus';
		}else{
			// untuk bIds = NULL dibuat di model lawannya
		}
		return $result;
	}

	function read_nexcode($keyname=NULL,$separator="-"){
		$cond = new stdClass();
		$cond->order = array('id'=>'di_id desc');
		$cond->where[] = array('di_key'=>$keyname);
		$lastRow = $this->get_attribute($cond,'default_increment',$this->SINGLE);

		if(!empty($lastRow)){
			$nextId = $this->additional_function->set_value($lastRow,'di_next');
		}else{
			$cond = new stdClass();
			$cond->field = array('auto_increment');
			$cond->where[] = array('table_name'=>$keyname);
			$cond->order = array('id'=>'update_time desc');
			$dataIncrement = $this->get_attribute($cond,'INFORMATION_SCHEMA.TABLES',$this->SINGLE);
			$lastIncrement = $this->additional_function->set_value($dataIncrement,'auto_increment');

			$nextId = (!empty($lastIncrement)?$lastIncrement:1);
		}
		return $keyname.$separator.sprintf('%03d',$nextId);
	}

	function execute_nexcode($keyname=NULL,$separator="-"){
		$nextId = $this->get_next_id2($keyname);
		return $keyname.$separator.sprintf('%03d',$nextId);
	}

}

/* End of file Controller.php */
/* Location:  ./system/core/Controller.php */

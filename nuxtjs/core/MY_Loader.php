<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2012, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Loader extends CI_Loader{

	function __construct(){

    	parent::__construct();
    	
		$this->CI =& get_instance();
		$this->prefixName = NULL;
	}

	// $this->load->specific_module('lead'); 
	// akan cek pada folder :
	// libraries/module_bac/lead.php
	// libraries/module_lead.php
	// $this->module_lead->method();
	function specific_module($subModule=NULL,$params = NULL){
		$unique = $this->CI->access->get_access('unique_name');
		$currAccess = $this->CI->access->get_access_by_unique($this->CI->additional_function->set_value($unique,0),'app_type');
		$appName = $this->CI->additional_function->set_value($currAccess,'da_description');
		$this->prefixName = strtolower($appName);

		$filename = @$_SESSION['access_merchant'].'/Mod_'.$subModule;
		if(!file_exists(APPPATH.'libraries/'.$filename.'.php')){
			$filename = @$_SESSION['access_user'].'/Mod_'.$subModule;
			if(!file_exists(APPPATH.'libraries/'.$filename.'.php')){
				$filename = @$_SESSION['access_domain'].'/Mod_'.$subModule;
				if(!file_exists(APPPATH.'libraries/'.$filename.'.php')){
					$filename = $this->prefixName.'/Mod_'.$subModule;
					if(!file_exists(APPPATH.'libraries/'.$filename.'.php')){
						$filename = 'Module_'.$subModule;
						if(!file_exists(APPPATH.'libraries/'.$filename.'.php')){
							die('no module for this request : '.ucfirst($subModule));
						}
					}
				}
			}
		}
		
		

		// $filename harus lowercase
		// nama file harus ucfirst
		// classname harus ucfirst
		$this->library($filename,$params,'module_'.$subModule);
	}

	// $this->load->specific_model('lead'); 
	// akan cek pada folder :
	// models/model_bac/lead.php
	// models/master/lead_data.php
	// models/master/lead_operation.php
	// models/master/lead.php
	// $this->model_lead->get_detail();
	function specific_model($model=NULL){
		$unique = $this->CI->access->get_access('unique_name');
		$currAccess = $this->CI->access->get_access_by_unique($this->CI->additional_function->set_value($unique,0),'app_type');
		$appName = $this->CI->additional_function->set_value($currAccess,'da_description');
		$this->prefixName = strtolower($appName);

		$filename = 'model_'.@$_SESSION['access_merchant'].'/Model_'.$model;
		// var_dump(APPPATH.'models/'.$filename.'.php');
		if(!file_exists(APPPATH.'models/'.$filename.'.php')){
			$filename = 'model_'.@$_SESSION['access_user'].'/Model_'.$model;
			// var_dump(APPPATH.'models/'.$filename.'.php');
			if(!file_exists(APPPATH.'models/'.$filename.'.php')){
				$filename = 'model_'.@$_SESSION['access_domain'].'/Model_'.$model;
				// var_dump(APPPATH.'models/'.$filename.'.php');
				if(!file_exists(APPPATH.'models/'.$filename.'.php')){
					$filename = 'model_'.$this->prefixName.'/Model_'.$model;
					// var_dump(APPPATH.'models/'.$filename.'.php');
					if(!file_exists(APPPATH.'models/'.$filename.'.php')){
						$filename = 'master/'.ucfirst($model).'_data';
						// var_dump(APPPATH.'models/'.$filename.'.php');
						if(!file_exists(APPPATH.'models/'.$filename.'.php')){
							$filename = 'master/'.ucfirst($model);
							// var_dump(APPPATH.'models/'.$filename.'.php');
							if(!file_exists(APPPATH.'models/'.$filename.'.php')){
								$filename = ucfirst($model);
								// var_dump(APPPATH.'models/'.$filename.'.php');
								if(!file_exists(APPPATH.'models/'.$filename.'.php')){
									die('no model for this request : '.ucfirst($model));
								}
							}
						}
					}
				}
			}
		}

		// $filename harus lowercase
		// nama file harus ucfirst
		// classname harus ucfirst
		$this->model($filename,'model_'.$model);
	}
}

/* End of file Controller.php */
/* Location:  ./system/core/Controller.php */

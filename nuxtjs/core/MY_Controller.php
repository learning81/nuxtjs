<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// PR : http://aazni.net/index.php/merchant/merchant0003/module/berita/detail
/**
 * CodeIgniter
 *
 * NOTICE OF LICENSE
 * An open source application development framework for PHP 5.1.6 or newer
 *
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2012, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller{

	var $php_status = FALSE;

	function __construct(){
    	parent::__construct();

		// karena setiap simpan data di table audit harus ada agent id nya
		$_SESSION['prefix'] = 'profile_';
		$this->user_data->init_access_session();

		// set default value
		
		// session_write_close();

		// set param untuk website jika pembuatan terakhir sudah lewat 3 hari
		// $this->filecache->cache_save('cli_debug',TRUE,'session');
		$configdate = $this->filecache->cache_read('date','application','global');
		if(empty($configdate) || date('Y-m-d', strtotime($configdate . " + 3 days")) < date('Y-m-d')){
			// $this->filecache->cache_save('cli_debug',TRUE,'session');
			// $this->additional_function->background_exec('cache','set_app_param',TRUE);
			$this->cache_data->set_app_param();
		}

		$this->log->monitor_category(config_item('logMonitorCategory'));

		// profiler hanya diaktifkan selain pada server production
		// dan http vars harus true
		$profilerState = $this->additional_function->get_elm_priority(
			array('key'=>'profiler','val'=>$this->uri->uri_to_assoc()),
			array('key'=>'profiler','val'=>$_POST),
			array('key'=>'profiler','val'=>$_GET)
		);
		$this->output->enable_profiler(
			ENVIRONMENT !== 'server_production' && 
			($profilerState === TRUE || strtolower($profilerState) === 'true')?
			TRUE:
			FALSE
		);

		// coba beberapa x refresh
		// set_cookie('var1','val1',60);
		// $username = get_cookie('var1');
		// delete_cookie('var1');
		// set_cookie($name, $value, $expire (60*60*24*360), $domain, $path, $prefix);
		// get_cookie('username')
		// delete_cookie($name, $domain, $path, $prefix);

		$this->load->dbforge();
		// $this->myforge = $this->load->dbforge($this->other_db, TRUE);

		$this->config->set_item('current_merchant',NULL); // utk controller merchant
 		$this->config->set_item('current_product',NULL); // utk controller product

		$this->config->set_item('og_title','Laboratorium Online');
		$this->config->set_item('og_image','');
		$this->config->set_item('fb_app_id','');
		$this->config->set_item('og_description','Laboratorium Online Yang Dibangun untuk Belajar');
		$this->config->set_item('og_url',config_item('base_url'));
		$this->config->set_item('og_site_name',config_item('og_title'));

		/*
        $accepted_origins = array("http://localhost", "http://192.168.1.1", "http://example.com");
		if (isset($_SERVER['HTTP_ORIGIN'])) {
		  // same-origin requests won't set an origin. If the origin is set, it must be valid.
		  if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
		    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		  } elseif(FALSE){
		    header("HTTP/1.0 403 Origin Denied");exit;
		  }
		}
		*/

		if(isset($_GET['tag'])){
		    chdir(dirname($_SERVER['SCRIPT_FILENAME']).'/');
		    exec('git checkout -f HEAD'.' 2>&1',$output1);
		    exec('git checkout -f '.$_GET['tag'].' 2>&1',$output2);
		    echo "<pre>";
		    print_r($output1);
		    print_r($output2);
		    echo "</pre>";
		}

		if(FALSE){
		    chdir(dirname($_SERVER['SCRIPT_FILENAME']).'/');
		    exec('whoami'.' 2>&1',$output4);
		    // exec('git checkout -f HEAD'.' 2>&1',$output1);
		    // exec('git checkout -f sit_kamus'.' 2>&1',$output2);
		    exec('git add --all'.' 2>&1',$output1);
		    exec('git commit -am "update kamus"'.' 2>&1',$output2);
		    exec('git push gitlab sit_kamus'.' 2>&1',$output3);
		    echo "<pre>";
		    print_r($output4);
		    print_r($output1);
		    print_r($output2);
		    print_r($output3);
		    echo "</pre>";
			exit;
		}

		$this->load->specific_module('panel');

		// set connection
		$this->load->database();
		$this->curr_connection = $this->db;
		$this->cache_db = NULL;
		$this->db_connection_config = config_item('dbConnection');
		$this->extra_connection_config = config_item('userConnection');
	}

	/**
	 * Default url redirect to first page
	 */
	// function index(){
	// 	$this->load_default_page();
	// }

	/**
	 * check for every url request
	 * 1. check if user is logged in by checking $_SESSION['profile_uId'] if existed
	 * 		if not logged in : redirect to login page
	 * 2. check if module is existed in current class
	 *		if method is not existed : redirect to default allowed module access
	 */
	function _remap($method, $params = array()){
		if(isset($_GET['tag'])){
		    if(isset($_GET['debug']) && $_GET['debug'] === 'true'){
		        die($execPath);
		    }

		    chdir(dirname($_SERVER['SCRIPT_FILENAME']).'/');
		    pclose(popen('git checkout -f HEAD',"r"));
		    pclose(popen('git checkout -f '.$_GET['tag'],"r"));
		    header('location:'.config_item('global_instMainUrl').'version/'.$_GET['tag']);exit;
		}

		$className = strtolower(get_class($this));

		$this->php_status = $this->additional_function->is_php_passed();
		if(FALSE === $this->php_status){
			$this->_load_php_failed();
		}elseif($className == 'migrate'){
			call_user_func_array(array($this, $method), $params);
		}else{
			// ketika membuka halaman yang terprotect dan belum login, check credential yang dimiliki
			// jika tidak memiliki credential, update filecache dengan session id sekarang
			$this->user_data->read_credential();
			$this->filecache->init($this->additional_function->set_value($_SESSION,'profile_userId'));

			// bentuk test page
			if(strtolower($method) == 'module' && !empty($params)){// jika load module dengan akses [baseUrl][controllerName]/module/$params[0]
				$testPage = $params[0];
			}elseif(FALSE !== strpos($method,'load_page')){
				$firstParam = str_replace('load_page_','',$method);
				$params = $this->additional_function->array_merge(array(array($firstParam),$params));

				$method = 'load_page';
				$testPage = $className.'/'.strtolower($method);
			}else{// selain itu akses [baseUrl][controllerName][method]
				$testPage = $className.'/'.strtolower($method);
			}

			// 1. cek, apakah halaman bisa diakses (hak user atau hak publik)
			$cond = new stdClass();
			$cond->where[] = array(
				'dm_detail' => $testPage
			);
			$public = ($this->user_data->check_user_session() ? FALSE : TRUE); // apakah anonymous (blm login) : public, atau registered user : sesuai access name
			$allowedMenu = $this->main_menu->get_detail(array('include_public'=>$public,'bypass_access'=>TRUE),$cond,$this->main_menu->COUNT);
			// 2. cek, apakah sudah ada didatabase
			$isManaged = $this->main_menu->is_managed($testPage);
			// 3. cek, apakah methodnya ada?
			if(isset($_GET['method_exist']) && !empty($_GET['method_exist'])){
				$method = $_GET['method_exist'];
				$isExists = TRUE;
				unset($_GET['method_exist']);
			}else{
				$isExists = method_exists($this,$method);
			}
			// 4. cek, apakah user login
			$loggedIn = $this->user_data->check_user_session();
			// 5. cek, apakah termasuk dalam controller yg di bypass? (jika method pada bypass controller ingin di protect, tambahkan pada $isManaged)
			// semua controller di bawah ini bisa diakses langsung oleh user non login
			$bypassController = array('main','cache','toll','toll','cron','json_api','assets','external');

			// user login = user sudah login
			// method exists = method terdapat di class yang sudah ditentukan
			// menu exist di database = menu terdaftar di database
			// menu diizinkan = menu diizinkan akses, anynoum akses menu public, registered user akses menu sesuai access name

			// jika [user login(v)] dan [menu exist di database(x)] = menu diizinkan(x);

			// [user_ login]	[method_exists]		[menu_exist_di_database]	[menu_diizinkan]	[hasil]
			// 	v				v					v							v				lanjut
			// 	v				v					v							x				default
			// 	v				v					x							~				default
			// 	v				x					~							~				default
			// 	x				v					v							v				lanjut
			// 	x				v					v							x				login
			// 	x				v					x							~				lanjut
			// 	x				x					~							~				login

			// jika([user_login] && [method_exist] && [menu_exist_di_database] && [menu_diizinkan])
			// 	lanjut
			// jika([user_login] && (![method_exist] || ![menu_exist_di_database] || ![menu_diizinkan]))
			// 	default
			// jika(![user_login] && [method_exist])
			// 	jika([menu_exist_di_database] && ![menu_diizinkan])
			// 		login
			// 	lainnya : 
			// 		lanjut
			// jika(![user_login] && ![method_exist])
			// 	login

			// var_dump($loggedIn,$isExists,$isManaged,$allowedMenu,$method);exit;

			$errCode = $this->additional_function->generateCode();
			if(!defined('TRACECODE')) define('TRACECODE',date('ymdHis').'$'.$errCode);

			if($className != 'assets' /**/ && FALSE /**/){
				$statusurl = array('is_login'=>$loggedIn,'is_exists'=>$isExists,'is_managed'=>$isManaged,'allowed_menu'=>$allowedMenu,'method'=>$method);
				$postRawData = file_get_contents("php://input");
				$allHeaders = $this->additional_function->getRequestHeaders();

				$this->audit_process->insert_data(
					NULL,
					NULL,
					'traceroute',
					array(
						'1.method_params'=>$params,
						'2.status_url' => $statusurl,
						'3.post_raw_data' => $postRawData,
						'4.post'=>$_POST,
						'5.get'=>$_GET,
						'6.tracecode'=>TRACECODE
					)
				);

				$userId = (!empty($_SESSION['profile_userId'])?$_SESSION['profile_userId']:'NULL');
				$dataProcess = "=========================================================================================";
				$dataProcess .= "\n";
				$dataProcess .= "=========================================================================================";
				$dataProcess .= "\n";
				$dataProcess .= "user_ip : ".$this->additional_function->set_value($_SERVER,'REMOTE_ADDR'); // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "datetime : ".date('Ymd H:i:s'); // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "tracecode : ".TRACECODE; // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "date time : ".date('Y-m-d_H-i-s'); // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "computer_name : ".@gethostbyaddr($this->additional_function->set_value($_SERVER,'REMOTE_ADDR')); // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "user_agent : ".$this->additional_function->set_value($_SERVER,'HTTP_USER_AGENT'); // utk cli akan kosong
				$dataProcess .= "\n";
				$dataProcess .= "url : ".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
				$dataProcess .= "\n";
				$dataProcess .= "status url : ".print_r($statusurl,TRUE);
				$dataProcess .= "\n";
				$dataProcess .= "session : ".print_r($_SESSION,TRUE);
				$dataProcess .= "\n";	
				$dataProcess .= "get : ".print_r($_GET,TRUE);
				$dataProcess .= "\n";
				$dataProcess .= "post : ".print_r($_POST,TRUE);
				$dataProcess .= "\n";
				$dataProcess .= "post raw data: ".$postRawData;
				$dataProcess .= "\n";
				$dataProcess .= "allheader: ".print_r($allHeaders,TRUE);
				$dataProcess .= "\n\n";

				/*TRACE CODE*/
				$logFile = config_item('data_folder').date('Ymd').'/TRACEROUTE_['.TRACECODE.']_['.$userId.'].log';
				$this->additional_function->create_path(config_item('data_folder').date('Ymd').'/');

				if(!file_exists($logFile)){
					file_put_contents($logFile,"/* trace route dari semua request ke system Utama */\n");
					file_put_contents($logFile,"/* tercatat di my_controller::_remap */\n",FILE_APPEND);
					file_put_contents($logFile,"/* kalau issue permission di folder (dmY), maka simpan di data/ lalu move manual dan zip */\n",FILE_APPEND);
					file_put_contents($logFile,"\n",FILE_APPEND);
					file_put_contents($logFile,"\n",FILE_APPEND);
				}

				file_put_contents($logFile, $dataProcess,FILE_APPEND);

				// $strData = ''; // => biarkan sebagai inisiasi variable
				// $strData .= print_r($arrData, TRUE)."\n";
				// $strData .= "\n";
				// file_put_contents($logFile, $this->services_json->encode($arrData),FILE_APPEND);
				// file_put_contents($logFile, print_r($arrData, true),FILE_APPEND);
				// file_put_contents($logFile, $arrData ,FILE_APPEND);
			}

			/*
			if(empty($_SESSION['profile_landing']) && $method != 'landing' && $method != 'force_logout' && $method != 'logout'){
				$_SESSION['profile_landing'] = TRUE;
				header("location:".$this->config->item('global_instMerchantUrl').'landing');
			}
			*/

			if($loggedIn){
				if($isExists && (
					!$isManaged
					|| ($isManaged && $allowedMenu > 0)
					|| $method == 'module'
					|| in_array($className,$bypassController)
				)){
					// jika sudah login dan method exist dan:
					// 1. tidak ada di database atau
					// 2. ada di database dan boleh akses atau
					// 3. method adalah module atau
					// 4. controller boleh di bypass
					if(strtolower($method) != 'module' && !isset($params['content']['is_module'])){
						$params['content'] = call_user_func_array(array($this, $method), $params);
						$arrExtra = (!empty($params['content']['extra'])?$params['content']['extra']:NULL);

						$className2 = ( !empty($params['content']['currClass']) ?$params['content']['currClass']:NULL);
						$method2 = ( !empty($params['content']['currMethod']) ?$params['content']['currMethod']:NULL);

						$this->_load_view($className2,$method2,$params,$arrExtra,$className.'/'.$method);
					}else{
						// $this->module_params = $params;
						call_user_func_array(array($this, $method), $params);
					}
				}else{
					// selain itu kembali ke halaman awal user login
					$this->load_default_page();
				}
			}else{
				if(!$isExists){
					// jika blm login dan halamana tidak ada
					header("location:".$this->config->item('global_instMerchantUrl'));
				}elseif($isManaged && $allowedMenu == 0){
					// jika blm login tapi mencoba membuka halaman yang sudah login
					$partUrl = explode('index.php/',$_SERVER['REQUEST_URI']);
					$endUrl = (!empty($partUrl[1])?$partUrl[1]:'');
					$_SESSION['prevent_url'] = config_item('base_url').$endUrl;

					header("location:".$this->config->item('global_instMainUrl').'login');
				}else{
					if(strtolower($method) != 'module' && !isset($params['content']['is_module'])){
						$params['content'] = call_user_func_array(array($this, $method), $params);
						$arrExtra = (!empty($params['content']['extra'])?$params['content']['extra']:NULL);

						$className2 = ( !empty($params['content']['currClass']) ?$params['content']['currClass']:NULL);
						$method2 = ( !empty($params['content']['currMethod']) ?$params['content']['currMethod']:NULL);
						$this->_load_view($className2,$method2,$params,$arrExtra,$className.'/'.$method);
					}else{
						// $this->module_params = $params;
						call_user_func_array(array($this, $method), $params);
					}
				}
			}
		}
	}
	
	/**
	* Show error page<br />
	* Stop process because php version checking is not passed
	*/
	function _load_php_failed()
	{
		$this->_load_view('common','phpversionfail',NULL);
	}
	
	function load_default_page($menuName=NULL){
		if(!empty($menuName))
			$firstMenu = array($menuName);
		else
			$firstMenu = $this->access->get_access('first_load_menu');

		// check if first menu is existed in allowed menu
		$pageAccess = $this->access->get_access('page_access');

		$allowedLoaded = $this->additional_function->array_intersect(array($firstMenu, $pageAccess));

		$loadedMenu = NULL;
		if(isset($allowedLoaded[0])){
			$loadedMenu = $allowedLoaded[0];
		}elseif($this->user_data->check_user_session()){
			if(isset($pageAccess[0]))
				$loadedMenu = $pageAccess[0];
			elseif('sangpetualang' == $_SESSION['access_matrix'])
				$loadedMenu = 'admin/access_list';
			else{
				echo 'no loaded menu defined for this user <br /> ';
				echo '<a href="'.config_item('global_instMainUrl').'force_logout">Relogin</a>';
				exit;
			}
		}

		$cond = new stdClass();
		$cond->where[] = array(
			'dm_name' => $loadedMenu
		);
		$menuDetail = $this->main_menu->get_detail(array('bypass_access'=>TRUE),$cond,$this->main_menu->SINGLE);

		if(!empty($menuDetail)){
			$redirectedUrl = str_replace('[baseUrl]',config_item('base_url'),$menuDetail->url);
			header('location:'.$redirectedUrl);
		}elseif('sangpetualang' == $_SESSION['access_matrix'])
			header('location:'.config_item('global_instAdminUrl').'access_list');
		else
			header('location:'.config_item('global_instMainUrl').'no_page');
		exit;
	}

	// memanggil module dari url controller/module/namamodul
	function module(){
		$arrParam = func_get_args();

		if(empty($arrParam[0])) $arrParam[0] = 'mainmenu'; // set default module
		if(empty($arrParam[1])) $arrParam[1] = 'index'; // set default method

		$module_params = array_slice($arrParam, 2);

		$defParam = $this->additional_function->set_param($module_params);
		$username = $this->additional_function->get_elm_priority(
			array('key'=>'username','val'=>$defParam),
			array('key'=>'username','val'=>$_GET),
			array('key'=>'username','val'=>$_POST)
		);
		$password = $this->additional_function->get_elm_priority(
			array('key'=>'password','val'=>$defParam),
			array('key'=>'password','val'=>$_GET),
			array('key'=>'password','val'=>$_POST)
		);

		// khusus untuk IS
		if(!empty($username) && !empty($password)){
		    $this->user_data->set_user_session($username,urldecode($password),TRUE);
		}

		$params['content'] = $this->_load_module($arrParam[0],$arrParam[1],$module_params);

		// check if it's allowed module and load module library and module view
		$isExists = method_exists($this->{'module_'.$arrParam[0]},$arrParam[1]);
			if(!$isExists) $arrParam[1] = 'index';

		$this->_load_view('module_view',$arrParam[0].'/'.$arrParam[1],$params);
	}
	
	function _load_module($moduleName,$moduleMethod,$module_params=NULL){
		$this->load->specific_module($moduleName,array("className"=>$this->config->item('currCtrlrName')));

		// jika method tidak ada
		// jadikan method sebagai id yg di search
		// method jadi 'index'
		if(!method_exists($this->{'module_'.$moduleName},$moduleMethod)){
			$itemId = $moduleMethod;
			$moduleMethod = 'index';
			array_unshift($module_params,$itemId);
		}
		return $this->{'module_'.$moduleName}->{$moduleMethod}($module_params);
	}

	function _load_view($folder,$subfolder,$params,$master=NULL,$defautPage=NULL){
		$testTheme = NULL;

		$allParam = array();
	    $allConfig = $this->additional_function->array_linear($allParam,FALSE,1,2,TRUE);

	    $allParam = $this->additional_function->array_merge(array(
	    	$allConfig,
	    	(array) $master
	    ));

		if(isset($params['content']['cross_browser']) && $params['content']['cross_browser'] == TRUE){
			header("Access-Control-Allow-Origin: *");
		}

		if(@array_key_exists('use_header',$params['content'])){
			if($params['content']['use_header'] === TRUE){
				$path = get_template($testTheme,'common/master_header');
				load_template($path,$allParam,TRUE);
			}elseif($params['content']['use_header'] !== FALSE){
				$path = get_template($testTheme,'common/'.$params['content']['use_header']);
				load_template($path,$allParam,TRUE);
			}
		}

		if(@array_key_exists('use_sidebar',$params['content']) && $params['content']['use_sidebar'] !== FALSE){
			if($params['content']['use_sidebar'] === TRUE){
				$path = get_template($testTheme,'common/master_sidebar');
				load_template($path); // tidak butuh penutup
			}elseif($params['content']['use_sidebar'] !== FALSE){
				$path = get_template($testTheme,'common/'.$params['content']['use_sidebar']);
				load_template($path);
			}
		}

		$path = get_template($testTheme,"$folder/$subfolder",$defautPage);
		load_template($path,$params['content']);

		if(@array_key_exists('close_sidebar',$params['content']) && $params['content']['close_sidebar'] !== FALSE && $params['content']['close_sidebar'] !== TRUE){
			$path = get_template($testTheme,'common/'.$params['content']['close_sidebar']);
			load_template($path);
		}

		if(@array_key_exists('use_footer',$params['content'])){
			if($params['content']['use_footer'] === TRUE){
				$path = get_template($testTheme,'common/master_footer');
				load_template($path,$allParam);
			}elseif($params['content']['use_footer'] !== FALSE){
				$path = get_template($testTheme,'common/'.$params['content']['use_footer']);
				load_template($path,$allParam);
			}
		}
	}

	// untuk memanggil module dari controller bukan dari url
	function load_module($module,$method=NULL,$params=NULL){
		if(empty($module))
			die('no module loaded');
		if(empty($method))
			$method = 'init';

		$loadedModule = array($module,$method);

		$allParam = $this->additional_function->array_merge(array($loadedModule,$params));
		return call_user_func_array(array($this,'module'),$allParam);
	}
}

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */

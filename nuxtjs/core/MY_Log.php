<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2012, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Logging Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Logging
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/errors.html
 */
if(!defined('EXT')) define('EXT','.php');
class MY_Log extends CI_Log{

	protected $_log_path;
	protected $_threshold		= 1;
	protected $_threshold_max	= 0;
	protected $_threshold_array	= array();
	protected $_date_fmt		= 'Y-m-d H:i:s';
	protected $_enabled			= TRUE;
	protected $_levels			= array('ERROR' => 1, 'DEBUG' => 2,  'INFO' => 3, 'ALL' => 4);

	// dari my_log
	var $message;
	var $function;
	var $occurence;
	var $_cat = array();
	var $_active='once';

	/**
	 * Constructor
	 */
	public function __construct(){
		$config =& get_config();

		$this->_log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';

		if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path))
		{
			$this->_enabled = FALSE;
		}

		if (is_numeric($config['log_threshold']))
		{
			$this->_threshold = (int) $config['log_threshold'];
		}
		elseif (is_array($config['log_threshold']))
		{
			$this->_threshold = $this->_threshold_max;
			$this->_threshold_array = array_flip($config['log_threshold']);
		}

		if ($config['log_date_format'] != '')
		{
			$this->_date_fmt = $config['log_date_format'];
		}

		// dari my_log
		$this->_log_path = (empty($this->_log_path)?'../../':$this->_log_path);

		$this->_customfile = 'specified_log-'.date('Y-m-d');
		// admin => on login information
		// dataflow => all log instead admin
		// once => just data need to be tracking
		// readonly => all data retrieving
		// transact => all data manipulation
		// model => method in model
		// view => method in view
		// controller => method in controller
		// library => method in library
		$this->_cat = array('dataflow','model','readonly','transact','view','controller','library','once','admin');
	}

	// --------------------------------------------------------------------

	/**
	 * Write Log File
	 *
	 * Generally this function will be called using the global log_message() function
	 *
	 * @param	string	the error level
	 * @param	string	the error message
	 * @param	bool	whether the error is a native PHP error
	 * @return	bool
	 */
	public function write_log($level = 'error', $msg, $php_error = FALSE)
	{
		if ($this->_enabled === FALSE)
		{
			return FALSE;
		}

		$level = strtoupper($level);

		if (( ! isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold))
			AND ! isset($this->_threshold_array[$this->_levels[$level]]))
		{
			return FALSE;
		}


		$filepath = $this->_log_path.'log-'.date('Y-m-d').'.php';
		$message  = '';

		if ( ! file_exists($filepath))
		{
			$newfile = TRUE;
			$message .= "<"."?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}

		if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
		{
			return FALSE;
		}

		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' --> '.$msg."\n";

		flock($fp, LOCK_EX);
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);

		if (isset($newfile) AND $newfile === TRUE)
		{
			@chmod($filepath, FILE_WRITE_MODE);
		}
		return TRUE;
	}

	//dari my_log
	function monitor_category($cat='')
	{
		$this->_active = $cat;
	}

	function monitor_occurence($occurence='') // can filled by 'if false'
	{
		$this->occurence = $occurence;
	}

	/**
	 *
	 */
	function specified_log($status, $function, $msg, $desc=NULL) 
	{
		if(is_array($status)){
			if(!in_array($this->_active,$status))
			return FALSE;
		}elseif(!empty($status)){
			if($status!=$this->_active)
			return FALSE;
		}

		// status is for only check if the category printed is in array category
		$this->function = strtoupper($function);

		$srcMsg = $msg;
		$srcMsg = str_replace(array("\n","\r\n","\n\r","\r"),"\r", $srcMsg);
		$srcMsg = str_replace("\t"," ", $srcMsg);
		$totalMsg = explode("\r",$srcMsg);
		$this->message = NULL;
		foreach($totalMsg as $msg)
		{
			$this->message .= trim($msg).' ';
		}

		$this->message = strtolower($this->message);
		if(!empty($desc))
			$this->message .= " \n\t\t".$desc;

		if ($this->_enabled === FALSE)
		{
			return FALSE;
		}
		
		$this->write_to_specifiedlogfile();		
	}
	
	function set_customfile($filename,$blank=FALSE)
	{
		if(! empty($filename))
			$this->_customfile = $filename;

		if($blank)
			@unlink($this->_log_path.$this->_customfile.EXT);
	}

	function get_path(){
		return $this->_log_path;
	}
	
	function custom_log($message,$filename=null,$active=FALSE)
	{
		$srcMsg = $message;
		$srcMsg = str_replace(array("\n","\r\n","\n\r","\r"),"\r", $srcMsg);
		$srcMsg = str_replace("\t"," ", $srcMsg);
		$totalMsg = explode("\r",$srcMsg);
		$this->message = NULL;
		foreach($totalMsg as $msg)
		{
			$this->message .= trim($msg).' ';
		}

		if(!$active) return;

		if(! empty($filename)){
			$filepath = $this->_log_path.$filename.EXT;
		}
		else{
			$filepath = $this->_log_path.$this->_customfile.EXT;
		}

		$full_message = null;
		if ( ! file_exists($filepath))
		{
			$full_message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}
			
		if ( ! $fp = @fopen($filepath, 'a'))
		{
			return FALSE;
		}

		$message = $full_message.$this->message;

		fwrite($fp, $message."\n");
		fclose($fp);
	
		return TRUE;		
	}
	
	
	function _keysinarray($check1,$base1)
	{
		if($check1 == 'is_array')  
		{
			if(is_array($base1)) return TRUE;
		 	else return FALSE;
		}
		if($check1 == 'is_object')  
		{
			if(is_object($base1)) return TRUE;
		 	else return FALSE;
		}
		else if($check1 == 'is_string') 
		{
			if(gettype($base1) == 'string') return TRUE;
		 	else	return FALSE;
		}
		else if($check1 == 'is_numeric') 
		{
			if(is_numeric($base1)) return TRUE;
		 	else return FALSE;
		}
		else if($check1 == 'is_true') 
		{
			if($base1) return TRUE;
		 	else return FALSE;
		}
		else if(is_array($base1))
		{			
			if(is_array($check1))
			{
				foreach($check1 as $key => $value)
				{
					if(is_array($value))
					{
						foreach($value as $part => $content)
						{
							if(! isset($base1[$key]))
							{
								return FALSE;
								break;
							}
							$result = $this->_keysinarray($content,$base1[$key]);							
						}						
					}
					else if(array_key_exists($value,$base1)) $result = TRUE;
					else $result = FALSE;
					if($result === FALSE)
					{
						$result = FALSE;
						break;
					}
				}				
				return $result;
			}
			else
			{
				if(array_key_exists($check1,$base1)) return TRUE;
				else return FALSE;
			}
		}
		else if($check1 == $base1) return TRUE;
		else return FALSE;
	}	
	
	function test_equals($check, $base, $function) // $this->occurence = 'none' or 'if false' and $this->_active must be 'output_testing'
	{
		if($this->_active!='output_testing')
		return FALSE;

		$this->function = $function;

		$result = $this->_keysinarray($check,$base);
		
		if($result === FALSE) $this->message = "FALSE";
		else $this->message = "TRUE";

		if(($this->occurence == 'if false' && $this->message == "FALSE") || ($this->occurence != 'if false'))
		{		
			$this->write_to_specifiedlogfile(); 
			
			if($this->message == 'FALSE')
			{
				$this->test_false($base,$function." on FALSE");
				$this->test_empty($base,$function." on EMPTY");				
			}
			
			// echo only "\n"
			$filepath = $this->_log_path.'specified_log-'.date('Y-m-d').EXT;		
			if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
			{
				return FALSE;
			}
			flock($fp, LOCK_EX);	
			fwrite($fp, "\n");
			flock($fp, LOCK_UN);
			fclose($fp);
		
			@chmod($filepath, FILE_WRITE_MODE); 		
			return TRUE;
		}
	}

	function test_empty($check,$function)
	{
		$this->function = $function;
		if(empty($check)) {
			$this->message = "TRUE";
		} else {
			$this->message = "FALSE";
		}		
					
		$this->write_to_specifiedlogfile();
	}

	function test_false($check,$function)
	{
		$this->function = $function;	
		if($check == FALSE) {
			$this->message = "TRUE";
		} else {
			$this->message = "FALSE";
		}		
		
		$this->write_to_specifiedlogfile();		
	}
	
	function write_to_specifiedlogfile()
	{
		$filepath = $this->_log_path.'specified_log-'.date('Y-m-d').EXT;
		$message  = '';

		if ( ! file_exists($filepath))
		{
			$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}

		if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
		{
			return FALSE;
		}

		$message .= $this->function."\n\t\t".date($this->_date_fmt). ' --> '.$this->message."\n";
		
		flock($fp, LOCK_EX);	
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);
	
		@chmod($filepath, FILE_WRITE_MODE); 		
		return TRUE;		
	}
	
	/* testing
	 	if loaded in CI
		$this->log->monitor_category('once');	// category will be printed
		$this->log->specified_log(array('dataflow','controller','once'),$params,'main > index');	// categori assigned
		
		$this->log->monitor_category('output_testing');		
		$this->log->monitor_occurence('if false');		
		$this->log->test_equals('is_array',$base,'main > index');

		$this->log->set_customfile($filename); // without extention, base file for log file instead of 'custom_log.php'
		$this->log->custom_log($comment,$file,bol($show));	// categori assigned,$file without extention
		or
		$GLOBALS['debug'] = new MY_Log();
		$GLOBALS['debug']->set_customfile($filename); // base/default file for log file instead of 'custom_log.php', without extention, 
		$GLOBALS['debug']->custom_log($comment,$file);	// categori assigned (to particular file) ,$file without extention
	*/	
}
// END Log Class

/* End of file Log.php */
/* Location:  ./system/libraries/Log.php */

<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_panel{

	var $CI = NULL;

	function __construct($params=NULL){
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');
		$this->CI->load->specific_model('user');

		$module_methods = get_class_methods('Module_panel');
		$module_menus = $this->CI->additional_function->array_minus($module_methods,array('__construct','index'));

		$user_menus = $this->CI->access->get_access('panel_user');

		// check dari module
		if(!empty($_SESSION['access_matrix']) && $_SESSION['access_matrix'] == 'sangpetualang'){
			$this->user_panel = $module_menus;
		}else{
			$this->user_panel = $this->CI->additional_function->array_intersect(array($module_menus,$user_menus));
		}

		// per method
		// if(!$this->CI->access->get_access('unique_name','is5_staff')) die('module user ini hanya dibolehkan untuk support bsim');

		// check dari session
		// $allSession = $this->CI->additional_function->get_array($_SESSION['profile_session'],'access_matrix');
		// if(empty($allSession) || !in_array('sangpetualang', $allSession)) die('channel ini harus paralled login dengan admin');
	}

	function index(){
		$this->CI->load->specific_module('panel');
		$module_methods = get_class_methods('Module_panel');
		$system_panel = $this->CI->additional_function->array_minus($module_methods,array('__construct','index','index2'));

		$user_panel = $this->CI->access->get_access('panel_user');

		if(($_SESSION['access_matrix'] == 'sangpetualang')){
			$all_panel = $system_panel;
		}else{
			$all_panel = $this->CI->additional_function->array_intersect(array($system_panel,$user_panel));
		}
		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'all_panel' => $all_panel,
		);
	}
	
	function index2(){
		$allModules = $this->CI->additional_function->get_file_list(APPPATH.'libraries',NULL,NULL,'Module_');
		$allPages = array();
		if(!empty($allModules)){
			foreach($allModules as $module){
				$currName = str_replace(array('Module_','.php'),'',$module);
				$this->CI->load->specific_module(str_replace(array('Module_','.php'),'',$module));
				$module_methods = get_class_methods(str_replace('.php','',$module));
				if(!empty($module_methods)){
					foreach($module_methods as $method){
						if(FALSE !== strpos($method,'management')) $allPages[$currName] = 'module/'.$currName.'/management/';
					}
				}
			}
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'all_panel' => $allPages,
		);
	}
	
	function update_password(){
		if(!in_array('update_password',$this->user_panel)){
			header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		}

		// jika belum login arahkan ke halaman login
		if(FALSE == $this->CI->user_data->check_user_session()) 
			header('location:'.config_item('global_instMainUrl').'login');
		
		// ini adalah halaman login, jadi harus bisa diakses oleh user yang bersangkutan sesuai akses 'panel_user'
		if(!in_array(__FUNCTION__,$this->user_panel))
			header('location:'.config_item('global_instStaticUrl').'panel_user');

		// info variable on page
		$state = FALSE;
		$comment = array();

		$password = $this->CI->input->post('new_password',TRUE);
		$password2 = $this->CI->input->post('conf_password',TRUE);
		if(!empty($password)){
			$this->CI->load->model('master/user_operation');
			$result = $this->CI->user_operation->update_password($password);
			$state = $result['state'];
			$comment = $result['comment'];
		}

		return array(
					'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
					'all_menus' => $this->user_panel,
					'comment' => $comment,
					'state' => $state
		);
	}

	// melakukan update akses dari satu uwer, berbeda dengan method switch_session
	// menampilkan semua session dari user yang login (aktif)
	function update_access(){
		if(!in_array('update_access',$this->user_panel)){
			header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		}

		// jika belum login arahkan ke halaman login
		if(FALSE == $this->CI->user_data->check_user_session()) 
			header('location:'.config_item('instMainUrl').'login');

		// ini adalah halaman login, jadi harus bisa diakses oleh user yang bersangkutan sesuai akses 'panel_user'
		if(!in_array(__FUNCTION__,$this->user_panel))
			header('location:'.config_item('global_instStaticUrl').'panel_user');

		if(array_key_exists('access',$_POST)){
			$_SESSION['access_matrix'] = $_POST['access'];
		}

		$cond = new stdClass();
		$cond->where[] = sprintf('%s = \'app_type\'','da_rule_access');
		$accLst = $this->CI->access->get_detail(NULL,$cond);
		$accLstAll = $this->CI->additional_function->array_linear(
			$accLst,
			FALSE,
			$this->CI->access->get_field_name('da_ACCESS_NAME'),
			$this->CI->access->get_field_name('da_DESCRIPTION')
		);

		return array(
					'use_header' => 'admin_header',
					'use_footer' => 'admin_footer',
					'all_menus' => $this->user_panel,
					'accLst' => $accLstAll
		);
	}

	// melakukan perpindahan login (termasuk kemampuan method update_access), berbeda dengan method update_access
	// melakukan multi user login
	function switch_session(){
		if(!in_array('switch_session',$this->user_panel)){
			header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		}

		// jika belum login arahkan ke halaman login
		if(FALSE == $this->CI->user_data->check_user_session()) 
			header('location:'.config_item('instMainUrl').'login');

		// semua user bisa switch session, jadi di comment dulu
		// // ini adalah halaman login, jadi harus bisa diakses oleh user yang bersangkutan sesuai akses 'panel_user'
		// if(!in_array(__FUNCTION__,$this->user_panel))
		// 	header('location:'.config_item('global_instStaticUrl').'panel_user');

		if(!empty($_POST)){
			$sessionPart = explode('-',$_POST['session']);
			$agentId = trim($sessionPart[0]);
			$sessionName = trim($sessionPart[1]);
			$this->CI->user_data->switch_session($agentId);
			$_SESSION['access_matrix'] = $sessionName;
			// $this->CI->load_default_page();
		}

		$allSession = array();
		if(!empty($_SESSION['profile_session']) && is_array($_SESSION['profile_session'])){
			foreach ($_SESSION['profile_session'] as $idxSess => $valSess) {
				foreach ($valSess['access_matrix_list'] as $idxAcc => $valAcc){
					$allSession[sprintf('%s-%s',$idxSess,$valAcc)] = sprintf('%s - %s',$valSess['profile_name'],$valAcc);
				}
			}
		}

		return array(
					'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
					'all_menus' => $this->user_panel,
					'all_session' => $allSession
		);
	}

	// melakukan remove session dari daftar method switch_session
	function list_session(){
		if(!in_array('list_session',$this->user_panel)){
			header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		}

		// jika belum login arahkan ke halaman login
		if(FALSE == $this->CI->user_data->check_user_session()) 
			header('location:'.config_item('instMainUrl').'login');

		// semua user bisa switch session, jadi di comment dulu
		// // ini adalah halaman login, jadi harus bisa diakses oleh user yang bersangkutan sesuai akses 'panel_user'
		// if(!in_array(__FUNCTION__,$this->user_panel))
		// 	header('location:'.config_item('global_instStaticUrl').'panel_user');

		if(!empty($_POST)){
			$sessionPart = explode('-',$_POST['del_cache']);
			$this->CI->filecache->remove_user(trim($sessionPart[0]));
		}

		$allCache = $this->CI->filecache->get_all_user();
		$lstCache = array();
		if(is_array($allCache)){
			foreach ($allCache as $idxCache => $valCache) {
				if($valCache != 'temp' && $valCache != 'global') 
					array_push($lstCache,$valCache);
			}
		}

		return array(
					'use_header' => 'admin_header',
					'use_footer' => 'admin_footer',
					'all_menus' => $this->user_panel,
					'all_cache' => $lstCache
		);
	}

	function query_online(){
		$allSession = $this->CI->additional_function->get_array($_SESSION['profile_session'],'access_matrix');
		if(!in_array('sangpetualang', $allSession)) die('channel ini harus paralled login dengan admin');

		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
			'all_menus' => $this->user_panel
		);
	}

} 

<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_access
{
	var $CI = NULL;
		
	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');
	}
	
	// restful api request
	function index($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);
		$returnType = $this->CI->access->MULTIPLE;

		// sebagai ID
		// id sebagai default sa_access_name karena nilai ini bisa mengelompokkan suatu akses
		$menuName = $this->CI->additional_function->set_value($arrParams,0);
		if(FALSE !== strpos($menuName,',')){
			$returnType = $this->CI->access->SINGLE;
			$cond->where[] = array('sa_access_name'=>$menuName);
		}elseif(!empty($menuName)){
			$cond->where_in[] = array(
				"fieldName"=>'sa_access_name',
				"fieldValue"=>$menuName
			);
		}

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){
			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','mainmenu') &&  FALSE){
				$menuList = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->access->update($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$result = $this->CI->access->remove($_DELETE,$cond);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->access->update($_POST,NULL,'insert');
				}
				$accessLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnUri = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnUri) && !is_numeric($returnUri) && strlen($returnUri)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->access,strtoupper($returnUri),'number');
			}

			// ambil data
			$result = $this->CI->access->get_detail($_GET,$cond,$returnType);
			$accessLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'accessLst' => $accessLst
		);
	}

	// membuat semua acces name lain yang satu app dari accessname sekarang menjadi kondisi maintenance
	function maintain_app($arrParam=NULL){
		$allAccess = $this->CI->access->get_detail(
			array(
				'bypass_access'=>true,
				'filter_sa_rule_access'=>'app_type',
				'filter_sa_rule_value'=>$this->CI->additional_function->set_value($arrParam,0,'number'),
				'field'=>'sa_access_name'
			)
		);
		$allAccess = $this->CI->additional_function->get_array(
			$allAccess,
			$this->CI->access->get_field_name('sa_access_name')
		);
		$cond = new stdClass();
		$cond->where = sprintf('%s in (\'%s\')','sa_access_name',$this->CI->additional_function->array_to_string($allAccess,"','",TRUE));
		$result = $this->CI->access->update(
			array(
				'filter_sa_rule_access'=>'site_maintenance',
				'sa_rule_value'=>1
			),
			$cond,
			'update'
		);
		if(!$result['state'] && !empty($allAccess)){
			foreach ($allAccess as $idxAcc => $valAcc) {
				$this->CI->access->update(
					array(
						'sa_access_name'=>$valAcc,
						'sa_rule_access'=>'site_maintenance',
						'sa_rule_value'=>1
					),
					NULL,
					'insert'
				);
			}
		}
		die('app sudah dalam status maintenance');
	}

	function rebuild_global_param(){
		$this->CI->load->specific_model('cache');
		$this->CI->filecache->remove_file('application','global');
		$this->CI->cache_data->set_app_param();
		echo $this->CI->services_json->encode('rebuild app global config');exit;
	}
} 

<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_page
{
	var $CI = NULL;
		
	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');

		$this->CI->load->specific_model('page');
	}
	
	// restful api request
	function index($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$pageId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($pageId)){
			$returnType = $this->CI->model_page->SINGLE;
			$cond->where[] = array('mp2_id'=>$pageId);
		}elseif(!empty($pageId)){
			$cond->where_in[] = array(
				"fieldName"=>'mp2_id',
				"fieldValue"=>$pageId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$PageLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->model_page->update($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$result = $this->CI->model_page->remove($_DELETE,$cond);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->model_page->update($_POST,NULL,'insert');
				}
				$PageLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_page->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_page->get_detail($_GET,$cond,$returnType);
			$PageLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'PageLst' => $PageLst
		);
	}

	function detail($arrParams=NULL){
		$defaultParam = $this->CI->additional_function->set_param($arrParams);
		$pageId = $this->CI->additional_function->get_elm_priority(
			array('key'=>'pp_id','val'=>$_POST),
			array('key'=>'pp_id','val'=>$_GET),
			array('key'=>'pp_id','val'=>$defaultParam),
			array('key'=>'id','val'=>$defaultParam)
		);

		$cond = new stdClass();
		$page = $this->CI->model_page->get_detail(array('filter_pp_id'=>$pageId),$cond,$this->CI->model_page->SINGLE);
		
		return array(
			'use_header'=>'master_header',
			'use_footer'=>'master_footer',
			'page'=>$page
		);
	}

	function management(){
		// if(!in_array('management',$this->user_panel)){
		// 	header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		// }

		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer'
		);
	}

} 

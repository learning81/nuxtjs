<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_kamus
{
	var $CI = NULL;
		
	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');

		$this->CI->load->specific_model('kamus');

		$this->path = config_item('app_folder').'core/kamus/';

		$allSession = $this->CI->additional_function->get_array($_SESSION['profile_session'],'access_matrix');
		if(empty($allSession) || (FALSE === in_array('sangpetualang', $allSession))) die('channel ini harus paralled login dengan admin');
	}
	
	// restful api request
	function index($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$pageId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($pageId)){
			$returnType = $this->CI->model_kamus->SINGLE;
			$cond->where[] = array('dkm_id'=>$pageId);
		}elseif(!empty($pageId)){
			$cond->where_in[] = array(
				"fieldName"=>'dkm_id',
				"fieldValue"=>$pageId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$PageLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->model_kamus->update($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$result = $this->CI->model_kamus->remove($_DELETE,$cond);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->model_kamus->update($_POST,NULL,'insert');
				}
				$dictLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_kamus->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_kamus->detail($_GET,$cond,$returnType);
			$dictLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'dictLst' => $dictLst
		);
	}

	// restful api request
	function request($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		$method = $this->CI->additional_function->get_elm_priority(
			array('key'=>'method','val'=>$defParam),
			array('key'=>'method','val'=>$_GET),
			array('key'=>'method','val'=>$_POST),
			'get'
		);

		// sebagai ID
		$pageId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($pageId)){
			$returnType = $this->CI->model_kamus->SINGLE;
			$cond->where[] = array('dkm_id'=>$pageId);
		}elseif(!empty($pageId)){
			$cond->where_in[] = array(
				"fieldName"=>'dkm_id',
				"fieldValue"=>$pageId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($method) == 'put'
			|| strtolower($method) == 'post'
			|| strtolower($method) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','kamus') &&  FALSE){
				$PageLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($method) == 'put'){
					$result = $this->CI->model_kamus->update($_POST,$cond,'update');
				}elseif(strtolower($method) == 'delete'){
					$result = $this->CI->model_kamus->remove($_POST,$cond);
				}elseif(strtolower($method) == 'post'){
					$result = $this->CI->model_kamus->update($_POST,NULL,'insert');
				}
				$dictLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_kamus->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_kamus->detail($_GET,$cond,$returnType);
			$dictLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'dictLst' => $dictLst
		);
	}

	// restful api request
	function index_key($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$pageId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($pageId)){
			$returnType = $this->CI->model_kamus->SINGLE;
			$cond->where[] = array('dkk_id'=>$pageId);
		}elseif(!empty($pageId)){
			$cond->where_in[] = array(
				"fieldName"=>'dkk_id',
				"fieldValue"=>$pageId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$PageLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->model_kamus->update_key($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$result = $this->CI->model_kamus->remove_key($_DELETE,$cond);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->model_kamus->update_key($_POST,NULL,'insert');
				}
				$keyLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_kamus->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_kamus->detail_key($_GET,$cond,$returnType);
			$keyLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'keyLst' => $keyLst
		);
	}

	// restful api request
	function request_key($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);
		
		$method = $this->CI->additional_function->get_elm_priority(
			array('key'=>'method','val'=>$defParam),
			array('key'=>'method','val'=>$_GET),
			array('key'=>'method','val'=>$_POST),
			'get'
		);

		// sebagai ID
		$pageId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($pageId)){
			$returnType = $this->CI->model_kamus->SINGLE;
			$cond->where[] = array('dkk_id'=>$pageId);
		}elseif(!empty($pageId)){
			$cond->where_in[] = array(
				"fieldName"=>'dkk_id',
				"fieldValue"=>$pageId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($method) == 'put'
			|| strtolower($method) == 'post'
			|| strtolower($method) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$PageLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($method) == 'put'){
					$result = $this->CI->model_kamus->update_key($_POST,$cond,'update');
				}elseif(strtolower($method) == 'delete'){
					$result = $this->CI->model_kamus->remove_key($_POST,$cond);
				}elseif(strtolower($method) == 'post'){
					$result = $this->CI->model_kamus->update_key($_POST,NULL,'insert');
				}
				$keyLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_kamus->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_kamus->detail_key($_GET,$cond,$returnType);
			$keyLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'keyLst' => $keyLst
		);
	}

	function kamus_generate(){
		$this->CI->load->specific_model('kamus');

		$this->CI->additional_function->create_path($this->path);

		$allKamus = $this->CI->model_kamus->detail(array('limit'=>FALSE));
		$kamus_file = 'kamus_'.date('YmdHis');
		$this->CI->additional_function->writetoFile($this->path.$kamus_file,"kamus::".date('Y-m-d H:i:s'));
		if(!empty($allKamus)){
			foreach ($allKamus as $kamus){
				if(!empty($kamus->dkm_updated))
					$this->CI->additional_function->writetoFile($this->path.$kamus_file,$this->CI->services_json->encode($kamus));
			}
		}

	    chdir(dirname($_SERVER['SCRIPT_FILENAME']).'/');
	    exec('whoami'.' 2>&1',$output4);
	    // exec('git checkout -f HEAD'.' 2>&1',$output1);
	    // exec('git checkout -f sit_kamus'.' 2>&1',$output2);
	    exec('git add --all'.' 2>&1',$output1);
	    exec('git commit -am "update kamus"'.' 2>&1',$output2);
	    exec('git push gitlab sit_kamus'.' 2>&1',$output3);
	    // echo "<pre>";
	    // print_r($output4);
	    // print_r($output1);
	    // print_r($output2);
	    // print_r($output3);
	    // echo "</pre>";
		// exit;

		$this->CI->additional_function->downloadFile($this->path.$kamus_file);
	}

	function key_generate(){
		$this->CI->load->specific_model('kamus');

		$this->CI->additional_function->create_path($this->path);

		$allKeys = $this->CI->model_kamus->detail_key(array('limit'=>FALSE,'filter_dkk_master_uid'=>'filled'));
		$key_file = 'key_'.date('YmdHis');
		$this->CI->additional_function->writetoFile($this->path.$key_file,"key::".date('Y-m-d H:i:s'));
		if(!empty($allKeys)){
			foreach ($allKeys as $key){
				if(!empty($key->dkk_updated))
					$this->CI->additional_function->writetoFile($this->path.$key_file,$this->CI->services_json->encode($key));
			}
		}

	    chdir(dirname($_SERVER['SCRIPT_FILENAME']).'/');
	    exec('whoami'.' 2>&1',$output4);
	    // exec('git checkout -f HEAD'.' 2>&1',$output1);
	    // exec('git checkout -f sit_kamus'.' 2>&1',$output2);
	    exec('git add --all'.' 2>&1',$output1);
	    exec('git commit -am "update kamus"'.' 2>&1',$output2);
	    exec('git push gitlab sit_kamus'.' 2>&1',$output3);
	    // echo "<pre>";
	    // print_r($output4);
	    // print_r($output1);
	    // print_r($output2);
	    // print_r($output3);
	    // echo "</pre>";
		// exit;

		$this->CI->additional_function->downloadFile($this->path.$key_file);
	}

	function kamus_process(){
		if(!empty($_FILES) && !empty($_FILES['kamus'])){
			$config = array('path'=>$this->path);
			$result = $this->CI->additional_function->upload_file($_FILES['kamus'],$config);
		}

		$allFiles = $this->CI->additional_function->get_file_list($this->path,array('.','..'));
		if(!empty($allFiles)){
			foreach ($allFiles as $idxFile => $file) {
				$currType = '';
				$lineMsg = '';
				if($fp=fopen($this->path.$file, 'r')){
					$line = 0;
					while (!feof($fp)){
					    $lineMsg=fgets($fp);

					    if($line == 0){
					    	$type = explode('::',$lineMsg);
					    	$currType = $type[0];
					    }elseif($currType == 'kamus'){
					    	$currMsg = $this->CI->services_json->decode($lineMsg);
					    	if(!empty($currMsg)){
						    	$currUid = $this->CI->additional_function->set_value($currMsg,'dkm_uid');
						    	$currParent = $this->CI->additional_function->set_value($currMsg,'dkm_parent');
						    	$currDate = $this->CI->additional_function->set_value($currMsg,'dkm_updated');
						    	$currTitle = $this->CI->additional_function->set_value($currMsg,'dkm_title');
						    	$currDesc = (!empty($currMsg->dkm_desc)?$currMsg->dkm_desc:'');
						    	$currFlag = (isset($currMsg->dkm_active)?$currMsg->dkm_active:1);

						    	$checkData = $this->CI->model_kamus->detail(array('filter_dkm_uid'=>$currUid),NULL,$this->CI->model_kamus->SINGLE);
						    	$checkDate = $this->CI->additional_function->set_value($checkData,'dkm_updated');

						    	$data['dkm_uid'] = $currUid;
						    	$data['dkm_parent'] = $currParent;
						    	$data['dkm_title'] = $currTitle;
						    	$data['dkm_desc'] = $currDesc;
						    	$data['dkm_active'] = $currFlag;
						    	$data['dkm_updated'] = $currDate;
						    	$currResult = 'kamus : tidak ada perubahan data';
						    	if(empty($checkData)){ // insert baru
						    		$result = $this->CI->model_kamus->update($data,NULL,'insert');
						    		if($result['state']) $currResult = 'kamus : insert = > '.$currUid;
						    	}elseif($currDate > $checkDate){ // update
						    		$data['filter_dkm_uid'] = $currUid;
						    		$result = $this->CI->model_kamus->update($data,NULL,'update');
						    		if($result['state']) $currResult = 'kamus : update = > '.$currUid;
						    	}
						    	echo $currResult.'<br />';
					    	}else{
						    	echo 'Tidak ada proses penyimpanan kamus<br />';
					    	}
					    }elseif($currType == 'key'){
					    	$currMsg = $this->CI->services_json->decode($lineMsg);
					    	if(!empty($currMsg)){
						    	$currUid = $this->CI->additional_function->set_value($currMsg,'dkk_uid');
						    	$currDate = $this->CI->additional_function->set_value($currMsg,'dkk_updated');
						    	$currKey = $this->CI->additional_function->set_value($currMsg,'dkk_key');
						    	$currDesc = $this->CI->additional_function->set_value($currMsg,'dkk_desc');
						    	$currMasterUID = $this->CI->additional_function->set_value($currMsg,'dkk_master_uid');
						    	$currFlag = (isset($currMsg->dkk_active)?$currMsg->dkk_active:1);

						    	$checkData = $this->CI->model_kamus->detail_key(array('filter_dkk_uid'=>$currUid),NULL,$this->CI->model_kamus->SINGLE);
						    	$checkDate = $this->CI->additional_function->set_value($checkData,'dkk_updated');

						    	$data['dkk_key'] = $currKey;
						    	$data['dkk_desc'] = $currDesc;
						    	$data['dkk_master_uid'] = $currMasterUID;
						    	$data['dkk_active'] = $currFlag;
						    	$data['dkk_updated'] = $currDate;
						    	$data['dkk_uid'] = $currUid;
						    	$currResult = 'key : tidak ada perubahan data';
						    	if(empty($checkData)){ // insert baru
						    		$result = $this->CI->model_kamus->update_key($data,NULL,'insert');
						    		if($result['state']) $currResult = 'key : insert = > '.$currUid;
						    	}elseif($currDate > $checkDate){ // update
						    		$data['filter_dkk_uid'] = $currUid;
						    		$result = $this->CI->model_kamus->update_key($data,NULL,'update');
						    		if($result['state']) $currResult = 'key : update = > '.$currUid;
						    	}
						    	echo $currResult.'<br />';
					    	}else{
						    	echo 'Tidak ada proses penyimpanan key<br />';
					    	}
					    }

					    $line++;
					}
					fclose($fp);
				}

				unlink($this->path.$file);
			}

		}
        $this->CI->db->query("update default_kamus_key set dkk_master = (select dkm_id from default_kamus_master where dkm_uid = dkk_master_uid limit 1)");
		
		exit;
	}

	function get_hierarchy($arrParams=array()){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$currentId = $this->CI->additional_function->get_elm_priority(
			array('key'=>'current_uid','val'=>$defParam),
			array('key'=>'current_uid','val'=>$_GET),
			array('key'=>'current_uid','val'=>$_POST),
			$this->CI->additional_function->set_value($arrParams,0)/*,
			'11e7bd604f77ad0faad2791a141940f7'*/
		);

		if(!empty($currentId)){
			$parentUid = $this->CI->model_kamus->_get_parent($currentId);
			$allChild = $this->CI->model_kamus->get_hierarchy($parentUid,TRUE);
			echo $this->CI->services_json->encode($allChild);exit;
		}else{
			echo $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>"no parent id is defined"));exit;
		}
	}

	function management($uriVars=array()){
		if(empty($_SESSION['access_matrix']) || $_SESSION['access_matrix'] != 'sangpetualang'){
			$_SESSION['prevent_url'] = config_item('global_instMainUrl').'module/panel/kamus';
			header("location:".config_item('global_instMainUrl').'login');
		}

		$defParam = $this->CI->additional_function->set_param($uriVars);
		$masterId = $this->CI->additional_function->get_elm_priority(
			array('key'=>'master_id','val'=>$defParam),
			array('key'=>'master_id','val'=>$_GET),
			array('key'=>'master_id','val'=>$_POST)
		);

		$this->CI->load->specific_model('kamus');
		$cond = new stdClass();
		$cond->field = 'dkm_uid,dkm_title';
		$allKamus = $this->CI->model_kamus->detail(array('limit'=>FALSE,'ordid'=>'dkm_title','ordtype'=>'asc','filter_dkm_active'=>1),$cond);

		$this->CI->config->set_item('title','kumpulan kamus');
		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
			'all_master' => $allKamus,
			'master_id' => $masterId
		);
	}

	function kamus_cetak($uriVars=array()){
		if(empty($_SESSION['access_matrix']) || $_SESSION['access_matrix'] != 'sangpetualang'){
			$_SESSION['prevent_url'] = config_item('global_instMainUrl').'module/panel/kamus';
			header("location:".config_item('global_instMainUrl').'login');
		}

		$defParam = $this->CI->additional_function->set_param($uriVars);
		$masterId = $this->CI->additional_function->get_elm_priority(
			array('key'=>'master_id','val'=>$defParam),
			array('key'=>'master_id','val'=>$_GET),
			array('key'=>'master_id','val'=>$_POST)
		);
		$childInclude = $this->CI->additional_function->get_elm_priority(
			array('key'=>'child_include','val'=>$defParam),
			array('key'=>'child_include','val'=>$_GET),
			array('key'=>'child_include','val'=>$_POST),
			0
		);

		$this->CI->load->specific_model('kamus');
		$cond = new stdClass();
		$kamus = $this->CI->model_kamus->detail(array('filter_dkm_id'=>$masterId),$cond,$this->CI->model_kamus->SINGLE);
		$currentUid = $this->CI->additional_function->set_value($kamus,'dkm_uid');
		$currentTitle = $this->CI->additional_function->set_value($kamus,'dkm_title');

		$parentUid = $this->CI->model_kamus->_get_parent($currentUid,TRUE);
		$allData = $this->CI->model_kamus->get_hierarchy($parentUid,TRUE);

		$this->CI->config->set_item('title','kumpulan kamus');

		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer',
			'master_id' => $masterId,
			'master_title' => $currentTitle,
			'child_include' => $childInclude,
			'allData' => $allData
		);
	}

	function kamus_detail($uriVars=array()){
		if(empty($_SESSION['access_matrix']) || $_SESSION['access_matrix'] != 'sangpetualang'){
			$_SESSION['prevent_url'] = config_item('global_instMainUrl').'module/panel/kamus';
			header("location:".config_item('global_instMainUrl').'login');
		}

		$defParam = $this->CI->additional_function->set_param($uriVars);
		$masterId = $this->CI->additional_function->get_elm_priority(
			array('key'=>'master_id','val'=>$defParam),
			array('key'=>'master_id','val'=>$_GET),
			array('key'=>'master_id','val'=>$_POST)
		);
		$childInclude = $this->CI->additional_function->get_elm_priority(
			array('key'=>'child_include','val'=>$defParam),
			array('key'=>'child_include','val'=>$_GET),
			array('key'=>'child_include','val'=>$_POST),
			0
		);
		$openstatus = $this->CI->additional_function->get_elm_priority(
			array('key'=>'open','val'=>$defParam),
			array('key'=>'open','val'=>$_GET),
			array('key'=>'open','val'=>$_POST),
			0
		);

		$this->CI->load->specific_model('kamus');
		$cond = new stdClass();
		$kamus = $this->CI->model_kamus->detail(array('filter_dkm_uid'=>$masterId),$cond,$this->CI->model_kamus->SINGLE);
		$currentUid = $this->CI->additional_function->set_value($kamus,'dkm_uid');
		$currentTitle = $this->CI->additional_function->set_value($kamus,'dkm_title');

		$parentUid = $this->CI->model_kamus->_get_parent($currentUid,TRUE);
		$allData = $this->CI->model_kamus->get_hierarchy($parentUid,TRUE);

		$this->CI->config->set_item('title','kumpulan kamus');

		if(!empty($_GET['title'])) $this->CI->config->set_item('title',$_GET['title']);
		
		return array(
			'use_header' => 'master_header_js',
			'use_footer' => 'master_footer_js',
			'use_sidebar' => FALSE,
			'master_id' => $masterId,
			'master_title' => $currentTitle,
			'child_include' => $childInclude,
			'allData' => $allData,
			'openstatus' => $openstatus
		);
	}
	
} 

<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_user
{
	var $CI = NULL;
	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');
		$this->CI->load->specific_model('user');
	}
	
	// restful api request
	function index($arrParams=array()){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$itemId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($itemId)){
			$returnType = $this->CI->model_excel->SINGLE;
			$cond->where[] = array('du_id'=>$itemId);
		}elseif(!empty($itemId)){
			$cond->where_in[] = array(
				"fieldName"=>'du_id',
				"fieldValue"=>$itemId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){
			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','mainmenu') &&  FALSE){
				$menuList = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->model_user->update($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$_DELETE['bypass_access'] = TRUE;
					$result = $this->CI->model_user->remove($_DELETE);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->model_user->update($_POST,$cond,'insert');
				}
				$userLst = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_user->MULTIPLE;
			}
			// ambil data untuk semua $_SESSION['accessName'], jadi jgn force local
			$result = $this->CI->model_user->get_detail($_GET,$cond,$returnType);
			$userLst = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'userLst' => $userLst
		);
	}

	// restful api request
	function request($arrParams=array()){
		header('Content-Type: application/json');
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$itemId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($itemId)){
			$returnType = $this->CI->model_user->SINGLE;
			$cond->where[] = array('du_id'=>$itemId);
		}elseif(!empty($itemId)){
			$cond->where_in[] = array(
				"fieldName"=>'du_id',
				"fieldValue"=>$itemId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		$method = $this->CI->additional_function->get_elm_priority(
			array('key'=>'method','val'=>$defParam),
			array('key'=>'method','val'=>$_GET),
			array('key'=>'method','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(strtolower($_SERVER['REQUEST_METHOD']) == 'get'){
			// if(strtolower($method) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->model_user->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->model_user->get_detail($_GET,$cond,$returnType);
			$resultLst = $this->CI->services_json->encode($result);
		}elseif(
			strtolower($method) == 'put'
			|| strtolower($method) == 'post'
			|| strtolower($method) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$resultLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($method) == 'put'){
					header("cache-control: max-age=no-store");

					$result = $this->CI->model_user->update($_POST,$cond,'update');
				}elseif(strtolower($method) == 'delete'){
					header("cache-control: max-age=no-store");
					
					$result = $this->CI->model_user->remove($_POST,$cond);
				}elseif(strtolower($method) == 'post'){
					header("cache-control: max-age=no-store");
					
					$result = $this->CI->model_user->update($_POST,NULL,'insert');
				}
				$resultLst = $this->CI->services_json->encode($result);
			}
		}else{
			die($this->services_json->encode(array('state'=>FALSE,'msg'=>'request is not defined')));
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'resultLst' => $resultLst
		);
	}

	public function switch_language($arrParams=NULL){
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$language = $this->CI->additional_function->get_elm_priority(
			array('key'=>'language','val'=>$defParam),
			array('key'=>'language','val'=>$_POST),
			array('key'=>'language','val'=>$_GET)
		);
		if($language == 'indonesia') unset($_SESSION['default_language']);
		else $_SESSION['default_language'] = ($language);
	}

	public function check_loggedin($arrParams=NULL){
		$result = $this->CI->model_user->check_user_session();
		echo $this->CI->services_json->encode($result);exit;
	}

	public function get_session($arrParams=NULL){
		echo $this->CI->services_json->encode($_SESSION);exit;
	}

	function get_panel_user(){
		$this->CI->load->specific_module('user');
		$module_methods = get_class_methods('Module_user');
		$module_menus = $this->CI->additional_function->array_minus($module_methods,array('__construct','index'));
		echo $this->CI->services_json->encode($module_menus);exit;
	}

	// untuk parameter jsonUrl:field pada table js
	final function get_all_field($arrParams=array()){
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$dc_filter = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_filter','val'=>$defParam),
			array('key'=>'dc_filter','val'=>$_POST),
			array('key'=>'dc_filter','val'=>$_GET)
		);

		$this->CI->load->specific_model('column');
		$tableName = 'default_user';
		$allField = $this->CI->model_column->get_all_field($tableName,$dc_filter);
		return array(
			'allField' => $allField
		);
	}

	// untuk header dari js dengan dinamis column
	final function get_din_field($arrParams=array()){
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$dc_filter = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_filter','val'=>$defParam),
			array('key'=>'dc_filter','val'=>$_POST),
			array('key'=>'dc_filter','val'=>$_GET)
		);

		$this->CI->load->specific_model('column');
		$tableName = 'default_user';
		return array(
			'allField' => $this->CI->model_column->get_din_field($tableName,$dc_filter,1)
		);
	}

	// untuk mendapatkan attribute dari setiap column
	final function get_attr_field($arrParams=NULL){
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$tableName = 'default_user';
		$colName = $this->CI->additional_function->get_elm_priority(
			array('key'=>'column','val'=>$defParam),
			array('key'=>'column','val'=>$_GET),
			array('key'=>'column','val'=>$_POST)
		);

		$this->CI->load->specific_model('column');

		$result = $this->CI->model_column->get_attr_field($tableName,$colName);

		return array(
			'detailField' => $result
		);

	}

	// menambahkan field untuk table user
	final function add_field($arrParams=NULL){
		$result = array(
			'state' => FALSE,
			'msg' => 'blm di proses'
		);
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$tableName = 'default_user';
		$dc_title = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_title','val'=>$defParam),
			array('key'=>'dc_title','val'=>$_GET),
			array('key'=>'dc_title','val'=>$_POST)
		);
		$slug = 'du_'.$this->CI->additional_function->get_slug($dc_title);
		$dc_show = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_show','val'=>$defParam),
			array('key'=>'dc_show','val'=>$_GET),
			array('key'=>'dc_show','val'=>$_POST)
		);
		$dc_width = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_width','val'=>$defParam),
			array('key'=>'dc_width','val'=>$_GET),
			array('key'=>'dc_width','val'=>$_POST)
		);
		$dc_order = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_order','val'=>$defParam),
			array('key'=>'dc_order','val'=>$_GET),
			array('key'=>'dc_order','val'=>$_POST)
		);
		$dc_filter = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_filter','val'=>$defParam),
			array('key'=>'dc_filter','val'=>$_GET),
			array('key'=>'dc_filter','val'=>$_POST)
		);
		$dc_multiple = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_multiple','val'=>$defParam),
			array('key'=>'dc_multiple','val'=>$_GET),
			array('key'=>'dc_multiple','val'=>$_POST)
		);
		$dc_protected = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_protected','val'=>$defParam),
			array('key'=>'dc_protected','val'=>$_GET),
			array('key'=>'dc_protected','val'=>$_POST)
		);

		$dc_type = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_type','val'=>$defParam),
			array('key'=>'dc_type','val'=>$_GET),
			array('key'=>'dc_type','val'=>$_POST)
		);
		$dc_default = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_default','val'=>$defParam),
			array('key'=>'dc_default','val'=>$_GET),
			array('key'=>'dc_default','val'=>$_POST)
		);

		$dc_t_type = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_t_type','val'=>$defParam),
			array('key'=>'dc_t_type','val'=>$_GET),
			array('key'=>'dc_t_type','val'=>$_POST)
		);
		$dc_t_width = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_t_width','val'=>$defParam),
			array('key'=>'dc_t_width','val'=>$_GET),
			array('key'=>'dc_t_width','val'=>$_POST)
		);
		$dc_comment = NULL;
		if(!empty($defParam['dc_comment'])) $dc_comment = $defParam['dc_comment'];
		elseif(!empty($_GET['dc_comment'])) $dc_comment = $_GET['dc_comment'];
		elseif(!empty($_POST['dc_comment'])) $dc_comment = $_POST['dc_comment'];

		if(!empty($dc_title)){
			$data = array();
			$data['dc_table'] = $tableName;
			$data['dc_slug'] = $slug;
			$data['dc_title'] = $dc_title;
			$data['dc_show'] = (!empty($dc_show)?1:0);
			$data['dc_width'] = (!empty($dc_width)?intval($dc_width):100);
			$data['dc_order'] = (!empty($dc_order)?intval($dc_order):0);
			$data['dc_filter'] = $dc_filter;
			$data['dc_protected'] = $dc_protected;
			$data['dc_multiple'] = (!empty($dc_multiple)?intval($dc_multiple):0);
			$data['dc_comment'] = $dc_comment;
			
			$data['dc_type'] = $dc_type;

			if($data['dc_type'] == 'text' || $data['dc_type'] == 'textarea') $currType = 'single';
			else $currType = 'multiple';

			$default = explode("\n",$dc_default);
			if($currType == 'single' && count($default)>1) $result['msg'] = 'default data tidak sesuai dengan type data';

			if($currType == 'single'){
				$currDefault = $default[0];
			}elseif($currType == 'multiple'){
				$currDefault = array();
				foreach ($default as $idxDefault => $valDefault) {
					$partDefault = explode('-',$valDefault);
					$idx = trim($partDefault[0]);
					$val = (!empty($partDefault[1])?trim($partDefault[1]):$idx);
					$currDefault[$idx] = $val;
				}
			}
			$data['dc_default'] = $this->CI->services_json->encode($currDefault);

			$data['dc_t_type'] = (!empty($dc_t_type)?$dc_t_type:'varchar');
			$data['dc_t_width'] = (!empty($dc_t_width)?intval($dc_t_width):150);
			if($data['dc_t_type'] == 'date') $attr = $data['dc_t_type'];
			elseif($data['dc_t_type'] == 'text') $attr = $data['dc_t_type'];
			else $attr = sprintf('%s(%s)',$data['dc_t_type'],$data['dc_t_width']);

			$this->CI->load->specific_model('column');
			
			// cek kolom di table
			$cond = new stdClass();
			$cond->query = sprintf("SHOW COLUMNS FROM %s where field = '%s'",$tableName,$slug);
			$currResult = $this->CI->model_column->get_detail(NULL,$cond,$this->CI->user_data->SINGLE);

			if(empty($currResult)){
				$this->curr_connection = $this->CI->load->database($this->CI->user_data->db_connection_config,TRUE);		
				$this->curr_connection->reset_query();
				$currResult = @$this->curr_connection->query(sprintf("alter table %s add column %s %s NULL COMMENT '%s'",$tableName,$slug,$attr,addslashes($dc_comment)));
				$this->curr_connection->reset_query();
				if($currResult===TRUE){
				}
			}else{
				$data['dc_protected'] = 1; // paksa untuk di protect
				$result = $this->CI->model_column->update($data,NULL,'insert');
				$result['msg'] = 'kolom ini sudah tersedia';
			}

		}
		echo $this->CI->services_json->encode($result);exit;
	}

	// update field untuk table user
	// hanya boleh update field, type(sejenis), default(sejenis), show, width, order
	// 1. check apakah sudah ada di table user
	// 2. check apakah sudah ada attribute di table column (jika ada :update, jika blm :tambah)
	// * tidak bisa edit t_type, t_type diambil secara default dari attribute table
	final function update_field($arrParams=NULL){
		$result = array(
			'state' => FALSE,
			'msg' => 'blm di proses'
		);
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$tableName = 'default_user';
		$dc_slug = $this->CI->additional_function->get_elm_priority(
			array('key'=>'filter_dc_slug','val'=>$defParam),
			array('key'=>'filter_dc_slug','val'=>$_GET),
			array('key'=>'filter_dc_slug','val'=>$_POST)
		);
		$dc_t_width = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_t_width','val'=>$defParam),
			array('key'=>'dc_t_width','val'=>$_GET),
			array('key'=>'dc_t_width','val'=>$_POST)
		);
		$dc_comment = NULL;
		if(!empty($defParam['dc_comment'])) $dc_comment = $defParam['dc_comment'];
		elseif(!empty($_GET['dc_comment'])) $dc_comment = $_GET['dc_comment'];
		elseif(!empty($_POST['dc_comment'])) $dc_comment = $_POST['dc_comment'];

		$this->CI->load->specific_model('column');

		// cek kolom di table
		$cond = new stdClass();
		$cond->query = sprintf("SHOW COLUMNS FROM %s where field = '%s'",$tableName,$dc_slug);
		$result = $this->CI->model_column->get_detail(NULL,$cond,$this->CI->user_data->SINGLE);
		$currType = $this->CI->additional_function->set_value($result,'Type');
		$currType = explode('(',$currType);
		$currType = $currType[0];

		// jika ada kolom pasti ada type
		if(!empty($currType)){
			$data = array();
			$data['dc_t_type'] = $currType;
			$data['dc_t_width'] = (!empty($dc_t_width)?intval($dc_t_width):150);

			if($data['dc_t_type'] == 'date') $attr = $data['dc_t_type'];
			elseif($data['dc_t_type'] == 'text') $attr = $data['dc_t_type'];
			else $attr = sprintf('%s(%s)',$data['dc_t_type'],$data['dc_t_width']);

			$this->curr_connection = $this->CI->load->database($this->CI->user_data->db_connection_config,TRUE);		
			$this->curr_connection->reset_query();
			$result = @$this->curr_connection->query(sprintf("alter table %s modify column %s %s COMMENT '%s'",$tableName,$dc_slug,$attr,addslashes($dc_comment)));
			$this->curr_connection->reset_query();

			if($result===TRUE){
				$dc_title = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_title','val'=>$defParam),
					array('key'=>'dc_title','val'=>$_GET),
					array('key'=>'dc_title','val'=>$_POST)
				);
				$dc_show = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_show','val'=>$defParam),
					array('key'=>'dc_show','val'=>$_GET),
					array('key'=>'dc_show','val'=>$_POST)
				);
				$dc_width = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_width','val'=>$defParam),
					array('key'=>'dc_width','val'=>$_GET),
					array('key'=>'dc_width','val'=>$_POST)
				);
				$dc_order = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_order','val'=>$defParam),
					array('key'=>'dc_order','val'=>$_GET),
					array('key'=>'dc_order','val'=>$_POST)
				);

				$dc_type = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_type','val'=>$defParam),
					array('key'=>'dc_type','val'=>$_GET),
					array('key'=>'dc_type','val'=>$_POST)
				);
				$dc_default = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_default','val'=>$defParam),
					array('key'=>'dc_default','val'=>$_GET),
					array('key'=>'dc_default','val'=>$_POST)
				);
				$dc_filter = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_filter','val'=>$defParam),
					array('key'=>'dc_filter','val'=>$_GET),
					array('key'=>'dc_filter','val'=>$_POST)
				);
				$dc_protected = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_protected','val'=>$defParam),
					array('key'=>'dc_protected','val'=>$_GET),
					array('key'=>'dc_protected','val'=>$_POST)
				);
				$dc_multiple = $this->CI->additional_function->get_elm_priority(
					array('key'=>'dc_multiple','val'=>$defParam),
					array('key'=>'dc_multiple','val'=>$_GET),
					array('key'=>'dc_multiple','val'=>$_POST)
				);

				if(!empty($dc_title)){
					$data['dc_table'] = $tableName;
					$data['filter_dc_table'] = $tableName;
					$data['dc_slug'] = $dc_slug;
					$data['filter_dc_slug'] = $dc_slug;
					$data['dc_title'] = $dc_title;
					$data['dc_show'] = (!empty($dc_show)?1:0);
					$data['dc_width'] = (!empty($dc_width)?intval($dc_width):100);
					$data['dc_order'] = (!empty($dc_order)?intval($dc_order):0);
					$data['dc_filter'] = $dc_filter;
					$data['dc_protected'] = $dc_protected;
					$data['dc_multiple'] = (!empty($dc_multiple)?intval($dc_multiple):0);
					$data['dc_comment'] = $dc_comment;
					
					$data['dc_type'] = $dc_type;

					if($data['dc_type'] == 'text' || $data['dc_type'] == 'textarea') $currType = 'single';
					else $currType = 'multiple';

					$default = explode("\n",$dc_default);
					if($currType == 'single' && count($default)>1) $result['msg'] = 'default data tidak sesuai dengan type data';

					if($currType == 'single'){
						$currDefault = $default[0];
					}elseif($currType == 'multiple'){
						$currDefault = array();
						foreach ($default as $idxDefault => $valDefault) {
							$partDefault = explode('-',$valDefault);
							$idx = trim($partDefault[0]);
							$val = (!empty($partDefault[1])?trim($partDefault[1]):$idx);
							$currDefault[$idx] = $val;
						}
					}
					$data['dc_default'] = $this->CI->services_json->encode($currDefault);
					$result = $this->CI->model_column->update($data,NULL,'update');
					if(!$result['state']){
						$result = $this->CI->model_column->update($data,NULL,'insert');
					}
				}else{
					$result['msg'] = 'tidak bisa ditambahkan ke table column karena tidak ada title';
				}
			}else{
				$result = array(
					'state' => FALSE,
					'msg' => 'column tidak bisa di modify : '.sprintf('alter table %s modify column %s %s',$tableName,$dc_slug,$attr)
				);
			}

		}else{
			$this->remove_field(array('column'=>$dc_slug));
		}

		echo $this->CI->services_json->encode($result);exit;
	}

	// secara paralel
	// 1. hapus di table user
	// 2. hapus di table attribute user
	final function remove_field($arrParams=NULL){
		$this->CI->load->specific_model('column');
		$result = array(
			'state' => FALSE,
			'msg' => 'blm di proses'
		);
		$defParam = $this->CI->additional_function->set_param($arrParams);
		$tableName = 'default_user';
		$dc_slug = $this->CI->additional_function->get_elm_priority(
			array('key'=>'dc_slug','val'=>$defParam),
			array('key'=>'dc_slug','val'=>$_GET),
			array('key'=>'dc_slug','val'=>$_POST),
			array('key'=>'filter_dc_slug','val'=>$defParam),
			array('key'=>'filter_dc_slug','val'=>$_GET),
			array('key'=>'filter_dc_slug','val'=>$_POST)
		);
		$currColumn = $this->CI->model_column->get_detail(array('filter_dc_slug'=>$dc_slug),NULL,$this->CI->model_column->SINGLE);
		$currProtected = $this->CI->additional_function->set_value($currColumn,$this->CI->model_column->get_field_name('dc_protected'));
		if($currProtected != 1){
			// hapus di table user
			$cond = new stdClass();
			$cond->query = sprintf("SHOW COLUMNS FROM %s where field = '%s'",$tableName,$dc_slug);
			$currRes = $this->CI->model_column->get_detail(NULL,$cond,$this->CI->user_data->SINGLE);
			if(!empty($currRes)){
				$this->curr_connection = $this->CI->load->database($this->CI->user_data->db_connection_config,TRUE);
				$this->curr_connection->reset_query();
				$currRes = @$this->curr_connection->query(sprintf('alter table %s drop column %s',$tableName,$dc_slug));
				$this->curr_connection->reset_query();
			}

			// hapus di table attribut column
			$cond = new stdClass();
			$cond->where[] = array('dc_slug'=>$dc_slug);
			$cond->where[] = array('dc_table'=>$tableName);
			$this->CI->model_column->remove(NULL,$cond,TRUE);

			$result = array(
				'state' => TRUE,
				'msg' => 'column sudah di hapus'
			);
		}else{
			$result = array(
				'state' => FALSE,
				'msg' => 'kolom terproteksi'
			);
		}
		echo $this->CI->services_json->encode($result);exit;
	}

	final function create_user_cache(){
		$lstUnique = $this->access->get_access('unique_name');
		$arrParam['unique_name'] = $lstUnique[0];
		if($arrParam['unique_name'] == 'is1_staff' || $arrParam['unique_name'] == 'is3_staff'){
			$allUser = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],FALSE,TRUE,NULL,FALSE);
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],TRUE,FALSE,NULL,FALSE);
		}elseif($arrParam['unique_name'] == 'is2_staff'){
			$allUser = $this->user_data->get_detail(array('unique_name'=>'is1_staff','limit'=>FALSE,'bypass_instance'=>TRUE));
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_detail();
		}elseif($arrParam['unique_name'] == 'corporate_staff'){
			$allUser = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],FALSE,TRUE,NULL,FALSE);
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],TRUE,FALSE,NULL,FALSE);
		}

		$this->filecache->cache_save('allUser1',$allUser,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$allUser2 = $this->additional_function->get_array($allUser,'agent_id');
		$this->filecache->cache_save('allUser2',$allUser2,'user');//[agentId1,agentId2,..]
		$allUser3 = $this->additional_function->array_linear($allUser,FALSE,0,1);
		$this->filecache->cache_save('allUser3',$allUser3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$allUser4 = $this->additional_function->array_linear($allUser,FALSE,0,1);
		if(!empty($allUser4))
			asort($allUser4);
		$this->filecache->cache_save('allUser4',$allUser4,'user');//[agentId1:name1,agentId2:name2,..]
		$allUser5 = $this->additional_function->array_linear($allUser,FALSE,1,0);
		$this->filecache->cache_save('allUser5',$allUser5,'user');//[agentId1,agentId2,..]

		// semua user yang bisa diakses tidak termasuk user login
		$this->filecache->cache_save('allChild1',$allChild,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$allChild2 = $this->additional_function->get_array($allChild,'agent_id');
		$this->filecache->cache_save('allChild2',$allChild2,'user');//[agentId1,agentId2,..]
		$allChild3 = $this->additional_function->array_linear($allChild,FALSE,0,1);
		$this->filecache->cache_save('allChild3',$allChild3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$allChild4 = $this->additional_function->array_linear($allChild,FALSE,0,1);
		if(!empty($allChild4))
			asort($allChild4);
		$this->filecache->cache_save('allChild4',$allChild4,'user');//[agentId1:name1,agentId2:name2,..]
		$allChild5 = $this->additional_function->array_linear($allChild,FALSE,1,0);
		$this->filecache->cache_save('allChild5',$allChild5,'user');//[agentId1,agentId2,..]
		
		// semua user yang bisa diakses tidak termasuk user dan login khusus level 2
		$this->filecache->cache_save('duaNdChild1',$duaNdChild,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$duaNdChild2 = $this->additional_function->get_array($duaNdChild,'agent_id');
		$this->filecache->cache_save('duaNdChild2',$duaNdChild2,'user');//[agentId1,agentId2,..]
		$duaNdChild3 = $this->additional_function->array_linear($duaNdChild,FALSE,0,1);
		$this->filecache->cache_save('duaNdChild3',$duaNdChild3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$duaNdChild4 = $this->additional_function->array_linear($duaNdChild,FALSE,0,1);
		if(!empty($duaNdChild4))
			asort($duaNdChild4);
		$this->filecache->cache_save('duaNdChild4',$duaNdChild4,'user');//[agentId1:name1,agentId2:name2,..]
		$duaNdChild5 = $this->additional_function->array_linear($duaNdChild,FALSE,1,0);
		$this->filecache->cache_save('duaNdChild5',$duaNdChild5,'user');//[agentId1,agentId2,..]
	}

	final function create_active_cache(){
		$lstUnique = $this->access->get_access('unique_name');
		$arrParam['unique_name'] = $lstUnique[0];
		if($arrParam['unique_name'] == 'is1_staff' || $arrParam['unique_name'] == 'is3_staff'){
			$allUser = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],FALSE,TRUE,NULL,TRUE,'is1_staff');
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],TRUE,FALSE,NULL,TRUE);
		}elseif($arrParam['unique_name'] == 'is2_staff'){
			$allUser = $this->user_data->get_detail(array('unique_name'=>'is1_staff','filter_su_active'=>TRUE,'bypass_instance'=>TRUE));
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_detail();
		}elseif($arrParam['unique_name'] == 'corporate_staff'){
			$allUser = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],FALSE,TRUE,NULL,TRUE);
			$tmpAllUser = $allUser;
			$allChild = array_splice($tmpAllUser,1,count($tmpAllUser));
			$tmpAllUser = NULL;
			$duaNdChild = $this->user_data->get_hierarchy($_SESSION['pdeden_userId'],TRUE,FALSE,NULL,TRUE);
		}

		$this->filecache->cache_save('activeUser1',$allUser,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$allUser2 = $this->additional_function->get_array($allUser,'agent_id');
		$this->filecache->cache_save('activeUser2',$allUser2,'user');//[agentId1,agentId2,..]
		$allUser3 = $this->additional_function->array_linear($allUser,FALSE,0,1);
		$this->filecache->cache_save('activeUser3',$allUser3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$allUser4 = $this->additional_function->array_linear($allUser,FALSE,0,1);
		if(!empty($allUser4))
			asort($allUser4);
		$this->filecache->cache_save('activeUser4',$allUser4,'user');//[agentId1:name1,agentId2:name2,..]
		$allUser5 = $this->additional_function->array_linear($allUser,FALSE,1,0);
		$this->filecache->cache_save('activeUser5',$allUser5,'user');//[agentId1,agentId2,..]

		// semua user yang bisa diakses tidak termasuk user login
		$this->filecache->cache_save('activeChild1',$allChild,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$allChild2 = $this->additional_function->get_array($allChild,'agent_id');
		$this->filecache->cache_save('activeChild2',$allChild2,'user');//[agentId1,agentId2,..]
		$allChild3 = $this->additional_function->array_linear($allChild,FALSE,0,1);
		$this->filecache->cache_save('activeChild3',$allChild3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$allChild4 = $this->additional_function->array_linear($allChild,FALSE,0,1);
		if(!empty($allChild4))
			asort($allChild4);
		$this->filecache->cache_save('activeChild4',$allChild4,'user');//[agentId1:name1,agentId2:name2,..]
		$allChild5 = $this->additional_function->array_linear($allChild,FALSE,1,0);
		$this->filecache->cache_save('activeChild5',$allChild5,'user');//[agentId1,agentId2,..]
		
		// semua user yang bisa diakses tidak termasuk user dan login khusus level 2
		$this->filecache->cache_save('activeNdChild1',$duaNdChild,'user');//[{agentId1,name1,..},{agentId2,name2,..}]
		$duaNdChild2 = $this->additional_function->get_array($duaNdChild,'agent_id');
		$this->filecache->cache_save('activeNdChild2',$duaNdChild2,'user');//[agentId1,agentId2,..]
		$duaNdChild3 = $this->additional_function->array_linear($duaNdChild,FALSE,0,1);
		$this->filecache->cache_save('activeNdChild3',$duaNdChild3,'user');//[agentId1:name1,agentId2:name2,..] as hierarchy format
		$duaNdChild4 = $this->additional_function->array_linear($duaNdChild,FALSE,0,1);
		if(!empty($duaNdChild4))
			asort($duaNdChild4);
		$this->filecache->cache_save('activeNdChild4',$duaNdChild4,'user');//[agentId1:name1,agentId2:name2,..]
		$duaNdChild5 = $this->additional_function->array_linear($duaNdChild,FALSE,1,0);
		$this->filecache->cache_save('activeNdChild5',$duaNdChild5,'user');//[agentId1,agentId2,..]
	}
} 

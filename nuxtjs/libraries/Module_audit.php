<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_audit
{
	var $CI = NULL;
		
	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');

		$this->CI->load->model('master/audit_process');
	}
	
	// restful api request
	function index($arrParams=array()){
		header('Content-Type: application/json');
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);

		// sebagai ID
		$itemId = $this->CI->additional_function->set_value($arrParams,0,'number');
		if(is_numeric($itemId)){
			$returnType = $this->CI->audit_process->SINGLE;
			$cond->where[] = array('da2_id'=>$itemId);
		}elseif(!empty($itemId)){
			$cond->where_in[] = array(
				"fieldName"=>'da2_id',
				"fieldValue"=>$itemId
			);
		}
		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
		$method = $this->CI->additional_function->get_elm_priority(
			array('key'=>'method','val'=>$defParam),
			array('key'=>'method','val'=>$_GET),
			array('key'=>'method','val'=>$_POST)
		);
		if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(strtolower($_SERVER['REQUEST_METHOD']) == 'get'){
			// if(strtolower($method) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnType = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnType) && !is_numeric($returnType) && strlen($returnType)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnType),'number');
			}elseif($returnType == ''){
				$returnType = $this->CI->audit_process->MULTIPLE;
			}
			// ambil data
			$result = $this->CI->audit_process->get_detail($_GET,$cond,$returnType);
			$resultLst = $this->CI->services_json->encode($result);
		}elseif(
			strtolower($method) == 'put'
			|| strtolower($method) == 'post'
			|| strtolower($method) == 'delete'
			){

			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','lead') &&  FALSE){
				$resultLst = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($method) == 'put'){
					header("cache-control: max-age=no-store");

					$result = $this->CI->audit_process->update($_POST,$cond,'update');
				}elseif(strtolower($method) == 'delete'){
					header("cache-control: max-age=no-store");
					
					$result = $this->CI->audit_process->remove($_POST,$cond);
				}elseif(strtolower($method) == 'post'){
					header("cache-control: max-age=no-store");
					
					$result = $this->CI->audit_process->update($_POST,NULL,'insert');
				}
				$resultLst = $this->CI->services_json->encode($result);
			}
		}else{
			die($this->services_json->encode(array('state'=>FALSE,'msg'=>'request is not defined')));
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'resultLst' => $resultLst
		);
	}

	function management(){
		// if(!in_array('management',$this->user_panel)){
		// 	header("location:".$this->CI->config->item('global_instMainUrl').'landing');
		// }

		return array(
			'use_header' => 'admin_header',
			'use_footer' => 'admin_footer'
		);
	}

} 

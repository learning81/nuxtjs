<?php
/**
 * 
 * Because this module is generated from controller,
 * all param from method is only one array param
 * @author akhyar
 *
 */
class Module_mainmenu
{
	var $CI = NULL;

	function __construct($params=NULL)
	{
		$this->CI =& get_instance();
		
		$this->CI->config->set_item('currModuleName',str_replace('module_', '', strtolower(get_class($this))));
		$this->CI->config->set_item('currModuleUrl',$this->CI->config->item('currCtrlrUrl')."module/".$this->CI->config->item('currModuleName').'/');
		
		$this->CI->load->specific_model('main_menu');
	}
	
	// restful api request
	function index($arrParams=NULL){
		$cond = new stdClass();
		$uriVars = array_slice($arrParams, 1);
		$defParam = $this->CI->additional_function->set_param($uriVars);
		$returnType = $this->CI->main_menu->MULTIPLE;

		// sebagai ID
		$menuName = $this->CI->additional_function->set_value($arrParams,0);
		if(FALSE !== strpos($menuName,',')){
			$returnType = $this->CI->main_menu->SINGLE;
			$cond->where[] = array('dm_name'=>$menuName);
		}elseif(!empty($menuName)){
			$cond->where_in[] = array(
				"fieldName"=>'dm_name',
				"fieldValue"=>$menuName
			);
		}

		// sebagai parameter model
		$checkField = $this->CI->additional_function->get_elm_priority(
			array('key'=>'field','val'=>$defParam),
			array('key'=>'field','val'=>$_GET),
			array('key'=>'field','val'=>$_POST)
		);
			if(!empty($checkField)) $cond->field = $checkField;

		$result = NULL;
		if(
			strtolower($_SERVER['REQUEST_METHOD']) == 'put'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'post'
			|| strtolower($_SERVER['REQUEST_METHOD']) == 'delete'
			){
			if($this->CI->access->get_access('site_maintenance',1) || !$this->CI->access->get_access('page_access','mainmenu') &&  FALSE){
				$menuList = $this->CI->services_json->encode(array('state'=>FALSE,'msg'=>'anda tidak memiliki akses ke halaman ini'));
			}else{
				if(strtolower($_SERVER['REQUEST_METHOD']) == 'put'){
					parse_str(file_get_contents("php://input"),$_PUT);
					$result = $this->CI->main_menu->update($_PUT,$cond,'update');
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'delete'){
					parse_str(file_get_contents("php://input"),$_DELETE);
					$result = $this->CI->main_menu->remove($_DELETE,$cond);
				}elseif(strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
					$result = $this->CI->main_menu->update($_POST,NULL,'insert');
				}
				$menuList = $this->CI->services_json->encode($result);
			}
		}else{ // if(strtolower($_SERVER['REQUEST_METHOD']) == 'get')
			// tentukan return type hanya pada get karena akan mengembalikan data
			$returnUri = $this->CI->additional_function->get_elm_priority(
				array('key'=>'return','val'=>$defParam),
				array('key'=>'return','val'=>$_GET)
			);
			if(!empty($returnUri) && !is_numeric($returnUri) && strlen($returnUri)>0){
				// string selain numeric
				$returnType = $this->CI->additional_function->set_value($this->CI->main_menu,strtoupper($returnUri),'number');
			}

			// ambil data
			$result = $this->CI->main_menu->get_detail($_GET,$cond,$returnType);
			$menuList = $this->CI->services_json->encode($result);
		}

		return array(
			'use_header' => FALSE,
			'use_footer' => FALSE,
			'menuList' => $menuList
		);
	}
	
}

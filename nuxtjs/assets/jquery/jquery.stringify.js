(function( $ ){
    function v(x) {
        return x < 10 ? "0" + x : x
    }
    Date.prototype.toJSON = function () {
        return this.getUTCFullYear() + "-" + v(this.getUTCMonth() + 1) + "-" + v(this.getUTCDate()) + "T" + v(this.getUTCHours()) + ":" + v(this.getUTCMinutes()) + ":" + v(this.getUTCSeconds()) + "Z"
    };
    String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function () {
        return this.valueOf();
    };
    var i = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        m = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        p,
        c, y = {
            "\u0008": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\u000c": "\\f",
            "\r": "\\r",
            '"': '\\"',
            "\\": "\\\\"
        },
        C;

    function I(x) {
        m.lastIndex = 0;
        return m.test(x) ? '"' + x.replace(m, function (E) {
            var u = y[E];
            if (typeof u === "string") return u;
            return "\\u" + ("0000" + (+E.charCodeAt(0)).toString(16)).slice(-4)
        }) + '"' : '"' + x + '"'
    }
    function J(x, E) {
        var u, A, s = p,
            t, w = E[x];
        if (w && typeof w === "object" && typeof w.toJSON === "function") w = w.toJSON(x);
        if (typeof C === "function") w = C.call(E, x, w);
        switch (typeof w) {
        case "string":
            return I(w);
        case "number":
            return isFinite(w) ?
                String(w) : "null";
        case "boolean":
        case "null":
            return String(w);
        case "object":
            if (!w) return "null";
            p += c;
            t = [];
            if (typeof w.length === "number" && !w.propertyIsEnumerable("length")) {
                A = w.length;
                for (x = 0; x < A; x += 1) t[x] = J(x, w) || "null";
                E = t.length === 0 ? "[]" : p ? "[\n" + p + t.join(",\n" + p) + "\n" + s + "]" : "[" + t.join(",") + "]";
                p = s;
                return E
            }
            if (C && typeof C === "object") {
                A = C.length;
                for (x = 0; x < A; x += 1) {
                    u = C[x];
                    if (typeof u === "string")
                        if (E = J(u, w)) t.push(I(u) + (p ? ": " : ":") + E)
                }
            } else
                for (u in w)
                    if (Object.hasOwnProperty.call(w, u))
                        if (E = J(u, w)) t.push(I(u) +
                            (p ? ": " : ":") + E);
            E = t.length === 0 ? "{}" : p ? "{\n" + p + t.join(",\n" + p) + "\n" + s + "}" : "{" + t.join(",") + "}";
            p = s;
            return E
        }
    }

    $.extend({
        prfl_stringify:true,
        stringify: function (x, E, u) {
            E = E;
            u = u;
            var A;
            c = p = "";
            if (typeof u === "number")
                for (A = 0; A < u; A += 1) c += " ";
            else if (typeof u === "string") c = u;
            if ((C = E) && typeof E !== "function" && (typeof E !== "object" || typeof E.length !== "number")) throw new Error("JSON.stringify");
            return J("", {
                "": x
            })
        },
        parse: function (x, E) {
            var u = E;

            function A(s, t) {
                var w, M, H = s[t];
                if (H && typeof H === "object")
                    for (w in H)
                        if (Object.hasOwnProperty.call(H,
                            w)) {
                            M = A(H, w);
                            if (M !== undefined) H[w] = M;
                            else delete H[w]
                        }
                return u.call(s, t, H)
            }
            i.lastIndex = 0;
            if (i.test(x)) x = x.replace(i, function (s) {
                return "\\u" + ("0000" + (+s.charCodeAt(0)).toString(16)).slice(-4)
            });
            if (/^[\],:{}\s]*$/.test(x.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) {
                x = eval("(" + x + ")");
                return typeof u === "function" ? A({
                    "": x
                }, "") : x
            }
            throw new Error("JSON.parse");
        },
        toString:function(paramObj){
            var paramStr = '';
            for(prtStr in paramObj){
                paramStr += '&'+prtStr+'='+encodeURI(paramObj[prtStr]);
            }
            return paramStr.substring(1);
        }
    });
})( jQuery );
//$.stringify(); -> json ke string
//$.parse(); -> string ke json
//$.toString(); ->json ke getURL
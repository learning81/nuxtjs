(function( $ ){
    var xhrPool = [];
    $(document).ajaxSend(function(e, jqXHR, options){
      xhrPool.push(jqXHR);
    });
    $(document).ajaxComplete(function(e, jqXHR, options) {
      xhrPool = $.grep(xhrPool, function(x){return x!=jqXHR});
    });
    //event.stopImmediatePropagation()

    $.extend({
        prflutils:true,
        reorder: function (items,order){
                // var items = [1,2,3,4,5,6,7,8,9,0]; => jenis array
                // var items = {'satu':'satu','dua':'dua','tiga':'tiga'}; => jenis object
                // var order = [5,3]; => data 1 pindah ke 5, data 2 pindah ke 3
                // var result = $.reorder(items,order);
                // var items = [3,4,2,5,1,6,7,8,9,0];
                // var items = {'satu':'satu','dua':'dua','tiga':'tiga'};

                if(order === undefined)
                    return items;

                var allKeys = [];
                var allValues = [];
                for(var i in items){
                    allKeys.push(i);
                    allValues.push(items[i]);
                }
                var itemLn = allValues.length;// tidak bisa items.length karena item(object) tidak memiliki length

                var newItem = {};
                var newKey = {};
                for(var i in order){
                    newItem[(order[i]-1)] = allValues.splice(0,1);
                    newKey[(order[i]-1)] = allKeys.splice(0,1);
                }
                for(var i=0;i<itemLn;i++){
                    if(newItem[i] === undefined){
                        newItem[i] = allValues.splice(0,1);
                        newKey[i] = allKeys.splice(0,1);
                    }
                }
                if($.isArray(items)){ // jika array, key akan dimulai dari 0
                    var result = newItem;
                }else{ // jika object, key adalah key asal dari nilai tersebut
                    var result = {};
                    for(var i in newKey){
                        result[newKey[i]] = newItem[i];
                    }
                }
                // for(var i in result){
                //     console.log('key '+i+' value '+result[i]);
                // }
                return result;
        },
        assocArraySize: function(obj){
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        },
        renameField: function(field,ociState){
            if(ociState)
                return field.toUpperCase();
            else
                return field.toLowerCase();
        },
        getAllInput: function(parentDiv){
            var data = {};

            if(typeof parentDiv == 'string' && parentDiv.indexOf("#") < 0 && parentDiv.indexOf(".") < 0){
                parentDiv = '#'+parentDiv;
            }

            // dapatkan sesuai dengan attr name, input hidden sebagai filter saat ajax request
            var allHidden = $(parentDiv).find("input[type=hidden]");
            for(var i=0; i<allHidden.length; i++){
                var name = $(allHidden[i]).attr('name');
                var value = $(allHidden[i]).val();
                data[name] = value;
            }
            var allInput = $(parentDiv).find("input[type=text]");
            for(var i=0; i<allInput.length; i++){
                var name = $(allInput[i]).attr('name');
                var value = $(allInput[i]).val();
                data[name] = value;
            }
            var allInput = $(parentDiv).find("input[type=password]");
            for(var i=0; i<allInput.length; i++){
                var name = $(allInput[i]).attr('name');
                var value = $(allInput[i]).val();
                data[name] = value;
            }
            var allRadio = $(parentDiv).find("input[type=radio]");
            for(var i=0; i<allRadio.length; i++){
                if($(allRadio[i]).is(':checked')){
                    var name = $(allRadio[i]).attr('name');
                    var value = $(allRadio[i]).val();
                    data[name] = value;
                }
            }
            var allRadio = $(parentDiv).find("input[type=checkbox]");
            for(var i=0; i<allRadio.length; i++){
                var name = $(allRadio[i]).attr('name');
                var value = $(allRadio[i]).val();
                if($(allRadio[i]).is(':checked')){
                    data[name] = value;
                }else{
                    data[name] = '';
                }
            }
            var allSelect = $(parentDiv).find("select");
            for(var i=0; i<allSelect.length; i++){
                var name = $(allSelect[i]).attr('name');
                var options = $(allSelect[i]).find('option');
                data[name] = $(allSelect[i]).val();
            }
            var allTexarea = $(parentDiv).find("textarea");
            for(var i=0; i<allTexarea.length; i++){
                var name = $(allTexarea[i]).attr('name');
                var value = $(allTexarea[i]).val();
                data[name] = value;
            }
            return data;
        },
        decodeHtml: function(html){
            var txt = document.createElement("textarea");
                txt.innerHTML = html;
            return txt.value;
        },
        // type hidden namenya harus ada 'filter_*'
        // keys "data" sebagai input dari oracle harus all capital
        setAllInput: function(parentDiv,data){
            if(typeof parentDiv == 'string' && parentDiv.indexOf("#") < 0 && parentDiv.indexOf(".") < 0){
                parentDiv = '#'+parentDiv;
            }
            if(data === undefined) return false;
            // untuk data dari json tidak perlu memakai filter_ karena akan tidak terbaca
            var allHidden = $(parentDiv).find("input[type=hidden]");
            // karena field dari database, hapus dulu filter yg ada di <input name=? />
            for(var i=0; i<allHidden.length; i++){
                if($(allHidden[i]).attr('name')){
                    var currField = (global['ociconnect']
                        ?$(allHidden[i]).attr('name').toUpperCase().replace('FILTER_','')
                        :$(allHidden[i]).attr('name').toLowerCase().replace('filter_','')
                    );

                    if(data && data[currField] !== undefined){
                        data[currField] = (typeof data[currField] == 'string' ? data[currField].trim() : data[currField] );
                        $(allHidden[i]).val($.decodeHtml($.trim(data[currField])));
                    }
                }
            }
            var allInput = $(parentDiv).find("input[type=text]");
            for(var i=0; i<allInput.length; i++){
                var attrName = $(allInput[i]).attr('name');
                if(attrName){
                    var currField = (global['ociconnect']
                        ?$(allInput[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allInput[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    currField = (global['ociconnect']
                        ?$(allInput[i]).attr('name').replace('_time_incl','').toUpperCase()
                        :$(allInput[i]).attr('name').replace('_time_incl','').toLowerCase()
                    );
                    oDate = (global['ociconnect']
                        ?'_ODATE'
                        :''
                    );
                    
                    if(data && data[currField] !== undefined){
                        data[currField] = (typeof data[currField] == 'string' ? data[currField].trim() : data[currField] );
                        if(attrName.indexOf('_time_incl')>-1 && data[currField]){
                            var currArr = data[currField].split(' ');
                            data[currField] += (currArr[currArr.length - 1] ? ' ' : '00:00:00');
                            $(allInput[i]).val($.format.date(data[currField],'dd MMMM yyyy HH:mm:ss'));
                        }else if(data[currField+oDate]){
                            $(allInput[i]).val($.decodeHtml($.trim(data[currField+oDate])));
                        }else{
                            $(allInput[i]).val($.decodeHtml($.trim(data[currField])));
                        }
                    }
                }
            }
            var allInput = $(parentDiv).find("input[type=password]");
            for(var i=0; i<allInput.length; i++){
                var attrName = $(allInput[i]).attr('name');
                if(attrName){
                    var currField = (global['ociconnect']
                        ?$(allInput[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allInput[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    currField = (global['ociconnect']
                        ?$(allInput[i]).attr('name').replace('_time_incl','').toUpperCase()
                        :$(allInput[i]).attr('name').replace('_time_incl','').toLowerCase()
                    );
                    oDate = (global['ociconnect']
                        ?'_ODATE'
                        :''
                    );
                    
                    if(data && data[currField] !== undefined){
                        $(allInput[i]).val($.decodeHtml($.trim(data[currField])));
                    }
                }
            }
            var allRadio = $(parentDiv).find("input[type=radio]");
            for(var i=0; i<allRadio.length; i++){
                if($(allRadio[i]).attr('name')){
                    var currField = (global['ociconnect']
                        ?$(allRadio[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allRadio[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    if(data && data[currField] !== undefined && $.trim(data[currField]) == $.trim($(allRadio[i]).val())){
                        $(allRadio[i]).prop('checked',true);
                    }
                }
            }
            var allCheckbox = $(parentDiv).find("input[type=checkbox]");
            for(var i=0; i<allCheckbox.length; i++){
                if (allCheckbox && $(allCheckbox[i]).attr('name')){
                    var currField = (global['ociconnect']
                        ?$(allCheckbox[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allCheckbox[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    if (data && (data[currField] === true || data[currField] == 1))
                        $(allCheckbox[i]).prop('checked',true);
                    else
                        $(allCheckbox[i]).prop('checked',false);
                }
            }
            var allSelect = $(parentDiv).find("select");
            for(var i=0; i<allSelect.length; i++){
                if(allSelect && $(allSelect[i]).attr('name')){
                    var currField = (global['ociconnect']
                        ?$(allSelect[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allSelect[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    if(data && data[currField] !== undefined){
                        // harus dengan string pembeda titik koma
                        data[currField] = (typeof data[currField] == 'string' ? data[currField].trim() : data[currField] );
                        var tmpData = '';
                        if(data[currField] && data[currField].split){
                            tmpData = data[currField].split(';');
                        }else if(data[currField]){
                            tmpData = data[currField];
                        }
                        $(allSelect[i]).val(tmpData);
                    }
                }
            }
            var allTexarea = $(parentDiv).find("textarea");
            for(var i=0; i<allTexarea.length; i++){
                if($(allTexarea[i]).attr('name')){
                    var currField = (global['ociconnect']
                        ?$(allTexarea[i]).attr('name').replace('filter_','').toUpperCase()
                        :$(allTexarea[i]).attr('name').replace('filter_','').toLowerCase()
                    );
                    if(data && data[currField] !== undefined)
                        $(allTexarea[i]).val($.decodeHtml($.trim(data[currField])));
                }
            }
            var allSpan = $(parentDiv).find("span.data");
            for(var i=0; i<allSpan.length; i++){
                var attrName = $(allSpan[i]).attr('id');

                var currField = (global['ociconnect']
                    ?$(allSpan[i]).attr('id').replace('filter_','').toUpperCase()
                    :$(allSpan[i]).attr('id').replace('filter_','').toLowerCase()
                );
                currField = (global['ociconnect']
                    ?$(allSpan[i]).attr('id').replace('_time_incl','').toUpperCase()
                    :$(allSpan[i]).attr('id').replace('_time_incl','').toLowerCase()
                );
                oDate = (global['ociconnect']
                    ?'_ODATE'
                    :''
                );
                
                if(data && data[currField] !== undefined){
                    if(attrName.indexOf('_time_incl')>-1 && data[currField]){
                        $(allSpan[i]).text($.format.date(data[currField],'dd MMMM yyyy HH:mm:ss'));
                    }else if(data[currField+oDate]){
                        $(allSpan[i]).text($.decodeHtml($.trim(data[currField+oDate])));
                    }else{
                        $(allSpan[i]).text($.decodeHtml($.trim(data[currField])));
                    }
                }
            }
        },
        setAjax: function(url,method,data,callback,async,retType,errorCB){
            async = (async !== false?true:false);

            // if(!data) data = {};

            // if(data['disable_random'] == undefined)
            //     data['specific_id'] = this.randomString(8, '#aA');

            retType = (retType?retType:'json'); // xml,html,script,json,jsonp,text
            $.ajax({ 
              url: url,
              type: method, 
              contentType : 'application/x-www-form-urlencoded',
              async : async,
              dataType : retType,
              data : data,
              success: function(result, status, xhr){
                if(typeof callback == 'function')
                    callback(result,"status :"+status+"\n"+xhr.getAllResponseHeaders()+result);
              }, 
              error: function(xhr, status, error) {
                if(typeof errorCB == 'function')
                    errorCB(xhr.responseText,"status :"+xhr.status+"\n"+xhr.getAllResponseHeaders()+xhr.responseText);
              } 
            });     
        },
        getObjClone: function(obj){
            return JSON.parse(JSON.stringify(obj))
        },
        setCollapse: function(dom,collapse){

            if(collapse){
                var trigger = $("<i class='fa fa-plus-square-o'></i>").appendTo($(dom).children('.judul-panel'));
                $(dom).children('.content-panel').hide();
            }else{
                var trigger = $("<i class='fa fa-minus-square-o'></i>").appendTo($(dom).children('.judul-panel'));
                $(dom).children('.content-panel').show();
            }
            $(trigger).css('float','right');
            $(trigger).css('cursor','pointer');

            $.each(trigger,function(i,v){
                $(trigger).on('click',(function(trigger){
                        return function(e){
                            var collaps = $(trigger).hasClass('fa-plus-square-o');
                            if(collaps){
                                $(trigger).removeClass('fa-plus-square-o');
                                $(trigger).addClass('fa-minus-square-o');
                                $(trigger).parent().parent().children('.content-panel').show();
                            }else{
                                $(trigger).removeClass('fa-minus-square-o');
                                $(trigger).addClass('fa-plus-square-o');
                                $(trigger).parent().parent().children('.content-panel').hide();
                            }
                        }
                })(trigger));
            });
        },
        setToggle: function(nameSide,judulSide,args,close){

            var id = document.getElementById(nameSide);
            if(!id) return;

            // isi dari content dihilangkan sementara
            var varSemantara = id.innerHTML;
            id.innerHTML = '';
            id.style.border = '1px solid #ccc';
            id.style.padding = '2px';
            id.style.marginBottom = '10px';
            // id.style.borderRadius = '5px';
            // id.style.boxShadow = '10px 10px 4px -9px rgba(0,0,0,0.75)';
            id.style.backgroundColor = '#fff';

            // buat div
            var judul = document.createElement( 'div' );
            var isi = document.createElement( 'div' );
            
            // buat class
            var classJudul = nameSide+'_judul';
            var classIsi =  nameSide+'_isi';
            judul.setAttribute("class", classJudul);
            judul.setAttribute("name", classJudul);
            isi.setAttribute("class",classIsi);
            id.appendChild(judul);
            id.appendChild(isi);

            // buat judul
            var judulText = document.createTextNode(judulSide);
            judul.style.backgroundColor = '#F2F2F2';
            judul.style.color = '#333';
            judul.style.border='1px solid #eee';
            // judul.style.borderRadius = '3px';
            judul.style.padding = '10px';
            judul.style.cursor = 'pointer';
            judul.style.fontSize= '14px';
            judul.style.fontWeight= 'bold';
            judul.appendChild(judulText);
            
            //kembalikan isi content 
            isi.innerHTML += varSemantara;
            isi.style.padding = '10px';
            isi.style.backgroundColor = '#fff';
            isi.style.overflow = 'auto';
            if(args && args.height != undefined)
              isi.style.height = args.height+'px';
            if(args && args.width != undefined)
              isi.style.width = args.width+'px';

            // click
            for(var i = 0; i<id.childNodes.length; i++){
              var divJudul = id.childNodes[0]
              var divIsi = id.childNodes[1]
            }
            // buat div untuk icon min - plus
            var divicon = document.createElement( 'div' );
            divJudul.appendChild(divicon);
            $(divicon).addClass('fa');
            $(divicon).css('float','right');
            
            if(close){
              divIsi.style.display = 'none';
              $(divicon).addClass('fa-chevron-down');
            }else{
              divIsi.style.display = 'block';
              $(divicon).addClass('fa-chevron-up');
            }

            divJudul.onclick = function(){
                if($(divIsi).css('display') != 'none'){
                    $(divIsi).css('display','none');
                    $(divicon).removeClass('fa-chevron-up');
                    $(divicon).addClass('fa-chevron-down');
                }else{
                    $(divIsi).css('display','block');
                    $(divicon).removeClass('fa-chevron-down');
                    $(divicon).addClass('fa-chevron-up');
                }
            };
        },
        /**
         selectId = selectId;
         key = {'id':'id','value':'name'};
         arrValue = [{'id':'1','name':'akhyar'},{'id':'2','name':'azni'},{'id':'3','name':'joni'}];
         or
         arrValue = {1:'akhyar',2:'azni',3:'joni'};
         defaultValue = '<option val=-1>== daftar user member ==</option>';
         selectedValue = 3;
         result : <select id=selectId>
                <option value=1>akhyar</option>
                <option value=2>azni</option>
                <option value=3>joni</option>
         </select>
         */
        create_select: function (selectId,arrValue,key,headerValue,selectedValue,multiple){
            arrSelected = [];
            if(selectedValue instanceof Array)
                arrSelected = selectedValue;
            else arrSelected.push(selectedValue);

            if(!key) key = {'id':'id','value':'name'};
            var result = '';

            if(arrValue){
                for(var i in arrValue){
                    if(arrValue[i]!=null && arrValue[i][key.id]){
                        if(arrSelected && arrSelected.indexOf(arrValue[i][key.id]) > -1)
                            result += '<option value="'+$.trim(arrValue[i][key.id])+'" selected=\'selected\'">'+arrValue[i][key.value]+'</option>';
                        else
                            result += '<option value="'+$.trim(arrValue[i][key.id])+'">'+arrValue[i][key.value]+'</option>';
                    }else{
                        if(typeof arrValue[i] != 'function'){
                            if(arrSelected && arrSelected.indexOf(i) > -1)
                                result += '<option value="'+i+'" selected=\'selected\'>'+arrValue[i]+'</option>';
                            else
                                result += '<option value="'+i+'">'+arrValue[i]+'</option>';
                        }
                    }
                }
            }
            var multiple = (multiple?'multiple="multiple"':'');
            result = '<select id='+selectId+' name='+selectId+' '+multiple+'>'+(headerValue?headerValue+result:result)+'</select>';
            return result;
        },
        loadStart: function (elm){
            return;
            var loadDiv = null;
            var elm = (elm?elm:'body');
            if($(elm).find('.loading').length == 0){
                loadDiv = $('<div class=loading></div>').appendTo(elm);
                loadDiv2 = $('<div class="boxloading col-lg-3 col-md-3 col-sm-12"></div>').appendTo(loadDiv);
                loadDiv3 = $('<div class=" fa fa-close">&nbsp;</div>').appendTo(loadDiv2);

                $(loadDiv).css('backgroundColor','rgba(100,100,100,0.7)');
                $(loadDiv).css('zIndex',1000000);
                $(loadDiv).css('position','fixed');
                $(loadDiv).css('float','left');
                $(loadDiv).css('top',0);
                $(loadDiv).css('left',0);
                $(loadDiv).css('width',$(window).width());
                $(loadDiv).css('height',$(window).height());

                $(loadDiv2).css('backgroundRepeat','no-repeat');
                $(loadDiv2).css('backgroundPosition','top center');
                $(loadDiv2).css('textAlign','right');
                $(loadDiv2).css('float','left');
                $(loadDiv2).css('color','white');
                $(loadDiv2).css('opacity',0.7);
                $(loadDiv2).css('height','100%');
                $(loadDiv2).css('marginLeft',($(window).width() - $(loadDiv2).width())/2 );
                $(loadDiv2).css('marginTop',($(window).height() - $(loadDiv2).width())/2 );

                $(loadDiv3).on('click',function(){
                    $(loadDiv).hide();
                });
            }else{
                loadDiv = $(elm).find('.loading');
            }
            $(loadDiv).show();
        },
        loadEnd: function (){
            return;
            var loadDiv = $('.loading');
            if(loadDiv)
                $(loadDiv).hide();
        },
        windowOpen: function (url,name,option){
            var name = (!name?'_top':name);
            var popup = window.open(url,name,option);
            if(!popup || popup.outerHeight === 0){
                //$.windowOpen(global['ReportUrl']+'corp_excel_act_report/?'+param,'newWindow','width=1024,height=768,resizable=yes,scrollbars=yes')
                // alert('Harap matikan popup blocker untuk situs ini');
                if(console && console.log) console.log(url);
            }
        },
        abortAllAjax:function(){
          $.each(xhrPool, function(idx, jqXHR) {
            jqXHR.abort();
          });
        },
        changeClass: function (object,oldClass,newClass){
            // remove:
            //object.className = object.className.replace( /(?:^|\s)oldClass(?!\S)/g , '' );
            // replace:
            var regExp = new RegExp('(?:^|\\s)' + oldClass + '(?!\\S)', 'g');
            object.className = object.className.replace( regExp , newClass );
            // add
            //object.className += " "+newClass;
            // $.changeClass(myInput.submit,"stategood_disabled"," stategood_enabled");
            // kenapa ga' gunakan removeclass dan addclass??
        },
        randomString: function (length, chars){
            var mask = '';
            if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
            if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            if (chars.indexOf('#') > -1) mask += '0123456789';
            if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
            var result = '';
            for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
            return result;
            // console.log(randomString(16, 'aA'));
            // console.log(randomString(32, '#aA'));
            // console.log(randomString(64, '#A!'));
        },
        toRp: function (bilangan){
            // var bilangan = "23456789,32";
                
            var number_string = bilangan.toString(),
                split   = number_string.split(','),
                sisa    = split[0].length % 3,
                rupiah  = split[0].substr(0, sisa),
                ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
                    
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return rupiah;
        },
        remove_key: function (arrayName,key){
            var x;
            var tmpArray = {};
            for(x in arrayName){
                if(x!=key) { tmpArray[x] = arrayName[x]; }
            }
            return tmpArray;
        },
        newDialog: function (elm,open,callback){
            if(typeof elm == 'string' && elm.indexOf("#") < 0 && elm.indexOf(".") < 0){
                elm = '#'+elm;
            }

            var content = '<div id=content_dialog >'+$(elm).html()+'</div>';
            $(elm).empty();
            var contentObj = $(content).appendTo($(elm));
            $(contentObj).css('overflow','auto');
            $(contentObj).css('height',($(elm).height() - 50)+'px');

            var title = $(elm).attr('title');
            title = '<div id=title_dialog>'+(title?title:'dialog title here')+'</div>';
            var titleObj = $(title).prependTo($(elm));
            $(titleObj).css('border','1px solid #333');
            $(titleObj).css('padding','10px 25px');
            $(titleObj).css('textAlign','center');
            $(titleObj).css('marginBottom','10px');
            $(titleObj).css('position','relative');

            var close = '<span id=close_dialog >X</span>';
            var closeObj = $(close).prependTo($(elm));
            $(closeObj).css('position','absolute');
            $(closeObj).css('float','right');
            $(closeObj).css('right','10px');
            $(closeObj).css('top','10px');
            $(closeObj).css('padding','0 10px');
            $(closeObj).css('border-radius','3px');
            $(closeObj).css('border','1px solid grey');
            $(closeObj).css('cursor','pointer');
            $(closeObj).css('zIndex',2);
            $(closeObj).on('click',function(result){
                $(this).parent().toggle();
            });

            $(elm).css('position','fixed');
            $(elm).css('float','left');
            $(elm).css('top',0);
            $(elm).css('left',0);
            $(elm).css('zIndex',99);
            // $(elm).draggable();

            if(!open) $(elm).hide();

            if(callback) callback();

            return $(elm);
        },
        viewport: function (){
            var e = window, a = 'inner';
            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
        },
        attachClock: function (currDiv){
            if(typeof currDiv == 'string' && currDiv.indexOf("#") < 0 && currDiv.indexOf(".") < 0){
                currDiv = '#'+currDiv;
            }

            if($(currDiv)){
                var timerPlace = $(currDiv).find('#timer_place');
                if(timerPlace.length == 0){
                    timerPlace = $('<span></span>').appendTo(currDiv);
                    $(timerPlace).attr('id','timer_place');
                }

                $(currDiv).css('position','fixed');
                $(currDiv).css('right','35px');
                $(currDiv).css('top',0);
                $(currDiv).css('backgroundColor','#fff');
                $(currDiv).css('padding','5px');
                $(currDiv).css('fontSize','150%');
                $(currDiv).css('zIndex',100);
                $(timerPlace).css('padding',"5px 10px");
                function startTime(){
                    var today = new Date();
                    var h = today.getHours();
                    var m = today.getMinutes();
                    var s = today.getSeconds();
                    m = (m<10?'0'+m:m);
                    s = (s<10?'0'+s:s);
                    $(timerPlace).html(h + ":" + m + ":" + s);
                    var t = setTimeout(startTime, 500);
                }
                startTime();
            }
        },
        closeOnOutside: function (container){
            if(typeof container == 'string' && container.indexOf("#") < 0 && container.indexOf(".") < 0){
                container = '#'+container;
            }
            if($(container)){
                $(document).mouseup(function(e){ // outside toggle
                    var container = $(".box-common-rightbar");

                    // if the target of the click isn't the container nor a descendant of the container
                    if (!$(container).is(e.target) && $(container).has(e.target).length === 0) 
                    {
                        $(container).hide();
                    }
                });  
            }
        },
        base64toBlob: function (image, contentType) {
            var base64Data = image.replace(/^data:image\/(png|jpg|jpeg|gif);base64,/, "");
                    contentType = contentType || '';
                    var sliceSize = 1024;
                    var byteCharacters = atob(base64Data);
                    var bytesLength = byteCharacters.length;
                    var slicesCount = Math.ceil(bytesLength / sliceSize);
                    var byteArrays = new Array(slicesCount);

                    for(var sliceIndex = 0; sliceIndex<slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
            // var formDataToUpload = new FormData(form);
            // formDataToUpload.append("image", blob);
            // ajax data: formDataToUpload,// Add as Data the Previously create formData
            // ajax type: "POST",
            // $file = $_FILES['image']['tmp_name'];
        },
        sleep: function (milliseconds){
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        },
        getNumeric: function(inputstring){
            var num = inputstring.replace(/[^0-9]/g, ''); 
            var num = isNaN(parseInt(num, 10)) ? 0 : parseInt(num, 10);
            return parseInt(num,10); 
        },
        is_numeric: function(inputstring){
            return $.isNumeric(inputstring); 
        },
        //  new Date(2017, 1, 1), which is the 1st of February 2017
        formatDate: function(formatDate, formatString){
            if(typeof formatDate == 'string' && formatDate.indexOf('/') !== -1){ // jika formatnya adalah dd/mm/yyyy
                // var partDate = formatDate.split('/'); if(partDate>12) alert('salah format');
                formatDate = new Date(formatDate.split('/').reverse().join('-'));
            }else if(formatDate){
                formatDate = new Date(formatDate);
            }else{
                formatDate = new Date();
            }

            if(formatDate instanceof Date) {
                    var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                    var yyyy = formatDate.getFullYear();

                    var yy = yyyy.toString().substring(2);
                    var m = formatDate.getMonth() + 1;//  + 1 => The value returned by getMonth() is an integer between 0 and 11
                    var mm = m < 10 ? "0" + m : m;
                    var mmm = months[m];
                    var d = formatDate.getDate();
                    var dd = d < 10 ? "0" + d : d;
                    
                    var h = formatDate.getHours();
                    var hh = h < 10 ? "0" + h : h;
                    var n = formatDate.getMinutes();
                    var nn = n < 10 ? "0" + n : n;
                    var s = formatDate.getSeconds();
                    var ss = s < 10 ? "0" + s : s;
                    formatString = formatString.replace(/yyyy/i, yyyy);
                    formatString = formatString.replace(/yy/i, yy);
                    formatString = formatString.replace(/mmm/i, mmm);
                    formatString = formatString.replace(/mm/i, mm);
                    formatString = formatString.replace(/m/i, m);
                    formatString = formatString.replace(/dd/i, dd);
                    formatString = formatString.replace(/d/i, d);
                    formatString = formatString.replace(/hh/i, hh);
                    formatString = formatString.replace(/h/i, h);
                    formatString = formatString.replace(/nn/i, nn);
                    formatString = formatString.replace(/n/i, n);
                    formatString = formatString.replace(/ss/i, ss);
                    formatString = formatString.replace(/s/i, s);
                    return formatString;
                } else {
                    return "";
                }
                // formatDate(new Date(), "d mmm yyyy hh:nn:ss")
        },
        reload: function (){
            location.reload();
            // window.location.reload(true);
        },
        jwtdecode: function(token){
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        
            return JSON.parse(jsonPayload);
        },
        addDay: function(currdate, numdate){ // currdate = new Date([yyyy-mm-dd HH:mm:ss]), numdate = integer
            return currdate.setDate(currdate.getDate()+numdate);
        },
        addHour: function(currdate, numhour){ // currdate = new Date([yyyy-mm-dd HH:mm:ss]), numhour = integer
            return currdate.setHours(currdate.getHours()+numhour);
        },
        diffdate: function(lastdate, currdate){ // lastdate = new Date([yyyy-mm-dd HH:mm:ss]), currdate = new Date([yyyy-mm-dd HH:mm:ss])
            if(typeof lastdate == 'string' && lastdate.indexOf('/') !== -1){ // jika formatnya adalah dd/mm/yyyy
                // var partDate = lastdate.split('/'); if(partDate>12) alert('salah format');
                lastdate = new Date(lastdate.split('/').reverse().join('-'));
            }else if(lastdate){
                lastdate = new Date(lastdate);
            }else{
                lastdate = new Date();
            }

            if(typeof currdate == 'string' && currdate.indexOf('/') !== -1){ // jika formatnya adalah dd/mm/yyyy
                // var partDate = currdate.split('/'); if(partDate>12) alert('salah format');
                currdate = new Date(currdate.split('/').reverse().join('-'));
            }else if(currdate){
                currdate = new Date(currdate);
            }else{
                currdate = new Date();
            }

            return parseInt(lastdate - currdate); // in micro second
            // return parseInt( ( (lastdate - currdate) /1000 )/3600); // in hour
        },
        adddate: function(fdate, numdate){ // lastdate = new Date([yyyy-mm-dd HH:mm:ss]), currdate = new Date([yyyy-mm-dd HH:mm:ss])
            if(typeof fdate == 'string' && fdate.indexOf('/') !== -1){ // jika formatnya adalah dd/mm/yyyy
                // var partDate = fdate.split('/'); if(partDate>12) alert('salah format');
                fdate = new Date(fdate.split('/').reverse().join('-'));
            }else if(fdate){
                fdate = new Date(fdate);
            }else{
                fdate = new Date();
            }

            fdate.setDate(fdate.getDate()+numdate);
            return $.formatDate(fdate,'dd/mm/YYYY');
        },
        subtractdate: function(fdate, numdate){ // lastdate = new Date([yyyy-mm-dd HH:mm:ss]), currdate = new Date([yyyy-mm-dd HH:mm:ss])
            if($.findstring(fdate,'/')){
                fdate = new Date(fdate.split('/').reverse().join('-'));
            }
            var lastDate = new Date(fdate);
            lastDate.setDate(fdate.getDate()-numdate);
            return $.formatDate(new Date(lastDate),'dd/mm/YYYY');
        },
        objlen: function (obj){
            var L=0;
            if(typeof obj == 'object' && obj.length){
                L = obj.length;
            }else if(typeof obj == 'object'){
                $.each(obj, function(i, elem) {
                    L++;
                });
            }
            return L;
        },
        findstring: function (str,search){
            if(str.toLowerCase().indexOf(search.toLowerCase()) !== -1){
            // if(str.toLowerCase().search(search.toLowerCase()) !== -1){
              return true;
            }
            return false;
        },
        getdomstring: function (currDiv){
            if(typeof currDiv == 'string' && currDiv.indexOf("#") < 0 && currDiv.indexOf(".") < 0){
                currDiv = '#'+currDiv;
            }
            return $(currDiv).prop('outerHTML');
        },
        newfunc: function (){}
    });

    // Automatically cancel unfinished ajax requests 
    // when the user navigates elsewhere.
    var oldbeforeunload = window.onbeforeunload;
    window.onbeforeunload = function() {
      var r = oldbeforeunload ? oldbeforeunload() : undefined;
      if (r == undefined) {
        // only cancel requests if there is no prompt to stay on the page
        // if there is a prompt, it will likely give the requests enough time to finish
        $.abortAllAjax();
      }
      return r;
    }

})( jQuery );

/* contoh :
$('.update_category').customepopup();
$('.update_category').customepopup("addButton",{
  title:'Reload',
  className:'navigation',
  callback:function(prflObj){
  }
});
*/

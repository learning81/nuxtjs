(function( $ ){

    if(!$.prfl_stringify){      
      console.log('stringify belum di load');       
      return false;     
    }       
    if(!$.prflutils){       
      console.log('utils belum di load');       
      return false;     
    }

    // semua this dalam gridAPI adalah simasgrid object
    var gridAPI = {
        utils:{
            createHeader:function(){
                // buat baris title
                if($(this.gridObj).attr('title')){
                    this.titleDiv = $('<div>'+$(this.gridObj).attr('title').toUpperCase()+'</div>').appendTo(this.gridObj);
                    $(this.titleDiv).attr('id','titleDiv');
                    $(this.titleDiv).css('textAlign','center');
                }

                this.headerDiv = $('<div></div>').appendTo(this.gridObj);
                $(this.headerDiv).attr('id','headerDiv');
                $(this.headerDiv).addClass('grid_header');

                this.headerTable = $('<table></table>').appendTo(this.headerDiv);
                $(this.headerTable).attr('id','headerTable');

                // buat baris header
                this.titleTR = $('<tr></tr>').appendTo(this.headerTable);
                // tambah kan td untuk responsive
                var td = $('<td>[[responsive]]</td>').appendTo(this.titleTR);
                $(td).attr('width',150);
                $(td).addClass('td-responsive');
                // tambah kan checkbox
                if(this.option.checkBoxAdded === undefined || this.option.checkBoxAdded){
                    var td = $('<td>&nbsp;</td>').appendTo(this.titleTR);
                    $(td).addClass('td-checked');
                    $(td).attr('width',20);
                    var chkbox = $('<input></input>').appendTo(td);
                    $(chkbox).attr('type','checkbox');
                    $(td).css('textAlign','center');

                    // ketika checkbox pada searchbox di klik
                    $(chkbox).on('change',$.proxy(function(){
                        var checkState = $(this.titleTR).find('input[type=checkbox]').prop('checked');
                        var allTr = $(this.contentTable).find('tr');
                        $.each(allTr,function( index, value ) {
                            $(value).find('input[type=checkbox]').prop('checked',checkState);
                        });
                    },this));
                }
                // isikan field
                if(typeof this.option.field === 'object'){
                    this.tdStorage.push(-1);
                    if(this.option.checkBoxAdded){
                        this.tdStorage.push(-1);
                    }
                    for(var i in this.option.field){
                        if(this.option.field[i].visibility === undefined || this.option.field[i].visibility !== 'hidden'){

                            this.tdStorage.push(i);

                            var td = $('<td></td>').appendTo(this.titleTR);
                            $(td).attr('id',this.option.field[i].id);
                            $(td).text(this.option.field[i].text);
                            $(td).css('textAlign','center');

                            if(this.option.field[i].sort === true){
                                var sort = $('<i class="fa fa-sort"></i>').appendTo(td);
                                $(sort).css('cursor','pointer');
                                $(sort).css('float','right');
                                $(sort).css('marginRight','5px');
                                $(sort).css('marginRight','5px');
                                $(sort).on('click',$.proxy(function(){
                                    var state = '';
                                    if($(this['sort']).hasClass('fa-sort-down')){
                                        state = 'down';
                                    }else if($(this['sort']).hasClass('fa-sort-up')){
                                        state = 'up';
                                    }

                                    var tr = $(this['sort']).parent().parent();
                                    var trChild = $(tr).children('td');
                                    $(trChild).each(function(idx,val){
                                        $(val).children('i').removeClass('fa-sort-down');
                                        $(val).children('i').removeClass('fa-sort-up');
                                        $(val).children('i').addClass('fa-sort');
                                    });

                                    var id = $(this['sort']).parent().attr('id');
                                    if(state == 'down'){
                                        $(this['sort']).removeClass('fa-sort');
                                        $(this['sort']).removeClass('fa-sort-down');
                                        $(this['sort']).addClass('fa-sort-up');
                                        this['grid'].setPostData({'ordid':id,'ordtype':'desc'});
                                    }else if(state == 'up'){
                                        $(this['sort']).removeClass('fa-sort');
                                        $(this['sort']).removeClass('fa-sort-up');
                                        $(this['sort']).addClass('fa-sort-down');
                                        this['grid'].setPostData({'ordid':id,'ordtype':'asc'});
                                    }else{
                                        $(this['sort']).removeClass('fa-sort');
                                        $(this['sort']).addClass('fa-sort-down');
                                        this['grid'].setPostData({'ordid':''});
                                    }
                                    this['grid'].rebuildGrid();
                                },{'grid':this,'sort':sort}));
                            }

                            if(this.option.field[i].width === undefined){
                                $(td).attr('width',200);
                            }else{
                                $(td).attr('width',this.option.field[i].width);
                            }
                        }
                    }
                    for(var i=0; i<this.tdStorage.length;i++){
                        if(this.tdStorage[i] != -1) this.storageTd[this.tdStorage[i]] = i;
                    }
                }

                if(this.option.useSearch === undefined || this.option.useSearch !== false){
                    // buat baris searching
                    this.searchTR = $('<tr></tr>').appendTo(this.headerTable);
                    $(this.searchTR).addClass('box-search')
                    // tambah kan td untuk responsive
                    var td = $('<td>&nbsp;</td>').appendTo(this.searchTR);
                    $(td).attr('width',150);
                    $(td).addClass('td-responsive');
                    // tambah kan checkbox
                    if(this.option.checkBoxAdded === undefined || this.option.checkBoxAdded){
                        var td = $('<td>&nbsp;</td>').appendTo(this.searchTR);
                        //$(td).text('search >');
                        $(td).css('fontWeight',900);
                    }
                    // tambahkan input untuk searching
                    if(typeof this.option.field === 'object'){
                        for(var i in this.option.field){
                            if(this.option.field[i].visibility === undefined || this.option.field[i].visibility !== 'hidden'){
                                var td = $('<td></td>').appendTo(this.searchTR);
                                $(td).attr('id','src_'+this.option.field[i].id);
                                $(td).css('textAlign','center');

                                if(this.option.field[i].search === undefined || this.option.field[i].search !== 'disabled'){
                                    var input = $('<input></input>').appendTo(td);
                                    $(input).attr('id','filter_'+this.option.field[i].id);
                                    $(input).attr('placeholder','search ...');
                                    $(input).css('width','100%');
                                    $(input).css('boxSizing','border-box');
                                    $(input).addClass('box-search');

                                    // ketika input pada searchbox di klik
                                    $(input).on('keypress',(function(gridObj){
                                            return function(e){
                                                if(e.which == 13){
                                                    var td = $(gridObj.searchTR).find('td');
                                                    for(var x=0; x<td.length; x++){
                                                        var field = $(td[x]).find('input').attr('id');
                                                        var value = $(td[x]).find('input').val();
                                                        if(field !== undefined && field !== '' && value !== undefined && value !== ''){
                                                            gridObj.postData[field] = value;
                                                        }else{
                                                            delete gridObj.postData[field];
                                                        }
                                                    }
                                                    gridObj.rebuildGrid();
                                                }
                                            }
                                    })(this));
                                }

                                if(this.option.field[i].width === undefined){
                                    $(td).attr('width',200);
                                }else{
                                    $(td).attr('width',this.option.field[i].width);
                                }
                            }
                        }
                    }
                }
            },
            createBody:function(){ // pass this as this.gridObj
                this.contentDiv = $('<div></div>').insertAfter(this.headerDiv);
                $(this.contentDiv).attr('id','contentDiv');
                $(this.contentDiv).addClass('grid_container');
                $(this.contentDiv).height(this.option.height);
                this.contentTable = $('<table></table>').appendTo(this.contentDiv);
                $(this.contentTable).attr('id','contentTable');

                this.loadingDiv = $('<div id="loading">Loading Data ...</div>').appendTo(this.contentDiv);

                // ketika table container di scroll
                $(this.contentDiv).on('scroll',$.proxy(function(){
                    // scroll kiri
                    var left = $(this.contentDiv).scrollLeft();
                    $(this.headerDiv).scrollLeft(left);

                    if($(this.contentTable).find('tr').length < this.totalData)
                        gridAPI.utils.triggerNew.call(this,0);
                },this));

                if (this.option.onFinished && typeof this.option.onFinished == 'function')
                    this.option.onFinished(this);
            },
            createFooter:function(){
                this.footerDiv = $('<div></div>').appendTo(this.gridObj);
                $(this.footerDiv).attr('id','footerDiv');
                $(this.footerDiv).addClass('grid_footer');

                this.counter = $('<div id="counter">No Data</div>').appendTo(this.footerDiv);
            },
            resetData:function(){ // pass this as this.gridObj
                // hapus table
                $(this.contentTable).empty();

                // isi data
                if(this.reqState){
                    // alert('ada data lain yang sedang di load');
                    $.abortAllAjax();
                    alert('data sedang digenerate ulang ...');
                    this.reqState = false;
                    gridAPI.utils.resetData.call(this);
                }else{
                    this.reqState = true;
                    // hapus data dibawah ini karena cari data total harus dengan parameter berikut di hapus
                    delete this.postData.single_inst;
                    delete this.postData.single_filter;
                    delete this.postData.multi_inst;
                    delete this.postData.multi_filter;
                    delete this.postData.offset;
                    this.dataStorage = {};
                    this.rawData = [];
                    // hapus sementar limit karena jumlah tidak butuh parameter limit, !! tambahkan limit kembali pada request data ajax
                    var tmpLimit = this.postData.limit;
                    delete this.postData.limit;

                    var currObj = this;
                    if(typeof this.option.countUrl !== 'undefined'){
                        $.ajax({
                            url: this.option.countUrl,
                            data: this.postData,
                            async: true, // pending, seharusnya true tapi jadi salah load data
                            dataType: 'json',
                            success: function(totalRow){
                                gridAPI.utils.reCalculate.call(currObj,totalRow,tmpLimit)
                            }
                        });                
                    }else if(typeof this.option.countData !== 'undefined'){
                        gridAPI.utils.reCalculate.call(currObj,this.option.countData,tmpLimit)
                    }
                }
            },
            reCalculate:function(totalRow,tmpLimit){
                this.reqState = false;
                if(totalRow>0){
                    this.totalData = totalRow;
                    // mulai request data;
                    this.setPostData({limit:tmpLimit}); // menambahkan kembali limit
                    $(this.counter).text('../'+this.totalData);
                    gridAPI.utils.insertData.call(this,0);
                }else{
                    $(this.counter).text('No Data');
                    $(this.loadingDiv).hide();
                }
            },
            insertData:function(level,allTrIds,postData){ // pass this as this.gridObj
                if(!this.reqState && (level === 0 || typeof this.option.jsonUrl == 'string')){
                    $(this.loadingDiv).show();

                    this.reqState = true;

                    var currObj = this;
                    if(typeof this.option.jsonUrl !== 'undefined'){
                        $.ajax({    
                            url: (typeof this.option.jsonUrl == 'string'?this.option.jsonUrl:this.option.jsonUrl[level].url),
                            data: this.postData,
                            async: true,
                            dataType: 'json',
                            type: 'GET',
                            success: function(data){
                                gridAPI.utils.insertLevel1.call(currObj,data);
                            }
                        });                
                    }else if(typeof this.option.jsonData !== 'undefined'){
                        gridAPI.utils.insertLevel1.call(currObj,this.option.jsonData);
                    }
                }else if(level > 0){
                    if(postData.offset !== undefined) delete postData.offset; // hapus offset, data dibatasi oleh allTrIds, !! harus kembalikan utk ajax request selanjutnya
                    if(postData.ordid !== undefined) delete postData.ordid; // hapus offset, data dibatasi oleh allTrIds, !! harus kembalikan utk ajax request selanjutnya
                    if(postData.ordtype !== undefined) delete postData.ordtype; // hapus offset, data dibatasi oleh allTrIds, !! harus kembalikan utk ajax request selanjutnya
                    if(postData.limit !== undefined) postData['limit'] = false; // hapus offset, data dibatasi oleh allTrIds, !! harus kembalikan utk ajax request selanjutnya

                    if(postData.ref_field !== undefined) postData.field = postData.ref_field; // hapus offset, data dibatasi oleh allTrIds, !! harus kembalikan utk ajax request selanjutnya
                    $.ajax({    
                        url: this.option.jsonUrl[level].url,
                        data: postData,
                        async: true,
                        dataType: 'json',
                        success: $.proxy(function(data){

                            // kosongkan semua data yang sudah terdefinisi order nya pada call saat ini
                            for(var j in allTrIds){
                                for(var l in this.option.jsonUrl[level].order){
                                    var currOrder = this.option.jsonUrl[level].order[l] - 1;
                                    if(this.dataStorage[allTrIds[j]][currOrder]) this.dataStorage[allTrIds[j]][currOrder] = '';
                                    var tdPos = this.storageTd[currOrder];
                                    if(tdPos > 0) $(this.contentTable).find('#'+allTrIds[j]).children('td').eq(tdPos).html('');
                                }
                            }

                            // mulai pengecekan dari setiap data yang di dapat pada call saat ini
                            // data adalah data yang didapat dari call ajax saat ini
                            // refId adalah key yg diambil dari trid setiap data
                            for(var refId in data){

                                // index dari this.storagedata sebagai data refersnsi dari request sebelumnya
                                var fieldCheck = this.option.jsonUrl[(level-1)].ref_data;

                                // setiap data mulai dilooping untuk dilakukan pengecekan
                                for(var x in data[refId]){

                                    // check apakah data storage sama dengan data ids sekarang
                                    trIds = [];
                                    for(var j in allTrIds){
                                        var currData = this.dataStorage[allTrIds[j]][fieldCheck];
                                        if(currData != undefined){
                                            var currArrData = currData.split('|');
                                            if($.inArray(refId,currArrData) !== -1)
                                                trIds.push(allTrIds[j])
                                        }
                                    }

                                    // convert ke array karena saat populasi data mengambil index this.option.field yg numeric sbg index dari td
                                    var allValues = [];
                                    for(var j in data[refId][x]){
                                          allValues.push(data[refId][x][j]);
                                    }

                                    // proses data yang memiliki nilai order
                                    for(var i=0; i<trIds.length;i++){
                                        var currValue = allValues.slice(0); // reset value untuk proses
                                        trId = trIds[i];

                                        if(trId !== undefined){
                                            for(var j in this.option.jsonUrl[level].order){
                                              var firstVal = currValue.splice(0,1);
                                              var tmpArrData = [];
                                              if(this.dataStorage[trId][(this.option.jsonUrl[level].order[j] - 1)])
                                                tmpArrData = this.dataStorage[trId][(this.option.jsonUrl[level].order[j] - 1)].split('|');
                                              tmpArrData.push(firstVal[0]);
                                              this.dataStorage[trId][(this.option.jsonUrl[level].order[j] - 1)] = tmpArrData.join('|');
                                            }
                                        }

                                        if(typeof this.option.field === 'object'){
                                            for(var j in this.option.field){
                                                  // compare data storage dengan data sekarang
                                                  // jika belum ada storage, ambil data pertama
                                                  if(this.dataStorage[trId][j] === undefined && currValue.length > 0){
                                                      var firstVal = currValue.splice(0,1);
                                                      var tmpArrData = [];
                                                      if(this.dataStorage[trId][j])
                                                        tmpArrData = this.dataStorage[trId][j].split('|');
                                                      tmpArrData.push(firstVal[0]);
                                                      this.dataStorage[trId][j] = tmpArrData.join('|');
                                                  }
                                            }

                                            for(var k in this.tdStorage){
                                              var currKey = this.tdStorage[k];
                                              var currData = this.dataStorage[trId][currKey];
                                              if(currData){
                                                $(this.contentTable).find('#'+trId).children('td').eq(k).html(currData.split('|').join('<br />'));
                                              }
                                            }
                                        }
                                        var allKeys = Object.keys(this.dataStorage[trId]);
                                        var maxKeys = allKeys[allKeys.length - 1];
                                        this.dataStorageLn = (this.dataStorageLn < maxKeys?maxKeys:this.dataStorageLn);

                                    }
                                }
                            }

                            var allRefData = [];
                            if(this.option.jsonUrl[level].ref_data !== undefined){
                                for(var i in allTrIds){
                                    if(this.dataStorage[allTrIds[i]][this.option.jsonUrl[level].ref_data]){
                                        var currData = this.dataStorage[allTrIds[i]][this.option.jsonUrl[level].ref_data].split('|');
                                        for(var j in currData)
                                            allRefData.push(currData[j]);
                                    }
                                }
                            }

                            if(typeof this.option.jsonUrl[(level+1)] == 'object'){
                                // hanya jika ada level selanjutnya
                                postData['multi_inst'] = this.option.jsonUrl[level].ref_field;
                                postData['multi_filter'] = allRefData;
                                for(var j in this.option.jsonUrl[level].ref_param){
                                    postData[j] = this.option.jsonUrl[level].ref_param[j];
                                }
                                gridAPI.utils.insertData.call(this,++level,allTrIds,postData);
                            }else{
                                gridAPI.utils.attach_eventtr.call(this,allTrIds);
                                gridAPI.utils.attach_hovertr.call(this,allTrIds);
                                gridAPI.utils.attach_overtr.call(this,allTrIds);

                                var tdIdx = null;
                                for(var x in allTrIds){
                                    tdIdx = 0;
                                    for(var j in this.option.field){
                                        if(this.option.field[j].visibility === undefined || this.option.field[j].visibility !== 'hidden'){
                                            var currHTML = $(this.contentTable).find('#'+allTrIds[x]).children('td').eq(tdIdx).html();
                                            if(currHTML == 'loading ..')
                                                $(this.contentTable).find('#'+allTrIds[x]).children('td').eq(tdIdx).html('');
                                            tdIdx++;
                                        }
                                        if(j == this.dataStorageLn) break;
                                    }
                                }
                            }
                        },this)
                    });                
                }else{
                    alert('ada data lain yang sedang di load');
                }
            },
            insertLevel1:function(data){
                var level = 0;
                var allRefData = [];
                var allTrIds = [];
                if(data !== null && data.length > 0 && $.isArray(data)){
                    for(var i=0; i<data.length;i++){
                        this.rawData.push(data[i]);

                        var currTr = $('<tr></tr>').appendTo(this.contentTable);
                        // tambah kan td untuk responsive
                        var td = $('<td>&nbsp;</td>').appendTo(currTr);
                        $(td).attr('width',150);
                        $(td).addClass('td-responsive');

                        // menambahkan callback
                        // langsung ditambahkan ke td yg memiliki kemampuan menjalankan event
                        // hanya ditambahkan di request pertama karena request pertama sudah ada semua td dengan isi loading ..
                        if(typeof this.option.responsiveCB == 'function'){
                            $(td).on('click',(function(param){
                                    return function(e){
                                        param.func(param.tr,param.td,e);
                                    }
                            })({func:this.option.responsiveCB,tr:currTr,td:td}));
                            $(td).css('cursor','pointer');
                        }

                        // tambah kan checkbox
                        if(this.option.checkBoxAdded === undefined || this.option.checkBoxAdded){
                            var td = $('<td></td>').appendTo(currTr);
                            $(td).addClass('td-checked');
                            var chkbox = $('<input></input>').appendTo(td);
                            $(chkbox).attr('type','checkbox');
                            $(td).attr('width',20);
                            $(td).css('textAlign','center');
                        }

                        // convert ke array karena saat populasi data mengambil index this.option.field yg numeric sbg index dari td
                        var allValues = [];
                        var ref = 0;
                        var trId = null;
                        var defO = 0;
                        for(var j in data[i]){
                              if(ref == 0){ // tentukan id awal adalah nilai pertama, jgn gunakan index j, karena index j bisa jadi adalah literal (sl_in, acc_id)
                                    defO += 1;
                                    if(data[i][j]){
                                        if(typeof data[i][j] == 'string') data[i][j] = data[i][j].replace('tmp_','tmpx_');
                                    }else{
                                        data[i][j] = 'tmp_'+( defO + $.objlen(this.dataStorage) );
                                    }
                                    trId = data[i][j];
                                    $(currTr).attr('id',data[i][j]);
                                    allTrIds.push(trId);
                              }
                              allValues.push(data[i][j]);
                              ref++;
                        }

                        // proses data yang memiliki nilai order
                        this.dataStorage[trId] = {};
                        if(typeof this.option.jsonUrl == 'object'){
                            for(var j in this.option.jsonUrl[level].order){
                              var firstVal = allValues.splice(0,1);
                              var storageKey = this.option.jsonUrl[level].order[j] - 1;
                              this.dataStorage[trId][storageKey] = firstVal[0];

                              // kumpulkan data referensi dari data order
                              if(storageKey == this.option.jsonUrl[level].ref_data)
                                allRefData.push(firstVal[0]);
                            }
                        }

                        if(typeof this.option.field === 'object'){
                            for(var j in this.option.field){ // key dari this.option.field = key dari this.storageData
                                  // compare data storage dengan data sekarang
                                  // jika belum ada storage, ambil data pertama
                                  if(this.dataStorage[trId][j] === undefined && allValues.length > 0){
                                      var firstVal = allValues.splice(0,1);
                                      this.dataStorage[trId][j] = firstVal[0];
                                  }

                                  // check data ada kalau diluar order yang didefinisikan
                                  if(typeof this.option.jsonUrl == 'object' && j == this.option.jsonUrl[level].ref_data)
                                    allRefData.push(this.dataStorage[trId][j]);

                                  if(this.option.field[j].visibility === undefined || this.option.field[j].visibility !== 'hidden'){
                                        var td = $('<td></td>').appendTo(currTr);
                                        if(this.dataStorage[trId][j] === undefined)
                                          $(td).text('loading ..');
                                        else{
                                            var currVal = ((this.dataStorage[trId][j] !== undefined && this.dataStorage[trId][j] !== null) ?this.dataStorage[trId][j].toString():'');
                                            if (currVal === null || currVal === false)
                                                $(td).html('&nbsp;');
                                            else if (~currVal.indexOf('|'))
                                                $(td).html(currVal.split('|').join('<br />'));
                                            else
                                                $(td).html(currVal);
                                        }

                                        if(this.option.field[j].width === undefined){
                                            $(td).attr('width',200);
                                        }else{
                                            $(td).attr('width',this.option.field[j].width);
                                        }
                                        $(td).css('textAlign',this.option.field[j].align);

                                        // menambahkan callback
                                        // langsung ditambahkan ke td yg memiliki kemampuan menjalankan event
                                        // hanya ditambahkan di request pertama karena request pertama sudah ada semua td dengan isi loading ..
                                        if(typeof this.option.callback == 'function' && (this.option.field[j].addEvent == undefined || this.option.field[j].addEvent)){
                                            $(td).on('click',(function(param){
                                                    return function(e){
                                                        param.func(param.tr,e,param.gridObj);
                                                    }
                                            })({ func: this.option.callback, tr: currTr, gridObj: this.gridObj}));
                                            $(td).css('cursor','pointer');
                                        }
                                  }
                            }
                            this.dataStorage[trId][this.option.field.length] = 'numrows|'+(this.rawData.length-1);
                        }
                    }

                    this.reqState = false;
                    if(typeof this.option.jsonUrl !== 'undefined' && typeof this.option.jsonUrl[1] == 'object'){
                        // pencarian baru hanya butuh param multi_inst dan multi_filter, serta param tambahan dari ref_param
                        var newPostdata = {};//$.getObjClone(this.postData);
                        newPostdata['multi_inst'] = this.option.jsonUrl[level].ref_field;
                        newPostdata['multi_filter'] = allRefData;
                        if(typeof this.option.jsonUrl[level].ref_param == 'object'){
                            for(var i in this.option.jsonUrl[level].ref_param)
                                newPostdata[i] = this.option.jsonUrl[level].ref_param[i];
                        }
                        gridAPI.utils.insertData.call(this,++level,allTrIds,newPostdata);
                    }else{ // hanya ada satu json url
                        gridAPI.utils.attach_eventtr.call(this,allTrIds);
                        gridAPI.utils.attach_hovertr.call(this,allTrIds);
                        gridAPI.utils.attach_overtr.call(this,allTrIds);
                    }

                    $(this.counter).text($(this.contentTable).find('tr').length+'/'+this.totalData);
                    $(this.loadingDiv).hide();

                    if($(this.contentTable).find('tr').length < this.totalData)
                        gridAPI.utils.triggerNew.call(this,0);

                    // diset disini karena menunggu semua data request ajax paling terakhir di load,
                    // fungsi ini akan menambah css menjadi mobile
                    if(this.option.formatResponsive) this.setResponsive();
                }else{
                    if(data && data.msg) alert(data.msg);
                    else alert('data yang ditampilkan tidak dikenal');
                }
            },
            triggerNew:function(){ // jika data total masih blm memenuhi seluruh area table maka akan trigger baru
                // scroll atas
                // showHeight + max(currHeight) = allHeight + 17;
                var allHeight = $(this.contentDiv).prop('scrollHeight'); // total panjang div (permanent)
                var showHeight = $(this.contentDiv).innerHeight(); // total data yang tampil (permanen)
                var currHeight = $(this.contentDiv).scrollTop(); // jarak hidden atas dari div yg tampil (dinamis)

                // ketika sroll div maksimal sebatas bawah div yg terlihat
                if(allHeight - 18 < showHeight + currHeight && this.totalData > $.assocArraySize(this.dataStorage) && !this.reqState){
                    var newOffset = (this.postData.offset?this.postData.offset+this.postData.limit:this.postData.limit);
                    this.setPostData({offset:newOffset});
                    gridAPI.utils.insertData.call(this,0);
                }
            },
            attach_eventtr:function(allIds){ // dijalankan terakhir setelah tr setiap request telah terbentuk, jadi tinggal re-looping sehingga batasi limit seminimal mungkin
                if($.isArray(allIds) && typeof this.option.onDataLoad == 'function'){
                    for(var i=0; i<allIds.length;i++){
                        var currTr = $(this.contentTable).find('#'+allIds[i]);
                        this.option.onDataLoad(currTr,this.dataStorage[allIds[i]],this.tdStorage,this.storageTd);
                    }
                }
            },
            attach_hovertr:function(allIds){ // dijalankan terakhir setelah tr setiap request telah terbentuk, jadi tinggal re-looping sehingga batasi limit seminimal mungkin
                if($.isArray(allIds) && typeof this.option.onTrHover == 'function'){
                    for(var i=0; i<allIds.length;i++){
                        var currTr = $(this.contentTable).find('#'+allIds[i]);
                        $(currTr).on('mouseover',(function(param){
                                return function(e){
                                    param.func(param.tr,param.data);
                                }
                        })({func:this.option.onTrHover,tr:currTr,data:this.dataStorage[allIds[i]]}));
                    }
                }
            },
            attach_overtr:function(allIds){ // dijalankan terakhir setelah tr setiap request telah terbentuk, jadi tinggal re-looping sehingga batasi limit seminimal mungkin
                if($.isArray(allIds) && typeof this.option.onTrOver == 'function'){
                    for(var i=0; i<allIds.length;i++){
                        var currTr = $(this.contentTable).find('#'+allIds[i]);
                        $(currTr).on('mouseout',(function(param){
                                return function(e){
                                    param.func(param.tr,param.data);
                                }
                        })({func:this.option.onTrOver,tr:currTr,data:this.dataStorage[allIds[i]]}));
                    }
                }
            },
            addButton:function(obj){ // pass this as this.gridObj
                var href = $('<button></button>').appendTo(this.footerDiv);
                $(href).text(obj.title);
                $(href).addClass('grid_nav');
                $(href).addClass('btn');
                $(href).addClass('btn-default');
                $(href).addClass(obj.className);
                $(href).on('click',$.proxy(function(){
                    obj.callback(this.gridObj);
                },this));
            }
        },
        generate:function(options){
            this.setObject = function(currDom){
                this.gridObj = currDom; // dom in div simasgrid, ascendent table simasgrid
                $(this.gridObj).addClass('master_grid');
            },
            this.postData = {};
            this.setPostData = function(obj){
                for(var i in obj){
                    this.postData[i] = obj[i];
                }
            },
            this.setPostData2 = function(obj){
                if(obj[0]){
                    for(var i in obj[0]){
                        this.postData[i] = obj[0][i];
                    }
                }
            },
            this.delPostData = function(obj){
                if(obj[0]){
                    for(var i in obj[0]){
                        delete this.postData[obj[0][i]];
                    }
                }
            },
            this.emptyPostData = function(obj){
                if(this.postData && this.postData.length>0){
                    for(var i in this.postData){
                        delete this.postData[i];
                    }
                }
            },
            this.init = function(options){
                this.option = $.extend({
                    // These are the defaults.
                    field:[
                            {id:'satu',text:'field1',type:'string',width:150,'align':'center'}, //,'visibility':'hidden'
                            {id:'dua',text:'field2',type:'string'},
                            {id:'tiga',text:'field3',type:'string','align':'right'},
                            {id:'empat',text:'field4',type:'string',width:400,'align':'center','addEvent':false},
                            {id:'lima',text:'field5',type:'string',width:150},
                            {id:'enam',text:'field6',type:'string','search':'disabled'},
                            {id:'tujuh',text:'field7',type:'string','search':'disabled'},
                            {id:'delapan',text:'field8',type:'string','search':'disabled'},
                            {id:'sembilan',text:'field9',type:'string'},
                            {id:'sepuluh',text:'field10',type:'string','sort':true}
                        ],
                    jsonData:[{'id':'','nama':'akhyar'},{'id':'','nama':'faisal'},{'id':'','nama':'haikal'}],
                    // jsonUrl:'http://localhost/template/crm/index.php/admin/module/lead?bypass_access=true&active=false&field=sl_id,sl_name,sl_app&filter_sl_app=3',
                    // return {"msg":somemessage} untuk menampilkan pesan error di call level 1
                    // jsonUrl:[
                      // order is start from 1 to max this.option.field
                      // order untuk exclude jgn diisi 0 karena akan masuk ke td check box
                      // order : kolom yg dijadikan referensi tidak boleh di override
                      // hasil urutan order akan disimpan di this.dataStorage
                      // ref_data : kolom data yang diambil,start from 0 from dari this.dataStorage
                      // ref_data : ref pada td data tidak boleh di override, karena akan dipakai untuk membaca data
                      // ref_data : data pada kolom ini harus jadi unique key di call data selanjutnya (kalau bisa malah primary key)
                      // ref_field : kolom field yang menjadi referensi untuk call selanjutnya, kol ref_field harus ada di call selanjutnya !!
                      // ref_param : parameter get tambahan yang akan dikirim, jika url json url1 sama dengan json url2, tambahkan request : 2 atau lebih
                    //   {'ref_data':5,'ref_field':'sl_id','ref_param':{'bypass_access':true},'order':[6,1,2],'url':'http://localhost/template/crm/index.php/admin/module/lead?bypass_access=true&active=false&field=sl_id,sl_name,sl_app&filter_sl_app=3'}, // ambil dari simas lead link to polis
                    //   {'ref_data':5,'ref_field':'sl_id','ref_param':{},'order':[-1,3,4],'url':'http://localhost/template/crm/index.php/admin/module/lead?bypass_access=true&active=false&field=sl_id,sl_remark,sl_created&filter_sl_app=3'}, // ambil dari simas lead link to polis
                    //   {'ref_data':5,'ref_field':'sl_id','ref_param':{},'order':[-1,7,8],'url':'http://localhost/template/crm/index.php/admin/module/lead?bypass_access=true&active=false&field=sl_id,sl_name,sl_remark&filter_sl_app=3'} // ambil dari simas lead link to polis
                    // ],
                    countData:1,
                    // countUrl:'http://localhost/template/crm/index.php/admin/module/lead?bypass_access=true&active=false&filter_sl_app=3&return=count',
                    limit:5,
                    width:500,
                    height:300,
                    checkBoxAdded:false,
                    useSearch:true,
                    formatResponsive:false,
                    responsiveCB:null,
                    // responsiveCB:function(tr,td,e){// jika td responsive diklik, karena hanya utk melakukan request ajax jadi hanya butuh tr
                    //     console.log('responsiveCB');
                    //     console.log(tr);
                    //     console.log(td);
                    //     console.log(e);
                    // },
                    // saat seluruh ajax sudah diloa, hanya saat tampilan responsive
                    // untuk pengaturan tampilan masih bisa menggunakan onDataLoad() tp jgn sampai konflik dengan pengaturan desktop
                    onResponsiveLoad:null,
                    // onResponsiveLoad:function(gridObj){
                    //      gridObj refer to this
                    //      console.log(gridObj);
                    //      if($.isArray(allTrIds)){
                    //         for(var i=0; i<allTrIds.length;i++){
                    //             console.log($(allTrIds[i]));
                    //         }
                    //      }
                    //      var allTrIds = $(gridObj.contentTable).find('tr');
                    //      if(allTrIds.length>0){
                    //          for(var i=0; i<allTrIds.length;i++){
                    //              $(allTrIds[i]).find('td').not('.td-responsive').css('display','none');
                    //          }
                    //      }
                    // },
                    callback:null,
                    // callback:function(tr,e,domObj){ // jika tr diklik, karena hanya utk melakukan request ajax jadi hanya butuh tr
                    //     domObj refer to div simasgrid as asendent table simasgrid
                    //     console.log('callback');
                    //     console.log(tr);
                    //     console.log(e);
                    //     var currId = $(tr).attr('id');
                    //     var tableObj = $(domObj).simasgrid('getObj');
                    //     var partData = tableObj.dataStorage[currId][tdCol];
                    // },
                    onDataLoad:null,
                    // onDataLoad:function(tr,data,tdStorage,storageTd){ // saat tr di load, karena untuk susunan tampilan jadi butuh posisi data di table
                    //   console.log('dataload');
                    //   console.log(tr);
                    //   console.log(data);
                    //   var currGrid = $('#list_user').simasgrid('getObj');
                    //   if(currGrid.option.formatResponsive){
                    //         $(tr).find('td').eq(0).css('textAlign','left');
                    //         var txt = 'Customer : '+data[3];
                    //         txt += '<br />Mobile : '+data[5];
                    //         txt += '<br />Bon : '+data[6];
                    //         txt += '<br />Deadline : '+data[7];
                    //         txt += '<br /><span class="text-right col-prfl-12">';
                    //         txt += '<a target="_blank" href="'+global['instAdmin2Url']+'productdetail/productid/'+data[1]+'/due_date/'+data[7]+'/'+'">Detail</a>';
                    //         txt += '</span>';
                    //         $(tr).find('td').eq(0).html(txt);
                    //   }
                    // },
                    onTrHover:null,
                    // onTrHover:function(tr,data){ // saat tr di load, karena hanya utk melakukan request ajax jadi hanya butuh tr
                    //   console.log('dataHover');
                    //   console.log(tr);
                    //   console.log(data);
                    // },
                    onTrOver:null,
                    // onTrOver:function(tr,data){ // saat tr di load, karena hanya utk melakukan request ajax jadi hanya butuh tr
                    //   console.log('dataOver');
                    //   console.log(tr);
                    //   console.log(data);
                    // },
                    onFinished:null,
                    // onFinished:function(gridObj){ // saat semua selesai di load
                    //     gridObj refer to this
                    // },
                    postData:{'ordid':'sl_id','ordtype':'asc'}
                }, options );
                // if (this.option.onFinished) this.option.onFinished();

                this.rawData = []; // harus format array karena proses pengisian dengan td harus compare index
                this.dataStorage = {}; // harus format array karena proses pengisian dengan td harus compare index
                this.tdStorage = []; // referensi key data storage dengan key td [3,4,6] td[0] = dataStorage[3],td[1] = dataStorage[4],td[2] = dataStorage[6],
                this.storageTd = {}; // kebalikan dari this.tdStorage
                this.dataStorageLn = 0; // harus format array karena proses pengisian dengan td harus compare index

                this.setPostData(this.option.postData);
                this.setPostData({"offset":0});
                this.setPostData({"limit":(this.option.limit == undefined?5:this.option.limit)});

                this.reqState = false; // true = sedang ada request data
                this.totalData = 0;
                gridAPI.utils.createHeader.apply(this);
                gridAPI.utils.createBody.apply(this);
                gridAPI.utils.createFooter.apply(this);
            },
            this.rebuildGrid = function(){
                // hapus div content
                $(this.contentDiv).remove();
                // inisiasi ulang div content dengan option dan post data terbaru
                gridAPI.utils.createBody.apply(this);

                // populate ulang data
                gridAPI.utils.resetData.apply(this);
                // posisikan scroll keawal
                this.resize(this.option.width);
                $(this.headerDiv).scrollLeft(0);
                $(this.contentDiv).scrollLeft(0);
            },
            this.setResponsive = function(){ // ubah bentuk ke mobile 
                if(this.option.formatResponsive){
                    $(this.titleTR).hide();
                    $(this.searchTR).hide();
                    $(this.contentDiv).find('td').css('display','block');
                    $(this.contentDiv).find('td').width($(this.footerDiv).width());
                    $(this.contentDiv).find('table').css('overflowX','hidden');

                    if(this.option.onResponsiveLoad) 
                        this.option.onResponsiveLoad(this);
                }else{
                    alert('tidak diizikan, silahkan update konfigurasi');
                }
            },
            this.selectedData = function(){ 
                var allTr = $(this.contentTable).find('tr');
                // console.log($(this.contentTable));
                var allTrChecked = [];
                if(this.option.checkBoxAdded){
                    $.each(allTr,function( index, value ){
                            var chkState = $(value).find('td.td-checked').children('input[type=checkbox]').prop('checked');
                            if(chkState) allTrChecked.push(value);
                    });
                }
                return allTrChecked;
            },
            this.addNav = function(obj){
                gridAPI.utils.addButton.apply(this,obj);
            },
            this.resize = function(width){
                this.option.width = width;
                $(this.gridObj).css('width',width);
            },
            // mendapatkan hanya gridObj saat ini
            this.getObj = function(){
                return this;
            },
            // mendapatkan semua gridObj
            this.getGridObj = function(gridObj){
                return gridObj;
            }
        }
    };
    gridObj = [];
    $.fn.simasgrid = function(methodOrOptions) {
        var myDOM = this.get(0);
        var domId = $(myDOM).attr('id');

        // document parent :
        // newGridObj = $('#table').simasgrid('getGridObj');
        // document child :
        // window.gridObj = window.opener.newGridObj;
        // $($(window.opener.document).find('#table')).simasgrid('rebuildGrid');

        if(typeof methodOrOptions == 'object' || typeof methodOrOptions == 'undefined'){ // option setter
            if(gridObj[domId] === undefined){
                // inisiasi grid object baru
                gridObj[domId] = new gridAPI.generate();
                gridObj[domId].setObject(myDOM);
                gridObj[domId].init(methodOrOptions);
                gridObj[domId].rebuildGrid();
            }else{
                if(typeof methodOrOptions == 'object'){
                    for (var i in methodOrOptions){
                        gridObj[domId]['option'][i] = methodOrOptions[i];
                    }
                }
            }
        }else if (methodOrOptions == 'getGridObj') {
            return gridObj[domId][ methodOrOptions ](gridObj);
        }else if (typeof gridObj[domId][methodOrOptions] == 'function') { // invoce method
            // return gridObj[domId][ methodOrOptions ].apply( gridObj[domId], Array.prototype.slice.call( arguments, 1 ));
            return gridObj[domId][ methodOrOptions ](Array.prototype.slice.call( arguments, 1 ));
        }else if (typeof methodOrOptions == 'string'){ // option getter dan setter
            if(arguments[1] !== undefined)
                gridObj[domId]['option'][methodOrOptions] = arguments[1];
            else
                return gridObj[domId]['option'][methodOrOptions];
        }else if(methodOrOptions !== undefined){ // tidak ketemu method ini
            $.error('Method ' +  methodOrOptions + ' does not exist on jQuery.simasgrid' );
        }else{
            $.error('Param ' +  methodOrOptions + ' is not allowed' );
        }
    };

    $.extend({      
        prfl_simasgrid:true,        
    });
})( jQuery );

/*
$('#table').simasgrid();
$('#table').simasgrid("resize",'100%');
setTimeout(function(){ 
    alert("interval is running"); 
    var selected = $('#table').simasgrid("selectedData"); // jika selected data masih null berarti data masih di dlm keadaan async
    $('#table').simasgrid("rebuildGrid");
    var height = $('#table').simasgrid("height");
    console.log(height); // still show 50 (should 100 instead as initialize)

    $('#table1').simasgrid();
    var height = $('#table1').simasgrid("height");
    console.log(height); // still show 50 (should 100 instead as initialize)
}, 10000);
$('#table').simasgrid("addNav",{
  title:'Reload',
  className:'navigation',
  callback:function(simasGrid){
        console.log(simasGrid);
        var allTr = $(simasGrid).simasgrid('selectedData');
        var currObj = $(simasGrid).simasgrid('getObj');
        currObj.postData={};
        $(simasGrid).simasgrid('rebuildGrid');
  }
});

var tableObj = $('#table').simasgrid('getObj');
var partData = tableObj.dataStorage[trId][tdCol];
var headerDiv = tableObj.headerDiv;
*/
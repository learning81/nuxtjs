(function( $ ){
    var ua = navigator.userAgent.toLowerCase();
    $.extend({
        version:"0.1",
        msg:"semua ajax request harus ada id referensi yang sama",
        isStrict:document.compatMode == "CSS1Compat",
        isOpera:ua.indexOf("opera") > -1,
        isOperaMobile:ua.indexOf("opera") > -1 && ua.indexOf("x11"),
        isSafari:(/webkit|khtml/).test(ua),
        isSafari3:this.isSafari && ua.indexOf("webkit/5") != -1,
        isIE:!this.isOpera && ua.indexOf("msie") > -1,
        isIE6:!this.isOpera && ua.indexOf("msie 6") > -1,
        isIE7:!this.isOpera && ua.indexOf("msie 7") > -1,
        isIE8:(this.isIE && document.documentMode),
        isGecko:!this.isSafari && ua.indexOf("gecko") > -1,
        isGecko3:!this.isSafari && ua.indexOf("rv:1.9") > -1,
        isBorderBox:this.isIE && !isStrict,
        isWindows:(ua.indexOf("windows") != -1 || ua.indexOf("win32") != -1),
        isMac:(ua.indexOf("macintosh") != -1 || ua.indexOf("mac os x") != -1),
        isAir:(ua.indexOf("adobeair") != -1),
        isLinux:(ua.indexOf("linux") != -1),
        isIphone:ua.indexOf("iphone") != -1 || ua.indexOf("ipad") != -1,
        isAndroid:ua.indexOf("android") != -1 || navigator.platform.indexOf("armv") != -1,
        isWindowsPhone:(ua.indexOf("windows phone") != -1),
        isSecure:window.location.href.toLowerCase().indexOf("https") === 0
    });
})( jQuery );
<?php
@ini_set( 'upload_max_size' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'max_execution_time', '300' );

if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

$isSecure = false;
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $isSecure = true;
}elseif(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
    $isSecure = true;
}

$protocol = ($isSecure ? 'https' : 'http');
$baseUrl	= $protocol."://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/'; 
$baseUrl	= str_replace(array('index.php/','index.php'),'',$baseUrl); // base url path dasar ke controller

// $servername = "localhost";
// $username = "username";
// $password = "password";

// // Create connection
// $conn = new mysqli($servername, $username, $password);

// // Check connection
// if ($conn->connect_error) {
//     die("Connection failed: " . $conn->connect_error);
// } 
// echo "Connected successfully";


// echo 'php.ini: ', get_cfg_var('cfg_file_path');
// phpinfo();
// exit;
/*
if($_SERVER['REMOTE_HOST'] != '192.168.137.3')
	die('under maintenance');
*/

/* Testing Logs */

$str = $randomized = '';
$dig = "012345678923456789";
$abc = "abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
$numchars = 5; $letters = 1; $digits = 1;
if($letters == 1){$str .= $abc;}
if($digits == 1){$str .= $dig;}
for($i=0; $i < $numchars; $i++){$randomized .= $str{rand() % strlen($str)};}

if(!empty($_GET['tq']) && !defined('TRACECODE')) define('TRACECODE',$_GET['tq']);
elseif(!defined('TRACECODE')) define('TRACECODE',date('YmdHis').'$'.$randomized);

// jika session hang,
// 1. restart server
// 2. session_write_close();
// 3. session_start();
// 4. session_destroy();exit;

// if(session_id() == '') {
//     session_start();
// }

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(FALSE){
	$logFile = dirname(__FILE__).'/full/'.TRACECODE.'.log';

	if(!file_exists($logFile)){
		file_put_contents($logFile,"/*trace route dari semua request ke system*/\n");
		file_put_contents($logFile,"/*tercatat di : index.php */\n",FILE_APPEND);
		file_put_contents($logFile,"\n",FILE_APPEND);
		file_put_contents($logFile, sprintf("TRACECODE : %s\n",TRACECODE),FILE_APPEND);
		file_put_contents($logFile, sprintf("USER IP : %s\n",@$_SERVER['REMOTE_ADDR']),FILE_APPEND);
		file_put_contents($logFile, sprintf("CompName : %s\n",@gethostbyaddr(@$_SERVER['REMOTE_ADDR'])),FILE_APPEND);
		file_put_contents($logFile, sprintf("UserAgent : %s\n",@$_SERVER['HTTP_USER_AGENT']),FILE_APPEND);
	}
	
	file_put_contents($logFile, sprintf("URL : %s\n",@$_SERVER['HTTP_HOST'].@$_SERVER['PHP_SELF']),FILE_APPEND);
	file_put_contents($logFile, sprintf("datetime : %s\n",date('YmdHis')),FILE_APPEND);
	file_put_contents($logFile, "SERVER\n",FILE_APPEND);
	file_put_contents($logFile, print_r($_SERVER,TRUE),FILE_APPEND);
	file_put_contents($logFile, "GET\n",FILE_APPEND);
	file_put_contents($logFile, print_r($_GET,TRUE),FILE_APPEND);
	file_put_contents($logFile, "POST\n",FILE_APPEND);
	file_put_contents($logFile, print_r($_POST,TRUE),FILE_APPEND);
	file_put_contents($logFile, "RAW DATA\n",FILE_APPEND);
	file_put_contents($logFile, print_r(file_get_contents("php://input"),TRUE),FILE_APPEND);
	file_put_contents($logFile, "SESSION\n",FILE_APPEND);
	file_put_contents($logFile, print_r($_SESSION,TRUE),FILE_APPEND);
}

/*
$tnsname = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 128.21.34.43)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ajsdb)))';
$conn = oci_connect('dev','linkdev',$tnsname);
// $stid = oci_parse($conn, "insert into crm.simbak_account (acc_id) values ('ACC0005')");
// $stid = oci_parse($conn, 'SELECT MAX(ACC_ID) AS "ACC_ID" FROM "CRM"."simbak_account"');
$stid = oci_parse($conn,"select sysdate from dual");
oci_execute($stid);
$row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
die('test oracle query');
/**/

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
date_default_timezone_set('Asia/Jakarta');
// if( ! ini_get('date.timezone')){
//    date_default_timezone_set('GMT');
// } 

 /**
 * Add temporary function
 */
if ( ! function_exists('date_create'))
{
	function date_create($date)
	{
		return strtotime($date);
	}
}

if ( ! function_exists('date_format'))
{
	function date_format($date,$format)
	{
		return date($format, $date);
	}
}

/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same directory
 * as this file.
 */
$system_path = dirname(__FILE__).'/codeigniter/versi3110/system/';
$system_path = dirname(__FILE__).'/codeigniter/versi3/';

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder than the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server. If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 */

$application_folder = dirname(__FILE__).'/';

/*
 *---------------------------------------------------------------
 * VIEW FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want to move the view folder out of the application
 * folder set the path to the folder here. The folder can be renamed
 * and relocated anywhere on your server. If blank, it will default
 * to the standard location inside your application folder. If you
 * do move this, use the full server path to this folder.
 *
 * NO TRAILING SLASH!
 */
$view_folder = $application_folder.'/views/';

	// echo $scriptfilename."<br />";
	// echo $system_path."<br />";
	// echo $application_folder."<br />";
	// echo $view_folder."<br />";
	// exit;
	

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 */
$scriptfilename = str_replace('\\','/',strtolower($_SERVER['SCRIPT_FILENAME']));
if(!empty($scriptfilename) && (
	// untuk kamus di laptop
	// masih pake db profile2
	strpos($scriptfilename,'/var/www/html/nuxtjs/index.php') !== FALSE
	|| strpos($scriptfilename,'/usr/tmpweb/development/nuxtjs/index.php') !== FALSE
	|| strpos($scriptfilename,'/usr/tmpweb/development/nuxtjs/telegram.php') !== FALSE
	)){
	$system_path = dirname(dirname(__FILE__)).'/codeigniter/versi3/';
	$application_folder = dirname(__FILE__).'/';
	$view_folder = $application_folder.'/views/';
	define('ENVIRONMENT', 'single');
}elseif(!empty($scriptfilename) && (
	// untuk kamus di server
	// harus pake db profile2
	strpos($scriptfilename,'/usr/tmpweb/development/apps/profile/nuxtjs/index.php') !== FALSE
	|| strpos($scriptfilename,'/usr/tmpweb/development/apps/profile/nuxtjs/telegram.php') !== FALSE
	|| strpos($scriptfilename,'/home/178213.cloudwaysapps.com/qmsmdpnssk/public_html/index.php') !== FALSE
	)){
	$system_path = dirname(dirname(__FILE__)).'/codeigniter/versi3/';
	$application_folder = dirname(__FILE__).'/';
	$view_folder = $application_folder.'/views/';
	define('ENVIRONMENT', 'collection');
}elseif(!empty($scriptfilename) && (
	strpos($scriptfilename,'c:/wamp64/www/profile2/full/index.php') !== FALSE
	|| strpos($scriptfilename,'/home/178213.cloudwaysapps.com/qmsmdpnssk/public_html/index.php') !== FALSE
	)){
	$system_path = dirname(dirname(__FILE__)).'/nuxtjs/codeigniter/versi3/';
	define('ENVIRONMENT', 'full_subapp');
}elseif(!empty($scriptfilename) && (
	// untuk kamus di server
	// harus pake db profile2
	strpos($scriptfilename,'/var/www/html/full/index.php') !== FALSE
	|| strpos($scriptfilename,'/home/178213.cloudwaysapps.com/qmsmdpnssk/public_html/index.php') !== FALSE
	)){
	$system_path = dirname(dirname(__FILE__)).'/nuxtjs/codeigniter/versi3/';
	define('ENVIRONMENT', 'full_subapp2');
}elseif(!empty($scriptfilename) && (
	strpos($scriptfilename,'c:/wamp64/www/profile2/index.php') !== FALSE
	|| strpos($scriptfilename,'/home/178213.cloudwaysapps.com/qmsmdpnssk/public_html/index.php') !== FALSE
	)){
	$system_path = dirname(__FILE__).'/nuxtjs/codeigniter/versi3/';
	$application_folder = dirname(__FILE__).'/full/';
	$view_folder = $application_folder.'views/';
	define('ENVIRONMENT', 'full_app');
}elseif(!empty($scriptfilename) && (
	// untuk kamus di server
	// harus pake db profile2
	strpos($scriptfilename,'/var/www/html/index.php') !== FALSE
	|| strpos($scriptfilename,'/home/178213.cloudwaysapps.com/qmsmdpnssk/public_html/index.php') !== FALSE
	)){
	$system_path = dirname(__FILE__).'/nuxtjs/codeigniter/versi3/';
	$application_folder = dirname(__FILE__).'/full/';
	$view_folder = $application_folder.'views/';
	define('ENVIRONMENT', 'full_app2');
}else{
	define('ENVIRONMENT', NULL);
}

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
if(	ENVIRONMENT == 'single2'
	|| ENVIRONMENT == 'collection2'
	|| ENVIRONMENT == 'full_app'
	|| ENVIRONMENT == 'kamus_app'
	|| ENVIRONMENT == 'full_app2'
	){
	error_reporting(1);
	// error_reporting(E_ALL);
	ini_set('display_errors', 1);
}elseif(ENVIRONMENT == 'single'
	|| ENVIRONMENT == 'collection'
	){
	ini_set('display_errors', 0);
	if (version_compare(PHP_VERSION, '5.3', '>='))
	{
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
	}
	else
	{
		error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
	}
}else{
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	echo 'The application environment is not set correctly.';
	exit(1); // EXIT_ERROR
}

/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here. For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT: If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller. Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 */
	// The directory name, relative to the "controllers" folder.  Leave blank
	// if your controller is not in a sub-folder within the "controllers" folder
	// $routing['directory'] = '';

	// The controller class file name.  Example:  mycontroller
	// $routing['controller'] = '';

	// The controller function you wish to be called.
	// $routing['function']	= '';

/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 */
	// $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (($_temp = realpath($system_path)) !== FALSE)
	{
		$system_path = $_temp.'/';
	}
	else
	{
		// Ensure there's a trailing slash
		$system_path = rtrim($system_path, '/').'/';
	}

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'Your system folder path does not appear to be set correctly. Please open the following file and correct this: '.pathinfo(__FILE__, PATHINFO_BASENAME);
		exit(3); // EXIT_CONFIG
	}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// Path to the system folder
	define('BASEPATH', str_replace('\\', '/', $system_path));

	// Path to the front controller (this file)
	define('FCPATH', dirname(__FILE__).'/');

	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		if (($_temp = realpath($application_folder)) !== FALSE)
		{
			$application_folder = $_temp;
		}

		define('APPPATH', $application_folder.DIRECTORY_SEPARATOR);
	}
	else
	{
		if ( ! is_dir(dirname(BASEPATH).'/'.$application_folder.DIRECTORY_SEPARATOR))
		{
			header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			echo 'Your application folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
			exit(3); // EXIT_CONFIG
		}

		define('APPPATH', dirname(BASEPATH).'/'.$application_folder.DIRECTORY_SEPARATOR);
	}

	// The path to the "views" folder
	if ( ! is_dir($view_folder))
	{
		if ( ! empty($view_folder) && is_dir(APPPATH.$view_folder.DIRECTORY_SEPARATOR))
		{
			$view_folder = APPPATH.$view_folder;
		}
		elseif ( ! is_dir(APPPATH.'views'.DIRECTORY_SEPARATOR))
		{
			header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			echo 'Your view folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
			exit(3); // EXIT_CONFIG
		}
		else
		{
			$view_folder = APPPATH.'views';
		}
	}

	if (($_temp = realpath($view_folder)) !== FALSE)
	{
		$view_folder = $_temp.DIRECTORY_SEPARATOR;
	}
	else
	{
		$view_folder = rtrim($view_folder, '/\\').DIRECTORY_SEPARATOR;
	}

	define('VIEWPATH', $view_folder);

	define('MASTERPATH', dirname(APPPATH).'/');


/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */
require_once BASEPATH.'core/CodeIgniter.php';

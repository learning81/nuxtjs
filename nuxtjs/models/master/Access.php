<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Access extends MY_Model
{
	function __construct(){
		parent::__construct();
		
		// load main table
		// pada da_rule_access = 'app_type', da_description harus berisi value sebagai nama aplikasi
		// unik = da_access_name, da_access_name+da_rule_access,da_description
		$this->mainTable = array(
			'default_access', // nama table
			'da_access_name(varchar-25)', // nama akses matrix
			'da_rule_access(varchar-25)', // nama parameter akses
			'da_rule_value(varchar-1000)', // nilai parameter akses, pemisah tanda ;
			'da_description(varchar-200)' // deskripsi tambahan
		);
	}
	
	// get access value by key
	function get_access($rule_access=NULL,$search=NULL){
		$data = NULL;

		// if user has been get session and he is not admin, always give output what ever rule access asked
		if(!empty($_SESSION['access_matrix'])){
			$cond = new stdClass();
			$cond->query = sprintf("SELECT * FROM %s.default_access
			WHERE da_rule_access = '{$rule_access}' 
			AND da_access_name = '{$_SESSION['access_matrix']}'",$this->schema);

			$result = $this->get_attribute($cond,null,$this->SINGLE);
			if(!empty($result)){
				$data = $result->{$this->get_field_name('da_rule_value')};
				if(!empty($data)){
					$data = explode(';',$data);
				}
			}

			if(isset($search) && !empty($data) && in_array($search,$data)){
				return TRUE;
			}elseif(isset($search)){
				return FALSE;
			}
		}
		return $data;
	}
	
	// mendapatkan app id berdasarkan app ('mnc' atau 1)
	// biasanya digunakan pada saat login
	function get_appid_by_app($app=NULL){ // in string
		$cond = new stdClass();
		$cond->query = sprintf("select da_rule_value 
								from %s.default_access 
								where da_rule_access = 'app_type' 
								and (da_rule_value = '%s' or lower(da_description) = '%s')
								",
			$this->schema,
			$app,
			strtolower($app)
		);
		$result = $this->get_attribute($cond,null,$this->SINGLE);
		if(!empty($result))
			return $this->additional_function->set_value($result,$this->get_field_name('da_rule_value'));
		return NULL;
	}

	// mendapatkan unique name berdasarkan app ('mnc' atau 1)
	// biasanya digunakan pada saat login
	function get_unique_by_app($app=NULL){ // in array
		$cond = new stdClass();
		$cond->query = sprintf("select da_rule_value 
								from %s.default_access 
								where da_rule_access = 'unique_name' and da_access_name 
								IN(
  									select da_access_name 
  									from %s.default_access 
  									where da_rule_access = 'app_type' 
  									and (lower(da_description) = '%s' or da_rule_value = '%s')
								)",
			$this->schema,
			$this->schema,
			strtolower($app),
			$app
		);
		$result = $this->get_attribute($cond,null);
		if(!empty($result))
			return $this->additional_function->get_array($result,$this->get_field_name('da_rule_value'));
		return NULL;
	}

	// mendapatkan data detail access
	// get_access_by_unique(NULL,NULL) => get current access name
	// get_access_by_unique(unique,NULL) => get access name by unique
	// ?? get_access_by_unique(NULL,'access_rule') => get current access value by access rule for current access name
	// get_access_by_unique(unique,'access_rule') => get current access value by access rule for the unique name
	function get_access_by_unique($uniqueName=NULL,$accessRule=NULL,$forceSplit=FALSE){

		$currUnique = $this->access->get_access('unique_name');
		if(empty($uniqueName)){
			$uniqueName = $this->additional_function->set_value($currUnique,0);
		}

		if(!empty($accessRule)){
			$searchField = $this->get_field_name('da_rule_value');
		}else{
			$searchField = $this->get_field_name('da_access_name');
			$accessRule = 'unique_name';
		}

		 // in array
		$cond = new stdClass();
		$cond->query = sprintf("select dest.da_access_name,sorc.da_rule_value unique_name,dest.da_rule_access,dest.da_rule_value, dest.da_description
								from %s.default_access dest
								left join %s.default_access sorc on sorc.da_access_name = dest.da_access_name
								where dest.da_rule_access = '%s'
								and sorc.da_rule_access = 'unique_name' and sorc.da_rule_value = '%s'",
			$this->schema,
			$this->schema,
			$accessRule,
			$uniqueName
		);
		$result = $this->get_attribute($cond,null,$this->SINGLE);

		if(!empty($result)){
			$result = $this->additional_function->set_value($result,$searchField);
			if($forceSplit) $result = $this->additional_function->string_to_array($result,';');
		}
		return $result;
	}

	// harus dibuat baru karena ngambil nilai dari description
	function get_appname_by_unique($uniqueName=NULL){ // get string
		$accessName = $this->get_access_by_unique($uniqueName=NULL);
		$cond = new stdClass();
		$cond->query = sprintf("SELECT %s FROM %s.%s WHERE  %s = 'app_type' and %s = '%s'",
			'da_description',
			$this->schema,
			'default_access',
			'da_rule_access',
			'da_access_name',
			$accessName
		);
		$result = $this->get_attribute($cond,null,$this->SINGLE);
		if(!empty($result))
			return strtolower($this->additional_function->set_value($result,$this->get_field_name('da_description')));
		return NULL;
	}

	/**
	 * $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	 * $cond = kondisi default mysql
	 *
	 * $arrParam = array(
	 * 		'limit'=> num or row
	 * 		'offset'=> start row
	 * 		'ordid'=> field yg akan diorder
	 * 		'ordtype'=> jenis order (asc,desc)
	 *
	 *     	'bypass_access' => TRUE, hanya untuk admin akan menampilkan semua akses
	 *
	 *		'single_inst' => fieldname, // field sebagai index dari data hasil
	 *		'single_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 *		'multi_inst' => fieldname, // field sebagai index dari data hasil
	 *		'multi_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 * );
	 * $result = $this->get_detail($arrParam,$cond);
	 */
	final function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_da_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'da_access_name';

		if(!empty($arrParam['field'])) 
			$cond->field = $arrParam['field'];

		$limit = $this->additional_function->set_value($arrParam,'limit');
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if($limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}


		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi single, dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// jika admin bisa bypass akses
		if(isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
			&& isset($arrParam['bypass_access']) 
			&& ($arrParam['bypass_access'] === TRUE || $arrParam['bypass_access'] === 'true')
		){
			// nothing
		}else{
			$accessLst = $this->filecache->cache_read('access_list','session');
			if(empty($accessLst)) $accessLst = array('');
			$cond->where_in[] = array('fieldName'=>'da_access_name','fieldValue'=>$accessLst);
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}

		$result = $this->get_attribute($cond,$this->get_table_name('default_access'),$returnType);

		if(!empty($result) && is_array($result)){ // utk multiple dan single yg sudah jadi multiple
			$lastResult = NULL;
			foreach ($result as $idxAccess => $valAcccess) {
				// update $result here

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) || empty($multiInst)){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valAcccess,$this->get_field_name($singleInst));
					else
						$instId = $idxAccess;

					// clone semua data
					$lastResult[$instId] = $valAcccess;
				}else{
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valAcccess,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valAcccess);
				}
			}
		}else{ // utk count
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && is_array($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	// jika yang akan diinput adalah rule_access = 'user_role_member', jika nilai kosong maka isikan dengan next id (insert dan update)
	final function update($arrParam,$objCond=NULL,$method){
		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^da_/i",strtolower($idxParam))){
					$main[$this->get_field_name($idxParam)] = $valParam;
				}
			}
		}
		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_da_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		if($_SESSION['access_matrix'] != 'sangpetualang'){
			return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
		}

		// khusus utk input dengan rule_access = 'user_role_member'
		// role id akan di generate khusus untuk admin
		if((@$arrParam['filter_da_rule_access'] == 'user_role_member' /*update*/|| @$arrParam['da_rule_access'] == 'user_role_member' /*insert*/) 
			&& !is_numeric($main[$this->get_field_name('da_rule_value')])
			&& isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
		){
			$lastRoleId = $this->get_detail(array(
				'filter_da_rule_access'=>'user_role_member',
				'field'=>'da_rule_value',
				'ordid'=>'length(da_rule_value) DESC, da_rule_value DESC',
				'bypass_access'=>TRUE
			),NULL,$this->SINGLE);
			$currRoleId = $this->additional_function->set_value($lastRoleId,$this->get_field_name('da_rule_value'),'number');
			$nextRoleId = $currRoleId + 1;
			$main[$this->get_field_name('da_rule_value')] = $nextRoleId;
		}
		// khusus utk input dengan rule_access = 'app_type'
		// role id akan di generate khusus untuk admin
		if((@$arrParam['filter_da_rule_access'] == 'app_type' /*update*/|| @$arrParam['da_rule_access'] == 'app_type' /*insert*/) 
			&& !is_numeric($main[$this->get_field_name('da_rule_value')])
			&& isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
		){
			$lastRoleId = $this->get_detail(array(
				'filter_da_rule_access'=>'app_type',
				'field'=>'da_rule_value',
				'ordid'=>'length(da_rule_value) DESC, da_rule_value DESC',
				'bypass_access'=>TRUE
			),NULL,$this->SINGLE);
			$currAppId = $this->additional_function->set_value($lastRoleId,$this->get_field_name('da_rule_value'),'number');
			$nextAppId = $currAppId + 1;
			$main[$this->get_field_name('da_rule_value')] = $nextAppId;
		}
		// untuk input dari array akan di konversi menjadi tanda ; terlebih dahulu
		if(is_array(@$main[$this->get_field_name('da_rule_value')]))
			$main[$this->get_field_name('da_rule_value')] = implode(';',$main[$this->get_field_name('da_rule_value')]);

		if($method == 'update'){
			$cond->field = array('da_access_name','da_rule_access','da_rule_value');
			$accessLst = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
			if(empty($accessLst))
				$accessLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			else
				$accessLst = array('state'=>TRUE,'msg'=>'data berhasil diperbaharui','data' => $accessLst);
		}elseif($method == 'insert'){
			$accessLst = array('state'=>TRUE,'msg'=>'data berhasil ditambahkan','
				data' => array(
								'da_access_name' => $this->additional_function->set_value($main,$this->get_field_name('da_access_name')),
								'da_rule_access' => $this->additional_function->set_value($main,$this->get_field_name('da_rule_access')),
								'da_rule_value' => $this->additional_function->set_value($main,$this->get_field_name('da_rule_value'))
							)
			);
		}

		if(empty($main['da_domain'])) $main['da_domain'] = config_item('base_url');
		if(!empty($main))
			$result = $this->set_attribute($main,$this->get_table_name('default_access'),$method,$cond);

		$this->audit_process->insert_data(
			NULL,
			$this->audit_process->ACCESS,
			$method.' access '.$this->additional_function->set_value($arrParam,$this->get_field_name('da_rule_access')),
			'Val: '.$this->services_json->encode($arrParam)
		);

		return $accessLst;
	}

	final function remove($arrParam,$objCond=NULL){
		$cond = $this->additional_function->clone_obj($objCond);

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_da_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		if(!empty($cond->where) || !empty($cond->where_in)){
			$cond->field = array('da_access_name','da_rule_access','da_rule_value');
			$accessLst = $this->get_detail($arrParam,$cond); // $source bisa bypass kondisi ke get_detail

			if(empty($accessLst))
				return array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			return array('state'=>FALSE,'msg'=>'tidak ada kondisi dalam metode penghapusan');
		}
		$result = $this->remove_attribute($this->get_field_name('default_access'),$cond,TRUE);

		if(!empty($accessLst) && is_array($accessLst)){
			foreach ($accessLst as $idxLst => $valLst) {
				$this->audit_process->insert_data(
					NULL,
					$this->audit_process->ACCESS,
					'hapus access : '.$this->additional_function->set_value($valLst,'da_rule_access'),
					'Val: '.$this->services_json->encode($cond)
				);
			}
		}

		return $accessLst;
	}

}
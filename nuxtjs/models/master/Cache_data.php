<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Cache_data extends MY_Model
{
	function __construct(){
		parent::__construct();
	}
	
	function set_app_param(){
		$this->filecache->remove_file('application');
		// $this->filecache->cache_save('cli_debug',TRUE,'session');

		// $this->additional_function->background_exec('cache','set_type_param',TRUE);
		// $this->additional_function->background_exec('cache','set_session_param',TRUE);
		// $this->additional_function->background_exec('cache','set_rolemember_param',TRUE);

		$this->set_type_param();
		$this->set_session_param();
		$this->set_rolemember_param();

		$this->filecache->cache_save('date',date('Y-m-d'),'application','global');
		// die('selesai membuat semua variable global cache');
	}

	function set_type_param(){
		$cond = new stdClass();
		$cond->query = sprintf("select 
									application.%s as \"app_id\",
									application.%s as \"access_name\", 
									state.%s as \"state\"
								from (select * from %s where %s = 'app_type') application
								left join (select * from %s where %s = 'site_maintenance') state
									on application.%s = state.%s
								order by application.%s
								",
			'da_rule_value',
			'da_description',
			'da_rule_value',
			'default_access',
			'da_rule_access',
			'default_access',
			'da_rule_access',
			'da_access_name',
			'da_access_name',
			'da_description'
		);
		$appAll = $this->get_attribute($cond);
		$activeApptype = array();
		$apptype = array();
		if(!empty($appAll)){
			foreach ($appAll as $key => $value){
				// app type yg salah satu accessnya tidak maintenance
				if($value->state != 1 && !in_array($value->access_name,$activeApptype))
					$activeApptype[$value->app_id] = $value->access_name;
				
				if(!in_array($value->access_name,$apptype))
					$apptype[$value->app_id] = $value->access_name;
			}
			unset($appAll);
		}
		$this->filecache->cache_save('active_apptype',$activeApptype,'application','global');
		$this->filecache->cache_save('apptype',$apptype,'application','global');
		// die('selesai membuat global apptype');
		// {1=>'aazni.net',2=>'marketplace.com'}
	}

	function set_session_param(){
		$cond = new stdClass();
		$cond->query = sprintf("select distinct
									%s as \"access_name\" 
								from %s
								order by %s
								",
			'da_access_name',
			'default_access',
			'da_access_name'
		);
		$sessionAll = $this->get_attribute($cond);
		$sessionAll = $this->additional_function->get_array($sessionAll,'access_name');
		$this->filecache->cache_save('session_name',$sessionAll,'application','global');
		// die('selesai membuat global apptype');
		// ['blog','marketplace','merchant_mplace1','merchant_mplace2']
	}

	function set_rolemember_param(){
		$cond = new stdClass();
		$cond->query = sprintf("select 
									application.%s as \"description\", 
									application.%s as \"app_id\",
									application.%s as \"access_name\", 
									member.%s as \"member_id\"
								from (select * from %s where %s = 'app_type') application
								left join (select * from %s where %s = 'user_role_member') member
								on application.%s = member.%s",
			'da_description',
			'da_rule_value',
			'da_access_name',
			'da_rule_value',
			'default_access',
			'da_rule_access',
			'default_access',
			'da_rule_access',
			'da_access_name',
			'da_access_name'
		);
		$memberAll = $this->get_attribute($cond);
		$rolemember = array();
		if(!empty($memberAll)){
			foreach ($memberAll as $key => $value){
				// set first var for ext_jenis
				if(empty($rolemember['member_'.strtolower($value->description)])){
					$rolemember['member_'.strtolower($value->description)] = array();
				}

				// insert data
				if(!empty($value->description) && !empty($value->member_id)){
						if(!in_array($value->member_id, $rolemember['member_'.strtolower($value->description)])){
							$rolemember['member_'.strtolower($value->description)][] = $value->member_id;
						}
				}
			}
			unset($memberAll);
		}
		$this->filecache->cache_save('rolemember',$rolemember,'application','global');
		// die('selesai membuat global rolemember');
		// {'member_aazni.net'=>1,'member_marketplace.com'=>2}
	}
}
<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Kamus_data extends MY_Model{
	function __construct(){
        parent::__construct();
        
        // relasi master => key adalah 1 : n
	}
    
    /* DATA MASTER */
	final function detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		if($_SESSION['access_matrix'] != 'sangpetualang'){
			return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
		}
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'dkm_id';

		$limit = (isset($arrParam['limit'])?$arrParam['limit']:TRUE);
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if(is_numeric($limit) && $limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(isset($limit) && ($limit === FALSE || $limit === 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}

		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}
		$result = $this->get_attribute($cond,$this->get_table_name('default_kamus_master'),$returnType);
		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxNews => $valNews) {
				// update $result here

				if(isset($result[$idxNews]->dkm_desc)){
					$result[$idxNews]->dkm_desc = $this->crypt->decrypt($result[$idxNews]->dkm_desc);
					$result[$idxNews]->dkm_desc = stripslashes($result[$idxNews]->dkm_desc);
				}

				if(isset($arrParam['short_desc']) && ($arrParam['short_desc'] == TRUE || $arrParam['short_desc'] == 'true')){
					$currDesc = $this->additional_function->set_value($valNews,'dkm_desc');
					$result[$idxNews]->dkm_desc = $this->additional_function->get_words_from_html($currDesc,15).' ...';
				}

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valNews,$this->get_field_name($singleInst));
					else
						$instId = $idxNews;

					// clone semua data
					$lastResult[$instId] = $valNews;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valNews,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valNews);
				}else{
					$lastResult[] = $valNews;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function update($arrParam,$objCond=NULL,$method){
		if($_SESSION['access_matrix'] != 'sangpetualang'){
			return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
		}

		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^dkm_/i",strtolower($idxParam))){
					if(strtolower($idxParam) == 'dkm_desc' || strtolower($idxParam) == 'dkm_title')
						$main[$this->get_field_name($idxParam)] = str_replace('$','&#36;',addslashes($valParam));
					else
						$main[$this->get_field_name($idxParam)] = $valParam;
				}
			}
		}

        $paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		// inisiasi return
		$dictLst = array('state'=>FALSE,'msg'=>'belum di proses','data'=>NULL);

		// jika title kosong jgn diisi dulu
		if(isset($main[$this->get_field_name('dkm_title')]) && empty($main[$this->get_field_name('dkm_title')])){
			return array('state'=>FALSE,'msg'=>'judul belum diisi','data'=>NULL);
		}

		if(!empty($main[$this->get_field_name('dkm_desc')])){
			$currDesc = $main[$this->get_field_name('dkm_desc')];

			$main[$this->get_field_name('dkm_desc')] = str_replace(
				array(
					config_item('base_url')
					,config_item('base_url').'index.php/'
					,config_item('base_url').'index.php'
				)
				,'[base_url]'
				,$currDesc
			);

			$main[$this->get_field_name('dkm_desc')] = $this->crypt->encrypt($main[$this->get_field_name('dkm_desc')]);
		}

		// check last user
		$cond->field = array('dkm_id','dkm_uid');
		$arrParam['bypass_access'] = TRUE;
		$lastDict = (array) $this->detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail

		$affected = 0;

		if(empty($main[$this->get_field_name('dkm_updated')])) $main[$this->get_field_name('dkm_updated')] = date('Y-m-d H:i:s');

		if($method == 'update' && !empty($main)){
			if(empty($lastDict)){
				$dictLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			}else{
				$allIds = $this->additional_function->get_array($lastDict,$this->get_field_name('dkm_id'));
				$cond = new stdClass();
				$cond->where_in[] = array(
					'fieldName'=>'dkm_id',
					'fieldValue'=>$allIds
				);
				$affected = $this->set_attribute($main,$this->get_table_name('default_kamus_master'),$method,$cond,TRUE);
				if($affected>0)
					$dictLst = array('state'=>TRUE,'msg'=>'data berhasil diupdate','data'=>$lastDict);
				else
					$dictLst = array('state'=>TRUE,'msg'=>'tidak ada parameter yg diupdate','data'=>$lastDict);
			}
		}elseif($method == 'insert' && !empty($main)){
			// $nextId = $this->get_next_id($this->get_field_name('dkm_id'),$this->get_table_name('default_kamus_master'));
			$nextId = $this->get_next_id2('default_kamus_master');
			$main[$this->get_field_name('dkm_id')] = $nextId;
			if(empty($main[$this->get_field_name('dkm_uid')]))
				$main[$this->get_field_name('dkm_uid')] = $this->get_next_id3();
			$affected = $this->set_attribute($main,$this->get_table_name('default_kamus_master'),$method,$cond);

			if($affected > 0){
				$cond = new stdClass();
				$cond->field = array('dkm_id','dkm_uid');
				$arrParam['bypass_access'] = TRUE;
				$lastDict = (array) $this->detail(array('filter_dkm_id'=>$nextId,'bypass_access'=>TRUE),$cond);
				$dictLst = array(
					'state'=>TRUE,
					'msg'=>'data berhasil ditambahkan',
					'data'=>$lastDict
				);

			}else{
				$dictLst = array('state'=>FALSE,'msg'=>'data tidak berhasil diinput','data'=>NULL);
			}
		}elseif(empty($main)){
			$dictLst = array(
				'state'=>FALSE,
				'msg'=>'tidak ada parameter data yang dikirimkan',
				'data'=>$lastDict
			);
		}

		// if(!empty($lastDict)){
		// 	foreach ($lastDict as $idxDict => $valDict){
		// 		$currDictId = $this->additional_function->set_value($valDict,$this->get_field_name('dkm_id'));
		// 		$this->audit_process->insert_data(
		// 			$currDictId,
		// 			$this->audit_process->KAMUS,
		// 			$method.' title '.$this->additional_function->set_value($arrParam,'dkm_title'),
		// 			'Val: '.$this->services_json->encode($arrParam)
		// 		);
		// 	}
		// }

		return $dictLst;
	}

	final function remove($arrParam,$objCond=NULL){
		if($_SESSION['access_matrix'] != 'sangpetualang'){
			return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
		}

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$lastDict = $this->detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
		if(empty($lastDict)){
			$dictLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			$affected = $this->remove_attribute('default_kamus_master',$cond,TRUE);
			$dictLst = array('state'=>TRUE,'msg'=>'data berhasil dihapus','data' => $lastDict);
		}

		// if(!empty($lastDict) && is_array($lastDict)){
		// 	foreach ($lastDict as $idxLst => $valLst) {
		// 		$this->audit_process->insert_data(
		// 			$this->additional_function->set_value($valLst,'dkm_id'),
		// 			$this->audit_process->KAMUS,
		// 			'hapus kamus : '.$this->additional_function->set_value($valLst,'dkm_title'),
		// 			'Val: '.$this->services_json->encode($cond)
		// 		);
		// 	}
		// }

		return $dictLst;
	}	

	final function param_as_cond($arrParam=NULL){
		$cond = new stdClass();

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dkm_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_pp_as_string')){
						$cond->where[] = sprintf("%s = '%s'",str_replace(array('filter_dkm_as_string','FILTER_DKM_AS_STRING'),'','dkm'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_dkm_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}

		return $cond;
	}

    /* KAMUS KEY */
	final function detail_key($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond_key($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'dkk_id';

		$limit = (isset($arrParam['limit'])?$arrParam['limit']:TRUE);
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if(is_numeric($limit) && $limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(isset($limit) && ($limit === FALSE || $limit === 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}

		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}

		if(!empty($arrParam['join_kamus']) && ($arrParam['join_kamus']==TRUE || $arrParam['join_kamus']=='true')){
			$cond->join[] = array(
				'refTable' => 'default_kamus_master',
				'cond' => "default_kamus_master.dkm_id = default_kamus_key.dkk_master",
				'direction' => 'right'
			);
			$cond->where[] = sprintf("%s = 1",'dkm_active');

			// jika semua key dari kamus di nonaktifkan tapi kamus masih aktif, maka comment baris dibawah
			// uncomment kembali ketika key aktif sudah dipasang
			$cond->where[] = sprintf("(%s != 0 OR %s IS NULL)",'dkk_active','dkk_active');
		}

		if(!empty($arrParam['filter_dkm_id'])){
			$cond->where[] = sprintf("%s = '%s'",'dkm_id',$arrParam['filter_dkm_id']);
		}

		if(!empty($arrParam['filter_alias_dkm_title'])){
			$cond->where[] = sprintf("lower(%s) like '%%%s%%'",'dkm_title',strtolower($arrParam['filter_alias_dkm_title']));
		}

		if(!empty($arrParam['filter_alias_dkm_desc'])){
			$cond->where[] = sprintf("lower(%s) like '%%%s%%'",'dkm_desc',strtolower($arrParam['filter_alias_dkm_desc']));
		}

		if(!empty($arrParam['filter_dkm_parent'])){
			if($arrParam['filter_dkm_parent'] == 'none')
				$cond->where[] = '(dkm_parent is null or dkm_parent not in (select dkm_uid from default_kamus_master where dkm_active = 1))';
			elseif($arrParam['filter_dkm_parent'] == 'filled')
				$cond->where[] = 'dkm_parent in (select dkm_uid from default_kamus_master where dkm_active = 1)';
		}

		$result = $this->get_attribute($cond,$this->get_table_name('default_kamus_key'),$returnType);

		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxNews => $valNews) {

				// karena right join, ketika id key tidak ada maka ganti dengan 1
				// !!! harus cuma satu data untuk diedit
				if(!empty($arrParam['join_kamus']) && ($arrParam['join_kamus']==TRUE || $arrParam['join_kamus']=='true') && empty($valNews->dkk_id)){
					$result[$idxNews]->dkk_id = 1;
					$result[$idxNews]->dkk_master = $valNews->dkm_id;
				}

				if(isset($arrParam['short_desc']) && ($arrParam['short_desc'] == TRUE || $arrParam['short_desc'] == 'true')){
					$currDesc = $this->additional_function->set_value($valNews,'dkm_desc');
					$result[$idxNews]->dkm_desc = $this->additional_function->get_words_from_html($currDesc,15).' ...';
				}

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valNews,$this->get_field_name($singleInst));
					else
						$instId = $idxNews;

					// clone semua data
					$lastResult[$instId] = $valNews;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valNews,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valNews);
				}else{
					$lastResult[] = $valNews;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function update_key($arrParam,$objCond=NULL,$method){
        if($_SESSION['access_matrix'] != 'sangpetualang'){
        	return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
        }

		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^dkk_/i",strtolower($idxParam))){
					$main[$this->get_field_name($idxParam)] = $valParam;
				}
			}
		}

        $paramCond =  $this->param_as_cond_key($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		// inisiasi return
		$dictLst = array('state'=>FALSE,'msg'=>'belum di proses','data'=>NULL);

		// jika title kosong jgn diisi dulu
		if(isset($main[$this->get_field_name('dkk_key')]) && empty($main[$this->get_field_name('dkk_key')])){
			return array('state'=>FALSE,'msg'=>'key belum ditentukan','data'=>NULL);
		}
		if(isset($main[$this->get_field_name('dkk_master')]) && empty($main[$this->get_field_name('dkk_master')])){
			return array('state'=>FALSE,'msg'=>'topik kamus belum ditentukan','data'=>NULL);
		}

		// check last user
		$cond->field = array('dkk_id','dkk_uid');
		$arrParam['bypass_access'] = TRUE;
		$lastDict = (array) $this->detail_key($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail

		$affected = 0;

		if(empty($main[$this->get_field_name('dkk_updated')])) $main[$this->get_field_name('dkk_updated')] = date('Y-m-d H:i:s');

		if($method == 'update' && !empty($main)){
			if(empty($lastDict)){
				$dictLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			}else{
				$allIds = $this->additional_function->get_array($lastDict,$this->get_field_name('dkk_id'));
				$cond = new stdClass();
				$cond->where_in[] = array(
					'fieldName'=>'dkk_id',
					'fieldValue'=>$allIds
				);
				$affected = $this->set_attribute($main,$this->get_table_name('default_kamus_key'),$method,$cond,TRUE);
				if($affected>0)
					$dictLst = array('state'=>TRUE,'msg'=>'data berhasil diupdate','data'=>$lastDict);
				else
					$dictLst = array('state'=>TRUE,'msg'=>'tidak ada parameter yg diupdate','data'=>$lastDict);
			}
		}elseif($method == 'insert' && !empty($main)){
			// $nextId = $this->get_next_id($this->get_field_name('dkm_id'),$this->get_table_name('default_kamus_key'));
			$nextId = $this->get_next_id2('default_kamus_key');
			$main[$this->get_field_name('dkk_id')] = $nextId;
			if(empty($main[$this->get_field_name('dkk_uid')]))
				$main[$this->get_field_name('dkk_uid')] = $this->get_next_id3();
			$affected = $this->set_attribute($main,$this->get_table_name('default_kamus_key'),$method,$cond);

			if($affected > 0){
				$cond = new stdClass();
				$cond->field = array('dkk_id','dkk_uid');
				$arrParam['bypass_access'] = TRUE;
				$lastDict = (array) $this->detail_key(array('filter_dkk_id'=>$nextId,'bypass_access'=>TRUE),$cond);
				$dictLst = array(
					'state'=>TRUE,
					'msg'=>'data berhasil ditambahkan',
					'data'=>$lastDict
				);

			}else{
				$dictLst = array('state'=>FALSE,'msg'=>'data tidak berhasil diinput','data'=>NULL);
			}
		}elseif(empty($main)){
			$dictLst = array(
				'state'=>FALSE,
				'msg'=>'tidak ada parameter data yang dikirimkan',
				'data'=>$lastDict
			);
		}

		// if(!empty($lastDict)){
		// 	foreach ($lastDict as $idxDict => $valDict){
		// 		$currKeyId = $this->additional_function->set_value($valDict,$this->get_field_name('dkk_id'));
		// 		$this->audit_process->insert_data(
		// 			$currKeyId,
		// 			$this->audit_process->KAMUS,
		// 			$method.' key '.$this->additional_function->set_value($arrParam,'dkk_key'),
		// 			'Val: '.$this->services_json->encode($arrParam)
		// 		);
		// 	}
		// }

		return $dictLst;
	}

	final function remove_key($arrParam,$objCond=NULL){
		if($_SESSION['access_matrix'] != 'sangpetualang'){
			return array('state'=>FALSE,'msg'=>'hanya boleh diakses oleh admin','data'=>NULL);
		}

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond_key($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$lastDict = $this->detail_key($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
		if(empty($lastDict)){
			$dictLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			$affected = $this->remove_attribute('default_kamus_key',$cond,TRUE);
			$dictLst = array('state'=>TRUE,'msg'=>'data berhasil dihapus','data' => $lastDict);
		}

		// if(!empty($lastDict) && is_array($lastDict)){
		// 	foreach ($lastDict as $idxLst => $valLst) {
		// 		$this->audit_process->insert_data(
		// 			$this->additional_function->set_value($valLst,'dkk_id'),
		// 			$this->audit_process->KAMUS,
		// 			'hapus key : '.$this->additional_function->set_value($valLst,'dkk_key'),
		// 			'Val: '.$this->services_json->encode($cond)
		// 		);
		// 	}
		// }

		return $dictLst;
	}	

	final function param_as_cond_key($arrParam=NULL){
		$cond = new stdClass();

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dkk_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_pp_as_string')){
						$cond->where[] = sprintf("%s = '%s'",str_replace(array('filter_dkk_as_string','FILTER_DKK_AS_STRING'),'','dkk'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_dkk_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}

		return $cond;
	}

	function _get_parent($currentId,$level=0){
		$currData = $this->detail(array('filter_dkm_uid'=>$currentId),NULL,$this->SINGLE);
		$parentId = $this->additional_function->set_value($currData,'dkm_parent');
		
		if(!empty($parentId) 
			&& $parentId != $currentId // batasi jgn parent id adalah id sendiri
			&& $level < 10 // batasi jgn sampai terjadi infinite looping terhadap parent id untuk children yg terus memutar
			){
			return $this->_get_parent($parentId,++$level);
		}else{
			return $currentId;
		}
	}

	// batasi cari child sampe level 10
	function _get_child($parentId,$level=0,$parentInclude=TRUE){
		$stageLevel = $level;

		$currChild = $this->detail(array('filter_dkm_parent'=>$parentId,'filter_dkm_active'=>1,'ordid'=>'dkm_title','ordtype'=>'asc','limit'=>FALSE));

		$children = array();
		if(!empty($currChild)){
			++$level;
			foreach ($currChild as $key => $child) {
				$children[$key] = array(
					'id' => $child->dkm_id,
					'uid' => $child->dkm_uid,
					'parent_uid' => $child->dkm_parent,
					'depth' => $level,
					'name' => $child->dkm_title
				);
				if($level < 10){
					$children[$key]['child'] = $this->_get_child($child->dkm_uid,$level,FALSE);
				}
			}
		}

		$result = array();
		if($parentInclude && $stageLevel == 0){ // parent include hanya boleh di level 1
			$currKamus = $this->detail(array('filter_dkm_uid'=>$parentId),NULL,$this->SINGLE);
			$result = array(
				'id' => $currKamus->dkm_id,
				'uid' => $currKamus->dkm_uid,
				'parent_uid' => $currKamus->dkm_parent,
				'depth' => ($level-1),
				'name' => $currKamus->dkm_title,
				'child' => $children
			);
		}else{
			$result = $children;
		}

		return $result;
	}

	function get_hierarchy($currentUid,$parentInclude=FALSE){
		// $currentUid = $this->_get_parent($currentUid); // ga usah cari keatas dulu
		$childrenIds = $this->_get_child($currentUid,0,$parentInclude);
		return $childrenIds;
	}
}
<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Audit_process extends MY_Model{

	function __construct(){

		parent::__construct();

		// index = sa_userid,sa_app
		// index = da2_userid, da2_inst_type+da2_inst_id,da2_app
		// hanya sa_id yg tidak boleh null
		$this->mainTable = array(
			'default_audit', // nama table
			'da2_id(integer)'
		);
	}
	
	// no need to edit
	function insert_data($instId,$instanceType,$detail,$data=NULL){
		$appType = $this->access->get_access('app_type');

		if($this->access->get_access('site_maintenance',1)){
			return FALSE;
		}

		$detail = $this->services_json->encode(str_replace("'","\'",$detail));
		$data = $this->services_json->encode(
												array(
										    		'param'=>$data,
										    		'url'=>$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'],
													'user_ip'=>$this->additional_function->set_value($_SERVER,'REMOTE_ADDR'), // utk cli akan kosong
											    	'computer_name' => @gethostbyaddr($this->additional_function->set_value($_SERVER,'REMOTE_ADDR')), // utk cli akan kosong
											    	'user_agent' => $this->additional_function->set_value($_SERVER,'HTTP_USER_AGENT') // utk cli akan kosong
												)

				);

		$main[$this->get_field_name('da2_id')] = $this->get_next_id2('default_audit');
		$main[$this->get_field_name('da2_userid')] = (isset($_SESSION['profile_userId']) ? $_SESSION['profile_userId'] : NULL);
		$main[$this->get_field_name('da2_inst_id')] = $instId;
		$main[$this->get_field_name('da2_inst_type')] = $instanceType;
		$main[$this->get_field_name('da2_detail')] = $detail;
		$main[$this->get_field_name('da2_action_time_incl')] = date('Y-m-d H:i:s');
		$main[$this->get_field_name('da2_data')] = addslashes($data);
		$main[$this->get_field_name('da2_app')] = $appType[0];

		$result = $this->set_attribute($main,$this->get_field_name('default_audit'),'insert');
		return $result;
	}

	// fungsi ini akan mengambil detail dari produk
	// $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	// $cond = kondisi default mysql

    // $arrInput = array(
	// );
	// $result = $this->get_detail($arrInput,$cond);
	function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'da2_id';

		$limit = $this->additional_function->set_value($arrParam,'limit');
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if($limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(!empty($limit) && ($limit == FALSE || $limit == 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}


		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}


		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}
		$result = $this->get_attribute($cond,$this->get_table_name('default_audit'),$returnType);
		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxNews => $valNews) {
				// update $result here

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valNews,$this->get_field_name($singleInst));
					else
						$instId = $idxNews;

					// clone semua data
					$lastResult[$instId] = $valNews;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valNews,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valNews);
				}else{
					$lastResult[] = $valNews;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function param_as_cond($arrParam=NULL){
		$cond = new stdClass();

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_da2_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_da2_as_string')){
						$cond->where[] = sprintf("%s = '%s'",str_replace(array('filter_da2_as_string','FILTER_DA2_AS_STRING'),'','da2_'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_da2_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}

		return $cond;
	}

}
<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Page_data extends MY_Model{
	function __construct(){
		parent::__construct();
	}
	
	/**
	 * HANYA MENGAKSES DATA YANG ADA DI DATABASE
	 * $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	 * $cond = kondisi default mysql
	 *
	 * $arrParams = array(
	 * 		'limit'=> num or row
	 * 		'offset'=> start row
	 * 		'ordid'=> field yg akan diorder
	 * 		'ordtype'=> jenis order (asc,desc)
	 * 		'filter_pp_*'=> value // filter otomatis sesuai column pada table profile_page
	 *		'bypass_access' => TRUE, // admin bisa bypass akses
	 *		'single_inst' => fieldname, // field sebagai index dari data hasil
	 *		'single_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 *		'multi_inst' => fieldname, // field sebagai index dari data hasil
	 *		'multi_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 * );
	 * $result = $this->get_detail($arrParams,$cond);
	 */
	final function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'pp_id';

		$limit = $this->additional_function->set_value($arrParam,'limit');
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if($limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(!empty($limit) && ($limit == FALSE || $limit == 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}


		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}
		$result = $this->get_attribute($cond,$this->get_table_name('profile_page'),$returnType);
		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			$this->load->specific_model('main_menu');
			foreach ($result as $idxNews => $valNews) {
				// update $result here
				$currSlug = $this->additional_function->set_value($valNews,'pp_slug');
				$valNews->publish = ($this->model_main_menu->is_active($currSlug)?1:0);

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valNews,$this->get_field_name($singleInst));
					else
						$instId = $idxNews;

					// clone semua data
					$lastResult[$instId] = $valNews;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valNews,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valNews);
				}else{
					$lastResult[] = $valNews;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function update($arrParam,$objCond=NULL,$method){
		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^pp_/i",strtolower($idxParam))){
					$main[$this->get_field_name($idxParam)] = $valParam;
				}
			}
		}
		if(!empty($main[$this->get_field_name('pp_title')])) $main[$this->get_field_name('pp_slug')] = str_replace('-','_',$this->additional_function->get_slug($main[$this->get_field_name('pp_title')]));

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		// inisiasi return
		$newsList = array('state'=>FALSE,'msg'=>'belum di proses','data'=>NULL);
		if(empty($_SESSION['access_matrix_list'])){
			return $newsList;
		}else if(!in_array('sangpetualang',$_SESSION['access_matrix_list']) && !in_array('owner profile',$_SESSION['access_matrix_list'])){
			return $newsList;
		}else if(in_array('owner profile',$_SESSION['access_matrix_list'])){
			$sessMerchant = $_SESSION['profile_merchant'];
			$currMerchant = $this->additional_function->set_value($main,$this->get_field_name('pp_merchant'));

			if(empty($sessMerchant)) return array('state'=>FALSE,'msg'=>'anda belum memiliki merchant','data'=>NULL);
			elseif(!empty($currMerchant) && in_array($currMerchant,$sessMerchant)) $main[$this->get_field_name('pp_merchant')] = $currMerchant;
			elseif(!empty($sessMerchant)) $main[$this->get_field_name('pp_merchant')] = $sessMerchant[0];
		}

		// jika title kosong jgn diisi dulu
		if($method == 'insert' && empty($main[$this->get_field_name('pp_title')])){
			return array('state'=>FALSE,'msg'=>'judul halaman belum diisi','data'=>NULL);
		}

		// check last user
		$cond->field = array('pp_id');
		$arrParam['bypass_access'] = TRUE;
		$lastNews = (array) $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail

		$affected = 0;

		if($method == 'update' && !empty($main)){
			if(empty($lastNews)){
				$pageLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			}else{
				$allIds = $this->additional_function->get_array($lastNews,$this->get_field_name('pp_id'));
				$cond = new stdClass();
				$cond->where_in[] = array(
					'fieldName'=>'pp_id',
					'fieldValue'=>$allIds
				);
				$affected = $this->set_attribute($main,$this->get_table_name('profile_page'),$method,$cond,TRUE);
				if($affected>0)
					$pageLst = array('state'=>TRUE,'msg'=>'data berhasil diupdate','data'=>$lastNews);
				else
					$pageLst = array('state'=>TRUE,'msg'=>'tidak ada parameter yg diupdate','data'=>$lastNews);
			}
		}elseif($method == 'insert' && !empty($main)){
			// $nextId = $this->get_next_id($this->get_field_name('pp_id'),$this->get_table_name('profile_page'));
			$nextId = $this->get_next_id2('profile_page');
			$main[$this->get_field_name('pp_id')] = $nextId;
			$main['pp_postdate'] = date('Y-m-d H:i:s');
			$affected = $this->set_attribute($main,$this->get_table_name('profile_page'),$method,$cond);

			if($affected > 0){
				$cond = new stdClass();
				$cond->field = array('pp_id');
				$arrParam['bypass_access'] = TRUE;
				$lastNews = (array) $this->get_detail(array('filter_pp_id'=>$nextId,'bypass_access'=>TRUE),$cond);
				$pageLst = array(
					'state'=>TRUE,
					'msg'=>'data berhasil ditambahkan',
					'data'=>$lastNews
				);

			}else{
				$pageLst = array('state'=>FALSE,'msg'=>'data tidak berhasil diinput','data'=>NULL);
			}
		}elseif(empty($main)){
			$pageLst = array(
				'state'=>FALSE,
				'msg'=>'tidak ada parameter data yang dikirimkan',
				'data'=>$lastNews
			);
		}

		$this->audit_process->insert_data(
			$this->additional_function->set_value($arrParam,$this->get_field_name('filter_pp_id')),
			$this->audit_process->PAGE,
			$method.' page '.$this->additional_function->set_value($arrParam,$this->get_field_name('pp_title')),
			'Val: '.$this->services_json->encode($arrParam)
		);
		return $pageLst;
	}

	final function remove($arrParam,$objCond=NULL){
		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$lastPage = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
		if(empty($pageLst)){
			$pageLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			$affected = $this->remove_attribute('profile_page',$cond,TRUE);
			$pageLst = array('state'=>TRUE,'msg'=>'data berhasil dihapus','data' => $pageLst);
		}

		if(!empty($lastPage) && is_array($lastPage)){
			foreach ($lastPage as $idxLst => $valLst) {
				$this->audit_process->insert_data(
					$this->additional_function->set_value($valLst,'pp_id'),
					$this->audit_process->PAGE,
					'hapus page : '.$this->additional_function->set_value($valLst,'pp_title'),
					'Val: '.$this->services_json->encode($cond)
				);
			}
		}

		return $pageLst;
	}	

	final function param_as_cond($arrParam=NULL){
		$cond = new stdClass();

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_pp_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_pp_as_string')){
						$cond->where[] = sprintf("%s = '%s'",str_replace(array('filter_pp_as_string','FILTER_PP_AS_STRING'),'','pp'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_pp_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}

		return $cond;
	}

}
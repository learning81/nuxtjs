<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Column_data extends MY_Model{
	function __construct(){
		parent::__construct();

		// unik = 
		$this->mainTable = array(
			'default_column', // nama table
			'dc_id(integer)',
			'dc_table(varchar-150)', // nama column pada table master
			'dc_slug(varchar-150)', // nama column pada table master, sebutannya column
			'dc_title(varchar-150)', // sebutannya field
			'dc_type(enum(text,textarea,select,checkbox,radio))', // jika type individual (text,textarea) bukan multiple (checkbox,radio) default harus individual
			'dc_multiple(char-1)', // jika berisi option apakah bisa diinput lebih dari 1 pilihan
			'dc_default(text)', // json (local value) atau string('dictionary:','') (dictionary data)
			'dc_show(char-1)',
			'dc_width(mediumint)',
			'dc_t_type(char,varchar,date,text,int,mediumint)',
			'dc_t_width(mediumint)',
			'dc_order(mediumint)',
			'dc_filter(varchar-150)',
			'dc_protected(char-1)'
		);
	}
	
	/**
	 * HANYA MENGAKSES DATA YANG ADA DI DATABASE
	 * $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	 * $cond = kondisi default mysql
	 *
	 * $arrParams = array(
	 * 		'limit'=> num or row
	 * 		'offset'=> start row
	 * 		'ordid'=> field yg akan diorder
	 * 		'ordtype'=> jenis order (asc,desc)
	 * 		'filter_dc_*'=> value // filter otomatis sesuai column pada table default_column
	 *		'bypass_access' => TRUE, // admin bisa bypass akses
	 *		'single_inst' => fieldname, // field sebagai index dari data hasil
	 *		'single_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 *		'multi_inst' => fieldname, // field sebagai index dari data hasil
	 *		'multi_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 * );
	 * $result = $this->get_detail($arrParams,$cond);
	 */
	final function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'dc_id';

		$limit = $this->additional_function->set_value($arrParam,'limit');
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if($limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(!empty($limit) && ($limit == FALSE || $limit == 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}

		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}
		$result = $this->get_attribute($cond,$this->get_table_name('default_column'),$returnType);
		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxNews => $valNews) {
				// update $result here

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valNews,$this->get_field_name($singleInst));
					else
						$instId = $idxNews;

					// clone semua data
					$lastResult[$instId] = $valNews;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valNews,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valNews);
				}else{
					$lastResult[] = $valNews;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function update($arrParam,$objCond=NULL,$method){
		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^dc_/i",strtolower($idxParam))){
					$main[$this->get_field_name($idxParam)] = $valParam;
				}
			}
		}

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		if($method == 'update'){
			$cond->field = array('dc_id','dc_slug','dc_title');
			$newsList = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
			if(empty($newsList)){
				$newsList = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			}else{
				$affected = $this->set_attribute($main,$this->get_table_name('default_column'),$method,$cond,TRUE);
				$newsList = array('state'=>TRUE,'msg'=>'data berhasil diperbaharui','data' => $newsList);
			}
		}elseif($method == 'insert'){
			$nextId = $this->get_next_id($this->get_field_name('dc_id'),'default_column');
			$main[$this->get_field_name('dc_id')] = $nextId;
			$affected = $this->set_attribute($main,$this->get_table_name('default_column'),$method,$cond);

			if($affected > 0){
				$lastNews = $this->get_detail(array('filter_dc_id'=>$nextId),NULL,$this->SINGLE);
				$newsList = array(
					'state'=>TRUE,
					'msg'=>'data berhasil ditambahkan',
					'data'=>$lastNews
				);
			}
		}

		$this->audit_process->insert_data(
			$this->additional_function->set_value($arrParam,$this->get_field_name('filter_dc_id')),
			$this->audit_process->BERITA,
			$method.' column '.$this->additional_function->set_value($arrParam,$this->get_field_name('dc_title')),
			'Val: '.$this->services_json->encode($arrParam)
		);
		return $newsList;
	}

	final function remove($arrParam,$objCond=NULL){
		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$newsList = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
		if(empty($newsList)){
			$newsList = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			$affected = $this->remove_attribute('default_column',$cond,TRUE);
			$newsList = array('state'=>TRUE,'msg'=>'data berhasil dihapus','data' => $newsList);
		}

		return $newsList;
	}	

	final function param_as_cond($arrParam=NULL){
		$cond = new stdClass();

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dc_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_dc_as_string')){
						$cond->where[] = sprintf("%s = '%s'",str_replace(array('filter_dc_as_string','FILTER_DC_AS_STRING'),'','pb'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_dc_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}

		return $cond;
	}

	// mendapatkan semua field dari table
	function get_all_field($tableName=NULL,$filter=NULL){

		// din field
		$dinField = $this->get_din_field($tableName,$filter);
		$oldDinField = $this->get_din_field($tableName);
		$dinFieldStr = $this->additional_function->array_to_string($oldDinField,"','",TRUE);

		// hapus din field dari fixed field
		$cond = new stdClass();
		$cond->query = sprintf("SHOW COLUMNS FROM %s where Field not in ('%s')",$this->get_table_name($tableName),$dinFieldStr);
		$result = $this->get_detail(NULL,$cond);
		$fixedField = $this->additional_function->get_array($result,'Field');

		// tambahkan kembali din field
		if(!empty($dinField)){
			foreach ($dinField as $idxField => $valField) {
				array_push($fixedField,$valField);
			}
		}

		return $fixedField;
	}

	// mendapatkan semua hanya dinamic field dari table
	// jika tidak ada filter maka keluarkan semuanya
	// jika ada filter hanya keluarkan yang sesuai
	// $order(1=dc_id,2=dc_order)
	function get_din_field($tableName=NULL,$filter=NULL,$order=1){
		$cond = new stdClass();
		$cond->field = 'dc_slug';
		if($order==1) $cond->order = array('id'=>$this->get_field_name('dc_id'));
		elseif($order==2) $cond->order = array('id'=>$this->get_field_name('dc_order'));
		if(!empty($filter)){
			$cond->where[] = sprintf('(%s in (\'%s\') OR %s is null OR %s = \'\')',
											$this->get_field_name('dc_filter'),
											$this->additional_function->array_to_string($filter,"','",TRUE),
											$this->get_field_name('dc_filter'),
											$this->get_field_name('dc_filter')
										);
		}
		$result = $this->get_detail(array('filter_dc_table'=>$tableName),$cond);
		$allResult = $this->additional_function->get_array($result,$this->get_field_name('dc_slug'));

		$cond = new stdClass();
		$cond->query = sprintf("SHOW COLUMNS FROM %s",$this->get_table_name($tableName));
		$result = $this->get_detail(NULL,$cond);
		$fixedField = $this->additional_function->get_array($result,'Field');

		$currResult = array();
		if(!empty($allResult)){
			foreach ($allResult as $idxResult => $valResult) {
				if(in_array($valResult,$fixedField))
					array_push($currResult,$valResult);
			}
		}

		return (!empty($currResult)?$currResult:NULL);
	}

	// mendapatkan attribute dari colom tertentu pada table colomn
	function get_attr_field($tableName=NULL,$colName=NULL){
		$cond = new stdClass();
		$cond->field = 'dc_slug';
		$cond->order = array('id'=>$this->get_field_name('dc_order'));
		$result = $this->get_detail(array('filter_dc_table'=>$tableName,'filter_dc_slug'=>$colName),NULL,$this->SINGLE);
		$fieldDefault = $this->get_field_name('dc_default');
		$default = $this->additional_function->set_value($result,$fieldDefault);
		$currType = $this->additional_function->set_value($result,$this->get_field_name('dc_type'));

		// parsing default
		// 1. jika string : ('dictionary_','')
		// 2. jika array : jadi pilihan untuk multiple
		$default = $this->additional_function->set_value($result,$fieldDefault);
		$default = $this->services_json->decode($default);

		// rebuild
		if(!empty($result->{$fieldDefault}))
			$result->{$fieldDefault} = $default;

		return $result;
	}
}
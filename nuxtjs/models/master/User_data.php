<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */

// session_list untuk memanfaatkan fitur yang hanya bisa dipakai oleh session tertentu
// child session digunakan pada saat query user agar selalu terinclude
// app_session digunakan untuk multiple user login, beda id user login

// usahakan setiap session dipakai di user_data

class User_data extends MY_Model{
	function __construct(){
		parent::__construct();
	}
	
	function set_user_session($username,$password,$bypassExist=FALSE){
		//$this->remove_session(); ini untuk apa? apa di crm ada? karena bakal hapus session profile session

		/*TRACKING LOGIN*/
		$userId = (!empty($_SESSION['profile_userId'])?$_SESSION['profile_userId']:NULL);
		$logFile = config_item('data_folder').date('Ymd').'/LOGIN_['.TRACECODE.']_['.$userId.'].log';
		$this->additional_function->create_path(config_item('data_folder').date('Ymd').'/');

		if(!file_exists($logFile)){
			file_put_contents($logFile,"/* tracking user login */\n");
			file_put_contents($logFile,"/* tercatat di model::user_data */\n",FILE_APPEND);
			file_put_contents($logFile,"/* harusnya di production di disable */\n",FILE_APPEND);
			file_put_contents($logFile,"\n",FILE_APPEND);
			file_put_contents($logFile,"\n",FILE_APPEND);
		}

		$dataProcess = "=========================================================================================";
		$dataProcess .= "User Mencoba Login\n";
		$dataProcess .= "=========================================================================================";
		$dataProcess .= "\n";
		$dataProcess .= "user_ip : ".$username;
		$dataProcess .= "\n";
		$dataProcess .= "password : ".$password;
		$dataProcess .= "\n";
		$dataProcess .= "tracecode : ".TRACECODE; // utk cli akan kosong
		$dataProcess .= "\n";
		$dataProcess .= "date time : ".date('Y-m-d_H-i-s'); // utk cli akan kosong
		$dataProcess .= "\n\n";
		file_put_contents($logFile, $dataProcess,FILE_APPEND);

		$encPassword = $this->crypt->encrypt($password);
		if($username=='admin' && $encPassword=='370c1e40403102081d0a045a0777'){
			$_SESSION['access_matrix'] = 'sangpetualang';
			$_SESSION['access_matrix_list'] = array('sangpetualang');

			$cond = new stdClass();
			$cond->field = 'distinct da_access_name';
			$cond->where[] = array('da_rule_access' => 'unique_name');
			$allAccess = $this->access->get_detail(array('bypass_access'=>TRUE),$cond);
			$allAccessName = $this->additional_function->get_array($allAccess,'da_access_name');

			if(!empty($allAccessName)){
				foreach ($allAccessName as $accessName) {
					array_push($_SESSION['access_matrix_list'],$accessName);
				}
			}

			$userId = 1;
			$name = 'admin';
		}elseif($user = $this->_check_login_from_local($username,$password)){
			$_SESSION['access_matrix'] = $user->access_name[0];
			$_SESSION['access_matrix_list'] = $user->access_name;
			$userId = $this->additional_function->set_value($user,'agent_id');

			// jika user login dari local tampbahkan local_staff pada $_SESSION['access_matrix_list']
			if(!empty($userId) && !in_array('local_staff',$_SESSION['access_matrix_list']))
				array_push($_SESSION['access_matrix_list'],'local_staff');

			$userDetail = $this->get_detail(array('filter_du_id'=>$userId,'filter_du_active'=>TRUE,'bypass_instance'=>TRUE),NULL,$this->SINGLE,TRUE);
			$name = $this->additional_function->set_value($userDetail,'du_name');
		}

		if(!empty($_SESSION['access_matrix']))
			$_SESSION['profile_userId'] = $userId;

		// catat user login ke table external
		$this->audit_process->insert_data(
			0,
			0,
			'user mencoba login',
			$this->services_json->encode(
				array(
					'login' => $username/*,
					'pswd' => $password*/
				)
			)
		);

		// set session
		if(!empty($_SESSION['access_matrix'])){
			$_SESSION['profile_update_password'] = FALSE;
			$_SESSION['profile_login'] = $username; // master agent utk relasi ke ext database
			$_SESSION['profile_name'] = $name; // master agent utk relasi ke ext database
			$_SESSION['profile_userId'] = $userId; // master agent utk relasi ke ext database
			$this->filecache->init($this->additional_function->set_value($_SESSION,'profile_userId'));

			// ambil semua hak akses anak			
			$allUnique = $this->access->get_access('child_unique');
			$cond = new stdClass();
			$cond->where_in[] = array(
				'fieldName' => 'da_rule_value',
				'fieldValue' => $allUnique
			);
			$cond->where[] = array('da_rule_access' => 'unique_name');
			$allAccess = $this->access->get_detail(NULL,$cond);
			$extraAcc = $this->additional_function->get_array($allAccess,'da_access_name');

			$_SESSION['access_matrix_list'] = $this->additional_function->array_merge(array($_SESSION['access_matrix_list'],$extraAcc));

			$dataProcess = "=========================================================================================";
			$dataProcess .= "User berhasil Login\n";
			$dataProcess .= "=========================================================================================";
			$dataProcess .= "\n";
			$dataProcess .= "session: ".print_r($_SESSION,TRUE);
			$dataProcess .= "\n\n";
			file_put_contents($logFile, $dataProcess,FILE_APPEND);

			// check user yg sedang login
			$userId = $this->filecache->cache_read('user_id','session');

			if(empty($userId) || !in_array($_SESSION['profile_userId'],$_SESSION['profile_session'])){
				// jika belum ada di $_SESSION['profile_session'], lalu tambahkan
				$_SESSION['profile_session'][$_SESSION['profile_userId']] = array(
					'profile_userId' => $_SESSION['profile_userId'],
					'profile_login' => $_SESSION['profile_login'],
					'profile_name' => $_SESSION['profile_name'],
					'access_matrix' => $_SESSION['access_matrix'],
					'access_matrix_list' => $_SESSION['access_matrix_list']
				);

				$this->save_session_cache();

				return array('status'=>TRUE);
			}elseif(in_array($_SESSION['profile_userId'],$_SESSION['profile_session'])){
				$this->switch_session($userId);
				return array('status'=>TRUE);
			}elseif(!$bypassExist){
				$this->user_data->remove_session();
				// unset($_SESSION['profile_userId']);
				// unset($_SESSION['access_matrix']);
				return array('status'=>FALSE,'msg'=>'<div id="error" style="color:red;">User ini sedang aktif di System !!!<br />Silahkan login kembali</div>');
			}else{
				echo json_encode(array('state'=>FALSE,'msg'=>'login status belum ditentukan'));exit;
			}
		}else{
			$this->audit_process->insert_data(
				0,
				0,
				'user gagal login',
				$this->services_json->encode(
					array(
						'username' => $username,
						'password' => $password
					)
				)
			);

			// status user login ke table external gagal login karena username password salah
			return array('status'=>FALSE,'msg'=>'<div id="error" style="color:red;">wrong username/password !!!<br /></div>');
		}

	}

	// buat user temporary
	function set_temporary_user($phone=NULL,$email=NULL){
		$result = array('state'=>FALSE,'msg'=>'belum diproses');
		if(empty($phone) && empty($email)){
			$result = array('state'=>FALSE,'msg'=>'tidak ada identitas yang disertakan');
		}else{
			$this->load->specific_model('user');
			$cond = new stdClass();
			$cond->where[] = sprintf("lower(%s) like '%%%s%%' OR lower(%s) like ",'du_hp1',$phone,'du_email',$email);
			$userDetail = $this->model_user->get_detail(NULL,NULL,$this->model_user->SINGLE,TRUE);
			$currUserId = $this->additional_function->set_value($userDetail,'du_id');
			if(empty($currUserId)){
				$data = array('du_hp1'=>$phone,'du_email'=>$email,'du_active'=>0,'du_pswd'=>'','du_name'=>'anonymouse');
				$userDetail = $this->user_data->update($data,NULL,'insert');
				$currUserId = $this->additional_function->set_value($userDetail[0],'du_id');
			}

			// login sementara user karena belum didaftarkan
			if(!empty($currUserId)){
				$_SESSION['profile_userId'] = $currUserId;
				$_SESSION['access_matrix'] = 'temporary_user';
				$result = array('state'=>TRUE,'msg'=>'user temporary telah dibuat','du_id'=>$currUserId);
			}else{
				$result = array('state'=>FALSE,'msg'=>'gagal buat user temporary');
			}
		}
		return $result;
	}

	/**
	 * UNTUK FORMAT RESTFULL
	 * HANYA DIJALAN KAN KETIKA SUDAH MEMILIKI SESSION LOGIN
	 * $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	 * $where = string kondisi tambahan yang ingin ditambahkan (harus string)
	 * terdiri atas 2 tahap
	 * 1. mencari semua id yang dicari dari default_user
	 * 2. mencari berdasarakan query user di _get_agent_query (objek $cond diabaikan)
	 *
	 * $arrParam = array(
	 *
	 *     'field' => tidak diproses !!!
	 *     'filter_*_id' => semua instance id
	 *
	 *     'order_by' => fieldname, field order pada method _get_agent_query
	 *     'order_type' => ordertype(asc/desc), jenis order yang ingin dilakukan
	 *     'limit' => (int)limit, batasan jumlah data
	 *     'offset' => (int)offset, start urutan data
	 * );
	 * $result = $this->get_detail($arrParams,$cond);
	 */
	final function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2,$initiate_bypass=FALSE){
		$cond = $this->additional_function->clone_obj($objCond);

		$singleState = FALSE;

		// memaksa user tertentu
		/*
		// check apakah bisa bypass user access (allowerd user ids)
		// jika admin bisa bypass akses
		$allowedUserIds = $this->filecache->cache_read('allUser2','user');
		if(isset($_SESSION['restfull_accessName']) 
			&& $_SESSION['restfull_accessName'] == 'sangpetualang' 
			&& isset($arrParam['bypass_access']) 
			&& ($arrParam['bypass_access'] === TRUE || $arrParam['bypass_access'] === 'true')
		){
			// nothing
		}else{
			// check apakah bisa bypass user access (allowerd user ids)
			$accBypassUser = $this->access->get_access('bypass_user');
			$accBypassUserVal = $this->additional_function->set_value($accBypassUser,0);

			if($accBypassUserVal != 1 && isset($arrParam['filter_du_id'])){
			 	$arrParam['filter_du_id'] = $this->additional_function->string_to_array($arrParam['filter_du_id']);
			 	$arrParam['filter_du_id'] = $this->additional_function->array_intersect(array($arrParam['filter_du_id'],$allowedUserIds));
			}elseif($accBypassUserVal != 1){
				$arrParam['filter_du_id'] = (empty($allowedUserIds)?array(''):$allowedUserIds);
			}
		}
		*/

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		if(array_key_exists('access_matrix',$_SESSION) && $_SESSION['access_matrix'] == 'sangpetualang'
		&& array_key_exists('bypass_access',$arrParam) && ($arrParam['bypass_access'] ==  TRUE || $arrParam['bypass_access'] ==  'true')){
			// bypass
		}elseif($initiate_bypass){
			// hanya untuk login jadi kondisi pembatasan si bypass
		}else{
			$arrIds = array();
			$currUnique = $this->access->get_access('unique_name');
			$allUniqueAccess = $this->access->get_access('child_unique');
			$allUniqueAccess = $this->additional_function->array_merge(array($currUnique,$allUniqueAccess));

			if(@in_array('owner',$allUniqueAccess)){
				$currAccess = $this->access->get_access_by_unique('owner','user_role_member');
				$role = $this->additional_function->set_value($currAccess,$this->access->get_field_name('da_rule_value'));
				$roleIds = $this->additional_function->set_value($role,'da_rule_value');
				$arrIds = $this->additional_function->array_merge(array($arrIds,array($roleIds)));
			}
			if(@in_array('customer',$allUniqueAccess)){
				$currAccess = $this->access->get_access_by_unique('customer','user_role_member');
				$role = $this->additional_function->set_value($currAccess,$this->access->get_field_name('da_rule_value'));
				$roleIds = $this->additional_function->set_value($role,'da_rule_value');
				$arrIds = $this->additional_function->array_merge(array($arrIds,array($roleIds)));
			}
			
			$cond->join[] = array(
				'refTable' => 'default_access c',
				'cond' => "((c.da_rule_access = 'user_role_member' and default_user.du_role = c.da_rule_value) or
							(c.da_rule_access = 'user_role_member' and default_user.du_role like CONCAT(c.da_rule_value,';%')) or
							(c.da_rule_access = 'user_role_member' and default_user.du_role like CONCAT(CONCAT('%;',c.da_rule_value),';%')) or
							(c.da_rule_access = 'user_role_member' and default_user.du_role like CONCAT('%;',c.da_rule_value)))",
				'direction' => 'left'
			);

			$cond->join[] = array(
				'refTable' => 'default_access d',
				'cond' => "d.da_access_name = c.da_access_name and d.da_rule_access = 'user_role_member'",
				'direction' => 'left'
			);

			$cond->where_in[] = array(
				'fieldName'=>'d.da_rule_value',
				'fieldValue'=>(!empty($arrIds)?$arrIds:array(''))
			);
		}

		$this->load->specific_module('user');
		$allDinField = $this->module_user->get_din_field();
		$allDinField = (!empty($allDinField['allField'])?$allDinField['allField']:NULL);

		if(!empty($arrParam['field'])) $searchField = $this->additional_function->string_to_array($arrParam['field']);
		else $searchField = NULL;

 		// wajib ada
		$allowedField = array('du_id','du_name','du_email','du_facebook','du_role','du_leader','du_last_login');

		if(!empty($searchField)){
			$newField = $this->additional_function->array_minus($searchField,$allowedField);
		}elseif(!empty($allDinField)){
			$newField = $this->additional_function->array_minus($allDinField,$allowedField);
		}

		$cond->field = 'distinct '. implode(',',$allowedField) . (!empty($newField)?','.implode(',',$newField):NULL);

		// "ordid":"-du_name desc"
		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'du_id';

		$limit = (isset($arrParam['limit'])?$arrParam['limit']:TRUE);
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if(is_numeric($limit) && $limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(isset($limit) && ($limit === FALSE || $limit === 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}


		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($multiInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}
		$result = $this->get_attribute($cond,$this->get_table_name('default_user'),$returnType);

		if(!empty($arrParam) && array_key_exists('translate',$arrParam) && ($arrParam['translate'] ==  TRUE || $arrParam['translate'] ==  'true')){
			$this->load->specific_module('user');
			$allDinField = $this->module_user->get_din_field();
			$allDinField = (is_array($allDinField['allField'])?$allDinField['allField']:NULL);
		}

		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxData => $valData) {
				// update $result here
				if(!empty($arrParam) && array_key_exists('translate',$arrParam) && ($arrParam['translate'] ==  TRUE || $arrParam['translate'] ==  'true')){
					if(!empty($allDinField)){
						foreach ($allDinField as $idxDin => $valDin) {
							$currVal = $this->additional_function->set_value($valData,$valDin);

							$attrField = $this->module_user->get_attr_field(array('column',$valDin));
							$attrDefault = (!empty($attrField['detailField'])?$attrField['detailField']->{$this->get_field_name('dc_default')}:NULL);
							$attrType = (!empty($attrField['detailField'])?$attrField['detailField']->{$this->get_field_name('dc_type')}:NULL);

							if(!empty($attrDefault) && ($attrType == 'select' || $attrType == 'checkbox' || $attrType == 'radio')){
								$partVal = $this->additional_function->string_to_array($currVal,';');
								$newVal = array();
								foreach ($partVal as $idxVal => $value) {
									array_push($newVal,$this->additional_function->set_value($attrDefault,$value));
								}
								$result[$idxData]->{$valDin} = $this->additional_function->array_to_string($newVal,'|',TRUE);
							}
						}
					}
				}

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) && !$singleState){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valData,$this->get_field_name($singleInst));
					else
						$instId = $idxData;

					// clone semua data
					$lastResult[$instId] = $valData;
				}elseif(!empty($multiInst) && !$singleState){	
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valData,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valData);
				}else{
					$lastResult[] = $valData;
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil itmem pertama
		if($singleState && !empty($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	// update hanya untuk default_user di local
	final function update($arrParam=NULL,$objCond=NULL,$method){
		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		$main = array();
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^du_/i",strtolower($idxParam))){
					if(is_array($valParam)){
						$currVal = $this->additional_function->array_to_string($valParam,';',TRUE);
						$main[$this->get_field_name($idxParam)] = $currVal;
					}
					else
						$main[$this->get_field_name($idxParam)] = (is_array($valParam)?implode(';',$valParam):$valParam);
				}
			}
		}

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		// check apakah ada data untuk password
		if(empty($arrParam['du_pswd'])){
			if(array_key_exists('du_pswd',$arrParam)) $main[$this->get_field_name('du_pswd')] = $this->additional_function->set_hash('default','password');
			else unset($main[$this->get_field_name('du_pswd')]);
		}else{
			$main[$this->get_field_name('du_pswd')] = $this->additional_function->set_hash($arrParam['du_pswd'],'password');
		}
		
		// inisiasi return
		$userLst = array('state'=>FALSE,'msg'=>'belum di proses','data'=>NULL);

		// check last user
		$cond->field = array('du_id','du_name','du_email');
		$arrParam['bypass_access'] = TRUE;
		$lastUser = (array) $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail

		if($method == 'update' && !empty($main)){
			if(empty($lastUser)){
				$userLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			}else{
				$allIds = $this->additional_function->get_array($lastUser,$this->get_field_name('du_id'));
				$cond = new stdClass();
				$cond->where_in[] = array(
					'fieldName'=>'du_id',
					'fieldValue'=>$allIds
				);
				$affected = $this->set_attribute($main,$this->get_table_name('default_user'),$method,$cond,TRUE);
				if($affected>0)
					$userLst = array('state'=>TRUE,'msg'=>'data berhasil diupdate','data'=>$lastUser);
				else
					$userLst = array('state'=>TRUE,'msg'=>'tidak ada parameter yg diupdate','data'=>$lastUser);
			}
		}elseif($method == 'insert' && !empty($main)){
			if(empty($arrParam['du_pswd'])) $main[$this->get_field_name('du_pswd')] = $this->additional_function->set_hash('default','password');
			
			// $nextId = $this->get_next_id($this->get_field_name('du_id'),'default_user');
			$nextId = $this->get_next_id2('default_user');
			$main[$this->get_field_name('du_id')] = $nextId;
			$affected = $this->set_attribute($main,$this->get_table_name('default_user'),$method,$cond);

			if($affected > 0){
				$cond = new stdClass();
				$cond->field = array('du_id','du_name','du_email');
				$arrParam['bypass_access'] = TRUE;
				$lastUser = (array) $this->get_detail(array('filter_du_id'=>$nextId,'bypass_access'=>TRUE),NULL,$this->MULTIPLE,TRUE);
				$userLst = array(
					'state'=>TRUE,
					'msg'=>'data berhasil ditambahkan',
					'data'=>$lastUser
				);
			}else{
				$userLst = array('state'=>FALSE,'msg'=>'data tidak berhasil diinput','data'=>NULL);
			}
		}elseif(empty($main)){
			$userLst = array(
				'state'=>FALSE,
				'msg'=>'tidak ada parameter data yang dikirimkan',
				'data'=>$lastUser
			);
		}

		if(!empty($lastUser)){
			foreach ($lastUser as $idxUser => $valUser){
				$currUserId = $this->additional_function->set_value($valUser,$this->get_field_name('du_id'));
				$this->audit_process->insert_data(
					$currUserId,
					$this->audit_process->USER,
					$method.' user '.$this->additional_function->set_value($arrParam,'du_name'),
					'Val: '.$this->services_json->encode($arrParam)
				);
			}
		}

		return $userLst;
	}

	// remove hanya pada default_user local, jadi bisa $cond = class
	final function remove($arrParam=NULL,$objCond=NULL){
		$cond = $this->additional_function->clone_obj($objCond);

		$paramCond =  $this->param_as_cond($arrParam);
		$cond = $this->additional_function->object_merge($cond,$paramCond);

		$lastUser = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
		if(empty($lastUser)){
			$userLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			$affected = $this->remove_attribute('default_user',$cond,TRUE);
			$userLst = array('state'=>TRUE,'msg'=>'data berhasil dihapus','data' => $lastUser);
		}

		if(!empty($lastUser) && is_array($lastUser)){
			foreach ($lastUser as $idxLst => $valLst) {
				$this->audit_process->insert_data(
					$this->additional_function->set_value($valLst,'du_id'),
					$this->audit_process->USER,
					'hapus user : '.$this->additional_function->set_value($valLst,'du_id'),
					'Val: '.$this->services_json->encode($cond)
				);
			}
		}

		return $userLst;
	}

	final function param_as_cond($arrParam=NULL){
		$cond = new stdClass();
		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_du_')){
					if(is_string($valParam) && strtolower($valParam) == 'none'){
						$cond->where[] = sprintf('%s is NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && strtolower($valParam) == 'filled'){
						$cond->where[] = sprintf('%s IS NOT NULL',str_replace(array('filter_','FILTER_'),'',$idxParam));
					}elseif(is_string($valParam) && FALSE!==strpos(strtolower($idxParam),'filter_du_as_string')){
						$cond->where[] = sprintf("lower(%s) like lower('%%%s%%')",str_replace(array('filter_du_as_string','FILTER_DU_AS_STRING'),'','du'.$idxParam),$valParam);
					}elseif(is_numeric($valParam)){
						$cond->where[] = sprintf('%s = %s',str_replace(array('filter_','FILTER_'),'',$idxParam),$valParam);
					}else{
						$cond->where_in[] = array(
							"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
							"fieldValue"=>$valParam
						);
					}
				}
			}
		}

		// check jika ada filter dari restful API
		// valuenya harus selalu single !!!
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_alias_du_')){
					$cond->where[] = sprintf("lower(%s) like '%%%s%%'",str_replace(array('filter_alias_','FILTER_ALIAS_'),'',$idxParam),strtolower($valParam));
				}
			}
		}
		return $cond;
	}

	// sebenarnya $apptype ini bernilai auto
	// tapi tetap bisa diisikan untuk future plan mungkin ada akses user yang beda apptype
	// contoh kasus admin ingin melihat user list beda apptype
	// untuk saat ini belum ada yang make $apptype

	// untuk cek akses selain di dalam method ini:
	/*
			if(
				$this->additional_function->set_value($this->user_data,'test') && 
				$this->access->get_access('app_type',$this->additional_function->set_value($this->user_data,'test'))
			)	
	*/
	// disini:
	/*
		if($appType == $this->additional_function->set_value($this,'agency'))
	*/
	/**
	 * utk $arrUserIds harus bernilai senilai pencarian
	 * khusus utk admin yg ingin mencari semua user bisa memakai param 'all'
	 */
	private function _check_login_from_local($username,$password){
		$username = trim(strtolower($username));

		$hashedPswd = $this->additional_function->set_hash($password,'password');
		$sql = "
			select 
			a.du_id as \"user_id\",a.du_name as \"username\",a.du_role as \"user_role_id\",
			c.da_access_name as \"access_name\"
			from default_user a
			left join default_access c
				on (
					(c.da_rule_access = 'user_role_member' and a.du_role = c.da_rule_value) or
					(c.da_rule_access = 'user_role_member' and a.du_role like CONCAT(c.da_rule_value,';%')) or
					(c.da_rule_access = 'user_role_member' and a.du_role like CONCAT(CONCAT('%;',c.da_rule_value),';%')) or
					(c.da_rule_access = 'user_role_member' and a.du_role like CONCAT('%;',c.da_rule_value))
					)
			where lower(a.du_email) = '$username' and (a.du_pswd = '$hashedPswd' or a.du_facebook = '$password')
			/*where lower(a.du_name) = 'admin' and (a.du_pswd = 'e3274be5c857fb42ab72d786e281b4b8' or a.du_facebook = 'e3274be5c857fb42ab72d786e281b4b8')*/
		";

		$cond = new stdClass();
		$cond->query = $sql;
		$data = $this->get_attribute($cond,NULL,$this->MULTIPLE);
		$user = NULL;
		if(!empty($data)){
			$user = new stdClass();
			$user->id = $this->additional_function->set_value($data[0],'user_id','string');
			$user->name = $this->additional_function->set_value($data[0],'username','string');
			$user->agent_id = urlencode($user->id);
			// dapatkan semua array akses
			$user->access_name = $this->additional_function->get_array($data,'access_name');
			$user->role_id = explode(';',$this->additional_function->set_value($data[0],'user_role_id'));

			if(in_array(0,$user->role_id)){
				if(is_array($user->access_name) && !in_array('sangpetualang',$user->access_name))
					array_push($user->access_name,'sangpetualang');
				else{
					$user->access_name = array('sangpetualang');
				}
			}
		}
		return $user;
	}

	// fungsi ini memastikan kalau user sudah login
	function check_user_session(){
		// $result = ((array_key_exists('profile_userId',$_SESSION) && !empty($_SESSION['profile_userId']))?TRUE:FALSE);
		$result = (!empty($_SESSION['profile_userId'])?TRUE:FALSE);
		return $result;
	}

	function check_user_cache($sleepTime=2,$currTime=0,$maxTime=30){
    	$currTime += $sleepTime;
		if($currTime > $maxTime){
			return FALSE;
		}

    	// sleep($sleepTime);

		$allowedUserIds = $this->filecache->cache_read('activeUser2','user');
		if(empty($allowedUserIds) || empty($allowedUserIds[0])){
			$return = $this->check_user_cache($sleepTime,$currTime,$maxTime);
		}else{
			$return = TRUE;
		}
		
		return $return;
	}
	
	function remove_session(){
		if(!empty($_SESSION['profile_session']) && is_array($_SESSION['profile_session'])){
			// hapus setiap cache session yg ada
			foreach ($_SESSION['profile_session'] as $idxSess => $valSess) {
				$this->switch_session($idxSess);
				$this->filecache->remove_file();
			}
		}elseif(!empty($_SESSION['access_matrix'])){
			// jika baru login langsung hapus dari session name
			$this->filecache->remove_file();
		}
		
		if(is_array($_SESSION)){
			foreach ($_SESSION as $key => $session) {
				if(FALSE!==strpos($key,$_SESSION['prefix']) || FALSE!==strpos($key,'access_') || FALSE!==strpos($key,'profile_') || FALSE!==strpos($key,'default_'))
					unset($_SESSION[$key]);
			}
		}
		session_destroy();
	}
	
	// reset connection to defined connection
	// this case use : $this->db_connection_config
	function reset_connection_example(){
		die('cannot executed as example;');
		// defined connection
		$this->curr_connection->reset_query();
		$this->curr_connection = $this->load->database($this->db_connection_config,TRUE);
		$this->curr_connection->reset_query();

		$this->curr_connection->select(
			array(
				$this->main_table_fields['uId'].' as "agent_id"',
				$this->main_table_fields['uId'].' as "id"',
				$this->main_table_fields['uName'].' as "name"',
				$this->main_table_fields['uName'].' as "name_order"',
				$this->main_table_fields['email'].' as "email"'
			)
		);
		if(!empty($userIds))
			$cond->curr_connection->where_in[] = array(
				"fieldName"=>$this->main_table_fields['uId'],
				"fieldValue"=>$userIds
			);
		if(!empty($where))
			$cond->curr_connection->where = $where;

		$this->curr_connection->from($this->main_table_fields['tableName']);
		$r = $this->curr_connection->get();
		$result = $r->result();

		// release conection and save cache to cache obj
		$this->db_tool = new stdClass();
		$this->cache_db = $this->curr_connection;
		$this->curr_connection = $this->db;
		$this->db->reset_query();
		$this->curr_connection->reset_query();

		return $result;
	}

	// ubah session login
	function switch_session($userId=NULL){
		if(!empty($userId) && !empty($_SESSION['profile_session']) && !empty($_SESSION['profile_session'][$userId])){
			foreach ($_SESSION['profile_session'][$userId] as $idxSession => $valSession) {
				$_SESSION[$idxSession] = $valSession;
			}
			$this->save_session_cache();
		}
	}

	// simban session cache
	function save_session_cache(){
		$this->filecache->remove_file('session');

		$this->filecache->cache_save('login',$_SESSION['profile_login'],'session');
		$this->filecache->cache_save('username',$_SESSION['profile_name'],'session');
		$this->filecache->cache_save('user_id',$_SESSION['profile_userId'],'session');

		$this->filecache->cache_save('access_name',$_SESSION['access_matrix'],'session');
		$this->filecache->cache_save('access_list',$_SESSION['access_matrix_list'],'session');
	}

	function get_fb_app_cred(){
		return config_item('f'.'b'.'a'.'p'.'p'.'i'.'d').'|'.config_item('f'.'b'.'a'.'p'.'p'.'s'.'e'.'c'.'r'.'e'.'t');
	}

	function get_fb_user_cred(){
		return NULL;
	}

	function init_access_session(){
		if(empty($_SESSION['profile_session'])) $_SESSION['profile_session'] = array();
	}

	function set_credential($agentId=NULL){
		// create credential :
		$agentId = $this->additional_function->set_value($_SESSION,'profile_userId','string',$agentId,NULL,FALSE);
		$code = $agentId.'::'.time();
		$credential = $this->crypt->encrypt($code);
		return $credential;
	}

	function read_credential($credential=NULL,$request=NULL){
		// read credential
		// user login harus pernah login terlebih dahulu agar sudah memiliki cache di file 'session'
		// proses ini juga akan menambah expired time dari filecache pada file session
		$credential = $this->additional_function->get_elm_priority(
			$credential,
			array('key'=>'credential','val'=>$_POST),
			array('key'=>'credential','val'=>$_GET)
		);
		$request = $this->additional_function->get_elm_priority(
			$request,
			array('key'=>'request','val'=>$_POST),
			array('key'=>'request','val'=>$_GET)
		);
		$origKey = $this->crypt->decrypt($credential);
		if(strstr($origKey,'::') !== FALSE){
			$arrKey = explode('::',$origKey);
			list($origId,$origTime) = explode('::',$origKey);
			$minsElapse = (time()-$origTime)/60;
		}

		// unset($_SESSION['profile_userId']);
		// unset($_SESSION['access_matrix']);
		if(isset($minsElapse) && $minsElapse < 60 && ($minsElapse == 0 || $minsElapse > 0)){
			$_SESSION['profile_userId'] = $origId;

			// untuk melakukan akses filecache, set dulu id filecache dimana
			$this->filecache->init($origId);
			$_SESSION['profile_login'] = $this->filecache->cache_read('login','session');
			$_SESSION['profile_name'] = $this->filecache->cache_read('username','session');

			$_SESSION['access_matrix'] = $this->filecache->cache_read('access_name','session');
			$_SESSION['access_matrix_list'] = $this->filecache->cache_read('access_list','session');

			$_SESSION['profile_session'][$_SESSION['profile_userId']] = array(
				'profile_userId' => $_SESSION['profile_userId'],
				'profile_login' => $_SESSION['profile_login'],
				'profile_name' => $_SESSION['profile_name'],
				'access_matrix' => $_SESSION['access_matrix'],
				'access_matrix_list' => $_SESSION['access_matrix_list']
			);

		}elseif(!empty($credential)){
			// $this->remove_session();
			if($request==='ajax'){
				$result = array(
					'state'=>FALSE,
					'msg'=>'credential is expired'
				);
				echo $this->services_json->encode($result);exit;
			}else{
				header('location:'.$this->config->item('global_instMainUrl').'logout');
				exit;
			}
		}
	}
} 
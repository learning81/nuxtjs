<?php
/**
 * Main Menu on Header every Page
 * This class is to maintenance "main menu" inside table : simas_main_menu
 * @author akhyar azni
 *
 */
class Main_menu extends MY_Model{

	var $table_name = NULL;
	
	// define table name for this object
	function __construct(){
		parent::__construct();

		// unik = 
		// index = dm_active, dm_parent, dm_public
		// reference ke table lain = dm_detail
		$this->mainTable = array(
			'default_mainmenu', // nama table
			'dm_id(integer)',
			'dm_name(varchar-50)', // referensi menu pada table di database lainnya
			'dm_name_indonesia(varchar-100)', // dalam bentuk bahasa indonesia
			'dm_name_english(varchar-100)', // dalam bentuk bahasa inggris
			'dm_name_tmp(varchar-100)', // sebagai temporary jika salah satu language tidak ada isi
			'dm_detail(varchar-250)', // halaman main menu (bisa method, url atau module,)
			'dm_active(number-1)', // status aktif menu
			'dm_order(integer)',
			'dm_parent(integer)', // diambil dari dm_id
			'dm_description(varchar-50)',
			'dm_public(number-1)', // jika sudah terdaftar di database (harus ada hak akses), apakah masih bisa diakses oleh publik
			'dm_fawesome(varchar-50)'
		);
	}
	
	/**
	 * HANYA MENGAKSES DATA YANG ADA DI DATABASE
	 * untuk data yang belum masuk ke database lihat method "get_all_pages"
	 * $arrParam = kondisi diluar penggunaan $cond (akan generate cond didalamnya)
	 * $cond = kondisi default mysql
	 *
	 * $arrParam = array(
	 * 		'limit'=> num or row
	 * 		'offset'=> start row
	 * 		'ordid'=> field yg akan diorder
	 * 		'ordtype'=> jenis order (asc,desc)
	 *
	 *     	'bypass_access' => TRUE, hanya untuk admin akan menampilkan semua akses
	 *	   	'include_public' => TRUE, termasuk akses dari publik
	 *     	'only_parent' => TRUE, hanya parent menu
	 *     	'disable_header' => TRUE, jangan tampilkan menu yang didisable header di default_mainmenu
	 *     	'path' => dm_detail, mencari dengan path tertentu, !!urldecode dari dm_detail
	 *	   	'include_child' => TRUE, mencari multiple child dari data saat ini	
	 *
	 *		'single_inst' => fieldname, // field sebagai index dari data hasil
	 *		'single_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 *		'multi_inst' => fieldname, // field sebagai index dari data hasil
	 *		'multi_filter' => data, // data dari single_inst yang dibawa dari request sebelumnya
	 * );
	 * $result = $this->get_detail($arrParam,$cond);
	 */
	final function get_detail($arrParam=NULL,$objCond=NULL,$returnType=2){
		$singleState = FALSE;

		$cond = $this->additional_function->clone_obj($objCond);

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dm_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		$orderId = $this->additional_function->set_value($arrParam,'ordid');
		$orderType = $this->additional_function->set_value($arrParam,'ordtype');
		if(!empty($orderId)){
			$cond->order['id'] = $orderId;
			if(!empty($orderType))
				$cond->order['type'] = $orderType;
		}
		if(empty($cond->order))
			$cond->order['id'] = 'dm_name';

		$limit = (isset($arrParam['limit'])?$arrParam['limit']:TRUE);
		$offset = $this->additional_function->set_value($arrParam,'offset');
		if(is_numeric($limit) && $limit>0){
			$cond->limit['row'] = $limit;
			if($offset > -1)
				$cond->limit['offset'] = $offset;
		}elseif(isset($limit) && ($limit === FALSE || $limit === 'false')){
			unset($cond->limit);
		}elseif($returnType != $this->SINGLE){
			$cond->limit['row'] = 25;
		}


		// konversi $returnType = single menjadi array berjumlah 1 data
		// hapus $returnType menjadi multiple untuk mendapatkan data
		// $this->limit = 1 tidak bisa menjadi dia request single karena bisa saja nilai limit di generate oleh proses,
		// pastikan single hanya dari $returnType = Single
		if($returnType == $this->SINGLE){
		    if(!empty($cond->limit))
		        die('untuk single data, tidak boleh ada kondisi limit');
		    // jika $return type 
		    $cond->limit = array(
		        'row' => 1
		    );
		    $returnType = $this->MULTIPLE;
		    $singleState = TRUE;
		}

		// jika admin bisa bypass akses
		if(isset($_SESSION['access_matrix']) 
			&& $_SESSION['access_matrix'] == 'sangpetualang' 
			&& isset($arrParam['bypass_access']) 
			&& ($arrParam['bypass_access'] === TRUE || $arrParam['bypass_access'] === 'true')
		){
			// nothing
		}else{
			// pastikan yang dicari hanya sesuai akses
			$allowed = $this->access->get_access('page_access');
			if(isset($arrParam['include_public']) && ($arrParam['include_public'] === TRUE || $arrParam['include_public'] === 'true')){
				// termasuk akses publik
				$cond->where[] = sprintf('( %s in (\'%s\') OR %s = 1 )',
					'dm_name',
					$this->additional_function->array_to_string($allowed,'\',\''),
					'dm_public'
				);
			}else{
				// tdk termasuk akses publik
				$cond->where_in[] = array('fieldName'=>'dm_name','fieldValue'=>$allowed);
			}
		}

		// jika yang ingin di cari hanya menu parent saja
		if(isset($arrParam['only_parent']) && ($arrParam['only_parent'] === TRUE || $arrParam['only_parent'] === 'true')){
			$cond->where[] = sprintf("(%s IS NULL OR %s = 0)",'dm_parent','dm_parent');
		}
		// bisa menghilangkan menu yang didisable
		if(isset($arrParam['disable_header']) && ($arrParam['disable_header'] === TRUE || $arrParam['disable_header'] === 'true')){
			$notAllowed = $this->access->get_access('disable_header');
			// jangan tampilkan yang disable header
			$cond->where_not_in[] = array('fieldName'=>'dm_name','fieldValue'=>$notAllowed);
		}
		// request dari path
		if(isset($arrParam['path']) && $arrParam['path']){
			$cond->where[] = array('dm_detail'=>$arrParam['path']);
		}
		// mencari menu dari nama
		if(isset($arrParam['filter_alias_dm_name']) && $arrParam['filter_alias_dm_name']){
			$cond->where[] = sprintf("lower(dm_name) like '%%%s%%'",$arrParam['filter_alias_dm_name']);
		}
		if(isset($arrParam['filter_alias_dm_detail']) && $arrParam['filter_alias_dm_detail']){
			$cond->where[] = sprintf("lower(dm_detail) like '%%%s%%'",$arrParam['filter_alias_dm_detail']);
		}
		if(isset($arrParam['filter_alias_dm_description']) && $arrParam['filter_alias_dm_description']){
			$cond->where[] = sprintf("lower(dm_description) like '%%%s%%'",$arrParam['filter_alias_dm_description']);
		}
		if(isset($arrParam['filter_alias_dm_parent']) && $arrParam['filter_alias_dm_parent']){

			$newCond = new stdClass();
			$newCond->field = array('dm_id');
			$arrMenu = $this->get_detail(array('filter_alias_dm_name'=>$arrParam['filter_alias_dm_parent'],'bypass_access'=>TRUE),$newCond);
			$arrId = $this->additional_function->get_array($arrMenu,$this->get_field_name('dm_id'));

			$cond->where_in[] = array(
				"fieldName"=>'dm_parent',
				"fieldValue"=>$arrId
			);
		}

		// filter untuk single instance, multi instance, atau biasa
		$singleInst = $this->additional_function->set_value($arrParam,'single_inst');
		$multiInst = $this->additional_function->set_value($arrParam,'multi_inst');
		if(!empty($singleInst) && !empty($arrParam['single_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($singleInst),
				"fieldValue"=>$arrParam['single_filter']
			);
		}elseif(!empty($singleInst) && !empty($arrParam['multi_filter'])){
			$cond->where_in[] = array(
				"fieldName"=>$this->get_field_name($multiInst),
				"fieldValue"=>$arrParam['multi_filter']
			);
		}

		$result = $this->get_attribute($cond,$this->get_table_name('default_mainmenu'),$returnType);

		if(!empty($result) && is_array($result)){
			$lastResult = NULL;
			foreach ($result as $idxMainmenu => $valMainmenu) {
				// update $result here
				$menuId = $this->additional_function->set_value($valMainmenu,$this->get_field_name('dm_id'));
				$detail = $this->additional_function->set_value($valMainmenu,$this->get_field_name('dm_detail'));

				if(preg_match("#http#", $detail))
					$result[$idxMainmenu]->url = $detail;
				elseif(!preg_match("#\/#", $detail)) // jika single word, berarti maksudnya adalah module
					$result[$idxMainmenu]->url = "[baseUrl][controllerName]/module/".$detail;
				else
					$result[$idxMainmenu]->url = "[baseUrl]".$detail;
					
				// jika hanya parent
				if(isset($arrParam['include_child']) && $arrParam['include_child']){
					$newCond = new stdClass();
					$newCond->where = array(
						'dm_parent' => $menuId
					);

					// apakah masih disable header
					if(isset($arrParam['disable_header']) && $arrParam['disable_header']){
						$arrParam['disable_header'] = TRUE;
					}

					$child_data = $this->get_detail($arrParam,$newCond);

					if(!empty($child_data)) 
						$result[$idxMainmenu]->child = $child_data;
				}

				// format untuk single instance, multi instance, atau biasa
				if(!empty($singleInst) || empty($multiInst)){
					// tentukan index dari data
					if(!empty($singleInst))
						$instId = $this->additional_function->set_value($valMainmenu,$this->get_field_name($singleInst));
					else
						$instId = $idxMainmenu;

					// clone semua data
					$lastResult[$instId] = $valMainmenu;
				}else{
					// tentukan index dari data
					$instId = $this->additional_function->set_value($valMainmenu,$this->get_field_name($multiInst));

					// tambahkan data dalam bentuk array
					if(empty($lastResult[$instId]))
						$lastResult[$instId] = array();
					array_push($lastResult[$instId],$valMainmenu);
				}
			}
		}else{
			$lastResult = $result;
		}

		// jika single state hanya ambil item pertama
		if($singleState && is_array($lastResult)){
		    $lastResult = reset($lastResult);
		}

		return $lastResult;
	}

	final function update($arrParam,$objCond=NULL,$method){
		$cond = $this->additional_function->clone_obj($objCond);

		// input data
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(preg_match("/^dm_/i",strtolower($idxParam))){
					if(strtolower($idxParam) == 'dm_order')
						$main[$this->get_field_name($idxParam)] = intval($valParam);
					else
						$main[$this->get_field_name($idxParam)] = (!empty($valParam)?$valParam:NULL);
				}
			}
		}

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dm_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		if($method == 'update'){
			$cond->field = array('dm_id','dm_name','dm_description');
			$mainmenuLst = $this->get_detail($arrParam,$cond); // $arrParam bisa bypass kondisi ke get_detail
			if(/*isset($main['dm_domain']) && */empty($main['dm_domain'])) $main['dm_domain'] = config_item('base_url');
			if(empty($mainmenuLst))
				$mainmenuLst = array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
			else
				$mainmenuLst = array('state'=>TRUE,'msg'=>'data berhasil diupdate','data' => $mainmenuLst);
		}elseif($method == 'insert'){
			$nextId = $this->get_next_id($this->get_field_name('dm_id'),'default_mainmenu');
			$main[$this->get_field_name('dm_id')] = $nextId;
			if(empty($main['dm_domain'])) $main['dm_domain'] = config_item('base_url');
			$mainmenuLst = array(
				'dm_id' => $nextId,
				'dm_name' => $this->additional_function->set_value($main,$this->get_field_name('dm_name')),
				'dm_description' => $this->additional_function->set_value($main,$this->get_field_name('dm_description'))
			);
		}

		$result = $this->set_attribute($main,'default_mainmenu',$method,$cond);

		$this->audit_process->insert_data(
			$this->additional_function->set_value($arrParam,$this->get_field_name('filter_dm_id')),
			$this->audit_process->MAINMENU,
			$method.' mainmenu '.$this->additional_function->set_value($arrParam,$this->get_field_name('dm_name')),
			'Val: '.$this->services_json->encode($arrParam)
		);

		return $mainmenuLst;
	}

	final function remove($arrParam,$objCond=NULL){
		$cond = $this->additional_function->clone_obj($objCond);

		// check jika ada filter dari restful API
		if(!empty($arrParam) && is_array($arrParam)){
			foreach ($arrParam as $idxParam => $valParam) {
				if(FALSE!==strpos(strtolower($idxParam),'filter_dm_')){
					$cond->where_in[] = array(
						"fieldName"=>str_replace(array('filter_','FILTER_'),'',$idxParam),
						"fieldValue"=>$valParam
					);
				}
			}
		}

		if(!empty($cond->where) || !empty($cond->where_in)){
			$cond->field = array('dm_id','dm_name','dm_description');
			$mainmenuLst = $this->get_detail($arrParam,$cond); // $source bisa bypass kondisi ke get_detail

			if(empty($mainmenuLst))
				return array('state'=>FALSE,'msg'=>'data tidak bisa diakses');
		}else{
			return array('state'=>FALSE,'msg'=>'tidak ada kondisi dalam metode penghapusan');
		}
		$result = $this->remove_attribute('default_mainmenu',$cond,TRUE);

		if(!empty($mainmenuLst) && is_array($mainmenuLst)){
			foreach ($mainmenuLst as $idxLst => $valLst) {
				$this->audit_process->insert_data(
					$this->additional_function->set_value($valLst,'dm_id'),
					$this->audit_process->MAINMENU,
					'hapus mainmenu : '.$this->additional_function->set_value($valLst,'dm_name'),
					'Val: '.$this->services_json->encode($cond)
				);
			}
		}

		return $mainmenuLst;
	}

	// mendapatkan semua pages yang ada di system
	// $existing = NULL => tidak melakukan cek pages yang sudah ada di database (ALL)
	// $existing = TRUE => hanya ambil pages yang sudah ada di database (utk akses tertentu, ambil saja dari method get_detail)
	// $existing = FALSE => hanya ambil pages yang belum ada di database
	// controller yg ada di exclude tidak akan tampil di main menu secara default
	function get_all_pages($existing=NULL,$exclude = array('main.php','admin.php','cache.php','toll.php','cron.php','json_api.php','assets.php','external.php','transaction.php','migrate.php')){

		// cari existing pages untuk semua data di database
		// bisa saja halaman ada didatabase tapi sudah dihapus di file
		$cond = new stdClass();
		$cond->field = sprintf('%s','dm_detail');
		$result = $this->get_attribute($cond,'default_mainmenu');
		$allExisting = $this->additional_function->get_array($result,$this->get_field_name('dm_detail'));

		// ambil semua method dari my_controller
		// method my_controller akan di exclude dari kelas controller yg lain
		$MYCTRLR_methods = get_class_methods('MY_Controller');

		// inisiasi variable
		$allPages = array();
		// lihat semua method dari controller yang ada
		$allClass = $this->additional_function->get_file_list(APPPATH.'controllers');
		foreach ($allClass as $idxClass => $valClass){
			// controller ini sudah di load
			// jangan include lagi file ini (cont : admin.php)
			if(
				strtolower(str_replace('.php','',$valClass))!==config_item('currCtrlrName')
				&& FALSE !== strpos($valClass,'.php')
				&& FALSE === strpos(strtolower($valClass),'.backup') // jgn di include
				&& FALSE === strpos(strtolower($valClass),'.tmp') // jgn di include
				){
				include_once(APPPATH.'controllers/'.$valClass);
			}

			// baca semua method dari kelas yang dibaca
			if(FALSE !== strpos(strtolower($valClass),'.php') && 
				!in_array(strtolower($valClass),$exclude) &&
				FALSE === strpos(strtolower($valClass),'.backup') && // jgn ambil methodnya
				FALSE === strpos(strtolower($valClass),'.tmp') // jgn ambil methodnya
			){
				$class_methods = get_class_methods(str_replace('.php','',$valClass));
				if(!empty($class_methods) && is_array($class_methods)){

					foreach ($class_methods as $idxMethod => $valMethod) {

						if(!in_array($valMethod,$MYCTRLR_methods)){
							array_push($allPages,strtolower(str_replace('.php','',$valClass)).'/'.$valMethod);
						}

					}

				}
			}
		}

		if($existing === TRUE)
			$allPages = $this->additional_function->array_intersect(array($allPages,$allExisting));
		elseif($existing === FALSE)
			$allPages = $this->additional_function->array_minus($allPages,$allExisting);

		return $allPages;
	}

	// mendapatkan semua module yang ada di system
	function get_all_modules(){
		// siapkan variable module
		$allModules = array();

		// lihat semua module yang ada
		$allModule = $this->additional_function->get_file_list(APPPATH.'libraries');
		foreach ($allModule as $idxModule => $valModule){
			if(FALSE !== strpos($valModule,'Module_') && FALSE !== strpos($valModule,'.php')){
				array_push($allModules,str_replace(array('.php','Module_'),'',$valModule));
			}
		}

		return $allModules;
	}

	// cek apakah test page ada di database
	// $testpage diambil dari uri_args dan harus di compare dengan dm_detail
	// harus test ke semua halaman di database
	function is_managed($detailPage=''){
		$cond = new stdClass();
		$cond->where[] = sprintf('%s = \'%s\'','dm_detail',$detailPage);
		// get attribute karena harus cek semua isi database
		$count = $this->get_attribute($cond,'default_mainmenu',$this->COUNT); 
		$result = ($count>0?TRUE:FALSE);
		return $result;
	}

	// hanya untuk mengetahui apakah pagename sudah ditambahkan ke database untuk semua user
	function check_load_page($slug_page=''){
		$cond = new stdClass();
		$cond->where[] = sprintf('%s = \'%s\'','dm_name',$slug_page);
		$count = $this->get_attribute($cond,'default_mainmenu',$this->COUNT); 
		$result = ($count>0?TRUE:FALSE);
		return $result;
	}

	// cek apakah halaman ini aktif , diakses oleh semua user
	function is_active($slugName=''){
		$cond = new stdClass();
		$cond->where[] = sprintf('%s = \'%s\'','dm_name',$slugName);
		$cond->where[] = sprintf('%s = 1','dm_active');
		// get attribute karena harus cek semua isi database
		$count = $this->get_attribute($cond,'default_mainmenu',$this->COUNT); 
		$result = ($count>0?TRUE:FALSE);
		return $result;
	}
}
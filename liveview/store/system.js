export default {
  namespaced:true,
  state:{
    'base_url':'http://localhost:9002/core/'
    ,'credential':null
  },
  mutations: {
    UPDATE_SYSTEM(state,data){
      for(var i in data){
        state[i]=data[i];
      }
    }
    ,UPDATE_BASE_URL(state,base_url){
      state.base_url=base_url;
    }
    ,UPDATE_CREDENTIAL(state,credential){
      state.credential=credential;
    }
  }
}
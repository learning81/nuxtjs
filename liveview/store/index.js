// belajar vuex lengkap : 
//   - https://id.nuxtjs.org/docs/2.x/directory-structure/store/
//   - https://nuxtjs.org/docs/2.x/directory-structure/store

import Vue from 'vue'
import Vuex from 'vuex'
import SYSTEM from './system'
import INST_MODEL1 from './model1'
import INST_SUBMODEL1 from './instance/submodel1'
import INST_SUBMODEL2 from './instance/submodel2'

Vue.use(Vuex);
Vue.config.devtools = true;

const createStore = () => {
  return new Vuex.Store({
    state: {
      currdata: {}
    },
    mutations: {
      UPDATE_CURRDATA(state,data){
        state.currdata=JSON.parse(JSON.stringify(data));
      }
    },
    actions:{
      ACTION_UPDATE_CURRDATA({commit},data){
        commit("UPDATE_CURRDATA",data);
      }
    },
    getters:{
      GET_CURRDATA(state){
        return state.currdata;;
      }
    },
    modules:{
      SYSTEM
      ,INST_MODEL1
      ,INST_SUBMODEL1
      ,INST_SUBMODEL2
    }
  })
}

export default createStore
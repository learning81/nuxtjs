export default {
  namespaced:true,
  state:{
    model1:{}
  },
  mutations: {
    UPDATE_MODEL1(state,data){
      state.model1=JSON.parse(JSON.stringify(data));
    }
  },
  actions:{
    ACTION_UPDATE_MODEL1({commit},data){
      commit("UPDATE_MODEL1",data);
    }
  },
  getters:{
    GET_MODEL1(state){
      return state.model1;;
    }
  }
}
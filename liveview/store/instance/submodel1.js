export default {
	namespaced:true,
	state:{
		submodel1:{}
	},
	mutations:{
		UPDATE_SUBMODEL1(state,data){
			state.submodel1=JSON.parse(JSON.stringify(data));
		}
	},
	actions:{
		ACTION_UPDATE_SUBMODEL1({commit},data){
			commit("UPDATE_SUBMODEL1",data);
		}
		,ACTION_UPDATE_SUBMODEL1_B({commit},data){
			return new Promise((resolve,reject)=>{
				commit("UPDATE_SUBMODEL1",data);
				setTimeOut(()=>{
					resolve(true);
				},5000)
			});
		}
		,async ACTION_UPDATE_SUBMODEL1_C ({ commit }) {
	      const ip = await this.$axios.$get('http://icanhazip.com')
	      commit('UPDATE_SUBMODEL1', {'ip':ip})
	    }
	},
	getters:{
		GET_SUBMODEL1:(state)=>{
			return state.submodel1;
		},
		GET_CURRDATA:(state, getters, rootState)=>{
			return rootState.currdata;
		},
		GET_SUBMODEL2:(state, getters, rootState)=>{
			return rootState.INST_SUBMODEL2.submodel2;
		}
	}
}
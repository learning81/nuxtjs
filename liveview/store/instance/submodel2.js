export default {
	namespaced:true,
	state:{
		submodel2:{}
	},
	mutations:{
		UPDATE_SUBMODEL2(state,data){
			state.submodel2=JSON.parse(JSON.stringify(data));
		}
	},
	actions:{
	  ACTION_UPDATE_SUBMODEL2({commit},data){
	    commit("UPDATE_SUBMODEL2",data);
	  }
	},
	getters:{
		GET_SUBMODEL2(state){
			return state.submodel2;;
		}
	}
}